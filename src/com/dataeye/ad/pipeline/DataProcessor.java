package com.dataeye.ad.pipeline;

import java.util.Collection;
import java.util.List;

import com.dataeye.ad.constant.Constant.KafkaEvent;
import com.dataeye.ad.kafka.ProducerAgent;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdPageStat;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.util.StringUtil;

public class DataProcessor {

    public static boolean SEND_KAFKA = false;

    /**
     * <pre>
     * 处理媒体账户数据 
     * @author sam.xie
     * @date Aug 1, 2017 1:47:34 PM
     * @param account
     */
    public static void processAccountDay(AdAccountStat account) {
        if (null == account) {
            return;
        }
        if (SEND_KAFKA) {
            account.setEvent(KafkaEvent.EVENT_ACCOUNT);
            ProducerAgent.send(account.getMediumAccountId() + "", StringUtil.gson.toJson(account));
        } else {
            DBPipeline.insertAccountDay(account);
        }
    }

    /**
     * <pre>
     * 处理计划统计数据
     * @author sam.xie
     * @date Aug 1, 2017 1:47:21 PM
     * @param list
     */
    public static void processPlanDay(List<AdPlanStat> list) {
        if (null == list || list.size() == 0) {
            return;
        }
        for (AdPlanStat plan : list) {
            if (SEND_KAFKA) {
                plan.setEvent(KafkaEvent.EVENT_PLAN);
                ProducerAgent.send(plan.getMediumAccountId() + "", StringUtil.gson.toJson(plan));
            } else {
                DBPipeline.insertPlanDay(plan);
            }
        }
    }

    /**
     * <pre>
     * 落地页点击到达数
     * @author sam.xie
     * @date Aug 1, 2017 2:22:43 PM
     * @param list
     */
    public static void processPageDay(Collection<AdPageStat> list) {
        if (null == list || list.size() == 0) {
            return;
        }
        for (AdPageStat page : list) {
            processPageDay(page);
        }
    }

    public static void processPageDay(AdPageStat page) {
        if (SEND_KAFKA) {
            page.setEvent(KafkaEvent.EVENT_PAGE);
            ProducerAgent.send(page.getH5PageId(), StringUtil.gson.toJson(page));
        } else {
            DBPipeline.insertPageDay(page);
        }
    }

    /**
     * <pre>
     * 落地页上包下载数
     * @author sam.xie
     * @date Aug 1, 2017 2:07:56 PM
     * @param list
     */
    public static void processPageDownloadDay(List<AdPageStat> list) {
        DBPipeline.insertPageDownloadDay(list);
    }

}
