package com.dataeye.ad.scheduler.job.sinafensitong.model;

/**
 * Created by VitAs on 2017/7/4.
 */
public class SinaPlanModel {

    //计划id
    private String id;

    //分组id
    private String groupId;

    //计划出价
    private String price;

    //计划状态 3-暂停  4-结束
    private String deliveryStatus;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public SinaPlanModel() {
    }

    public SinaPlanModel(String id, String groupId, String price, String deliveryStatus) {
        this.id = id;
        this.groupId = groupId;
        this.price = price;
        this.deliveryStatus = deliveryStatus;
    }
}
