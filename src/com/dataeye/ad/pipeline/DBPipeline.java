package com.dataeye.ad.pipeline;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;

import com.dataeye.ad.constant.Constant.Table;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdMediumDiscount;
import com.dataeye.ad.pipeline.po.AdPage;
import com.dataeye.ad.pipeline.po.AdPageStat;
import com.dataeye.ad.pipeline.po.AdPkgStat;
import com.dataeye.ad.pipeline.po.AdPlan;
import com.dataeye.ad.pipeline.po.AdPlanRelation;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.util.DBClient;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.GDTUtil;
import com.dataeye.ad.util.StringUtil;
import com.dataeye.util.log.DELogger;
import com.xunlei.jdbc.JdbcTemplate;
import com.xunlei.jdbc.RowCallbackHandler;
import com.xunlei.jdbc.util.DebugUtil;

public class DBPipeline {

    private static final Logger logger = DELogger.getLogger("sql");

    private static final Map<String, AdPlan> cachePlanMap = new HashMap<String, AdPlan>();

    /**
     * <pre>
     * 媒体账户余额统计
     * @author sam.xie
     * @date Apr 11, 2017 4:56:38 PM
     * @param account
     */
    public static void insertAccountDay(AdAccountStat account) {
        if (null == account) {
            return;
        }
        Date now = account.getUpdateTime();
        // 按天统计
        doInsert(SQL_INSERT_ACCOUNT_DAY, account.getStatDate(), account.getMediumId(), account.getMediumAccountId(),
                account.getCompanyId(), account.getBalance(), account.getCostToday(), now, account.getBalance(),
                account.getCostToday(), now);
        // 实时统计
        doInsert(_SQL_INSERT_ACCOUNT_REALTIME, DateUtil.getDateTrim10min(now), account.getMediumId(),
                account.getMediumAccountId(), account.getCompanyId(), account.getBalance(), account.getCostToday(),
                now, account.getBalance(), account.getCostToday(), now);
    }

    /**
     * <pre>
     * 新增计划统计数据
     * @author sam.xie
     * @date Feb 21, 2017 7:12:27 PM
     * @param po
     */
    public static void insertPlanDay(AdPlanStat po) {
        Date now = po.getUpdateTime();
        // 爱奇艺数据解析过程中会提前获取planId，这里直接使用，不会操作字典表
        // 其他媒体仍然通过数据库查询获取
        int planId = po.getPlanId();
        if (planId <= 0) {
            planId = getPlanId(po);
        }
        // 计划不存在
        if (planId <= 0) {
            return;
        }
        po.setPlanId(planId);
        // 过滤消耗，曝光，点击都为0的数据
        if (po.getCost() <= 0 && po.getShowNum() <= 0 && po.getClickNum() <= 0) {
            return;
        }
        int pkgId = 0;
        int pageId = 0;
        int osType = 0;
        int productId = 0;
        BigDecimal discountRate = BigDecimal.ZERO;
        // 获取关联规则
        AdPlanRelation relation = DBQuery.getPlanRelationByPlanId(po.getCompanyId(), po.getPlanId());
        if (relation != null) {
            pageId = relation.getPageId();
            pkgId = relation.getPkgId();
            osType = relation.getOsType();
            productId = relation.getProductId();
            /**
             * <pre/>
             *  加入更新产品Id 
             *  1. 如果关联规则已有产品关联关系，以已有的为基准
             * 2. 如果关联规则尚未配置产品关联，以爬虫数据为基准。
             */
            if (GDTUtil.shouldHandleDownload(po)) {
                // 新增AdPage表记录得到的pageId和AdPlanRelation表的关联的pageId不同时；更新
                if (po.getPageId() != pageId || po.getProductId() != productId) {
                    pageId = po.getPageId();
                    doUpdate(_SQL_UPDATE_PLAN_REALTION_PAGE, po.getPageId(), po.getProductId() > 0 ? po.getProductId()
                            : productId, now, po.getCompanyId(), po.getPlanId());
                }
            }
            if (GDTUtil.shouldHandleActive(po)) {
                if (po.getPkgId() != pkgId || po.getProductId() != productId) {
                    pkgId = po.getPkgId();
                    doUpdate(_SQL_UPDATE_PLAN_REALTION_PKG, po.getPageId(), po.getPkgId(),
                            po.getProductId() > 0 ? po.getProductId() : productId, now, po.getCompanyId(),
                            po.getPlanId());
                }
            }
        } else {
            // AdPlanRelation表中不存在该落地页和计划的关联规则时，新增
            if (GDTUtil.shouldHandleDownload(po)) {
                pageId = po.getPageId();
                doInsert(_SQL_INSERT_PLAN_REALTION_PAGE, po.getCompanyId(), po.getPlanId(), po.getPageId(),
                        po.getProductId() > 0 ? po.getProductId() : productId, now, now);
            }
            if (GDTUtil.shouldHandleActive(po)) {
                pkgId = po.getPkgId();
                doInsert(_SQL_INSERT_PLAN_REALTION_PKG, po.getCompanyId(), po.getPlanId(), po.getPageId(),
                        po.getPkgId(), po.getProductId() > 0 ? po.getProductId() : productId, now, now);
            }
        }
        // 获取折扣率
        AdMediumDiscount discount = DBQuery.getMediumDiscount(po.getCompanyId(), po.getMediumAccountId());
        if (discount != null) {
            discountRate = discount.getDiscountRate();
        }
        if (po.getStatDate().getTime() >= DateUtil.winTimestamp000()) {
            // 插入计划统计表，当天的数据会同步更新页面ID，包ID
            doInsert(_SQL_INSERT_PLAN_DAY, po.getStatDate(), planId, po.getmPlanId(), po.getMediumId(),
                    po.getMediumAccountId(), po.getCompanyId(), pageId, pkgId, po.getCost(), po.getShowNum(),
                    po.getClickNum(), po.getBid(), discountRate, now, osType, pageId, pkgId, po.getCost(),
                    po.getShowNum(), po.getClickNum(), po.getBid(), discountRate, now, osType);
            // 插入计划统计表（实时），只有当天才会处理，历史数据不处理
            doInsert(_SQL_INSERT_PLAN_REALTIME, DateUtil.getDateTrim10min(now), planId, po.getmPlanId(),
                    po.getMediumId(), po.getMediumAccountId(), po.getCompanyId(), pageId, pkgId, po.getCost(),
                    po.getShowNum(), po.getClickNum(), po.getBid(), discountRate, now, osType, pageId, pkgId,
                    po.getCost(), po.getShowNum(), po.getClickNum(), po.getBid(), discountRate, now, osType);
        } else {
            // 历史数据如果已经存在页面ID，包ID，将不会更新，否则也使用当前最新的页面ID，包ID
            doInsert(_SQL_INSERT_PLAN_DAYPAST, po.getStatDate(), planId, po.getmPlanId(), po.getMediumId(),
                    po.getMediumAccountId(), po.getCompanyId(), pageId, pkgId, po.getCost(), po.getShowNum(),
                    po.getClickNum(), po.getBid(), discountRate, now, osType, pageId, pkgId, po.getCost(),
                    po.getShowNum(), po.getClickNum(), po.getBid(), discountRate, now, osType);
        }
    }

    /**
     * <pre>
     * 落地页数据
     * @author sam.xie
     * @date Mar 6, 2017 8:17:23 PM
     * @param list
     */
    public static void insertPageDay(Collection<AdPageStat> list) {
        for (AdPageStat po : list) {
            insertPageDay(po);
        }
    }

    public static void insertPageDay(AdPageStat po) {
        Date now = po.getUpdateTime();
        // 插入页面表
        int pageId = doInsert(_SQL_INSERT_PAGE, po.getCompanyId(), po.getPageName(), po.getH5AppId(), po.getH5PageId(),
                po.getRemark(), now, now, po.getPageName());
        po.setPageId(pageId);
        if (po.getPv() > 0 || po.getUv() > 0 || po.getDownloadTimes() > 0) {
            // 插入页面统计表（按天）
            doInsert(_SQL_INSERT_PAGE_DAY, po.getStatDate(), po.getCompanyId(), pageId, po.getUniqueClickNum(),
                    po.getPv(), po.getUv(), po.getDownloadTimes(), now, po.getUniqueClickNum(), po.getPv(), po.getUv(),
                    po.getDownloadTimes(), po.getDownloadTimes(), now);
            // 插入页面统计表（实时），只有当天才会处理，历史数据不处理
            if (po.getStatDate().getTime() >= DateUtil.winTimestamp000()) {
                doInsert(_SQL_INSERT_PAGE_REALTIME, DateUtil.getDateTrim10min(now), po.getCompanyId(), pageId,
                        po.getUniqueClickNum(), po.getPv(), po.getUv(), po.getDownloadTimes(), now,
                        po.getUniqueClickNum(), po.getPv(), po.getUv(), po.getDownloadTimes(), po.getDownloadTimes(),
                        now);
            }
        }
        /**
         * <pre/>
         * update by sam.xie at 20170828, ad_tracking_*已经弃用
         * if (po.getActiveNum() > 0) {
         *             // 插入tracking统计表（按天）
         *             doInsert(_SQL_INSERT_TRACKING_DAY, po.getStatDate(), po.getCompanyId(), pageId, po.getActiveNum(), now,
         *                     po.getActiveNum(), now);
         *             // 插入tracking统计表（实时）
         *             if (po.getStatDate().getTime() >= DateUtil.winTimestamp000()) {
         *                 doInsert(_SQL_INSERT_TRACKING_REALTIME, po.getStatDate(), po.getCompanyId(), pageId, po.getActiveNum(),
         *                         now, po.getActiveNum(), now);
         *             }
         * }
         */

    }

    /**
     * 包激活数和注册数
     *
     * @param po
     */
    public static void insertPkgDay(AdPkgStat po) {
        Date now = po.getUpdateTime();
        // 插入ad_pkg包字典表
        int pkgId = doInsert(_SQL_INSERT_PKG, po.getCompanyId(), po.getPkgName(), po.getCPAppId(), po.getCPPkgId(),
                po.getRemark(), now, now);
        po.setPkgId(pkgId);
        if (po.getActiveNum() > 0 || po.getRegisterNum() > 0) {
            // 插入包统计表（按天）
            doInsert(_SQL_INSERT_PKG_STAT_DAY, po.getStatDate(), po.getCompanyId(), pkgId, po.getActiveNum(),
                    po.getRegisterNum(), now, po.getActiveNum(), po.getRegisterNum(), now);
            // 插入包统计表（实时）
            if (po.getStatDate().getTime() >= DateUtil.winTimestamp000()) {
                doInsert(_SQL_INSERT_PKG_STAT_REALTIME, po.getStatDate(), po.getCompanyId(), pkgId, po.getActiveNum(),
                        po.getRegisterNum(), now, po.getActiveNum(), po.getRegisterNum(), now);
            }
        }

    }

    /**
     * <pre>
     * 下载点击/跳转
     * @author sam.xie
     * @date Mar 15, 2017 11:20:55 AM
     * @param list
     */
    public static void insertPageDownloadDay(List<AdPageStat> list) {
        if (list.size() == 0) {
            return;
        }
        Date now = DateUtil.nowDate();
        int companyId = list.get(0).getCompanyId();
        int h5AppId = list.get(0).getH5AppId();
        // 获取当前所有落地页数据
        List<AdPage> pageList = DBQuery.getPageList(companyId, h5AppId);
        // 处理成页面-ID MAP，后面获取下载页对应的落地页id
        Map<String, Integer> pageIdMap = new HashMap<String, Integer>();
        for (AdPage page : pageList) {
            String rawPage = page.getPageName();
            String cleanPage = rawPage;
            if (companyId == 2 || companyId == 10) {
                // 优点落地页：res.digitcube.net/ads-page/swipe/index-aibei_twzw_49g.html
                String[] pageParts = rawPage.split("/");
                // 截取最后：index-aibei_twzw_49g.html
                cleanPage = pageParts[pageParts.length - 1].trim();
                // } else if (companyId == 3) {
            } else {
                // 落地页：www.huiyaohuyu.com/h5/tl/ry14/
                // 去掉域名：/h5/tl/ry14/
                int index = rawPage.indexOf("/");
                if (index >= 0) {
                    cleanPage = rawPage.substring(index).trim();
                } else {
                    continue;
                }
            }
            pageIdMap.put(cleanPage, page.getId());
        }
        // 部分落地页分android和ios，需要先汇总
        Map<Integer, Integer> pageIdTimesMap = new HashMap<Integer, Integer>();
        for (AdPageStat po : list) {
            String rawDownloadPage = po.getPageName();
            String downloadPage = rawDownloadPage;
            if (companyId == 2 || companyId == 10) {
                // 优点下载页：download-longyu_twzw_115_1.16.0_210/index-aibei_twzw_49g
                // 处理为：index-aibei_twzw_49g.html
                String[] pageParts = rawDownloadPage.split("/");
                downloadPage = pageParts[pageParts.length - 1] + ".html";
            } else {
                // 下载页：download_/tl/ry10/，download_ios_/tl/ry10/
                // 处理成：/tl/ry10/
                int index = rawDownloadPage.indexOf("/");
                if (index >= 0) {
                    downloadPage = rawDownloadPage.substring(index);
                } else {
                    continue;
                }
            }
            // 1.获取页面ID
            Integer pageId = pageIdMap.get(downloadPage);
            if (null == pageId) {
                continue;
            }
            // 合并数据
            int downloadTimes = po.getDownloadTimes();
            if (pageIdTimesMap.containsKey(pageId)) {
                downloadTimes += pageIdTimesMap.get(pageId);
                // 保存当前汇总值
                pageIdTimesMap.put(pageId, downloadTimes);
            } else {
                pageIdTimesMap.put(pageId, downloadTimes);
            }
            logger.info(po.getPageName() + " -> " + pageId);
            // 2.插入页面统计表（按天）
            doInsert(_SQL_INSERT_PAGE_DAY_DOWNLOAN_TIMES, po.getStatDate(), po.getCompanyId(), pageId, downloadTimes,
                    now, downloadTimes, now);
            // 3. 插入页面统计表（实时），只有当天才会处理，历史数据不处理
            if (po.getStatDate().getTime() >= DateUtil.winTimestamp000()) {
                doInsert(_SQL_INSERT_PAGE_REALTIME_DOWNLOAD_TIMES, DateUtil.getDateTrim10min(now), po.getCompanyId(),
                        pageId, downloadTimes, now, downloadTimes, now);
            }
        }
    }

    /**
     * <pre>
     * 根据计划对象获取计划ID
     * @author sam.xie
     * @date May 4, 2017 10:50:31 AM
     * @param po
     * @return
     */
    private static int getPlanId(AdPlanStat po) {
        /**
         * <pre/>
         * update by sam.xie at 2017.05.03
         * 计划字典表中新增了m_plan_id（媒体平台计划的ID），并且联合唯一索引也会加上m_plan_id
         * 解决问题：
         * 1.后续改价等更新操作会需要用到这个ID
         * 2.存在同名计划，需要通过ID来区分
         * 3.如果媒体平台计划更名，不作为新计划新增
         * 几个原则：
         * 1.如果存在m_plan_id，那么m_plan_id就是唯一约束
         * 2.如果不存在m_plan_id，那么plan_name就是唯一约束
         * 3.如果原来没有m_plan_id，现在有，就要补充
         * 操作流程：
         * 为了保证兼容历史数据，这里通过mPlanId和 planName匹配记录，有下面几种情况需要考虑
         * （mPlanId是当前爬取的媒体计划ID，m_plan_id是数据库的媒体计划ID
         *   planName是当前爬取的媒体计划名，plan_name是数据库的媒体计划名）
         * > 匹配记录，当作计划更新
         * > 不匹配记录
         *   > mPlanId为空（通过planName匹配）
         *      > 匹配记录，不处理
         *      > 不匹配记录，当作计划新增
         *   > mPlanId不为空（通过mPlanId匹配）
         *      > 匹配记录，表示planName变更过，当作计划更新
         *      > 不匹配记录（通过planName匹配，可能存在多条，这里只取一条）
         *          > 匹配记录
         *      		> m_plan_id为空，表示补全m_plan_id，当作计划更新
         *          	> m_plan_id不为空，表示计划同名但是ID不同名，当作计划新增
         *          > 不匹配记录，当作计划新增
         * ...
         */
        Date now = DateUtil.nowDate();
        JdbcTemplate jdbcTemplate = DBClient.getJdbcTemplateDeAdt();
        int planId = 0;
        String mPlanId = po.getmPlanId();
        String cacheKey = po.getCompanyId() + ":" + po.getMediumId() + ":" + po.getMediumAccountId() + ":"
                + po.getPlanName() + ":" + po.getmPlanId();
        AdPlan cachePlan = cachePlanMap.get(cacheKey);
        if (null != cachePlan) {
            planId = cachePlan.getId();
        } else {
            cachePlan = new AdPlan();
            queryAndSetPlanCache(po, cachePlan); // 从数据库查询计划并写入缓存对象
            planId = cachePlan.getId();
            if (planId > 0) {//
                cachePlanMap.put(cacheKey, cachePlan);
            }
        }
        if (planId > 0) {
            // 只有缓存和爬虫不一致，才更新数据库，同时更新缓存
            if (!isCacheEqualsCrawlerData(cachePlan, po)) {
                doUpdate(_SQL_UPDATE_PLAN, mPlanId, po.getPlanName(), po.getGroupId(), po.getGroupName(),
                        po.getPlanSwitch(), po.getPlanStatus(), po.getBid(), po.getPlanBidStrategy(),
                        po.getPlanExtend(), po.getLandingPage(), now, planId);
                cachePlan.setBid(po.getBid());
                cachePlan.setPlanBidStrategy(po.getPlanBidStrategy());
                cachePlan.setGroupId(po.getGroupId());
                cachePlan.setGroupName(po.getGroupName());
                cachePlan.setPlanStatus(po.getPlanStatus());
                cachePlan.setPlanSwitch(po.getPlanSwitch());
                cachePlan.setPlanExtend(po.getPlanExtend());
                cachePlan.setLandingPage(po.getLandingPage());
            }
        } else { // 计划不存在
            if (StringUtil.isEmpty(mPlanId)) { // 没有mPlanId，通过planName匹配
                planId = jdbcTemplate.queryForInt(_SQL_QUERY_PLANID_BY_PLANNAME, po.getMediumId(),
                        po.getMediumAccountId(), po.getCompanyId(), po.getPlanName());
                if (planId > 0) { // 匹配成功（说明数据库中m_plan_id不为空，至于为何爬虫获取的mPlanId为空，原因未知，暂不处理）
                    // DO NOTHING
                    return planId;
                } else {
                    // 匹配不成功，当新增计划处理
                    planId = doInsert(_SQL_INSERT_PLAN, po.getPlanName(), mPlanId, po.getGroupId(), po.getGroupName(),
                            po.getPlanSwitch(), po.getPlanStatus(), po.getBid(), po.getPlanBidStrategy(),
                            po.getPlanExtend(), po.getLandingPage(), po.getMediumId(), po.getMediumAccountId(),
                            po.getCompanyId(), now, now);
                }
            } else { // 单独使用mPlanId匹配m_plan_id
                planId = jdbcTemplate.queryForInt(_SQL_QUERY_PLANID_BY_MPLANID, po.getMediumId(),
                        po.getMediumAccountId(), po.getCompanyId(), mPlanId);
                if (planId > 0) { // 该计划已经存在，但是planName更新过，此时更新到plan_name中，同时更新其他字段
                    doUpdate(_SQL_UPDATE_PLAN, mPlanId, po.getPlanName(), po.getGroupId(), po.getGroupName(),
                            po.getPlanSwitch(), po.getPlanStatus(), po.getBid(), po.getPlanBidStrategy(),
                            po.getPlanExtend(), po.getLandingPage(), now, planId);
                } else { // 单独使用planName匹配plan_name（并且m_plan_id=''）
                    planId = jdbcTemplate.queryForInt(_SQL_QUERY_PLANID_BY_PLANNAME_MPLANIDNULL, po.getMediumId(),
                            po.getMediumAccountId(), po.getCompanyId(), po.getPlanName());
                    if (planId > 0) { // 历史计划没有设置m_plan_id，现在使用mPlanId补全，同时更新其他字段
                        doUpdate(_SQL_UPDATE_PLAN, mPlanId, po.getPlanName(), po.getGroupId(), po.getGroupName(),
                                po.getPlanSwitch(), po.getPlanStatus(), po.getBid(), po.getPlanBidStrategy(),
                                po.getPlanExtend(), po.getLandingPage(), now, planId);
                    } else { // 直接新增
                        planId = doInsert(_SQL_INSERT_PLAN, po.getPlanName(), mPlanId, po.getGroupId(),
                                po.getGroupName(), po.getPlanSwitch(), po.getPlanStatus(), po.getBid(),
                                po.getPlanBidStrategy(), po.getPlanExtend(), po.getLandingPage(), po.getMediumId(),
                                po.getMediumAccountId(), po.getCompanyId(), now, now);
                    }
                }
            }

        }
        return planId;
    }

    private static void queryAndSetPlanCache(AdPlanStat po, final AdPlan cachePlan) {
        JdbcTemplate jdbcTemplate = DBClient.getJdbcTemplateDeAdt();
        jdbcTemplate.query(_SQL_QUERY_PLAN_BY_MPLANIDNAME, new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {
                cachePlan.setId(rs.getInt("plan_id"));
                cachePlan.setBid(rs.getInt("plan_bid"));
                cachePlan.setPlanBidStrategy(rs.getString("plan_bid_strategy"));
                cachePlan.setGroupId(rs.getString("group_id"));
                cachePlan.setGroupName(rs.getString("group_name"));
                cachePlan.setPlanStatus(rs.getString("plan_status"));
                cachePlan.setPlanSwitch(rs.getInt("plan_switch"));
                cachePlan.setPlanExtend(rs.getString("plan_extend"));
                cachePlan.setLandingPage(rs.getString("landing_page"));
            }
        }, po.getMediumId(), po.getMediumAccountId(), po.getCompanyId(), po.getPlanName(), po.getmPlanId());
    }

    private static boolean isCacheEqualsCrawlerData(AdPlan cache, AdPlanStat po) {
        if (compareInt(cache.getBid(), po.getBid()) && compareStr(cache.getPlanBidStrategy(), po.getPlanBidStrategy())
                && compareInt(cache.getPlanSwitch(), po.getPlanSwitch())
                && compareStr(cache.getPlanStatus(), po.getPlanStatus())
                && compareStr(cache.getGroupId(), po.getGroupId())
                && compareStr(cache.getGroupName(), po.getGroupName())) {
            return true;
        }
        return false;
    }

    private static boolean compareStr(String a, String b) {
        if (StringUtil.convertEmptyStr(a, "").equals(StringUtil.convertEmptyStr(b, ""))) {
            return true;
        }
        return false;
    }

    private static boolean compareInt(Integer a, Integer b) {
        if ((a == null && b == null) || (a != null && a.equals(b))) {
            return true;
        }
        return false;
    }

    /**
     * 重置媒体计划统计表(按天)
     *
     * @param mediumAccountId
     * @param statDate
     */
    public static void reSetPlanStatDay(int mediumAccountId, String statDate) {
        Date now = DateUtil.nowDate();
        doUpdate(_SQL_RESET_PLAN_STAT_DAY, now, mediumAccountId, statDate);
    }

    public static void updateAccountCookie(AdMediumAccount account) {
        Date now = DateUtil.nowDate();
        doUpdate(_SQL_UPDATE_ACCOUNT_COOKIE, account.getCookie(), now, account.getId(), account.getCompanyId());

    }

    public static void updateAccountSkey(AdMediumAccount account) {
        Date now = DateUtil.nowDate();
        doUpdate(_SQL_UPDATE_ACCOUNT_SKEY, account.getSkey(), now, account.getId(), account.getCompanyId());

    }

    private static int doInsert(String sql, Object... args) {
        JdbcTemplate jdbcTemplate = DBClient.getJdbcTemplateDeAdt();
        logger.info(DebugUtil.merge(sql, args));
        return (int) jdbcTemplate.insert(sql, args);
    }

    private static int doUpdate(String sql, Object... args) {
        JdbcTemplate jdbcTemplate = DBClient.getJdbcTemplateDeAdt();
        logger.info(DebugUtil.merge(sql, args));
        return (int) jdbcTemplate.update(sql, args);
    }

    private static final String SQL_INSERT_ACCOUNT_DAY = "insert into "
            + Table.AC_MEDIUM_ACCOUNT_STAT_DAY
            + "(stat_date, medium_id, medium_account_id, company_id, balance, cost_today, update_time) values (?, ?, ?, ?, ?, ?, ?) on duplicate key update balance = ?, cost_today = ?, update_time = ?";
    private static final String _SQL_INSERT_ACCOUNT_REALTIME = "insert into "
            + Table.AC_MEDIUM_ACCOUNT_STAT_REALTIME
            + "(stat_time, medium_id, medium_account_id, company_id, balance, cost_today, update_time) values (?, ?, ?, ?, ?, ?, ?) on duplicate key update balance = ?, cost_today = ?, update_time = ?";
    private static final String _SQL_QUERY_PLAN_BY_MPLANIDNAME = "select plan_id, plan_name, m_plan_id, group_id, group_name, plan_switch, plan_status, plan_bid, plan_bid_strategy, plan_extend, landing_page from "
            + Table.AD_PLAN
            + " where medium_id = ? and medium_account_id = ? and company_id = ? and plan_name = ? and m_plan_id = ?";
    // private static final String _SQL_QUERY_PLANID_BY_MPLANIDPLANNAME = "select plan_id from " + Table.AD_PLAN
    // + " where medium_id = ? and medium_account_id = ? and company_id = ? and plan_name = ? and m_plan_id = ?";
    private static final String _SQL_QUERY_PLANID_BY_MPLANID = "select plan_id from " + Table.AD_PLAN
            + " where medium_id = ? and medium_account_id = ? and company_id = ? and m_plan_id = ?";
    private static final String _SQL_QUERY_PLANID_BY_PLANNAME_MPLANIDNULL = "select plan_id from " + Table.AD_PLAN
            + " where medium_id = ? and medium_account_id = ? and company_id = ? and plan_name = ? and m_plan_id = ''";
    private static final String _SQL_QUERY_PLANID_BY_PLANNAME = "select plan_id from " + Table.AD_PLAN
            + " where medium_id = ? and medium_account_id = ? and company_id = ? and plan_name = ?";
    private static final String _SQL_UPDATE_PLAN = "update "
            + Table.AD_PLAN
            + " set m_plan_id = ?, plan_name = ?, group_id = ?, group_name = ? , plan_switch = ?, plan_status = ?, plan_bid = ?, plan_bid_strategy = ?, plan_extend = ?, landing_page = ?, update_time = ?"
            + " where plan_id = ?";
    private static final String _SQL_INSERT_PLAN = "insert into "
            + Table.AD_PLAN
            + " (plan_name, m_plan_id, group_id, group_name, plan_switch, plan_status, plan_bid, plan_bid_strategy, plan_extend, landing_page, medium_id, medium_account_id, company_id, create_time, update_time)"
            + " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String _SQL_INSERT_PLAN_DAY = "insert into "
            + Table.AD_PLAN_STAT_DAY
            + " (stat_date, plan_id, ad_id, medium_id, medium_account_id, company_id, page_id, pkg_id, cost, show_num, click_num, bid, discount_rate, update_time, os_type)"
            + " values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) on duplicate key update page_id = ?, pkg_id = ?, cost = ?, show_num = ?, click_num = ?, bid = ?, discount_rate = ?, update_time = ?, os_type = ?";
    private static final String _SQL_INSERT_PLAN_REALTIME = "insert into "
            + Table.AD_PLAN_STAT_REALTIME
            + " (stat_time, plan_id, ad_id, medium_id, medium_account_id, company_id, page_id, pkg_id, cost, show_num, click_num, bid, discount_rate, update_time, os_type)"
            + " values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) on duplicate key update page_id = ?, pkg_id = ?, cost = ?, show_num = ?, click_num = ?, bid = ?, discount_rate = ?, update_time = ?, os_type = ?";
    private static final String _SQL_INSERT_PLAN_DAYPAST = "insert into "
            + Table.AD_PLAN_STAT_DAY
            + " (stat_date, plan_id, ad_id, medium_id, medium_account_id, company_id, page_id, pkg_id, cost, show_num, click_num, bid, discount_rate, update_time, os_type)"
            + " values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) on duplicate key update page_id = if(page_id = 0, ?, page_id), pkg_id = if(pkg_id = 0, ?, pkg_id), cost = ?, show_num = ?, click_num = ?, bid = ?, discount_rate = ?, update_time = ?, os_type = ?";

    private static final String _SQL_INSERT_PAGE = "insert into " + Table.AD_PAGE
            + " (company_id, page_name, h5_app_id, h5_page_id, remark, create_time, update_time)"
            + " values(?, ?, ?, ?, ?, ?, ?) on duplicate key update page_id = last_insert_id(page_id), page_name = ?";

    private static final String _SQL_INSERT_PAGE_DAY = "insert into "
            + Table.AD_PAGE_STAT_DAY
            + " (stat_date, company_id, page_id, unique_click_num, pv, uv, download_times, update_time)"
            + " values(?, ?, ?, ?, ?, ?, ?, ?) on duplicate key update unique_click_num = ?, pv = ?, uv = ?, download_times = if(? = 0, download_times, ?), update_time = ?";

    private static final String _SQL_INSERT_PAGE_REALTIME = "insert into "
            + Table.AD_PAGE_STAT_REALTIME
            + " (stat_time, company_id, page_id, unique_click_num, pv, uv, download_times, update_time)"
            + " values(?, ?, ?, ?, ?, ?, ?, ?) on duplicate key update unique_click_num = ?, pv = ?, uv = ?, download_times = if(? = 0, download_times, ?), update_time = ?";

    private static final String _SQL_INSERT_PAGE_DAY_DOWNLOAN_TIMES = "insert into " + Table.AD_PAGE_STAT_DAY
            + " (stat_date, company_id, page_id, download_times, update_time)"
            + " values(?, ?, ?, ?, ?) on duplicate key update download_times = ?, update_time = ?";

    private static final String _SQL_INSERT_PAGE_REALTIME_DOWNLOAD_TIMES = "insert into " + Table.AD_PAGE_STAT_REALTIME
            + " (stat_time, company_id, page_id, download_times, update_time)"
            + " values(?, ?, ?, ?, ?) on duplicate key update download_times = ?, update_time = ?";

    private static final String _SQL_INSERT_PKG = "insert into " + Table.AD_PKG
            + " (company_id, pkg_name, cp_app_id, cp_pkg_id, remark, create_time, update_time)"
            + " values(?, ?, ?, ?, ?, ?, ?) on duplicate key update pkg_id = last_insert_id(pkg_id)";

    private static final String _SQL_INSERT_PKG_STAT_DAY = "insert into " + Table.AD_PKG_STAT_DAY
            + " (stat_date, company_id, pkg_id, active_num, register_num, update_time)"
            + " values(?, ?, ?, ?, ?, ?) on duplicate key update active_num = ?, register_num = ?, update_time = ?";

    private static final String _SQL_INSERT_PKG_STAT_REALTIME = "insert into " + Table.AD_PKG_STAT_REALTIME
            + " (stat_time, company_id, pkg_id, active_num, register_num, update_time)"
            + " values(?, ?, ?, ?, ?, ?) on duplicate key update active_num = ?, register_num = ?, update_time = ?";

    private static final String _SQL_UPDATE_ACCOUNT_COOKIE = "update " + Table.AC_MEDIUM_ACCOUNT
            + " set cookie = ?, update_time = ? " + " where medium_account_id = ? and company_id = ?";

    private static final String _SQL_UPDATE_ACCOUNT_SKEY = "update " + Table.AC_MEDIUM_ACCOUNT
            + " set skey = ?, update_time = ? " + " where medium_account_id = ? and company_id = ?";

    private static final String _SQL_UPDATE_PLAN_REALTION_PAGE = "update " + Table.AD_PLAN_REALTION
            + " set page_id = ?, product_id = ?, update_time = ? where company_id = ? and plan_id = ?";

    private static final String _SQL_INSERT_PLAN_REALTION_PAGE = "insert into " + Table.AD_PLAN_REALTION
            + " (company_id, plan_id, page_id, product_id, create_time, update_time) values(?, ?, ?, ?, ?, ?) "
            + " on duplicate key update plan_id = last_insert_id(plan_id)";

    private static final String _SQL_UPDATE_PLAN_REALTION_PKG = "update " + Table.AD_PLAN_REALTION
            + " set page_id = ?, pkg_id = ?, product_id = ?, update_time = ? where company_id = ? and plan_id = ?";

    private static final String _SQL_INSERT_PLAN_REALTION_PKG = "insert into "
            + Table.AD_PLAN_REALTION
            + " (company_id, plan_id, page_id, pkg_id, product_id, create_time, update_time) values(?, ?, ?, ?, ?, ?, ?) "
            + " on duplicate key update plan_id = last_insert_id(plan_id)";

    private static final String _SQL_RESET_PLAN_STAT_DAY = "update " + Table.AD_PLAN_STAT_DAY
            + " set cost = 0, show_num = 0, click_num=0, update_time = ?" + " where plan_id in ( select plan_id from "
            + Table.AD_PLAN + " where medium_account_id = ? ) and stat_date = ?";
}
