package com.dataeye.ad.scheduler.job.gdt.model;

import java.util.List;

public class GDTModel {
	private List<Order> list;
	private Conf conf;

	public List<Order> getList() {
		return list;
	}

	public void setList(List<Order> list) {
		this.list = list;
	}

	public Conf getConf() {
		return conf;
	}

	public void setConf(Conf conf) {
		this.conf = conf;
	}

}

class Conf {
	private int page;
	private int pagesize;
	private int totalnum;
	private int totalpage;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPagesize() {
		return pagesize;
	}

	public void setPagesize(int pagesize) {
		this.pagesize = pagesize;
	}

	public int getTotalnum() {
		return totalnum;
	}

	public void setTotalnum(int totalnum) {
		this.totalnum = totalnum;
	}

	public int getTotalpage() {
		return totalpage;
	}

	public void setTotalpage(int totalpage) {
		this.totalpage = totalpage;
	}

}

class Order {

	/**
	 * 曝光
	 */
	private String viewcount;

	/**
	 * 点击量
	 */
	private String validclickcount;

	/**
	 * 点击率（实际点击率）
	 */
	private String ctr;

	/**
	 * 总花费（按分）
	 */
	private String cost;

	/**
	 * cpc（按分）
	 */
	private double cpc;

	/**
	 * 订单号
	 */
	private String ordername;

	/**
	 * 出价（按分）
	 */
	private String price;

	public String getViewcount() {
		return viewcount;
	}

	public void setViewcount(String viewcount) {
		this.viewcount = viewcount;
	}

	public String getValidclickcount() {
		return validclickcount;
	}

	public void setValidclickcount(String validclickcount) {
		this.validclickcount = validclickcount;
	}

	public String getCtr() {
		return ctr;
	}

	public void setCtr(String ctr) {
		this.ctr = ctr;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public double getCpc() {
		return cpc;
	}

	public void setCpc(double cpc) {
		this.cpc = cpc;
	}

	public String getOrdername() {
		return ordername;
	}

	public void setOrdername(String ordername) {
		this.ordername = ordername;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

}
