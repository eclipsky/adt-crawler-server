package com.dataeye.ad.scheduler.job.e360;

import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;

import com.dataeye.ad.scheduler.job.CrawlerFilter;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;
import com.google.gson.JsonObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by VitAs on 2017/7/10.
 */
public class E360Job extends CrawlerBaseJob {

    private final static Constant.CrawlerPlatform platform = Constant.CrawlerPlatform.E360;

    private final static Logger logger = ServerUtil.getLogger(platform);

    private final static String planUrl = "http://mobile.e.360.cn/adgroup/list?ad_model={0}&startdate={1}&enddate={2}&page={3}&size={4}";

    private final static String accountUrl = "http://e.360.cn/api/account/get-budget?tmp=1518489364376";

    public E360Job() {
        this(DateUtil.unixTimestamp000());
    }

    public E360Job(long time) {
        super(time, platform);
    }

    public E360Job(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }

    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        // 媒体平台的计划和组概念；对应adt的组和计划
        int pageNum = 1;
        int pageSize = 100;
        PageModel pageModel = new PageModel(pageNum, pageSize);
        List<AdPlanStat> poList = new ArrayList<>();
        // 投放广告分为RTB和PMP两种类型，需要根据ad_model字段进行两次爬取
        String adModel = Constant.CrawlerAdModel.RTB;
        List<AdPlanStat> partList = getPartofList(adModel, time, account, pageModel);
        if (partList.size() > 0) {
            poList.addAll(partList);
        }
        adModel = Constant.CrawlerAdModel.PMP;
        partList = getPartofList(adModel, time, account, pageModel);
        if (partList.size() > 0) {
            poList.addAll(partList);
        }
        // 入库
        DataProcessor.processPlanDay(poList);

    }

    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        // 请求
        String content = HttpUtil.getContentByGet(accountUrl, getHeaderMap(cookie, accountUrl));
        logger.info(content);
        // 解析
        AdAccountStat adAccountStat = parseAccount(content, account, time);
        // 入库
        DataProcessor.processAccountDay(adAccountStat);
    }

    /**
     * 获取计划单元集合
     *
     * @param adModel
     * @param time
     * @param account
     * @param pageModel
     * @return
     * @throws Exception
     */
    private List<AdPlanStat> getPartofList(String adModel, long time, AdMediumAccount account, PageModel pageModel)
            throws Exception {
        List<AdPlanStat> allList = new ArrayList<>();
        String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        String cookie = account.getCookie();
        int pageNum = 1;
        int pageSize = 100;
        String url = HttpUtil.urlParamReplace(planUrl, adModel, reportDate, reportDate,
                String.valueOf(pageModel.getPageId()), String.valueOf(pageModel.getPageSize()));
        String content = HttpUtil.getContentByGet(url, getHeaderMap(cookie, planUrl));
        logger.info(content);
        // 由于RTB和PMP列表展示不一样;加入adModel参数
        List<AdPlanStat> partList = parsePlan(content, account, time, pageModel, adModel);
        allList.addAll(partList);
        int totalNum = pageModel.getTotalNum();
        totalNum -= pageSize;
        while (totalNum > 0) {
            pageNum++;
            content = HttpUtil.getContentByGet(url, getHeaderMap(cookie, planUrl));
            logger.info(content);
            // 解析
            partList = parsePlan(content, account, time, pageModel, adModel);
            allList.addAll(partList);
            totalNum -= pageSize;
        }
        return allList;
    }

    /**
     * 解析投放计划响应内容
     *
     * @param content
     * @param account
     * @param time
     * @param pageModel
     * @return
     * @throws Exception
     */
    private List<AdPlanStat> parsePlan(String content, AdMediumAccount account, long time, PageModel pageModel,
                                       String adModel) throws Exception {
        List<AdPlanStat> poList = new ArrayList<>();
        Document doc = Jsoup.parse(content);
        // 获取分页参数
        String pager = doc.select(".pager span:first-child").text();
        // 通过正则表达式匹配字符串中的数字
        String regEx = "[^0-9]";
        Pattern pattern = Pattern.compile(regEx);
        Matcher matcher = pattern.matcher(pager);
        // 获取记录总数
        int totalNum = StringUtil.convertInt(matcher.replaceAll(" ").trim(), 0);
        pageModel.setTotalNum(totalNum);
        // 解析页面表格部分
        Element listData = doc.getElementById("lists");
        Elements list = listData.getElementsByTag("tr");
        if (list.size() > 0) {
            for (Element element : list) {
                Elements tds = element.getElementsByTag("td");
                // 排除列表无数据，只有异常信息的情况
                if (tds.size() > 1) {
                    AdPlanStat reportPO = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(),
                            account.getId());
                    poList.add(reportPO);
                    // 解析时，通过adModel判断字段位置
                    if (adModel.equals(Constant.CrawlerAdModel.RTB)) {
                        // RTB
                        // 计划id
                        String mPlanId = tds.select("input").attr("value");
                        if (StringUtil.isEmpty(mPlanId)) {
                            continue;
                        }
                        // 计划名称
                        String planName = tds.select("td:nth-child(2)").text();
                        // 组id
                        String groupId = tds.select("td:nth-child(3)>a").attr("href");
                        // 通过正则表达式匹配字符串中的数字
                        matcher = pattern.matcher(groupId);
                        String planGourpId = matcher.replaceAll("").trim();
                        // 组名称
                        String groupName = tds.select("td:nth-child(3)").text();
                        // 计划状态
                        String planStatus = tds.select("td:nth-child(4)>span").text();
                        // 广告模式
                        String model = tds.select("td:nth-child(5)").text();
                        // 计划出价
                        String bidStr = tds.select("td:nth-child(12)").text();
                        double planBid = StringUtil.convertDouble(bidStr, 0.00);
                        // 曝光量
                        String showNumStr = tds.select("td:nth-child(14)").text();
                        int showNum = StringUtil.convertInt(showNumStr, 0);
                        // 点击量
                        String clickNumStr = tds.select("td:nth-child(15)").text();
                        int clickNum = StringUtil.convertInt(clickNumStr, 0);
                        // 消耗
                        String costStr = tds.select("td:nth-child(19)").text();
                        double cost = StringUtil.convertDouble(costStr, 0.00);
                        reportPO.setmPlanId(mPlanId);
                        reportPO.setPlanName(planName);
                        reportPO.setGroupId(planGourpId);
                        reportPO.setGroupName(groupName);
                        reportPO.setPlanStatus(planStatus);
                        reportPO.setBid(new BigDecimal(planBid).multiply(new BigDecimal(100))
                                .setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
                        reportPO.setShowNum(showNum);
                        reportPO.setClickNum(clickNum);
                        reportPO.setCost(new BigDecimal(cost).multiply(new BigDecimal(100))
                                .setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
                    } else {
                        // PMP
                        // 计划id
                        String mPlanId = tds.select("input").attr("value");
                        // 计划名称
                        String planName = tds.select("td:nth-child(2)").text();
                        // /组id
                        String groupId = tds.select("td:nth-child(3)>a").attr("href");
                        // 通过正则表达式匹配字符串中的数字
                        matcher = pattern.matcher(groupId);
                        String planGourpId = matcher.replaceAll("").trim();
                        // 组名称
                        String groupName = tds.select("td:nth-child(3)").text();
                        // 计划状态
                        String planStatus = tds.select("td:nth-child(4)>span").text();
                        // 广告模式
                        String model = tds.select("td:nth-child(5)").text();
                        // 计划出价
                        String bidStr = tds.select("td:nth-child(12)").text();
                        double planBid = StringUtil.convertDouble(bidStr, 0.00);
                        // 曝光量
                        String showNumStr = tds.select("td:nth-child(13)").text();
                        int showNum = StringUtil.convertInt(showNumStr, 0);
                        // 点击量
                        String clickNumStr = tds.select("td:nth-child(14)").text();
                        int clickNum = StringUtil.convertInt(clickNumStr, 0);
                        // 消耗
                        String costStr = tds.select("td:nth-child(17)").text();
                        double cost = StringUtil.convertDouble(costStr, 0.00);
                        reportPO.setmPlanId(mPlanId);
                        reportPO.setPlanName(planName);
                        reportPO.setGroupId(planGourpId);
                        reportPO.setGroupName(groupName);
                        reportPO.setPlanStatus(planStatus);
                        reportPO.setBid(new BigDecimal(planBid).multiply(new BigDecimal(100))
                                .setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
                        reportPO.setShowNum(showNum);
                        reportPO.setClickNum(clickNum);
                        reportPO.setCost(new BigDecimal(cost).multiply(new BigDecimal(100))
                                .setScale(0, BigDecimal.ROUND_HALF_UP).intValue());

                    }

                }
            }
        }
        return poList;

    }

    /**
     * 解析账户余额响应内容
     *
     * @param content
     * @param account
     * @param time
     * @return
     * @throws Exception
     */
    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time) throws Exception {
        AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                account.getId());
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        int code = resultJson.get("code").getAsInt();
        if (code != 0) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, resultJson.get("msg").getAsString());
        }
        JsonObject dataJson = resultJson.get("data").getAsJsonObject();
        if (dataJson != null) {
            int balance = StringUtil.convertBigDecimal(dataJson.get("real_balance").getAsString(), 100, 0).intValue();
            accountStat.setBalance(balance);
        }
        return accountStat;
    }

    private HashMap<String, String> getHeaderMap(String cookie, String url) {
        HashMap<String, String> headerMap = new HashMap<>();
        headerMap
                .put("Accept", "application/json, text/plain, */*");
        headerMap.put("Accept-Encoding", "gzip, deflate");
        headerMap.put("Accept-Language", "en,zh-CN;q=0.8,zh;q=0.6");
        headerMap.put("Connection", "keep-alive");
        headerMap.put("Cookie", cookie);
        if (url.contains("mobile")) {
            headerMap.put("Host", "mobile.e.360.cn");
            headerMap.put("Upgrade-Insecure-Requests", "1");
        } else {
            headerMap.put("Host", "e.360.cn");
            headerMap.put("Referer", "http://e.360.cn/home/allindex?startDate=2018-02-06&endDate=2018-02-12");
        }
        headerMap
                .put("User-Agent",
                        "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.86 Safari/537.36");
        return headerMap;

    }

}
