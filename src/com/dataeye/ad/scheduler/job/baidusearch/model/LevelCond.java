package com.dataeye.ad.scheduler.job.baidusearch.model;

/**
 * Created by VitAs on 2017/7/3.
 */
public class LevelCond {

    private String userid;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public LevelCond(String userid) {
        this.userid = userid;
    }
}
