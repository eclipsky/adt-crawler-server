package com.dataeye.ad.scheduler.job.baidubaiyi;

import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.scheduler.job.CrawlerBaseExecutor;
import com.dataeye.ad.util.DateUtil;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Created by VitAs on 2017/8/7.
 */
public class BaiduBaiyiJobDay implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        CrawlerBaseExecutor.addJob(new BaiduBaiyiJob(DateUtil.unixTimestamp000() - Constant.ONEDAY));
    }
}
