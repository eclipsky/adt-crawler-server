package com.dataeye.ad.scheduler.job.sm.model;

/**
 * Created by VitAs on 2017/6/30.
 */
public class BizParam {

    private final boolean winfoOrIdeaEmpty = false;

    private FilterMapJson filterMapJson;

    private String uid;

    private final int parentLevel = 1;

    //推广计划 2;推广单元 3
    private int level;

    private String startDate;

    private String endDate;

    private int reqPageIndex;

    private int recordPerPage;

    public boolean isWinfoOrIdeaEmpty() {
        return winfoOrIdeaEmpty;
    }

    public FilterMapJson getFilterMapJson() {
        return filterMapJson;
    }

    public void setFilterMapJson(FilterMapJson filterMapJson) {
        this.filterMapJson = filterMapJson;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getParentLevel() {
        return parentLevel;
    }

    public int getLevel() {
        return level;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getReqPageIndex() {
        return reqPageIndex;
    }

    public void setReqPageIndex(int reqPageIndex) {
        this.reqPageIndex = reqPageIndex;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public BizParam() {
    }

    public BizParam(FilterMapJson filterMapJson, String uid, int level, String startDate, String endDate, int reqPageIndex, int recordPerPage) {
        this.filterMapJson = filterMapJson;
        this.uid = uid;
        this.level = level;
        this.startDate = startDate;
        this.endDate = endDate;
        this.reqPageIndex = reqPageIndex;
        this.recordPerPage = recordPerPage;
    }
}
