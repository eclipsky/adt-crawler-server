package com.dataeye.ad.constant;

import java.util.HashMap;
import java.util.Map;

import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.scheduler.job.autohome.AutoHomeJob;
import com.dataeye.ad.scheduler.job.baidubaiyi.BaiduBaiyiJob;
import com.dataeye.ad.scheduler.job.baiduinfo.BaiduInfoJob;
import com.dataeye.ad.scheduler.job.baidusearch.BaiduSearchJob;
import com.dataeye.ad.scheduler.job.bihe.BiheJob;
import com.dataeye.ad.scheduler.job.dongqiudi.DongqiudiJob;
import com.dataeye.ad.scheduler.job.e360.E360Job;
import com.dataeye.ad.scheduler.job.fenghuang.FengHuangJob;
import com.dataeye.ad.scheduler.job.gdt.GDTJob;
import com.dataeye.ad.scheduler.job.iqiyi.IQiYiJob;
import com.dataeye.ad.scheduler.job.kuaishou.KuaishouJob;
import com.dataeye.ad.scheduler.job.netease.NeteaseJob;
import com.dataeye.ad.scheduler.job.neteaseold.NeteaseOldJob;
import com.dataeye.ad.scheduler.job.pinyou.PinyouJob;
import com.dataeye.ad.scheduler.job.sinafensitong.SinaFensitongJob;
import com.dataeye.ad.scheduler.job.sinafuyi.SinaFuyiJob;
import com.dataeye.ad.scheduler.job.sinafuyiold.SinaFuyiOldJob;
import com.dataeye.ad.scheduler.job.sinasuperfensitong.SinaSuperFensitongJob;
import com.dataeye.ad.scheduler.job.sm.SmJob;
import com.dataeye.ad.scheduler.job.sogousearch.SogouSearchJob;
import com.dataeye.ad.scheduler.job.sohuhuisuan.SohuHuisuanJob;
import com.dataeye.ad.scheduler.job.sohuxinpinsuan.SohuXinpinsuanJob;
import com.dataeye.ad.scheduler.job.toutiao.ToutiaoJob;
import com.dataeye.ad.scheduler.job.tui.TuiQQJob;
import com.dataeye.ad.scheduler.job.uc.UCJob;
import com.dataeye.ad.scheduler.job.weixin.WeixinJob;
import com.dataeye.ad.scheduler.job.xiaomi.XiaomiJob;
import com.dataeye.ad.scheduler.job.xuli.XuliJob;
import com.dataeye.ad.scheduler.job.xunlei.XunleiJob;
import com.dataeye.ad.scheduler.job.yidianzixun.YidianzixunJob;
import com.dataeye.ad.scheduler.job.youku.YoukuJob;
import com.dataeye.ad.scheduler.job.zaker.ZakerJob;
import com.dataeye.ad.scheduler.job.zhihu.ZhiHuJob;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Constant {

    public static final Gson GSON = new Gson();

    public static final Gson GSON_NULL = new GsonBuilder().serializeNulls().create();

    public static final String DEFAULT_STR = "";

    public static final int DEFAULT_INT = 0;

    public static final int DEFAULT_DISCOUNT = 1;

    public static final int DEFAULT = 0;

    public static final int ONEDAY = 86400;

    public enum CrawlerPlatform {
        PINYOU(1, "品友优弛", "pinyou", PinyouJob.class), //
        TOUTIAO_AD(2, "今日头条", "toutiao", ToutiaoJob.class), //
        TENCENT_TUI(3, "TUI", "tencenttui", TuiQQJob.class), //
        BAIDU_SEARCH(4, "百度搜索", "baidusearch", BaiduSearchJob.class), //
        TENCENT_MTA(5, "MTA", "tencentmta", null), // MTA为NULL
        NETEASEOLD(6, "网易易效老版", "neteaseold", NeteaseOldJob.class), //
        FENGHUANG(7, "凤凰凤羽DSP", "fenghuang", FengHuangJob.class), //
        BEHE(8, "壁合", "bihe", BiheJob.class), //
        UC(9, "UC阿里汇川", "uc", UCJob.class), //
        IQIYI(10, "奇麟神算", "iqiyi", IQiYiJob.class), //
        SOHU_HUISUAN(11, "搜狐汇算", "sohuhuisuan", SohuHuisuanJob.class), //
        NETEASE(12, "网易易效", "netease", NeteaseJob.class), //
        TENCENT_GDT(13, "GDT", "tencentgdt", GDTJob.class), //
        BAIDU_INFO(14, "百度信息流", "baiduinfo", BaiduInfoJob.class), //
        SINA_FUYIOLD(15, "新浪扶翼旧版", "sinafuyiold", SinaFuyiOldJob.class), //
        SINA_FENSITONG(16, "新浪粉丝通", "sinafensitong", SinaFensitongJob.class), //
        SOGOU_SEARCH(17, "搜狗搜索", "sogousearch", SogouSearchJob.class), //
        SM_SEARCH(18, "神马搜索", "smsearch", SmJob.class), //
        KUAISHOU(19, "快手", "kuaishou", KuaishouJob.class),
        AUTOHOME(20, "汽车之家", "autohome", AutoHomeJob.class), //
        YIDIANZIXUN(21, "一点资讯", "yidianzixun", YidianzixunJob.class), //
        SOGOU_QIDIAN(22, "搜狗奇点", "sogouqidian"), // 暂未接入
        ZAKER(23, "ZAKER", "zaker", ZakerJob.class), // 
        YOUKU(24, "优酷", "youku", YoukuJob.class), //
        SINA_SUPERFENSITONG(25, "新浪超级粉丝通", "sinasuperfensitong", SinaSuperFensitongJob.class), //
        XIAOMI(26, "小米营销", "xiaomi", XiaomiJob.class),
        WEIXIN(27, "微信广告", "weixin", WeixinJob.class),
        E360(28, "360点睛", "e360", E360Job.class), //
        BAIDU_BAIYI(29, "百度百意", "baidubaiyi", BaiduBaiyiJob.class), //
        DONGQIUDI(30, "懂球帝", "dongqiudi", DongqiudiJob.class),
        SOHU_XINPINSUAN(31, "搜狐新品算", "sohuxinpinsuan", SohuXinpinsuanJob.class),
        SINA_FUYI(32, "新浪扶翼", "sinafuyi", SinaFuyiJob.class),
        ZHIHU(33, "知乎营销平台", "zhihu", ZhiHuJob.class),
        XULI(34, "旭力网盟", "xuli", XuliJob.class),
        XUNLEI(35, "迅雷广告营销", "xunlei", XunleiJob.class);

        private int id;
        private String name;
        private String label;
        private Class<? extends CrawlerBaseJob> jobClass;

        CrawlerPlatform(int id, String name, String label) {
            this.id = id;
            this.name = name;
            this.label = label;
        }

        CrawlerPlatform(int id, String name, String label, Class<? extends CrawlerBaseJob> jobClass) {
            this(id, name, label);
            this.jobClass = jobClass;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public Class<? extends CrawlerBaseJob> getJobClass() {
            return jobClass;
        }

        public void setJobClass(Class<? extends CrawlerBaseJob> jobClass) {
            this.jobClass = jobClass;
        }

        public static Map<Integer, CrawlerPlatform> getPlatformMap() {
            Map<Integer, CrawlerPlatform> platformMap = new HashMap<Integer, CrawlerPlatform>();
            for (CrawlerPlatform platform : CrawlerPlatform.values()) {
                platformMap.put(platform.getId(), platform);
            }
            return platformMap;
        }
    }

    public class PlatformType {
        public static final int MEDIUM = 1;

        public static final int H5 = 2;

        public static final int CP = 3;
    }

    /**
     * 登录状态
     *
     * @author sam.xie
     * @version 1.0
     * @date May 9, 2017 4:52:59 PM
     */
    public class LoginStatus {
        public static final int HAS_LOGGED = 0; // 已登录
        public static final int NOT_LOGGED = 1; // 未登录
        public static final int IS_LOGGING = 2; // 登录中
        // 下面几种状态属于登录失败的具体原因
        public static final int MOBILE_VERIFY = 3; // 手机验证
        public static final int PWD_ERROR = 4; // 密码错误
        public static final int ACCOUNT_FORBIDDEN = 5; // 账户被禁
        public static final int AUTOLOGIN_SUSPEND = 6; // 自动登录暂停

    }

    /**
     * 账户状态
     *
     * @author sam.xie
     * @version 1.0
     * @date May 9, 2017 4:53:08 PM
     */
    public class AccountStatus {
        public static final int NORMAL = 0; // 正常
        public static final int INVALID = 1; // 失效
        public static final int ABNORMAL = 2; // 异常
    }

    public class CrawlerPage {
        public static final int PLAN = 1; // 计划
        public static final int PLAN_GROUP = 2; // 计划组
    }

    public class CrawlerAdModel {
        public static final String RTB = "RTB";
        public static final String PMP = "PMP";
    }

    public class Visible {
        public static final int YES = 0;
        public static final int NO = 1;
    }

    public class TuiQQQueryOperator {
        public static final String EQUALS = "EQUALS";
        public static final String CONTAINS = "CONTAINS";
    }

    public class PlanSwitch {
        public static final int ON = 1;
        public static final int OFF = 0;
    }

    public class KafkaEvent {
        public static final String EVENT_ACCOUNT = "ACCOUNT";
        public static final String EVENT_PLAN = "PLAN";
        public static final String EVENT_PAGE = "PAGE";
        public static final String EVENT_PAGE_DOWNLOAD = "PAGE_DOWNLOAD";
    }

    public class Table {

        /**
         * IP代理表
         */
        public static final String IP_PROXY = "ip_proxy";

        /**
         * cookie表
         */
        public static final String AD_CRAWLER_COOKIE = "ad_crawler_cookie";

        /**
         * 关联表
         */
        public static final String AD_PLAN_REALTION = "ad_plan_relation";
        /**
         * 媒体折扣
         */
        public static final String AC_MEDIUM_DISCOUNT = "ac_medium_discount_rate";

        /**
         * 媒体平台
         */
        public static final String AC_MEDIUM = "ac_medium";
        /**
         * 媒体账户表
         */
        public static final String AC_MEDIUM_ACCOUNT = "ac_medium_account";

        /**
         * 媒体账户统计表（余额等）
         */
        public static final String AC_MEDIUM_ACCOUNT_STAT_DAY = "ac_medium_account_stat_day";

        /**
         * 媒体账户统计表（余额等），实时统计
         */
        public static final String AC_MEDIUM_ACCOUNT_STAT_REALTIME = "ac_medium_account_stat_realtime";

        /**
         * 计划表
         */
        public static final String AD_PLAN = "ad_plan";
        /**
         * 计划统计表（按天）
         */
        public static final String AD_PLAN_STAT_DAY = "ad_plan_stat_day";
        /**
         * 计划统计表（实时）
         */
        public static final String AD_PLAN_STAT_REALTIME = "ad_plan_stat_realtime";

        /**
         * H5平台
         */
        public static final String AD_H5 = "ac_h5";
        /**
         * H5账户表
         */
        public static final String AC_H5_ACCOUNT = "ac_h5_account";
        /**
         * H5应用表
         */
        public static final String AC_H5_APP = "ac_h5_app";
        /**
         * 落地页表
         */
        public static final String AD_PAGE = "ad_page";
        /**
         * 落地页统计表（按天）
         */
        public static final String AD_PAGE_STAT_DAY = "ad_page_stat_day";
        /**
         * 落地页统计表（实时）
         */
        public static final String AD_PAGE_STAT_REALTIME = "ad_page_stat_realtime";

        /**
         * CP平台
         */
        public static final String AD_CP = "ac_cp";
        /**
         * CP账户表
         */
        public static final String AC_CP_ACCOUNT = "ac_cp_account";
        /**
         * CP应用表
         */
        public static final String AC_CP_APP = "ac_cp_app";
        /**
         * 下载包表
         */
        public static final String AD_PKG = "ad_pkg";
        /**
         * 下载包统计表（按天）
         */
        public static final String AD_PKG_STAT_DAY = "ad_pkg_stat_day";
        /**
         * 下载包统计表（实时）
         */
        public static final String AD_PKG_STAT_REALTIME = "ad_pkg_stat_realtime";
        /**
         * Tracking统计表（按天）
         */
        @Deprecated
        public static final String AD_TRACKING_DAY = "ad_tracking_day";
        /**
         * Tracking统计表（实时）
         */
        @Deprecated
        public static final String AD_TRACKING_REALTIME = "ad_tracking_realtime";
    }
}
