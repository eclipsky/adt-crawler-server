package com.dataeye.ad.constant;

public class StatusCode {

	/** 请求正常处理 */
	public static final int SUCCESS = 200;
	/** 正确请求了但是没有数据 */
	public static final int SUCCESS_NO_DATA = 201;
	/** 参数不足 */
	public static final int PARAMETER_NOT_ENOUGH = 401;
	/** 登录态异常，需要重新登录 */
	public static final int RE_LOGIN = 402;
	/** 参数错误 */
	public static final int PARAMETER_ERROR = 403;
}
