package com.dataeye.ad.pipeline.po;

/**
 * <pre/>
 * 基本帐号表
 * @author sam.xie
 * @date Mar 6, 2017 4:36:15 PM
 * @version 1.0
 */
public abstract class AdAccountBase extends AdBase {

	/**
	 * 平台类型，1-媒体，2-H5，3-CP
	 */
	private int platformType;

	private int platformId;

	private String account;

	private String password;

	private int status;

	private int loginStatus;

	private String cookie;

	public int getPlatformType() {
		return platformType;
	}

	public void setPlatformType(int platformType) {
		this.platformType = platformType;
	}

	public int getPlatformId() {
		return platformId;
	}

	public void setPlatformId(int platformId) {
		this.platformId = platformId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getLoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(int loginStatus) {
		this.loginStatus = loginStatus;
	}

	public String getCookie() {
		return cookie;
	}

	public void setCookie(String cookie) {
		this.cookie = cookie;
	}

	@Override
	public String toString() {
		return "[company:" + getCompanyId() + ", platform:" + getPlatformId() + ", id:" + getId() + ", account:"
				+ getAccount() + "]";
	}

}
