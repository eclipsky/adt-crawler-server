package com.dataeye.ad.scheduler.job.sinasuperfensitong.model;

import java.util.List;

public class SinaWeiboPlanPastVo {
	
	private SinaWeiboPastThead thead;
	private List<SinaWeiboPlanPastTbody> tbody;
	
	public SinaWeiboPastThead getThead() {
		return thead;
	}
	public void setThead(SinaWeiboPastThead thead) {
		this.thead = thead;
	}
	public List<SinaWeiboPlanPastTbody> getTbody() {
		return tbody;
	}
	public void setTbody(List<SinaWeiboPlanPastTbody> tbody) {
		this.tbody = tbody;
	}
	
	

}
