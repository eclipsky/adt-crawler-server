package com.dataeye.ad.scheduler.job.zhihu;

import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.constant.Constant.*;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DBPipeline;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.scheduler.job.CrawlerFilter;
import com.dataeye.ad.util.*;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lingliqi
 * @create 2017-10-17 9:55
 */
public class ZhiHuJob extends CrawlerBaseJob {

    private final static CrawlerPlatform platform = CrawlerPlatform.ZHIHU;

    private final static Logger logger = ServerUtil.getLogger(platform);

    private final static String accountUrl = "http://xg.zhihu.com/api/v1/user/me";

    private final static String planUrl = "http://xg.zhihu.com/api/v1/ad?page={0}&perPage={1}&userId={2}&from={3}&to={4}&orderBy=id&direction=desc";

    public ZhiHuJob() {
        this(DateUtil.unixTimestamp000());
    }

    public ZhiHuJob(long time) {
        super(time, platform);
    }

    public ZhiHuJob(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }

    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        //请求
        String content = HttpUtil.getContentByGet(accountUrl, getHeadMap(cookie));
        logger.info(content);
        //解析
        AdAccountStat accountStat = parseAccount(content, account, time);
        //入库
        DataProcessor.processAccountDay(accountStat);
    }

    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        String statDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        String cookie = account.getCookie();
        String userId = account.getSkey();
        if (StringUtil.isEmpty(userId)) {
            ExceptionHandler.throwCookieException(ExceptionEnum.SKEY_NOT_EXISTS);
        }
        int page = 1;
        int pageSize = 20;
        PageModel pageModel = new PageModel(page, pageSize);
        String content = HttpUtil.getContentByGet(HttpUtil.urlParamReplace(planUrl, page + "", pageSize + "", userId, statDate, statDate), getHeadMap(cookie));
        logger.info(content);
        List<AdPlanStat> planList = parsePlan(content, account, time, pageModel);
        // 计划数为0，直接跳过
        int totalNum = pageModel.getTotalNum();
        // 入库
        DataProcessor.processPlanDay(planList);
        totalNum -= pageSize;
        while (totalNum > 0) {
            page++;
            content = HttpUtil.getContentByGet(HttpUtil.urlParamReplace(planUrl, page + "", pageSize + "", userId, statDate, statDate), getHeadMap(cookie));
            logger.info(content);
            planList = parsePlan(content, account, time, pageModel);
            // 入库
            DataProcessor.processPlanDay(planList);
            totalNum -= pageSize;
        }
    }

    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time) throws Exception {
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                account.getId());
        int balance = StringUtil.getJsonIntValue(resultJson, "balance");
        accountStat.setBalance(balance);
        String skey = StringUtil.getJsonStrValue(resultJson, "id");
        //在第一次的爬取的时候，更新账号的skey
        if (StringUtil.isEmpty(account.getSkey())) {
            account.setSkey(skey);
            DBPipeline.updateAccountSkey(account);
        }
        return accountStat;
    }

    private List<AdPlanStat> parsePlan(String content, AdMediumAccount account, long time, PageModel pageModel) throws Exception {
        List<AdPlanStat> planList = new ArrayList<AdPlanStat>();
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        int totalNum = StringUtil.getJsonIntValue(resultJson, "totalCount");
        pageModel.setTotalNum(totalNum);
        JsonArray dataJson = resultJson.get("result").getAsJsonArray();
        if (null != dataJson) {
            for (JsonElement element : dataJson) {
                JsonObject order = element.getAsJsonObject();
                // 媒体计划ID
                String mPlanId = order.get("id").getAsString();
                // 媒体计划名称
                String planName = order.get("name").getAsString();
                JsonObject campaign = order.get("campaign").getAsJsonObject();
                // 计划分组ID
                String groupId = campaign.get("id").getAsString();
                // 计划分组名称
                String groupName = campaign.get("name").getAsString();
                JsonObject counter = order.get("counter").getAsJsonObject();
                // 点击数
                int clickNum = StringUtil.getJsonIntValue(counter, "click");
                // 曝光数
                int showNum = StringUtil.getJsonIntValue(counter, "impression");
                // 消耗
                int cost = StringUtil.getJsonIntValue(counter, "cost");
                // 出价策略
                String planBidStrategy = order.get("bidType").getAsString();
                // 单价 已经是分为单位
                int planBid = StringUtil.getJsonIntValue(order, "price");
                // 计划状态
                String planStatus = order.get("status").getAsString();
                AdPlanStat reportPO = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(), account.getId());
                reportPO.setmPlanId(mPlanId);
                reportPO.setPlanName(planName);
                reportPO.setClickNum(clickNum);
                reportPO.setCost(cost);
                reportPO.setShowNum(showNum);
                reportPO.setBid(planBid);
                reportPO.setPlanBidStrategy(planBidStrategy);
                reportPO.setGroupId(groupId);
                reportPO.setGroupName(groupName);
                reportPO.setPlanStatus(planStatus);
                planList.add(reportPO);


            }
        }
        return planList;
    }


    private Map<String, String> getHeadMap(String cookie) {
        Map<String, String> headMap = new HashMap<>();
        headMap.put("Cookie", cookie);
        headMap.put("Host", "xg.zhihu.com");
        headMap.put("Accept", "*/*");
        headMap.put("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
        headMap.put("Accept-Encoding", "gzip, deflate");
        headMap.put("origin", "http://xg.zhihu.com");
        headMap.put("Connection", "keep-alive");
        headMap.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:56.0) Gecko/20100101 Firefox/56.0");
        return headMap;

    }

}
