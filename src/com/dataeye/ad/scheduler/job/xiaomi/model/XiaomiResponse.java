package com.dataeye.ad.scheduler.job.xiaomi.model;

import java.util.List;

public class XiaomiResponse {
    private String success;
    private int code;
    private String failMsg;
    private Result result;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getFailMsg() {
        return failMsg;
    }

    public void setFailMsg(String failMsg) {
        this.failMsg = failMsg;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {
        private Conf conf;
        private List<Item> list;

        public Conf getConf() {
            return conf;
        }

        public void setConf(Conf conf) {
            this.conf = conf;
        }

        public List<Item> getList() {
            return list;
        }

        public void setList(List<Item> list) {
            this.list = list;
        }

        public class Conf {
            private int page;
            private int pagesize;
            private int totalnum;

            public int getPage() {
                return page;
            }

            public void setPage(int page) {
                this.page = page;
            }

            public int getPagesize() {
                return pagesize;
            }

            public void setPagesize(int pagesize) {
                this.pagesize = pagesize;
            }

            public int getTotalnum() {
                return totalnum;
            }

            public void setTotalnum(int totalnum) {
                this.totalnum = totalnum;
            }

        }

        public class Item {
            private String campaignId; // 广告计划
            private String campaignName;
            private String groupId; // 广告组
            private String groupName;
            private String id; // 广告创意
            private String name;
            private String bid;
            private Bidding bidding;
            private int clickSum;
            private String cost;
            private int viewSum;
            private int downloadSum;
            private int assetCheckStatus; // 审核状态
            private int status; // 开关状态

            public int getAssetCheckStatus() {
                return assetCheckStatus;
            }

            public void setAssetCheckStatus(int assetCheckStatus) {
                this.assetCheckStatus = assetCheckStatus;
            }

            public String getCampaignId() {
                return campaignId;
            }

            public void setCampaignId(String campaignId) {
                this.campaignId = campaignId;
            }

            public String getCampaignName() {
                return campaignName;
            }

            public void setCampaignName(String campaignName) {
                this.campaignName = campaignName;
            }

            public String getGroupId() {
                return groupId;
            }

            public void setGroupId(String groupId) {
                this.groupId = groupId;
            }

            public String getGroupName() {
                return groupName;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public void setGroupName(String groupName) {
                this.groupName = groupName;
            }

            public String getBid() {
                return bid;
            }

            public void setBid(String bid) {
                this.bid = bid;
            }

            public int getClickSum() {
                return clickSum;
            }

            public void setClickSum(int clickSum) {
                this.clickSum = clickSum;
            }

            public String getCost() {
                return cost;
            }

            public void setCost(String cost) {
                this.cost = cost;
            }

            public int getViewSum() {
                return viewSum;
            }

            public void setViewSum(int viewSum) {
                this.viewSum = viewSum;
            }

            public int getDownloadSum() {
                return downloadSum;
            }

            public void setDownloadSum(int downloadSum) {
                this.downloadSum = downloadSum;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public Bidding getBidding() {
                return bidding;
            }

            public void setBidding(Bidding bidding) {
                this.bidding = bidding;
            }

            public class Bidding {
                private String bid;
                private int billingType;
                private String billingTypeName;
                public String getBid() {
                    return bid;
                }
                public void setBid(String bid) {
                    this.bid = bid;
                }
                public int getBillingType() {
                    return billingType;
                }
                public void setBillingType(int billingType) {
                    this.billingType = billingType;
                }
                public String getBillingTypeName() {
                    return billingTypeName;
                }
                public void setBillingTypeName(String billingTypeName) {
                    this.billingTypeName = billingTypeName;
                }
                
            }
        }
    }

}
