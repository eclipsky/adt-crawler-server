package com.dataeye.ad.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;

import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.util.log.DELogger;
import com.xunlei.jdbc.JdbcTemplate;
import com.xunlei.jdbc.RowCallbackHandler;

public class ConfigFromDB {

    private final static Logger logger = DELogger.getLogger("config_from_db");

    private static JdbcTemplate jdbcTemplate = DBClient.getJdbcTemplateDeAdt();

    private static final String CONFIG_DB = "de_adt";

    private static final String CONFIG_TABLE = "ac_config";

    private static final String CHECK_UPDATE_SQL = "select unix_timestamp(update_time) from information_schema.tables where table_schema = ? and table_name = ?";

    private static final String GET_CONFIG_SQL = "select vkey, value from " + CONFIG_DB + "." + CONFIG_TABLE;

    private static long cachedUpdateTime = 0;

    private static Map<String, String> configMap = new HashMap<String, String>();

    public static void init() {
        load();
        reLoad();
    }

    private static void load() {
        logger.info("de_adt.ac_config refreshing...");
        final Map<String, String> kv = new HashMap<String, String>();
        long lastUpdateTime = jdbcTemplate.queryForLong(CHECK_UPDATE_SQL, CONFIG_DB, CONFIG_TABLE);
        if (lastUpdateTime < 0) {
            ExceptionHandler.throwException("de_adt.ac_config is not exists! please check!");
        }
        if (cachedUpdateTime < lastUpdateTime) {
            // 配置表更新时间变更过，需要重新加载配置
            jdbcTemplate.query(GET_CONFIG_SQL, new RowCallbackHandler() {
                @Override
                public void processRow(ResultSet rs) throws SQLException {
                    kv.put(rs.getString("vkey"), rs.getString("value"));
                }
            });
            logger.info("de_adt.ac_config changed!" + configMap + " -> " + kv);

            flushConfig(kv);

            cachedUpdateTime = lastUpdateTime;
        } else {
            logger.info("de_adt.ac_config not changed!");
        }
    }

    private static void reLoad() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        load();
                        Thread.sleep(1000 * 60);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

        }).start();
    }

    public synchronized static void flushConfig(Map<String, String> kv) {
        configMap.clear();
        configMap.putAll(kv);

        // 广点通配置重新加载
        GDTUtil.initConfig();
    }

    public synchronized static String getConfig(String key) {
        return configMap.get(key);
    }

    public synchronized static String getConfig(String key, String defaultValue) {
        if (StringUtil.isEmpty(configMap.get(key))) {
            return defaultValue;
        } else {
            return configMap.get(key);
        }
    }
}
