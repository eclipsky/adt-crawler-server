package com.dataeye.ad.pipeline.po;

import java.util.Date;

public class AdPageStat extends AdStatBase {
    private int pageId;
    private String pageName;
    private int h5Id;
    private int h5AppId;
    private String h5PageId;
    private int pv;
    private int uv;
    private int totalClickNum;
    private int uniqueClickNum;
    private int downloadTimes;
    private int activeNum;
    private String remark;

    public AdPageStat(Date statDate, int companyId) {
        this.setStatDate(statDate);
        this.setCompanyId(companyId);
    }

    public AdPageStat(Date statDate, int companyId, int h5Id, int h5AppId) {
        this(statDate, companyId);
        this.setH5Id(h5Id);
        this.setH5AppId(h5AppId);
    }

    public int getPageId() {
        return pageId;
    }

    public void setPageId(int pageId) {
        this.pageId = pageId;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public int getH5Id() {
        return h5Id;
    }

    public void setH5Id(int h5Id) {
        this.h5Id = h5Id;
    }

    public int getH5AppId() {
        return h5AppId;
    }

    public void setH5AppId(int h5AppId) {
        this.h5AppId = h5AppId;
    }

    public String getH5PageId() {
        return h5PageId;
    }

    public void setH5PageId(String h5PageId) {
        this.h5PageId = h5PageId;
    }

    public int getPv() {
        return pv;
    }

    public void setPv(int pv) {
        this.pv = pv;
    }

    public int getUv() {
        return uv;
    }

    public void setUv(int uv) {
        this.uv = uv;
    }

    public int getDownloadTimes() {
        return downloadTimes;
    }

    public void setDownloadTimes(int downloadTimes) {
        this.downloadTimes = downloadTimes;
    }

    public int getActiveNum() {
        return activeNum;
    }

    public void setActiveNum(int activeNum) {
        this.activeNum = activeNum;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getTotalClickNum() {
        return totalClickNum;
    }

    public void setTotalClickNum(int totalClickNum) {
        this.totalClickNum = totalClickNum;
    }

    public int getUniqueClickNum() {
        return uniqueClickNum;
    }

    public void setUniqueClickNum(int uniqueClickNum) {
        this.uniqueClickNum = uniqueClickNum;
    }

}
