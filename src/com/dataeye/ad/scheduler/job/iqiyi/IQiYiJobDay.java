package com.dataeye.ad.scheduler.job.iqiyi;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.scheduler.job.CrawlerBaseExecutor;
import com.dataeye.ad.util.DateUtil;

public class IQiYiJobDay implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		CrawlerBaseExecutor.addJob(new IQiYiJob(DateUtil.unixTimestamp000() - Constant.ONEDAY));
	}

}
