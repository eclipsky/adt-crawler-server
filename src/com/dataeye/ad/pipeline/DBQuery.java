package com.dataeye.ad.pipeline;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.constant.Constant.PlatformType;
import com.dataeye.ad.constant.Constant.Table;
import com.dataeye.ad.pipeline.po.AdAccountWithApp;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdMediumDiscount;
import com.dataeye.ad.pipeline.po.AdPage;
import com.dataeye.ad.pipeline.po.AdPkg;
import com.dataeye.ad.pipeline.po.AdPlanRelation;
import com.dataeye.ad.scheduler.job.CrawlerFilter;
import com.dataeye.ad.util.DBClient;
import com.xunlei.jdbc.JdbcTemplate;
import com.xunlei.jdbc.RowCallbackHandler;
import org.apache.commons.lang.StringUtils;

public class DBQuery {

    private static JdbcTemplate jdbcTemplate = DBClient.getJdbcTemplateDeAdt();

    /**
     * <pre>
     * 获取媒体帐号列表
     * @author sam.xie
     * @date Mar 6, 2017 8:26:55 PM
     * @param mediumId
     * @return
     */
    public static List<AdMediumAccount> getMediumAccountList(int mediumId) {
        return getMediumAccountList(null, mediumId, null, null);
    }

    public static List<AdMediumAccount> getMediumAccountList(String mediumAccountIds) {
        return getMediumAccountList(null, mediumAccountIds);

    }

    public static List<AdMediumAccount> getMediumAccountList(int mediumId, CrawlerFilter filter) {
        if (filter != null) {
            Integer companyId = filter.getCompanyId();
            Integer accountId = filter.getAccountId();
            String account = filter.getAccount();
            return getMediumAccountList(companyId, mediumId, accountId, account);
        } else {
            return getMediumAccountList(mediumId);
        }

    }

    /**
     * <pre>
     * 获取H5帐号列表
     * @author sam.xie
     * @date Mar 6, 2017 8:26:00 PM
     * @param platform
     * @return
     */
    public static List<AdAccountWithApp> getH5AppList(int platform) {
        return getH5AppList(null, platform, null);
    }

    /**
     * <pre>
     * 获取CP帐号列表
     * @author sam.xie
     * @date Mar 6, 2017 8:32:12 PM
     * @param platform
     * @return
     */
    public static List<AdAccountWithApp> getCPAppList(int platform) {
        return getCPAppList(null, platform, null);
    }

    /**
     * <pre>
     * 获取包列表
     * @author sam.xie
     * @date Mar 6, 2017 3:17:15 PM
     * @param companyId
     * @param pkgName
     * @return
     */
    public static List<AdPkg> getPkgList(int companyId) {
        return getPkgList(companyId, null);
    }

    /**
     * <pre>
     * 获取页面列表
     * @author sam.xie
     * @date Mar 6, 2017 3:17:26 PM
     * @param companyId
     * @param pageName
     * @return
     */
    public static List<AdPage> getPageList(int companyId) {
        return getPageList(companyId, null, null);
    }

    public static List<AdPage> getPageList(int companyId, int appId) {
        return getPageList(companyId, appId, null);
    }

    /**
     * <pre>
     * 通过计划ID获取关联规则
     * @author sam.xie
     * @date Mar 7, 2017 11:06:28 AM
     * @param companyId
     * @param planId
     * @return
     */
    public static AdPlanRelation getPlanRelationByPlanId(int companyId, int planId) {
        // TODO 缓存
        List<AdPlanRelation> relations = getPlanRelationList(companyId, planId, null);
        if (relations.size() > 0) {
            return relations.get(0);
        }
        return null;
    }

    /**
     * <pre>
     * 通过页面获取关联规则是否存在
     * @author sam.xie
     * @date Mar 7, 2017 11:06:15 AM
     * @param companyId
     * @param pkgId
     * @return
     */
    public static boolean existsRelationByPageName(int companyId, int pkgId, String pageName) {
        // TODO 缓存
        String sql = "select count(1) as num from " + Table.AD_PLAN_REALTION + " t1 inner join " + Table.AD_PAGE
                + " t2 on t1.page_id = t2.page_id " + " where t1.company_id = " + companyId + " and t1.pkg_id = "
                + pkgId + " and lower(t2.page_name) like '%" + pageName.toLowerCase() + "%' limit 1";
        int count = jdbcTemplate.queryForInt(sql);
        if (count <= 0) {
            return false;
        }
        return true;
    }

    /**
     * <pre>
     * 通过包ID获取关联规则
     * @author sam.xie
     * @date Mar 7, 2017 11:06:15 AM
     * @param companyId
     * @param pkgId
     * @return
     */
    public static AdPlanRelation getPlanRelationByPkgId(int companyId, int pkgId) {
        // TODO 缓存
        List<AdPlanRelation> relations = getPlanRelationList(companyId, null, pkgId);
        if (relations.size() > 0) {
            return relations.get(0);
        }
        return null;
    }

    /**
     * <pre>
     * 通过媒体帐号ID获取折扣率
     * @author sam.xie
     * @date Mar 7, 2017 11:05:55 AM
     * @param companyId
     * @param mediumAccountId
     * @return
     */
    public static AdMediumDiscount getMediumDiscount(int companyId, int mediumAccountId) {
        // TODO 缓存
        List<AdMediumDiscount> relations = getMediumDiscountList(companyId, mediumAccountId);
        if (relations.size() > 0) {
            return relations.get(0);
        }
        return null;
    }

    /**
     * <pre>
     * 通过包名获取包数据
     * @author sam.xie
     * @date Mar 6, 2017 8:42:54 PM
     * @param pkgName
     * @return
     */
    public static AdPkg getPkgByName(Integer companyId, String pkgName) {
        List<AdPkg> list = getPkgList(null, pkgName);
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    /**
     * 批量查询媒体账号id列表
     * @param companyId
     * @param mediumAccountIds
     * @return
     */
    private static List<AdMediumAccount> getMediumAccountList(Integer companyId, String mediumAccountIds) {
        StringBuilder sb = new StringBuilder(
                "select company_id, medium_account_id, medium_id, medium_account, password, skey, remark, login_status, cookie, page_type from "
                        + Constant.Table.AC_MEDIUM_ACCOUNT
                        + " where company_id not in (1, 22, 78)" // demo或者测试公司，不需要爬取
                        + " and medium_id < 1000" // 对接了媒体数据
                        + " and status = 0" // 帐号状态
                        + " and sync_mode = 1" // 使用爬虫方式同步
        );
        if (null != companyId && companyId > 0) {
            sb.append(" and company_id = ").append(companyId);
        }
        if (StringUtils.isNotBlank(mediumAccountIds)) {
            sb.append(" and medium_account_id in (").append(mediumAccountIds).append(")");
        }
        String sql = sb.toString();
        final List<AdMediumAccount> list = new ArrayList<AdMediumAccount>();
        jdbcTemplate.query(sql, new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {
                AdMediumAccount account = new AdMediumAccount();
                account.setPlatformType(PlatformType.MEDIUM); // 设置为媒体
                list.add(account);
                account.setCompanyId(rs.getInt(1));
                account.setId(rs.getInt(2));
                account.setPlatformId(rs.getInt(3));
                account.setAccount(rs.getString(4));
                account.setPassword(rs.getString(5));
                account.setSkey(rs.getString(6));
                account.setRemark(rs.getString(7));
                account.setLoginStatus(rs.getInt(8));
                account.setCookie(rs.getString(9));
                account.setPageType(rs.getInt(10));
            }
        });
        return list;
    }

    private static List<AdMediumAccount> getMediumAccountList(Integer companyId, Integer mediumId, Integer accountId,
                                                              String account) {
        StringBuilder sb = new StringBuilder(
                "select company_id, medium_account_id, medium_id, medium_account, password, skey, remark, login_status, cookie, page_type from "
                        + Constant.Table.AC_MEDIUM_ACCOUNT
                        + " where company_id not in (1, 22, 78)" // demo或者测试公司，不需要爬取 
                        + " and medium_id < 1000" // 对接了媒体数据
                        + " and status = 0" // 帐号状态
                        + " and sync_mode = 1" // 使用爬虫方式同步
        );
        if (null != companyId && companyId > 0) {
            sb.append(" and company_id = " + companyId);
        }
        if (null != mediumId && mediumId > 0) {
            sb.append(" and medium_id = " + mediumId);
        }
        if (null != accountId && accountId > 0) {
            sb.append(" and medium_account_id = " + accountId);
        }
        if (null != account) {
            sb.append(" and medium_account = '" + account + "'");
        }
        String sql = sb.toString();
        final List<AdMediumAccount> list = new ArrayList<AdMediumAccount>();
        jdbcTemplate.query(sql, new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {
                AdMediumAccount account = new AdMediumAccount();
                account.setPlatformType(PlatformType.MEDIUM); // 设置为媒体
                list.add(account);
                account.setCompanyId(rs.getInt(1));
                account.setId(rs.getInt(2));
                account.setPlatformId(rs.getInt(3));
                account.setAccount(rs.getString(4));
                account.setPassword(rs.getString(5));
                account.setSkey(rs.getString(6));
                account.setRemark(rs.getString(7));
                account.setLoginStatus(rs.getInt(8));
                account.setCookie(rs.getString(9));
                account.setPageType(rs.getInt(10));
            }
        });
        return list;
    }

    private static List<AdAccountWithApp> getH5AppList(Integer companyId, Integer platform, Integer accountId) {
        StringBuilder sb = new StringBuilder(
                "select t1.company_id, t1.h5_id, t1.h5_account_id, t1.h5_account, t1.password, t1.login_status, t2.h5_app_id, t2.h5_app_name, t2.h5_app_code, t1.cookie from "
                        + Constant.Table.AC_H5_ACCOUNT
                        + " t1 inner join "
                        + Constant.Table.AC_H5_APP
                        + " t2 on t1.h5_account_id = t2.h5_account_id where t1.status = 0");
        if (null != companyId && companyId > 0) {
            sb.append(" and t1.company_id = " + companyId);
        }
        if (null != platform && platform > 0) {
            sb.append(" and t1.h5_id = " + platform);
        }
        if (null != accountId && accountId > 0) {
            sb.append(" and t1.h5_account_id = " + accountId);
        }
        String sql = sb.toString();
        final List<AdAccountWithApp> list = new ArrayList<AdAccountWithApp>();
        jdbcTemplate.query(sql, new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {
                AdAccountWithApp account = new AdAccountWithApp();
                account.setPlatformType(PlatformType.H5); // 设置为H5统计
                list.add(account);
                account.setCompanyId(rs.getInt(1));
                account.setPlatformId(rs.getInt(2));
                account.setId(rs.getInt(3));
                account.setAccount(rs.getString(4));
                account.setPassword(rs.getString(5));
                account.setLoginStatus(rs.getInt(6));
                account.setAppId(rs.getInt(7));
                account.setAppName(rs.getString(8));
                account.setAppCode(rs.getString(9));
                account.setCookie(rs.getString(10));
            }
        });
        return list;
    }

    private static List<AdAccountWithApp> getCPAppList(Integer companyId, Integer platform, Integer accountId) {
        StringBuilder sb = new StringBuilder(
                "select t1.company_id, t1.cp_id, t1.cp_account_id, t1.cp_account, t1.password, t1.login_status, t2.cp_app_id, t2.cp_app_name, t2.cp_app_code from "
                        + Constant.Table.AC_CP_ACCOUNT
                        + " t1 inner join "
                        + Constant.Table.AC_CP_APP
                        + " t2 on t1.cp_account_id = t2.cp_account_id where t1.status = 0");
        if (null != companyId && companyId > 0) {
            sb.append(" and t1.company_id = " + companyId);
        }
        if (null != platform && platform > 0) {
            sb.append(" and t1.cp_id = " + platform);
        }
        if (null != accountId && accountId > 0) {
            sb.append(" and t1.cp_account_id = " + accountId);
        }
        String sql = sb.toString();
        final List<AdAccountWithApp> list = new ArrayList<AdAccountWithApp>();
        jdbcTemplate.query(sql, new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {
                AdAccountWithApp account = new AdAccountWithApp();
                list.add(account);
                account.setCompanyId(rs.getInt(1));
                account.setPlatformId(rs.getInt(2));
                account.setId(rs.getInt(3));
                account.setAccount(rs.getString(4));
                account.setPassword(rs.getString(5));
                account.setLoginStatus(rs.getInt(6));
                account.setAppId(rs.getInt(7));
                account.setAppName(rs.getString(8));
                account.setAppCode(rs.getString(9));
            }
        });
        return list;
    }

    private static List<AdPkg> getPkgList(Integer companyId, String pkgName) {
        StringBuilder sb = new StringBuilder("select company_id, pkg_id, pkg_name, cp_app_id, cp_pkg_id from "
                + Table.AD_PKG + " where status = 0");
        if (null != companyId && companyId > 0) {
            sb.append(" and company_id = " + companyId + "");
        }
        if (null != pkgName) {
            sb.append(" and pkg_name = '" + pkgName + "'");
        }
        String sql = sb.toString();
        final List<AdPkg> list = new ArrayList<AdPkg>();
        jdbcTemplate.query(sql, new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {
                AdPkg pkg = new AdPkg();
                list.add(pkg);
                pkg.setCompanyId(rs.getInt(1));
                pkg.setId(rs.getInt(2));
                pkg.setPkgName(rs.getString(3));
                pkg.setCpAppId(rs.getString(4));
                pkg.setCpPkgId(rs.getString(5));
            }

        });
        return list;
    }

    private static List<AdPage> getPageList(Integer companyId, Integer appId, String pageName) {
        StringBuilder sb = new StringBuilder("select company_id, page_id, page_name, h5_app_id, h5_page_id from "
                + Table.AD_PAGE + " where status = 0");
        if (null != companyId && companyId > 0) {
            sb.append(" and company_id = " + companyId + "");
        }
        if (null != appId && appId > 0) {
            sb.append(" and h5_app_id = " + appId + "");
        }
        if (null != pageName) {
            sb.append(" and page_name = '" + pageName + "'");
        }
        String sql = sb.toString();
        final List<AdPage> list = new ArrayList<AdPage>();
        jdbcTemplate.query(sql, new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {
                AdPage page = new AdPage();
                list.add(page);
                page.setCompanyId(rs.getInt(1));
                page.setId(rs.getInt(2));
                page.setPageName(rs.getString(3));
                page.setH5AppId(rs.getString(4));
                page.setH5PageId(rs.getString(5));
            }
        });
        return list;
    }

    private static List<AdPlanRelation> getPlanRelationList(Integer companyId, Integer planId, Integer pkgId) {
        StringBuilder sb = new StringBuilder(
                "select company_id, product_id, plan_id, page_id, pkg_id, create_time, update_time, os_type from "
                        + Table.AD_PLAN_REALTION + " where 1 = 1");
        if (null != companyId && companyId > 0) {
            sb.append(" and company_id = " + companyId + "");
        }
        if (null != planId) {
            sb.append(" and plan_id = " + planId + "");
        }
        if (null != pkgId) {
            sb.append(" and pkg_id = " + pkgId + "");
        }
        final List<AdPlanRelation> list = new ArrayList<AdPlanRelation>();
        jdbcTemplate.query(sb.toString(), new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {
                AdPlanRelation relation = new AdPlanRelation();
                list.add(relation);
                relation.setCompanyId(rs.getInt(1));
                relation.setProductId(rs.getInt(2));
                relation.setId(rs.getInt(3));
                relation.setPageId(rs.getInt(4));
                relation.setPkgId(rs.getInt(5));
                relation.setCreateTime(rs.getDate(6));
                relation.setUpdateTime(rs.getDate(7));
                relation.setOsType(rs.getInt(8));
            }
        });
        return list;
    }

    private static List<AdMediumDiscount> getMediumDiscountList(Integer companyId, Integer mediumAccountId) {
        StringBuilder sb = new StringBuilder(
                "select t1.company_id, t2.medium_account_id, t2.discount_rate, t2.create_time, t2.update_time from "
                        + Table.AC_MEDIUM_ACCOUNT + " t1 inner join " + Table.AC_MEDIUM_DISCOUNT
                        + " t2 on t1.medium_account_id = t2.medium_account_id where 1 = 1");
        if (null != companyId && companyId > 0) {
            sb.append(" and t1.company_id = " + companyId + "");
        }
        if (null != mediumAccountId && mediumAccountId > 0) {
            sb.append(" and t2.medium_account_id = " + mediumAccountId + "");
        }
        final List<AdMediumDiscount> list = new ArrayList<AdMediumDiscount>();
        jdbcTemplate.query(sb.toString(), new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {
                AdMediumDiscount discount = new AdMediumDiscount();
                list.add(discount);
                discount.setCompanyId(rs.getInt(1));
                discount.setId(rs.getInt(2));
                discount.setDiscountRate(rs.getBigDecimal(3));
                discount.setCreateTime(rs.getDate(4));
                discount.setUpdateTime(rs.getDate(5));
            }
        });
        return list;
    }

    public static int getPlanIdForIQiYi(Integer companyId, Integer mediumId, Integer accountId, String planName,
                                        String groupName) {
        String sql = "select plan_id from "
                + Table.AD_PLAN
                + " where company_id = ? and medium_id = ? and medium_account_id = ? and plan_name = ? and group_name = ? limit 1";
        return jdbcTemplate.queryForInt(sql, companyId, mediumId, accountId, planName, groupName);
    }

    public static int getPlanIdForSinaWeiboAd(Integer companyId, Integer mediumId, Integer accountId, String mPlanId) {
        String sql = "select plan_id from " + Table.AD_PLAN
                + " where company_id = ? and medium_id = ? and medium_account_id = ? and m_plan_id = ? limit 1";
        return jdbcTemplate.queryForInt(sql, companyId, mediumId, accountId, mPlanId);
    }

    /**
     * 查询推广产品id（广点通）
     *
     * @param campaignId
     * @return
     */
    public static int getProductIdForGDT(String campaignId) {
        String sql = "select product_id from ad_campaign " +
                "where campaign_id=? limit 1";
        return jdbcTemplate.queryForInt(sql, campaignId);
    }
}
