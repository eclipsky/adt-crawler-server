package com.dataeye.ad.scheduler.job.sinafensitong.service;

import com.dataeye.ad.common.CookieException;
import com.dataeye.ad.common.ServerException;

import java.io.IOException;
import java.util.List;

/**
 * Created by huangzehai on 2017/3/28.
 */
@Deprecated
public interface PlanService {

    /**
     * 获取计划ID列表
     *
     * @param cookie
     * @param groupId
     * @return
     */
    List<Integer> getPlanIds(String cookie, Integer groupId) throws IOException, CookieException, ServerException;
}
