package com.dataeye.ad;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.quartz.Job;
import org.quartz.SchedulerException;
import org.springframework.stereotype.Service;

import com.dataeye.ad.cluster.ClusterNode;
import com.dataeye.ad.kafka.ConsumerAgent;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.scheduler.QuartzManager;
import com.dataeye.ad.util.ConfigFromDB;
import com.dataeye.ad.util.ServerUtil;
import com.xunlei.netty.httpserver.Bootstrap;
import com.xunlei.netty.httpserver.spring.BeanUtil;
import com.xunlei.netty.httpserver.spring.SpringBootstrap;
import com.xunlei.spring.Config;

@Service
public class Launch {

    @Config
    private int jobSwitch;

    @Config
    private int send2Kafka;

    @Config
    private int asConsumer;

    @Config
    private int clusterMode;

    public void init() {
        try {
            ConfigFromDB.init();
            if (jobSwitch > 0) {
                // 加载定时任务
                loadQuartzJobs();
                // 启动定时任务
                QuartzManager.start();
            }
            /**
             * <pre/>
             * 爬虫数据解析后，会有两种数据处理方式
             * 1.send2Kafka > 0，表示开启Kafka开关，爬虫解析后的消息发送到Kafka Topic
             * 2.send2Kafka = 0，表示数据直接入库，不经过Kafka
             * 一般情况下，配置send2Kafka > 0，将爬取与入库分离，可以提高爬虫效率，缓冲爬取与入库速度的不一致，Kafka保留一段时间的消息方便后期补跑
             * 在kafka集群升级维护或系统故障的时候，设置send2Kafka = 0，爬取数据直接入库，保证可用性
             */
            if (send2Kafka > 0) {
                DataProcessor.SEND_KAFKA = true;
            }
            /**
             * 当前爬虫server是否启动Kafka消费者，如果作为独立的Kafka消费者Server，需要把jobSwtich设置为0
             */
            if (asConsumer > 0) {
                ConsumerAgent.start();
            }
            /**
             * 作为集群模式，多个节点通过取模的方式处理对应的媒体帐号
             */
            if (clusterMode > 0) {
                ClusterNode.register();
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public static void main(String[] args) throws Exception {
        Bootstrap.main(args, new Runnable() {
            @Override
            public void run() {
                Launch launch = BeanUtil.getTypedBean(SpringBootstrap.getContext(), "launch");
                launch.init();
            }
        }, "classpath:applicationContext.xml");
    }

    @SuppressWarnings("unchecked")
    private static void loadQuartzJobs() throws ClassNotFoundException, SchedulerException {
        // 读取任务配置
        Properties props = ServerUtil.getConfig("jobconfig.properties");
        Map<Class<? extends Job>, String> jobMap = new HashMap<Class<? extends Job>, String>();
        for (String className : props.stringPropertyNames()) {
            String cronExpr = props.getProperty(className).trim();
            Class<? extends Job> clazz = (Class<? extends Job>) Class.forName(className.trim());
            jobMap.put(clazz, cronExpr);
        }
        // 加入定时调度
        QuartzManager.addCrawlerJobs(jobMap);
    }

}
