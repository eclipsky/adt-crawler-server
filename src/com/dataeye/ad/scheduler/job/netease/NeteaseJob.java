package com.dataeye.ad.scheduler.job.netease;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dataeye.ad.scheduler.job.CrawlerFilter;

import org.slf4j.Logger;

import com.dataeye.ad.common.CookieException;
import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.common.ServerException;
import com.dataeye.ad.constant.Constant.CrawlerPlatform;
import com.dataeye.ad.constant.Constant.PlanSwitch;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DBPipeline;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.scheduler.job.netease.model.ExtLimit;
import com.dataeye.ad.scheduler.job.netease.model.NeteaseNewRequest;
import com.dataeye.ad.util.CookieUtil;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * 网易易效新平台请求参数
 *
 * @author luzhuyou 2017/04/14
 */
public class NeteaseJob extends CrawlerBaseJob {

    private final static CrawlerPlatform platform = CrawlerPlatform.NETEASE;

    private final static Logger logger = ServerUtil.getLogger(platform);

    private final static String accountUrl = "http://p4p.163.com/homepage/financeInfo?";

    private final static String planUrl = "http://p4p.163.com/scheduling/schedulinglist?";

    private final static String planGroupUrl = "http://p4p.163.com/adplan/adlist?";

    public NeteaseJob() {
        this(DateUtil.unixTimestamp000());
    }

    public NeteaseJob(long time) {
        super(time, platform);
    }

    public NeteaseJob(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }

    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = CookieUtil.getCookieByAccount(account);
        // 分割cookie
        String[] cookieArray = cookie.split(";");
        cookie = cookieArray[0];
        // 请求
        String content = HttpUtil.getContentByPost(accountUrl + cookieArray[1], getHeaderMap(cookie), "");
        logger.info(content);
        // 解析
        AdAccountStat accountStat = parseAccount(content, account, time);
        DataProcessor.processAccountDay(accountStat);
    }

    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        String cookie = CookieUtil.getCookieByAccount(account);
        // 分割cookie
        String[] cookieArray = cookie.split(";");
        cookie = cookieArray[0];
        CookieUtil.isCookieValid(cookie);
        // 获取计划总数
        int pageId = 1;
        int pageSize = 50; // 正式环境改为50
        int start = 0;
        PageModel pageModel = new PageModel(pageId, pageSize);
        // 请求
        String content = HttpUtil.getContentByPost(planUrl + cookieArray[1], getHeaderMap(cookie),
                getFormDataSchedulingList(start, pageSize, reportDate, reportDate));
        logger.info(content);
        // 解析
        List<AdPlanStat> poList = parsePlan(content, account, time, pageModel);
        // 入库
        if (!poList.isEmpty()) {
            DataProcessor.processPlanDay(poList);
        }
        pageId = 2;
        pageModel.setPageId(pageId);

        while (pageId <= pageModel.getTotalPages()) {
            start = pageId * pageSize - pageSize;
            // 请求
            content = HttpUtil.getContentByPost(planUrl + cookieArray[1], getHeaderMap(cookie),
                    getFormDataSchedulingList(start, pageSize, reportDate, reportDate));
            logger.info(content);
            // 解析
            poList = parsePlan(content, account, time, pageModel);
            // 入库
            if (!poList.isEmpty()) {
                DataProcessor.processPlanDay(poList);
            }
            pageId++;
        }
    }

    @Override
    public void planGroupSpider(long time, AdMediumAccount account) throws Exception {
        String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        String cookie = CookieUtil.getCookieByAccount(account);
        // 分割cookie
        String[] cookieArray = cookie.split(";");
        cookie = cookieArray[0];
        CookieUtil.isCookieValid(cookie);
        // 获取计划总数
        int pageId = 1;
        int pageSize = 50; // 正式环境改为50
        int start = 0;
        PageModel pageModel = new PageModel(pageId, pageSize);
        // 请求
        String content = HttpUtil.getContentByPost(planGroupUrl + cookieArray[1], getHeaderMap(cookie),
                getFormDataSchedulingList(start, pageSize, reportDate, reportDate));
        logger.info(content);
        // 解析
        List<AdPlanStat> poList = parsePlanGroup(content, account, time, pageModel);
        // 入库
        if (!poList.isEmpty()) {
            DataProcessor.processPlanDay(poList);
        }
        pageId = 2;
        pageModel.setPageId(pageId);

        while (pageId <= pageModel.getTotalPages()) {
            start = pageId * pageSize - pageSize;
            // 请求
            content = HttpUtil.getContentByPost(planGroupUrl + cookieArray[1], getHeaderMap(cookie),
                    getFormDataSchedulingList(start, pageSize, reportDate, reportDate));
            logger.info(content);
            // 解析
            poList = parsePlanGroup(content, account, time, pageModel);
            // 入库
            if (!poList.isEmpty()) {
                DataProcessor.processPlanDay(poList);
            }
            pageId++;
        }
    }

    /**
     * <pre>
     * 解析响应内容，封装成标准的PO对象
     * @author luzhuyou
     * @date 2017/04/14
     * @param content
     * @param account
     * @param time
     * @param pageModel
     * @throws ServerException, CookieException
     */
    private List<AdPlanStat> parsePlan(String content, AdMediumAccount account, long time, PageModel pageModel)
            throws ServerException, CookieException {
        List<AdPlanStat> poList = new ArrayList<AdPlanStat>();
        // 返回结果集中类型不统一，同一个字段可能为字符串也可能为对象还可能为数字
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        JsonElement result = resultJson.get("result");
        if (null != result && result.getAsString().equals("error")) {
            ExceptionHandler.throwException(ExceptionEnum.SERVER_ERROR, content);
        }
        JsonElement code = resultJson.get("code");
        int status = code == null ? 0 : code.getAsInt();
        if (status != 0) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, content);
        }
        // 获取总记录数信息
        int totalNum = resultJson.get("total").getAsInt();
        pageModel.setTotalNum(totalNum);
        pageModel.setTotalPages(Double.valueOf(Math.ceil((double) totalNum / pageModel.getPageSize())).intValue());
        if (totalNum > 0) {
            // 获取内容
            JsonArray list = resultJson.get("invdata").getAsJsonArray();
            if (list != null) {
                for (JsonElement item : list) {
                    AdPlanStat reportPO = new AdPlanStat(time, account);
                    poList.add(reportPO);
                    JsonObject order = item.getAsJsonObject();
                    // 点击数
                    int clickNum = StringUtil.getJsonIntValue(order, "click_count");
                    // 消耗
                    double cost = StringUtil.getJsonDoubleValue(order, "cost");
                    // 曝光
                    int showNum = StringUtil.getJsonIntValue(order, "show_count");
                    // 媒体计划ID
                    String mPlanId = order.get("id").getAsInt() + "";
                    // 媒体计划名
                    // String planName = order.get("adPlanName").getAsString();
                    String planName = order.get("name").getAsString();
                    // 媒体计划分组ID
                    String groupId = order.get("ad_plan_id").getAsInt() + "";
                    // 媒体计划分组名
                    String groupName = order.get("adPlanName").getAsString();
                    // 媒体计划出价
                    int planBid = (int) (StringUtil.getJsonFloatValue(order, "pay") * 100);// 已经是分为单位
                    // 媒体计划出价策略
                    String planBidStrategy = StringUtil.getJsonIntValue(order, "cast_way") + ""; // 这里字段存疑，有待商榷
                    // 媒体计划状态
                    String planStatus = StringUtil.getJsonStrValue(order, "statusStr") + "";
                    // 媒体计划开关
                    int planSwitchRaw = StringUtil.getJsonIntValue(order, "turn");
                    Integer planSwitch = null;
                    if (planSwitchRaw == 1) {
                        planSwitch = PlanSwitch.ON;
                    } else if (planSwitchRaw == 10) {
                        planSwitch = PlanSwitch.OFF;
                    }
                    reportPO.setClickNum(clickNum);
                    reportPO.setCost(new BigDecimal(cost).multiply(new BigDecimal(100))
                            .setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
                    reportPO.setShowNum(showNum);
                    reportPO.setmPlanId(mPlanId);
                    reportPO.setPlanName(planName);
                    reportPO.setBid(planBid);
                    reportPO.setPlanBidStrategy(planBidStrategy);
                    reportPO.setGroupId(groupId);
                    reportPO.setGroupName(groupName);
                    reportPO.setPlanStatus(planStatus);
                    reportPO.setPlanSwitch(planSwitch);
                }
            }
        }
        return poList;
    }

    private List<AdPlanStat> parsePlanGroup(String content, AdMediumAccount account, long time, PageModel pageModel)
            throws ServerException, CookieException {
        List<AdPlanStat> poList = new ArrayList<AdPlanStat>();
        // 返回结果集中类型不统一，同一个字段可能为字符串也可能为对象还可能为数字
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        JsonElement result = resultJson.get("result");
        if (null != result && result.getAsString().equals("error")) {
            ExceptionHandler.throwException(ExceptionEnum.SERVER_ERROR, content);
        }
        JsonElement code = resultJson.get("code");
        int status = code == null ? 0 : code.getAsInt();
        if (status != 0) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, content);
        }
        // 获取总记录数信息
        int totalNum = resultJson.get("total").getAsInt();
        pageModel.setTotalNum(totalNum);
        pageModel.setTotalPages(Double.valueOf(Math.ceil((double) totalNum / pageModel.getPageSize())).intValue());
        if (totalNum > 0) {
            // 获取内容
            JsonArray list = resultJson.get("invdata").getAsJsonArray();
            if (list != null) {
                for (JsonElement item : list) {
                    AdPlanStat reportPO = new AdPlanStat(time, account);
                    poList.add(reportPO);
                    JsonObject order = item.getAsJsonObject();
                    // 点击数
                    int clickNum = StringUtil.getJsonIntValue(order, "click_count");
                    // 消耗
                    double cost = StringUtil.getJsonDoubleValue(order, "pay");
                    // 曝光
                    int showNum = StringUtil.getJsonIntValue(order, "show_count");
                    // 媒体计划ID
                    String mPlanId = order.get("id").getAsInt() + "";
                    // 媒体计划名
                    String planName = order.get("name").getAsString();
                    // 媒体计划状态
                    String planStatus = StringUtil.getJsonStrValue(order, "statusStr") + "";
                    // 媒体计划开关
                    int planSwitchRaw = StringUtil.getJsonIntValue(order, "turn");
                    Integer planSwitch = null;
                    if (planSwitchRaw == 1) {
                        planSwitch = PlanSwitch.ON;
                    } else if (planSwitchRaw == 10) {
                        planSwitch = PlanSwitch.OFF;
                    }
                    reportPO.setClickNum(clickNum);
                    reportPO.setCost(new BigDecimal(cost).multiply(new BigDecimal(100))
                            .setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
                    reportPO.setShowNum(showNum);
                    reportPO.setmPlanId(mPlanId);
                    reportPO.setPlanName(planName);
                    reportPO.setPlanStatus(planStatus);
                    reportPO.setPlanSwitch(planSwitch);
                }
            }
        }
        return poList;
    }

    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time) throws Exception {
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        JsonElement code = resultJson.get("code");
        int status = code == null ? 200000 : code.getAsInt();
        if (status != 200000) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, content);
        }

        JsonObject jsonObject = resultJson.get("info").getAsJsonObject();
        // 余额
        String balanceStr = jsonObject.get("balance").getAsString();
        int balance = StringUtil.convertBigDecimal(balanceStr, 100, 0).intValue();
        // 消耗
        String costTodayStr = jsonObject.get("todayCost").getAsString();
        int costToday = StringUtil.convertBigDecimal(costTodayStr, 100, 0).intValue();

        AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                account.getId());
        accountStat.setBalance(balance);
        accountStat.setCostToday(costToday);
        return accountStat;
    }

    public String getFormDataSchedulingList(int start, int limit, String beginDate, String endDate) {
        ExtLimit extLimit = new ExtLimit();
        extLimit.setDir("ASC");
        extLimit.setSort("create_time");
        extLimit.setStart(start);
        extLimit.setLimit(limit);
        NeteaseNewRequest neteaseNewRequest = new NeteaseNewRequest();
        neteaseNewRequest.setName("");
        neteaseNewRequest.setAd_plan_id("");
        neteaseNewRequest.setDateType(5); //自定义时间区间
        neteaseNewRequest.setStart_date(beginDate);
        neteaseNewRequest.setEnd_date(endDate);
        neteaseNewRequest.setExtLimit(extLimit);
        neteaseNewRequest.setDataStatus(1);// 所有计划
        Gson gson = new Gson();
        String params = gson.toJson(neteaseNewRequest);
        return params;
    }

    private Map<String, String> getHeaderMap(String cookie) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Cookie", cookie);
        headerMap.put("Content-Type", "application/json;charset=UTF-8");
        headerMap.put("Connection", "keep-alive");
        headerMap.put("Host", "p4p.163.com");
        headerMap
                .put("User-Agent",
                        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36");
        headerMap.put("X-Requested-With", "XMLHttpRequest");
        headerMap.put("Origin", "http://p4p.163.com");
        headerMap.put("Referer", "http://p4p.163.com/customer/app.html");
        headerMap.put("Accept", "application/json, text/plain, */*");
        headerMap.put("Accept-Language", "zh-CN,zh;q=0.8");
        headerMap.put("Accept-Encoding", "gzip, deflate");
        return headerMap;
    }

}
