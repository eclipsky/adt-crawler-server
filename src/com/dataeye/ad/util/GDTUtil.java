package com.dataeye.ad.util;

import static com.dataeye.ad.constant.Constant.CrawlerPlatform.TENCENT_GDT;

import java.util.HashSet;
import java.util.Set;

import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;

public class GDTUtil {

    public static Set<Integer> activeUserIds = new HashSet<Integer>();
    public static Set<Integer> activeCompanyIds = new HashSet<Integer>();
    public static Set<Integer> downloadUserIds = new HashSet<Integer>();
    public static Set<Integer> downloadCompanyIds = new HashSet<Integer>();

    public static void initConfig() {
        parse(activeUserIds, ConfigFromDB.getConfig("gdt_active_user_ids"));
        parse(activeCompanyIds, ConfigFromDB.getConfig("gdt_active_company_ids"));
        parse(downloadUserIds, ConfigFromDB.getConfig("gdt_download_user_ids"));
        parse(downloadCompanyIds, ConfigFromDB.getConfig("gdt_download_company_ids"));
    }

    private static void parse(Set<Integer> set, String value) {
        set.clear();
        if (!StringUtil.isEmpty(value)) {
            for (String id : value.trim().split(",")) {
                set.add(Integer.parseInt(id));
            }
        }
    }

    public static boolean shouldHandleDownload(AdMediumAccount po) {
        return controlCrawler(downloadCompanyIds, downloadUserIds, po);
    }

    public static boolean shouldHandleActive(AdMediumAccount po) {
        return controlCrawler(activeCompanyIds, activeUserIds, po);
    }

    public static boolean shouldHandleDownload(AdPlanStat po) {
        return controlPipeline(downloadCompanyIds, downloadUserIds, po);
    }

    public static boolean shouldHandleActive(AdPlanStat po) {
        return controlPipeline(activeCompanyIds, activeUserIds, po);
    }

    private static boolean controlCrawler(Set<Integer> companyIds, Set<Integer> userIds, AdMediumAccount po) {

        if (po.getPlatformId() == TENCENT_GDT.getId()) {
            if ((!companyIds.isEmpty() && companyIds.contains(po.getCompanyId()))
                    || (!userIds.isEmpty() && userIds.contains(po.getId()))) {
                return true;
            }
        }
        return false;
    }

    private static boolean controlPipeline(Set<Integer> companyIds, Set<Integer> userIds, AdPlanStat po) {
        if (po.getMediumId() == TENCENT_GDT.getId()) {
            if ((!companyIds.isEmpty() && companyIds.contains(po.getCompanyId()))
                    || (!userIds.isEmpty() && userIds.contains(po.getMediumAccountId()))) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        // reloadConfig();
        AdMediumAccount account = new AdMediumAccount();
        account.setPlatformId(13);
        account.setCompanyId(102);
        account.setId(177);
        System.out.println(shouldHandleDownload(account));
        System.out.println(shouldHandleActive(account));

        AdPlanStat plan = new AdPlanStat(0, account);
        System.out.println(shouldHandleDownload(plan));
        System.out.println(shouldHandleActive(plan));
    }
}
