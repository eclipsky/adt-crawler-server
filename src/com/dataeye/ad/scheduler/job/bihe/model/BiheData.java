package com.dataeye.ad.scheduler.job.bihe.model;

public class BiheData {

	private String ad_master_id;

	private String ad_campaign_id;

	private String ad_order_id;

	private String bid;
	private String view;
	private String click;
	private String reach;
	private String download;
	private String install;
	private String active;
	private String register;
	private String spend;
	private String spendc;
	private String other_monitor1;
	private String other_monitor2;
	private String other_monitor3;
	private String other_monitor4;
	private String other_monitor5;
	private String other_monitor6;
	private String other_monitor7;

	private String cpm;
	private String cpc;
	private String click_rate;
	private String ca_cate;
	private String cr_cate;
	private String ar_cate;
	private String as_cate;
	private String rs_cate;
	private String get_rate;
	private String va_cate;
	private String vr_cate;
	private String ad_order_name;
	private String id;

	private String name;

	private String has_child;

	public String getAd_master_id() {
		return ad_master_id;
	}

	public void setAd_master_id(String ad_master_id) {
		this.ad_master_id = ad_master_id;
	}

	public String getAd_campaign_id() {
		return ad_campaign_id;
	}

	public void setAd_campaign_id(String ad_campaign_id) {
		this.ad_campaign_id = ad_campaign_id;
	}

	public String getAd_order_id() {
		return ad_order_id;
	}

	public void setAd_order_id(String ad_order_id) {
		this.ad_order_id = ad_order_id;
	}

	public String getBid() {
		return bid;
	}

	public void setBid(String bid) {
		this.bid = bid;
	}

	public String getView() {
		return view;
	}

	public void setView(String view) {
		this.view = view;
	}

	public String getClick() {
		return click;
	}

	public void setClick(String click) {
		this.click = click;
	}

	public String getReach() {
		return reach;
	}

	public void setReach(String reach) {
		this.reach = reach;
	}

	public String getDownload() {
		return download;
	}

	public void setDownload(String download) {
		this.download = download;
	}

	public String getInstall() {
		return install;
	}

	public void setInstall(String install) {
		this.install = install;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getRegister() {
		return register;
	}

	public void setRegister(String register) {
		this.register = register;
	}

	public String getSpend() {
		return spend;
	}

	public void setSpend(String spend) {
		this.spend = spend;
	}

	public String getSpendc() {
		return spendc;
	}

	public void setSpendc(String spendc) {
		this.spendc = spendc;
	}

	public String getOther_monitor1() {
		return other_monitor1;
	}

	public void setOther_monitor1(String other_monitor1) {
		this.other_monitor1 = other_monitor1;
	}

	public String getOther_monitor2() {
		return other_monitor2;
	}

	public void setOther_monitor2(String other_monitor2) {
		this.other_monitor2 = other_monitor2;
	}

	public String getOther_monitor3() {
		return other_monitor3;
	}

	public void setOther_monitor3(String other_monitor3) {
		this.other_monitor3 = other_monitor3;
	}

	public String getOther_monitor4() {
		return other_monitor4;
	}

	public void setOther_monitor4(String other_monitor4) {
		this.other_monitor4 = other_monitor4;
	}

	public String getOther_monitor5() {
		return other_monitor5;
	}

	public void setOther_monitor5(String other_monitor5) {
		this.other_monitor5 = other_monitor5;
	}

	public String getOther_monitor6() {
		return other_monitor6;
	}

	public void setOther_monitor6(String other_monitor6) {
		this.other_monitor6 = other_monitor6;
	}

	public String getOther_monitor7() {
		return other_monitor7;
	}

	public void setOther_monitor7(String other_monitor7) {
		this.other_monitor7 = other_monitor7;
	}

	public String getCpm() {
		return cpm;
	}

	public void setCpm(String cpm) {
		this.cpm = cpm;
	}

	public String getCpc() {
		return cpc;
	}

	public void setCpc(String cpc) {
		this.cpc = cpc;
	}

	public String getClick_rate() {
		return click_rate;
	}

	public void setClick_rate(String click_rate) {
		this.click_rate = click_rate;
	}

	public String getCa_cate() {
		return ca_cate;
	}

	public void setCa_cate(String ca_cate) {
		this.ca_cate = ca_cate;
	}

	public String getCr_cate() {
		return cr_cate;
	}

	public void setCr_cate(String cr_cate) {
		this.cr_cate = cr_cate;
	}

	public String getAr_cate() {
		return ar_cate;
	}

	public void setAr_cate(String ar_cate) {
		this.ar_cate = ar_cate;
	}

	public String getAs_cate() {
		return as_cate;
	}

	public void setAs_cate(String as_cate) {
		this.as_cate = as_cate;
	}

	public String getRs_cate() {
		return rs_cate;
	}

	public void setRs_cate(String rs_cate) {
		this.rs_cate = rs_cate;
	}

	public String getGet_rate() {
		return get_rate;
	}

	public void setGet_rate(String get_rate) {
		this.get_rate = get_rate;
	}

	public String getVa_cate() {
		return va_cate;
	}

	public void setVa_cate(String va_cate) {
		this.va_cate = va_cate;
	}

	public String getVr_cate() {
		return vr_cate;
	}

	public void setVr_cate(String vr_cate) {
		this.vr_cate = vr_cate;
	}

	public String getAd_order_name() {
		return ad_order_name;
	}

	public void setAd_order_name(String ad_order_name) {
		this.ad_order_name = ad_order_name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHas_child() {
		return has_child;
	}

	public void setHas_child(String has_child) {
		this.has_child = has_child;
	}

}
