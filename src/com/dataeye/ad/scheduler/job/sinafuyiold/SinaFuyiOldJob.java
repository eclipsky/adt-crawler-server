package com.dataeye.ad.scheduler.job.sinafuyiold;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.scheduler.job.CrawlerFilter;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;

import com.dataeye.ad.common.CookieException;
import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.common.ServerException;
import com.dataeye.ad.constant.Constant.CrawlerPlatform;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * 新浪扶翼爬取任务.
 */
public class SinaFuyiOldJob extends CrawlerBaseJob {

    private final static CrawlerPlatform platform = CrawlerPlatform.SINA_FUYIOLD;

    private final static Logger logger = ServerUtil.getLogger(platform);

    private static final String pastPlanUrl = "http://client.pfp.sina.com.cn/gina-platform-client/report/advday.ng";

    private static final String planUrl = "http://amp.ad.sina.com.cn/ea/client/advert/advert!getAdvertPageByParam.action";

    private final static String accountUrl = "http://amp.ad.sina.com.cn/ea/client/proxy!getBinder.action";

    public SinaFuyiOldJob() {
        this(DateUtil.unixTimestamp000());
    }

    public SinaFuyiOldJob(long time) {
        super(time, platform);
    }

    public SinaFuyiOldJob(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }


    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        String cookie = account.getCookie();
        // 获取计划总数
        int pageId = 1;
        int pageSize = 50; // 正式环境改为50
        PageModel pageModel = new PageModel(pageId, pageSize);
        String content = "";
        List<AdPlanStat> poList = new ArrayList<AdPlanStat>();
        if (DateUtil.unixTimestamp000() == time) {
            //实时请求
            content = HttpUtil.getContentByPost(planUrl, getHeaderMap(cookie, true),
                    getFormDataAdList(time, pageId, pageSize, reportDate, reportDate));
            logger.info(content);
            //解析
            poList = parsePlan(content, account, time, pageModel);
        } else {
            // 历史请求
            content = HttpUtil.getContentByPost(pastPlanUrl, getHeaderMap(cookie, false),
                    getFormDataAdList(time, pageId, pageSize, reportDate, reportDate));
            logger.info(content);
            // 解析
            poList = parsePlanPast(content, account, time, pageModel);
        }
        int totalNum = pageModel.getTotalNum();
        // 入库
        if (!poList.isEmpty()) {
            DataProcessor.processPlanDay(poList);
        }
        totalNum -= pageSize;
        while (totalNum > 0) {
            pageId++;
            if (DateUtil.unixTimestamp000() == time) {
                //实时请求
                content = HttpUtil.getContentByPost(planUrl, getHeaderMap(cookie, true),
                        getFormDataAdList(time, pageId, pageSize, reportDate, reportDate));
                logger.info(content);
                //解析
                poList = parsePlan(content, account, time, pageModel);
            } else {
                // 历史请求
                content = HttpUtil.getContentByPost(pastPlanUrl, getHeaderMap(cookie, false),
                        getFormDataAdList(time, pageId, pageSize, reportDate, reportDate));
                logger.info(content);
                // 解析
                poList = parsePlanPast(content, account, time, pageModel);
            }
            // 入库
            if (!poList.isEmpty()) {
                DataProcessor.processPlanDay(poList);
            }
            totalNum -= pageSize;
        }
    }

    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        // 请求
        String content = HttpUtil.getContentByGet(accountUrl, getHeaderMap(cookie, true));
        logger.info(content);
        // 解析
        AdAccountStat accountStat = parseAccount(content, account, time);
        // 入库
        DataProcessor.processAccountDay(accountStat);
    }

    /**
     * 爬取投放计划数据解析（实时数据）
     *
     * @param response
     * @param account
     * @param time
     * @param pageModel
     * @return
     * @throws ServerException
     * @throws CookieException
     */
    private List<AdPlanStat> parsePlan(String response, AdMediumAccount account, long time, PageModel pageModel)
            throws ServerException, CookieException {
        List<AdPlanStat> poList = new ArrayList<AdPlanStat>();
        JsonObject resultJson = StringUtil.jsonParser.parse(response).getAsJsonObject();
        int status = resultJson.get("status").getAsInt();
        if (status != 0) {
            if (status == 127) {
                // 127:没有登录或登录过期
                ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, response);
            } else {
                ExceptionHandler.throwException(ExceptionEnum.REQUEST_ERROR, response);
            }
        } else {
            // 获取分页信息
            JsonObject conf = resultJson.get("data").getAsJsonObject().get("page").getAsJsonObject();
            int totalNum = conf.get("totalCount").getAsInt();
            pageModel.setTotalNum(totalNum);
            JsonArray list = conf.get("result").getAsJsonArray();
            if (list != null) {
                for (JsonElement item : list) {
                    AdPlanStat reportPO = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(),
                            account.getId());
                    poList.add(reportPO);
                    JsonObject order = item.getAsJsonObject();
                    // 点击数
                    int clickNum = StringUtil.getJsonIntValue(order, "ck");
                    // 消耗
                    double cost = StringUtil.getJsonDoubleValue(order, "oftenPay");
                    // 曝光
                    int showNum = StringUtil.getJsonIntValue(order, "impress");
                    // 计划名称
                    String planName = order.get("advertName").getAsString();
                    // 计划ID
                    String mPlanId = order.get("advertId").getAsString();
                    // 分组名称
                    String groupName = order.get("advertGroupName").getAsString();
                    // 计划状态
                    int planStatus = StringUtil.getJsonIntValue(order, "status");
                    // 计划开关
                    Integer planSwitch = null;
                    if (planStatus == 0) {
                        planSwitch = Constant.PlanSwitch.ON;
                    } else if (planStatus == 1) {
                        planSwitch = Constant.PlanSwitch.OFF;
                    }
                    // 出价
                    double bidPrice = StringUtil.getJsonDoubleValue(order, "bidPrice");
                    reportPO.setClickNum(clickNum);
                    reportPO.setCost(new BigDecimal(cost).multiply(new BigDecimal(100))
                            .setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
                    reportPO.setShowNum(showNum);
                    reportPO.setPlanName(planName);
                    reportPO.setmPlanId(mPlanId);
                    reportPO.setGroupName(groupName);
                    reportPO.setPlanStatus(planStatus + "");
                    reportPO.setBid(new BigDecimal(bidPrice).multiply(new BigDecimal(100))
                            .setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
                    reportPO.setPlanSwitch(planSwitch);
                }
            }
        }
        return poList;
    }


    /**
     * <pre>
     * 解析响应内容，封装成标准的PO对象（历史数据）
     * @author sam.xie
     * @date Feb 15, 2017 3:12:29 PM
     * @param time
     * @throws ServerException
     */
    private List<AdPlanStat> parsePlanPast(String response, AdMediumAccount account, long time, PageModel pageModel)
            throws ServerException, CookieException {
        List<AdPlanStat> poList = new ArrayList<>();
        // 返回结果集中类型不统一，同一个字段可能为字符串也可能为对象还可能为数字
        JsonObject resultJson = StringUtil.jsonParser.parse(response).getAsJsonObject();
        int status = resultJson.get("status").getAsInt();
        if (status != 0) {
            if (status == 127) {
                // 127:没有登录或登录过期
                ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, response);
            } else {
                ExceptionHandler.throwException(ExceptionEnum.REQUEST_ERROR, response);
            }
        } else {
            // 获取分页信息
            JsonObject conf = resultJson.get("data").getAsJsonObject();
            int totalNum = conf.get("totalCount").getAsInt();
            pageModel.setTotalNum(totalNum);
            // 获取分页内容
            JsonArray list = resultJson.get("data").getAsJsonObject().get("list").getAsJsonArray();
            if (list != null) {
                for (JsonElement item : list) {
                    AdPlanStat reportPO = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(),
                            account.getId());
                    poList.add(reportPO);
                    JsonObject order = item.getAsJsonObject();
                    // 点击数
                    int clickNum = StringUtil.getJsonIntValue(order, "click");
                    // 消耗
                    double cost = StringUtil.getJsonDoubleValue(order, "pay");
                    // 曝光
                    int showNum = StringUtil.getJsonIntValue(order, "impression");
                    // 计划名称
                    String planName = order.get("advertname").getAsString();
                    // 计划ID
                    String mPlanId = order.get("advertid").getAsString();
                    reportPO.setClickNum(clickNum);
                    reportPO.setCost(new BigDecimal(cost).multiply(new BigDecimal(100))
                            .setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
                    reportPO.setShowNum(showNum);
                    reportPO.setPlanName(planName);
                    reportPO.setmPlanId(mPlanId);
                }
            }
        }
        return poList;
    }

    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time) throws Exception {
        if (content.contains("please try again later")) {
            return null;
        }
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        int balance = StringUtil.convertBigDecimal(
                resultJson.get("data").getAsJsonObject().get("clientInfofy").getAsString(), 100, 0).intValue();
        AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                account.getId());
        accountStat.setBalance(balance);
        return accountStat;
    }

    public List<NameValuePair> getFormDataAdList(long time, int pageId, int pageSize, String startDate, String endDate) {
        List<NameValuePair> pairs = new ArrayList<>();
        if (time == DateUtil.unixTimestamp000()) {
            //当天实时
            pairs.add(new BasicNameValuePair("page.pageSize", pageSize + ""));
            pairs.add(new BasicNameValuePair("page.pageNo", pageId + ""));
        } else {
            //历史数据
            SinaFuyiRequest sinaRequest = new SinaFuyiRequest();
            sinaRequest.setStartDate(startDate);
            sinaRequest.setEndDate(endDate);
            sinaRequest.setPageNo(String.valueOf(pageId));
            sinaRequest.setPageSize(String.valueOf(pageSize));
            Gson gson = new Gson();
            String json = gson.toJson(sinaRequest);
            pairs.add(new BasicNameValuePair("data", json));
        }
        return pairs;
    }

    private Map<String, String> getHeaderMap(String cookie, boolean isAccount) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Cookie", cookie);
        if (isAccount) {
            headerMap.put("Host", "amp.ad.sina.com.cn");
        } else {
            headerMap.put("Host", "client.pfp.sina.com.cn");
            headerMap.put("Referer", "http://client.pfp.sina.com.cn/");
        }
        headerMap
                .put("User-Agent",
                        "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36");
        headerMap.put("X-Requested-With", "XMLHttpRequest");
        headerMap.put("Accept", "application/json, text/javascript, */*; q=0.01");
        headerMap.put("Accept-Language", "zh-CN,zh;q=0.8");
        headerMap.put("Accept-Encoding", "gzip, deflate");
        headerMap.put("Connection", "keep-alive");
        headerMap.put("Content-Type", "application/x-www-form-urlencoded");
        return headerMap;
    }
}
