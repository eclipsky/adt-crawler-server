package com.dataeye.ad.scheduler.job.weixin;

import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.constant.Constant.*;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.scheduler.job.CrawlerFilter;
import com.dataeye.ad.util.*;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lingliqi
 * @date 2017-12-25 19:01
 */
public class WeixinJob extends CrawlerBaseJob {

    private final static CrawlerPlatform PLATFORM = CrawlerPlatform.WEIXIN;

    private final static Logger LOGGER = ServerUtil.getLogger(PLATFORM);

    /**
     * 47小时59分的时间戳
     **/
    private final static int TEMP_INTERVAL = 172740;

    /**
     * 账户余额
     **/
    private static final String ACCOUNT_URL = "https://mp.weixin.qq.com/promotion/agency/checkMpAdMasterAuthorize?callback=jQuery19105172057786139144_1514199482615&with_account_info=1&token={0}&appid=&spid=&_=1514199482616";

    /**
     * 数据报表
     **/
    private static final String REPORT_URL = "https://mp.weixin.qq.com/promotion/MpGetAderMetrics?report_type=1&begin_time={0}&end_time={1}&dimensionType=2&contract_flag=2&token={2}&appid=&spid=&_=1514199543129";

    /**
     * 实时广告
     **/
    private static final String PLAN_URL = "https://mp.weixin.qq.com/promotion/advert_get?begin={0}&count={1}&type=&pos_type=NaN&status=&aid=&contract_flag=1000&action=get_advert_list&token={2}&appid=&spid=&_=1514255856725";

    /**
     * 广告出价
     **/
    private static final String BID_URL = "https://mp.weixin.qq.com/promotion/advert_manager?callback=jQuery19109350380500069756_1514199566916&action=get_advert_info&aid={0}&token={1}&appid=&spid=&_=1514199566919";

    public WeixinJob() {
        this(DateUtil.unixTimestamp000());
    }

    public WeixinJob(long time) {
        super(time, PLATFORM);
    }

    public WeixinJob(long time, CrawlerFilter filter) {
        super(time, PLATFORM, filter);
    }


    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        String token = CookieUtil.getCookieValue(cookie, "token");
        if (StringUtils.isBlank(token)) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_NOT_EXISTS, "token is blank");
        }
        // 请求
        String content = HttpUtil.getContentByHttpsGet(HttpUtil.urlParamReplace(ACCOUNT_URL, token), getHeaderMap(cookie));
        LOGGER.info(content);
        // 解析
        AdAccountStat accountStat = parseAccount(content, account, time);
        // 入库
        DataProcessor.processAccountDay(accountStat);
    }

    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        List<AdPlanStat> poList = officialSpider(time, account);
        DataProcessor.processPlanDay(poList);
    }

    /**
     * 公众号广告
     *
     * @param time
     * @param account
     * @return
     * @throws Exception
     */
    public List<AdPlanStat> officialSpider(long time, AdMediumAccount account) throws Exception {
        List<AdPlanStat> poList = new ArrayList<>();
        // todo 暂无分页 2017年12月26日11:28:54
        String cookie = account.getCookie();
        String token = CookieUtil.getCookieValue(cookie, "token");
        // 实时
        if (DateUtil.unixTimestamp000() == time) {
            int page = 0;
            int pageSize = 10;
            PageModel pageModel = new PageModel(page, pageSize);
            String content = HttpUtil.getContentByHttpsGet(HttpUtil.urlParamReplace(PLAN_URL, page + "", pageSize + "", token), getHeaderMap(cookie));
            LOGGER.info(content);
            List<AdPlanStat> tempList = parsePlan(content, account, time, pageModel);
            poList.addAll(tempList);
            int totalNum = pageModel.getTotalNum();
            totalNum -= pageSize;
            while (totalNum > 0) {
                page += pageSize;
                content = HttpUtil.getContentByHttpsGet(HttpUtil.urlParamReplace(PLAN_URL, page + "", pageSize + "", token), getHeaderMap(cookie));
                LOGGER.info(content);
                tempList = parsePlan(content, account, time, pageModel);
                poList.addAll(tempList);
                totalNum -= pageSize;
            }
            // 补全出价信息
            for (AdPlanStat stat : poList) {
                content = HttpUtil.getContentByHttpsGet(HttpUtil.urlParamReplace(BID_URL, stat.getmPlanId(), token), getHeaderMap(cookie));
                LOGGER.info(content);
                parserBid(content, stat);
            }
        } else {
            // 历史报表数据
            // 传入时间戳参数
            long tempTime = time + TEMP_INTERVAL;
            int page = 1;
            int pageSize = 10;
            PageModel pageModel = new PageModel(page, pageSize);
            String content = HttpUtil.getContentByHttpsGet(HttpUtil.urlParamReplace(REPORT_URL, time + "", tempTime + "", token), getHeaderMap(cookie));
            LOGGER.info(content);
            // 解析数据
            poList = parseReport(content, account, time, pageModel);
        }

        return poList;
    }

    /**
     * 朋友圈广告
     *
     * @param time
     * @param account
     * @return
     * @throws Exception
     */
    public List<AdPlanStat> momentSpider(long time, AdMediumAccount account) throws Exception {
        return null;
    }


    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time)
            throws Exception {
        if (!content.contains("jQuery")) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE);
        }
        //从JS Callback中获取JSON对象
        int balance = 0;
        int startIndex = content.indexOf("{");
        int endIndex = content.length();
        // 剔除结尾的`)`字符
        String result = content.substring(startIndex, endIndex - 1);
        JsonObject resultJson = StringUtil.jsonParser.parse(result).getAsJsonObject();
        JsonArray accountArray = resultJson.get("agencies").getAsJsonArray();
        for (JsonElement ele : accountArray) {
            JsonObject obj = ele.getAsJsonObject();
            int subBalance = StringUtil.convertInt(obj.get("balance").getAsJsonObject().get("CASH").getAsString(), 0);
            balance += subBalance;
        }
        AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                account.getId());
        accountStat.setBalance(balance);
        return accountStat;
    }

    private List<AdPlanStat> parsePlan(String content, AdMediumAccount account, long time, PageModel pageModel)
            throws Exception {
        List<AdPlanStat> poList = new ArrayList<>();
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        int code = resultJson.get("ret").getAsInt();
        if (code != 0) {
            ExceptionHandler.throwException(ExceptionEnum.COOKIE_EXPIRE, resultJson.get("ret_msg").getAsString());
        }
        pageModel.setTotalNum(resultJson.get("total_count").getAsInt());
        JsonArray dataJson = resultJson.get("data").getAsJsonArray();
        for (JsonElement ele : dataJson) {
            JsonObject obj = ele.getAsJsonObject();
            String mPlanId = obj.get("advert_id").getAsString();
            String planName = obj.get("ad_name").getAsString();
            int clickNum = StringUtil.getJsonIntValue(obj, "click_count");
            int showNum = StringUtil.getJsonIntValue(obj, "view_count");
            int cost = StringUtil.getJsonIntValue(obj, "cost");
            String planBidStrategy = obj.get("cost_type").getAsString();
            String planStatus = obj.get("status").getAsString();
            AdPlanStat reportPO = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(), account.getId());
            poList.add(reportPO);
            reportPO.setmPlanId(mPlanId);
            reportPO.setPlanName(planName);
            reportPO.setClickNum(clickNum);
            reportPO.setShowNum(showNum);
            reportPO.setCost(cost);
            reportPO.setPlanBidStrategy(planBidStrategy);
            reportPO.setPlanStatus(planStatus);
        }
        return poList;
    }

    private void parserBid(String content, AdPlanStat stat) throws Exception {
        int startIndex = content.indexOf("{");
        int endIndex = content.length();
        // 截取JSON字符串
        // 剔除结尾的`);`字符
        String result = content.substring(startIndex, endIndex - 2);
        JsonObject resultJson = StringUtil.jsonParser.parse(result).getAsJsonObject();
        int code = resultJson.get("base_resp").getAsJsonObject().get("ret").getAsInt();
        if (code != 0) {
            ExceptionHandler.throwException(ExceptionEnum.COOKIE_EXPIRE, resultJson.get("base_resp").getAsJsonObject().get("ret_msg").getAsString());
        }
        Integer bid = resultJson.get("advert_info").getAsJsonObject().get("price").getAsInt();
        stat.setBid(bid);
    }

    private List<AdPlanStat> parseReport(String content, AdMediumAccount account, long time, PageModel pageModel)
            throws Exception {
        List<AdPlanStat> poList = new ArrayList<>();
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        int code = resultJson.get("result").getAsInt();
        if (code != 0) {
            ExceptionHandler.throwException(ExceptionEnum.SERVER_ERROR);
        }
        JsonElement listElement = resultJson.get("list");
        if (null == listElement || listElement.isJsonNull()) {
            return null;
        }
        JsonArray listJson = listElement.getAsJsonArray();
        for (JsonElement ele : listJson) {
            JsonObject obj = ele.getAsJsonObject();
            if (!reportDate.equals(obj.get("date").getAsString())) {
                continue;
            }
            int clickNum = StringUtil.getJsonIntValue(obj, "click");
            int showNum = StringUtil.getJsonIntValue(obj, "imp");
            int cost = StringUtil.getJsonIntValue(obj, "cost");
            String mPlanId = obj.get("id").getAsString();
            String planName = obj.get("name").getAsString();
            AdPlanStat reportPO = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(),
                    account.getId());
            poList.add(reportPO);
            reportPO.setmPlanId(mPlanId);
            reportPO.setPlanName(planName);
            reportPO.setClickNum(clickNum);
            reportPO.setShowNum(showNum);
            reportPO.setCost(cost);
        }
        return poList;
    }


    private Map<String, String> getHeaderMap(String cookie) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Cookie", cookie);
        headerMap.put("Host", "mp.weixin.qq.com");
        headerMap.put("X-Requested-With", "XMLHttpRequest");
        headerMap.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0");
        headerMap.put("Accept", "*/*");
        headerMap.put("Accept-Language", "zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2");
        headerMap.put("Accept-Encoding", "gzip, deflate, br");
        headerMap.put("Connection", "keep-alive");
        headerMap.put("Content-Type", "application/x-www-form-urlencoded");
        return headerMap;
    }

}

