package com.dataeye.ad.scheduler.job.sinafuyi;

import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.constant.Constant.*;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DBPipeline;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.scheduler.job.CrawlerFilter;
import com.dataeye.ad.util.*;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by VitAs on 2017/8/17.
 */
public class SinaFuyiJob extends CrawlerBaseJob {

    private static final CrawlerPlatform platform = CrawlerPlatform.SINA_FUYI;

    private static final Logger logger = ServerUtil.getLogger(platform);

    //Get
    private static final String accountUrl = "http://client.ad.sina.com.cn/gina-client/finance/clientFinanceOverview.ng?" +
            "data=%7B%7D";
    //POST
    private static final String planUrl = "http://fy.ad.sina.com.cn/ea/advert/list";

    //GET
    private static final String reportUrl = "http://client.ad.sina.com.cn/gina-client/eareport/advindex.ng?" +
            "data={0}";
    //Skey认证参数获取
    private static final String serverUrl = "http://client.ad.sina.com.cn/gina-client/common/serviceLineList.ng?" +
            "data=%7B%7D";

    private static final String CLIENT_HOST = "client.ad.sina.com.cn";

    private static final String FY_HOST = "fy.ad.sina.com.cn";

    private static final String SERVICE_LINE_NAME = "扶翼广告";

    private static final String PLAN_BID_STRATEGY = "CPC";

    private static final String EA_ADOWNER_COOKIES_ID = "ea_adowner_cookies_id";

    private static final String EA_VISITOR_COOKIES_ID = "ea_visitor_cookies_id";

    public SinaFuyiJob() {
        this(DateUtil.unixTimestamp000());
    }

    public SinaFuyiJob(long time) {
        super(time, platform);
    }

    public SinaFuyiJob(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }

    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        String content = HttpUtil.getContentByGet(accountUrl, getHeadMap(accountUrl, cookie));
        logger.info(content);
        AdAccountStat accountStat = parseAccount(content, account, time);
        //入库
        DataProcessor.processAccountDay(accountStat);

    }

    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        //历史请求和实时请求分离
        if (time >= DateUtil.unixTimestamp000()) {
            planSpiderToday(time, account);
        } else {
            planSpiderPast(time, account);
        }


    }

    /**
     * 通过请求获取skey入库
     *
     * @param account
     * @throws Exception
     */
    private void skeySpider(AdMediumAccount account) throws Exception {
        logger.info(".....开始爬取Skey......");
        String cookie = account.getCookie();
        String content = HttpUtil.getContentByGet(serverUrl, getHeadMap(serverUrl, cookie));
        logger.info(content);
        String skey = parseSkey(content);
        if (null != skey) {
            account.setSkey(skey);
            logger.info("......." + skey + "......skey爬取完成");
            CookieUtil.updateSkey(skey, account);
        }
    }

    /**
     * 更新cookie
     *
     * @param cookie
     * @param skey
     * @return
     * @throws Exception
     */
    private String setFuyiCookie(String cookie, String skey) throws Exception {
        StringBuilder sb = new StringBuilder(cookie);
        sb.append(";" + EA_VISITOR_COOKIES_ID + "=");
        sb.append("%s");
        sb.append(";" + EA_ADOWNER_COOKIES_ID + "=");
        sb.append("%s");
        String fuyiCookieFormat = sb.toString();
        if (null != skey) {
            String fuyiCookie = String.format(fuyiCookieFormat, skey, skey);
            return fuyiCookie;
        }
        return null;

    }

    /**
     * 实时投放计划接口
     * 获取产品管理中的广告列表
     *
     * @param time
     * @param account
     * @throws Exception
     */
    private void planSpiderToday(long time, AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        String skey = account.getSkey();
        /**
         * <pre/>
         * 用于请求的实际cookie，默认值为从数据库中取出的cookie
         * 从数据库中取值到的cookie会存在以下几种情况
         * 1.cookie为失效，但是不包含请求计划单元所需的uuid字段；
         *   1.1数据库中取到的skey值不为空，拼接cookie和skey得到新的cookie作用于http请求
         *   1.2数据库中取到的skey值为空，先通过请求获取到uuid，入库更新媒体账号表后，再拼接新的cookie作用于http请求
         * 2.cookie未失效，且已经包含了请求计划单元所需的字段；此时直接作用于http请求
         */
        String fuyicookie = cookie;
        //判断cookie是否包含爬取计划所需的参数
        //不包含时，通过skey补全
        if (!cookie.contains(EA_ADOWNER_COOKIES_ID) || !cookie.contains(EA_VISITOR_COOKIES_ID)) {
            //判断skey是否为空
            if (null != skey && !skey.equals("")) {
                fuyicookie = setFuyiCookie(cookie, skey);
            } else {
                skeySpider(account);
                skey = account.getSkey();
                fuyicookie = setFuyiCookie(cookie, skey);
            }
        }
        int pageSize = 400;
        int pageNum = 0;
        PageModel pageModel = new PageModel(pageNum, pageSize);
        String content = HttpUtil.getContentByPost(planUrl, getHeadMap(planUrl, fuyicookie), getStatParams(pageNum, pageSize));
        logger.info(content);
        List<AdPlanStat> poList = new ArrayList<>();
        List<AdPlanStat> tempList = parsePlan(content, account, time, pageModel);
        poList.addAll(tempList);
        int totalNum = pageModel.getTotalNum();
        totalNum -= pageSize;
        while (totalNum > 0) {
            pageNum += pageSize;
            content = HttpUtil.getContentByPost(planUrl, getHeadMap(planUrl, fuyicookie), getStatParams(pageNum, pageSize));
            logger.info(content);
            tempList = parsePlan(content, account, time, pageModel);
            poList.addAll(tempList);
            totalNum -= pageSize;
        }
        //入库
        DataProcessor.processPlanDay(poList);

    }

    /**
     * 历史投放计划接口
     * 获取统计报表中的广告列表
     *
     * @param time
     * @param account
     * @throws Exception
     */
    private void planSpiderPast(long time, AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        int page = 1;
        int pageSize = 100;
        PageModel pageModel = new PageModel(page, pageSize);
        String content = HttpUtil.getContentByGet(
                HttpUtil.urlParamReplace(reportUrl, getReportParam(reportDate, page, pageSize)),
                getHeadMap(reportUrl, cookie));
        logger.info(content);
        List<AdPlanStat> poList = parseReport(content, account, time, pageModel);
        //入库
        DataProcessor.processPlanDay(poList);
        int totalNum = pageModel.getTotalNum();
        totalNum -= pageSize;
        while (totalNum > 0) {
            page++;
            content = HttpUtil.getContentByGet(
                    HttpUtil.urlParamReplace(reportUrl, getReportParam(reportDate, page, pageSize)),
                    getHeadMap(reportUrl, cookie));
            logger.info(content);
            poList = parseReport(content, account, time, pageModel);
            // 入库
            if (!poList.isEmpty()) {
                DataProcessor.processPlanDay(poList);
            }
            totalNum -= pageSize;
        }
    }

    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time)
            throws Exception {
        AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                account.getId());
        JsonObject jsonResult = StringUtil.jsonParser.parse(content).getAsJsonObject();
        //响应状态判断
        int status = jsonResult.get("status").getAsInt();
        if (status != 0) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE);
        }
        JsonObject jsonData = jsonResult.get("data").getAsJsonObject();
        if (null != jsonData) {
            JsonArray jsonList = jsonData.get("list").getAsJsonArray();
            if (jsonList.size() > 0) {
                for (JsonElement element : jsonList) {
                    JsonObject order = element.getAsJsonObject();
                    String serviceLineName = StringUtil.getJsonStrValue(order, "serviceLineName");
                    //取扶翼广告的余额
                    if (serviceLineName.equals(SERVICE_LINE_NAME)) {
                        int balance = StringUtil.getJsonIntValue(order, "balance");
                        accountStat.setBalance(balance);
                    }
                }
            }
        }
        return accountStat;
    }

    private String parseSkey(String content) throws Exception {
        JsonObject jsonResult = StringUtil.jsonParser.parse(content).getAsJsonObject();
        //响应状态判断
        int status = jsonResult.get("status").getAsInt();
        if (status != 0) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE);
        }
        JsonObject jsonData = jsonResult.get("data").getAsJsonObject();
        if (null != jsonData) {
            JsonObject jsonUser = jsonData.get("user").getAsJsonObject();
            if (null != jsonUser) {
                String uuid = jsonUser.get("uuid").getAsString();
                return uuid;
            }
        }
        return null;
    }

    private List<AdPlanStat> parsePlan(String content, AdMediumAccount account, long time, PageModel pageModel)
            throws Exception {
        List<AdPlanStat> poList = new ArrayList<>();
        JsonObject jsonResult = StringUtil.jsonParser.parse(content).getAsJsonObject();
        //响应状态判断
        int code = jsonResult.get("code").getAsInt();
        if (code != 0) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE);
        }
        JsonObject jsonData = jsonResult.get("data").getAsJsonObject();
        if (null != jsonData) {
            //设置总记录数
            int totalNum = jsonData.get("count").getAsInt();
            pageModel.setTotalNum(totalNum);
            //遍历投放计划列表
            if (jsonData.get("list").isJsonNull()) {
                return poList;
            }
            JsonArray jsonList = jsonData.get("list").getAsJsonArray();
            if (jsonList.size() > 0) {
                for (JsonElement element : jsonList) {
                    AdPlanStat reportPO = new AdPlanStat(time, account);
                    poList.add(reportPO);
                    JsonObject order = element.getAsJsonObject();
                    String mPlanId = order.get("advertId").getAsString();
                    String planName = order.get("advertName").getAsString();
                    String groupId = order.get("advertGroupId").getAsString();
                    String groupName = order.get("advertGroupName").getAsString();
                    int planSwitch = StringUtil.getJsonIntValue(order, "status");
                    //出价
                    String bidStr = order.get("biddingPrice").getAsString();
                    int bid = StringUtil.convertBigDecimal(bidStr, 100, 0).intValue();
                    int clickNum = StringUtil.getJsonIntValue(order, "onTimeClick");
                    int showNum = StringUtil.getJsonIntValue(order, "onTimePv");
                    //消耗
                    String costStr = order.get("onTimeCost").getAsString();
                    int cost = StringUtil.convertBigDecimal(costStr, 0)
                            .setScale(0, BigDecimal.ROUND_HALF_UP)
                            .intValue();
                    //biddingMode  值为1时，对应CPC
                    int planBidStrategy = StringUtil.getJsonIntValue(order, "biddingMode");
                    if (planBidStrategy == 1) {
                        reportPO.setPlanBidStrategy(PLAN_BID_STRATEGY);
                    }
                    reportPO.setmPlanId(mPlanId);
                    reportPO.setPlanName(planName);
                    reportPO.setGroupId(groupId);
                    reportPO.setGroupName(groupName);
                    reportPO.setPlanSwitch(planSwitch);
                    reportPO.setBid(bid);
                    reportPO.setClickNum(clickNum);
                    reportPO.setShowNum(showNum);
                    reportPO.setCost(cost);
                }
            }
        }
        return poList;
    }

    private List<AdPlanStat> parseReport(String content, AdMediumAccount account, long time, PageModel pageModel)
            throws Exception {
        List<AdPlanStat> poList = new ArrayList<>();
        JsonObject jsonResult = StringUtil.jsonParser.parse(content).getAsJsonObject();
        //响应状态判断
        int status = jsonResult.get("status").getAsInt();
        if (status != 0) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE);
        }
        JsonObject jsonData = jsonResult.get("data").getAsJsonObject();
        if (null != jsonData) {
            //设置总记录数
            int totalNum = jsonData.get("totalCount").getAsInt();
            pageModel.setTotalNum(totalNum);
            if (jsonData.get("list").isJsonNull()) {
                return poList;
            }
            //遍历投放计划列表
            JsonArray jsonList = jsonData.get("list").getAsJsonArray();
            if (jsonList.size() > 0) {
                for (JsonElement element : jsonList) {
                    AdPlanStat reportPO = new AdPlanStat(time, account);
                    poList.add(reportPO);
                    JsonObject order = element.getAsJsonObject();
                    String mPlanId = order.get("advertid").getAsString();
                    String planName = order.get("advertname").getAsString();
                    int clickNum = StringUtil.getJsonIntValue(order, "click");
                    int showNum = StringUtil.getJsonIntValue(order, "impression");
                    //消耗
                    String costStr = order.get("pay").getAsString();
                    int cost = StringUtil.convertBigDecimal(costStr, 100, 0)
                            .intValue();
                    reportPO.setmPlanId(mPlanId);
                    reportPO.setPlanName(planName);
                    reportPO.setClickNum(clickNum);
                    reportPO.setShowNum(showNum);
                    reportPO.setCost(cost);
                }
            }
        }
        return poList;
    }


    private String getReportParam(String reportDate, int page, int pageSize) {
        Map<String, Object> map = new HashMap<>();
        map.put("startDate", reportDate);
        map.put("endDate", reportDate);
        map.put("pageNo", page);
        map.put("pageSize", pageSize);
        map.put("order", "desc");
        map.put("ordertype", "");
        map.put("byDay", "1");
        String jsonParam = new Gson().toJson(map);
        return HttpUtil.urlEncode(jsonParam);
    }

    private List<NameValuePair> getStatParams(int pageNum, int pageSize) {
        List<NameValuePair> list = new ArrayList<>();
        list.add(new BasicNameValuePair("limit", pageSize + ""));
        list.add(new BasicNameValuePair("start", pageNum + ""));
        list.add(new BasicNameValuePair("showHidden", "0"));
        list.add(new BasicNameValuePair("orderBy.asc", "0"));
        return list;
    }

    private Map<String, String> getHeadMap(String url, String cookie) {
        Map<String, String> headMap = new HashMap<>();
        //通过url字符串中判断请求头的host地址
        if (url.contains(CLIENT_HOST)) {
            headMap.put("Host", "client.ad.sina.com.cn");
            headMap.put("Referer", "http://client.ad.sina.com.cn/");
            headMap.put("Origin", "http://client.ad.sina.com.cn");
        } else if (url.contains(FY_HOST)) {
            headMap.put("Host", "fy.ad.sina.com.cn");
            headMap.put("Pragma", "no-cache");
            headMap.put("Cache-Control", "no-cache");
        }
        headMap.put("Connection", "keep-alive");
        headMap.put("Cookie", cookie);
        headMap.put("Accept-Encoding", "gzip, deflate");
        headMap.put("Accept", "*/*");
        headMap.put("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
        headMap.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:54.0) Gecko/20100101 Firefox/54.0");
        return headMap;
    }


}
