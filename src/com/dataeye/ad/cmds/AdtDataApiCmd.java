package com.dataeye.ad.cmds;

import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.pipeline.DBQuery;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.scheduler.job.CrawlerFilter;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.util.log.DELogger;
import com.xunlei.netty.httpserver.cmd.BaseCmd;
import com.xunlei.netty.httpserver.cmd.CmdMapper;
import com.xunlei.netty.httpserver.component.XLHttpRequest;
import com.xunlei.netty.httpserver.component.XLHttpResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 提供ADT-WEB服务调用的接口类
 *
 * @author lingliqi
 * @date 2018-02-28 10:18
 */
@Service
public class AdtDataApiCmd extends BaseCmd {

    private final static Logger logger = DELogger.getLogger("adt_data_api");

    private static ExecutorService executor = Executors.newFixedThreadPool(10);

    /**
     * 即时刷新，同步指定媒体账号列表的数据
     * Post请求
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @CmdMapper("/adt/platformData/realTime")
    public Object SyncRealTimeData(XLHttpRequest request, XLHttpResponse response) throws Exception {
        logger.info("开始即时同步媒体数据...");
        String mediumAccountIds = request.getParameter("mediumAccountIds");
        if (StringUtils.isBlank(mediumAccountIds)) {
            logger.info("fail -> 媒体账号列表为空...");
            return "fail";
        }
        countDownRestore(mediumAccountIds);
        logger.info("即时媒体数据同步完成...");
        return "success";
    }

    /**
     * 并行同步媒体数据
     * 通过CountDownLatch阻塞，获取完成节点。
     *
     * @param ids
     * @throws Exception
     */
    private void countDownRestore(String ids) throws Exception {
        long time = DateUtil.unixTimestamp000();
        List<AdMediumAccount> accountList = DBQuery.getMediumAccountList(ids);
        logger.info("countDownJob start...");
        final CountDownLatch end = new CountDownLatch(accountList.size());
        for (AdMediumAccount account : accountList) {
            final int accountId = account.getId();
            final int companyId = account.getCompanyId();
            final int mediumId = account.getPlatformId();
            CrawlerFilter filter = new CrawlerFilter(accountId, companyId);
            Class<? extends CrawlerBaseJob> jobClass = Constant.CrawlerPlatform.getPlatformMap().get(mediumId).getJobClass();
            final CrawlerBaseJob job = jobClass.getConstructor(long.class, CrawlerFilter.class).newInstance(time,
                    filter);
            executor.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        logger.info(job + " -> " + accountId);
                        job.run();
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } finally {
                        end.countDown();
                        logger.info("countDown:" + end.getCount());
                    }
                }
            });

        }

        end.await();
        logger.info("countDownJob end...");
    }

}
