package com.dataeye.ad.scheduler.job.mta.model;

public class MtaPage {

	private int ifRealPage;

	private int size;

	public int getIfRealPage() {
		return ifRealPage;
	}

	public void setIfRealPage(int ifRealPage) {
		this.ifRealPage = ifRealPage;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

}