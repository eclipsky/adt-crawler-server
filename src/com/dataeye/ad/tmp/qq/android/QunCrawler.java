package com.dataeye.ad.tmp.qq.android;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;

import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.StringUtil;
import com.dataeye.util.log.DELogger;

public class QunCrawler {
    public static List<String> keywordList = new ArrayList<String>();
    public static final String URL = "http://qun.qq.com/cgi-bin/group_search/group_search?callback=jsonp3&retype=2&keyword=${0}&page={1}&wantnum={2}&city_flag=0&distance=1&ver=1&from=9&bkn=2050077793&style=1&lat=22524099&lon=113937830&_=1521027842553";
    public static final String COOKIE = "uin=o2330268629; p_uin=o2330268629; skey=MGmR3k1ZJV; vkey=8Ljid/JzWhFl1f4URZixN3F1RtLnEgtx8ae513d50201==";

    public static Logger logger_r = DELogger.getLogger("qun_r");
    public static Logger logger_c = DELogger.getLogger("qun_c");

    public static void main(String[] args) throws Exception {
        String prefix = "gongdou"; // chuanqi
        String file = "d:\\" + prefix + ".qun";
        logger_r = DELogger.getLogger(prefix + "_r");
        logger_c = DELogger.getLogger(prefix + "_c");
        QunCrawler qun = new QunCrawler();
        readKeywordFromFile(file);
        qun.spider();
    }

    public static void readKeywordFromFile(String file) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
        String line = "";
        while ((line = br.readLine()) != null) {
            keywordList.add(line);
        }
        br.close();
    }

    public void spider() throws Exception {
        for (String keyword : keywordList) { //
            search(keyword);
        }
    }

    public void search(String keyword) throws Exception {
        String items[] = keyword.split(",");
        String company = items[0];
        String searchTerm = items[1];
        // Logger logger_r = DELogger.getLogger("qun_r_" + keyword);
        // Logger logger_c = DELogger.getLogger("qun_c_" + keyword);
        System.out.println(">>>>> " + keyword);
        logger_r.info("开始:" + keyword + ".....");
        int page = 0;
        int pageSize = 100;
        int endflag = 0;
        int num = 0;
        try {
            while (endflag == 0 && page < 5) {
                String content = HttpUtil.getContentByGet(
                        HttpUtil.urlParamReplace(URL, searchTerm, page + "", pageSize + ""), getHeaderMap(COOKIE));
                logger_c.info(keyword + " >>> " + content);
                content = content.replace("jsonp3(", "").replace("})", "}");
                Response qun = StringUtil.gson.fromJson(content, Response.class);
                endflag = qun.getEndflag();
                int errorcode = qun.getErrorcode();
                if (errorcode > 0) {
                    break;
                }
                if (null != qun.getGroup_list()) {
                    for (Group group : qun.getGroup_list()) {
                        logger_r.info(keyword + " >>> " + group.getCode() + "");
                        num++;
                    }
                }
                Thread.sleep(5000 + (System.currentTimeMillis() % 1000) * 5);
                page++;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger_r.info("结束:" + keyword + ", num=" + num);
        Thread.sleep(20000);

    }

    public void parser() {

    }

    private Map<String, String> getHeaderMap(String cookie) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Host", "qun.qq.com");
        headerMap.put("Connection", "keep-alive");
        headerMap.put("Accept", "application/json, text/javascript, */*; q=0.01");
        headerMap.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        headerMap
                .put("User-Agent",
                        "Mozilla/5.0 (Linux; Android 5.1; m2 Build/LMY47D) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/40.0.2214.114 Mobile Safari/537.36 V1_AND_SQ_4.6.1_9_YYB_D 3.6.1 QQ/5.3.1.687 NetType/WIFI 10000519");
        headerMap
                .put("Referer",
                        "http://qqweb.qq.com/m/relativegroup/index.html?source=qun_rec&keyword=ds&sid=AWSAPtjyiVRg92WelXNMAqd0&_bid=165");
        headerMap.put("Accept-Encoding", "gzip, deflate");
        headerMap.put("Accept-Language", "en-US,en;q=0.8");
        headerMap.put("X-Requested-With", "com.tencent.qqlite");
        headerMap.put("Cookie", COOKIE);
        return headerMap;
    }

}

class Response {
    private int ec;
    private int endflag;
    private int exact;
    private int errorcode;
    private int gTotal;
    private List<Group> group_list;
    private List<String> redwords;
    private int usr_cityid;

    public int getEc() {
        return ec;
    }

    public void setEc(int ec) {
        this.ec = ec;
    }

    public int getEndflag() {
        return endflag;
    }

    public void setEndflag(int endflag) {
        this.endflag = endflag;
    }

    public int getExact() {
        return exact;
    }

    public void setExact(int exact) {
        this.exact = exact;
    }

    public int getErrorcode() {
        return errorcode;
    }

    public void setErrorcode(int errorcode) {
        this.errorcode = errorcode;
    }

    public int getgTotal() {
        return gTotal;
    }

    public void setgTotal(int gTotal) {
        this.gTotal = gTotal;
    }

    public List<Group> getGroup_list() {
        return group_list;
    }

    public void setGroup_list(List<Group> group_list) {
        this.group_list = group_list;
    }

}

class Group {
    private int code;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

}
