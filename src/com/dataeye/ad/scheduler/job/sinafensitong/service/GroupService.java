package com.dataeye.ad.scheduler.job.sinafensitong.service;

import com.dataeye.ad.common.CookieException;
import com.dataeye.ad.common.ServerException;

import java.io.IOException;
import java.util.List;

/**
 * Created by huangzehai on 2017/3/28.
 */
@Deprecated
public interface GroupService {
    List<Integer> getGroupIds(String cookie) throws IOException, CookieException, ServerException;
}
