package com.dataeye.ad.scheduler.job.uc.model;

public class UcResponse {

	private int status;

	private String message;

	private String extend;

	private UcData data;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getExtend() {
		return extend;
	}

	public void setExtend(String extend) {
		this.extend = extend;
	}

	public UcData getData() {
		return data;
	}

	public void setData(UcData data) {
		this.data = data;
	}

}
