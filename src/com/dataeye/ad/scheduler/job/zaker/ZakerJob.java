package com.dataeye.ad.scheduler.job.zaker;

import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.constant.Constant.*;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.scheduler.job.CrawlerFilter;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by VitAs on 2017/8/8.
 */
public class ZakerJob extends CrawlerBaseJob {

    private final static CrawlerPlatform platform = CrawlerPlatform.ZAKER;

    private final static Logger logger = ServerUtil.getLogger(platform);

    private final static String accountUrl = "http://dsp.myzaker.com/?r=/virtual/campaign/index";

    private final static String planUrl = "http://dsp.myzaker.com/?r=virtual/campaign/index&" +
            "page=1&starttime={0}&endtime={1}&pageSize=50&noCache=0&platform=all&" +
            "searchData[status]=-1&action=list&start={2}&length={3}&_=1502173956457";

    private final static String reportUrl = "http://dsp.myzaker.com/?r=virtual/report/trend&" +
            "page=1&timeGroup=day&starttime={0}&endtime={1}&pageSize=50&noCache=0&platform=all&" +
            "action=list&draw=3&start={2}&length={3}&_=1502178196984";


    //作为构建计划/报表请求头的判断参数
    private final static String URL_WITH = "action";

    public ZakerJob() {
        this(DateUtil.unixTimestamp000());
    }

    public ZakerJob(long time) {
        super(time, platform);
    }

    public ZakerJob(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }

    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        // 请求
        String content = HttpUtil.getContentByGet(accountUrl, getHeaderMap(cookie, accountUrl));
        logger.info(content);
        // 解析
        AdAccountStat accountStat = parseAccount(content, account, time);
        // 入库
        DataProcessor.processAccountDay(accountStat);


    }

    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        // 由于爬取计划单元时，获取不到点击，曝光和消耗
        // 需要在日报统计中去获取，所以构建map对象，以每个计划的id作为key值
        Map<String, AdPlanStat> map = reportSpider(time, account);
        List<AdPlanStat> poList = new ArrayList<>();
        String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        String cookie = account.getCookie();
        int page = 0;
        int pageSize = 200;
        PageModel pageModel = new PageModel(page, pageSize);
        String url = HttpUtil.urlParamReplace(planUrl, reportDate, reportDate, page + "", pageSize + "");
        String content = HttpUtil.getContentByGet(url, getHeaderMap(cookie, url));
        logger.info(content);
        // 解析
        List<AdPlanStat> onePageOfList = parsePlan(content, account, time, pageModel, map);
        poList.addAll(onePageOfList);
        int totalNum = pageModel.getTotalNum();
        totalNum -= pageSize;
        while (totalNum > 0) {
            page++;
            url = HttpUtil.urlParamReplace(planUrl, reportDate, reportDate, page + "", pageSize + "");
            content = HttpUtil.getContentByGet(url, getHeaderMap(cookie, url));
            logger.info(content);
            onePageOfList = parsePlan(content, account, time, pageModel, map);
            poList.addAll(onePageOfList);
            totalNum -= pageSize;
        }
        // 入库
        DataProcessor.processPlanDay(poList);

    }

    private Map<String, AdPlanStat> reportSpider(long time, AdMediumAccount account)
            throws Exception {
        // TODO 暂无数据展示 分页待完善
        String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        String cookie = account.getCookie();
        int page = 0;
        int pageSize = 200;
        PageModel pageModel = new PageModel(page, pageSize);
        String url = HttpUtil.urlParamReplace(reportUrl, reportDate, reportDate, page + "", pageSize + "");
        String content = HttpUtil.getContentByGet(url, getHeaderMap(cookie, url));
        logger.info(content);
        Map<String, AdPlanStat> map = parseReport(content, account, time, pageModel);
        return map;

    }


    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time)
            throws Exception {
        AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                account.getId());
        //jsoup解析html内容
        Document doc = Jsoup.parse(content);
        Element element = doc.getElementById("overview_balance");
        if (null != element) {
            int balance = StringUtil.convertBigDecimal(element.text(), 100, 0).intValue();
            accountStat.setBalance(balance);
        }
        return accountStat;
    }

    private List<AdPlanStat> parsePlan(String content, AdMediumAccount account, long time, PageModel pageModel, Map<String, AdPlanStat> map)
            throws Exception {
        List<AdPlanStat> poList = new ArrayList<>();
        JsonObject jsonResult = StringUtil.jsonParser.parse(content).getAsJsonObject();
        int totalNum = StringUtil.convertInt(jsonResult.get("iTotalDisplayRecords").getAsString(), 0);
        if (totalNum == 0) {
            ExceptionHandler.throwException(ExceptionEnum.SERVER_ERROR, "无投放计划数据, 或者数据解析异常");
        }
        pageModel.setTotalNum(totalNum);
        JsonArray listData = jsonResult.get("data").getAsJsonArray();
        if (listData.size() > 0) {
            for (JsonElement item : listData) {
                AdPlanStat adPlanStat = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(), account.getId());
                JsonObject order = item.getAsJsonObject();
                //计划ID
                String mPlanId = order.get("campaignid").getAsString();
                //计划名称
                String planName = order.get("campaignidName").getAsString();
                //分组ID
                String groupId = order.get("packageid").getAsString();
                //分组名称
                String groupName = order.get("package_name").getAsString();
                //计划出价
                double bidPrice = StringUtil.getJsonDoubleValue(order, "bidding");
                //计划状态
                String planStatus = order.get("statusText").getAsString();
                adPlanStat.setmPlanId(mPlanId);
                adPlanStat.setPlanName(planName);
                adPlanStat.setGroupName(groupName);
                adPlanStat.setGroupId(groupId);
                adPlanStat.setBid(new BigDecimal(bidPrice).multiply(new BigDecimal(100))
                        .setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
                adPlanStat.setPlanStatus(planStatus);
                //补全消耗点击曝光
                AdPlanStat reprotPO = map.get(mPlanId);
                if (null != reprotPO) {
                    adPlanStat.setShowNum(reprotPO.getShowNum());
                    adPlanStat.setClickNum(reprotPO.getClickNum());
                    adPlanStat.setCost(reprotPO.getCost());
                }
                poList.add(adPlanStat);
            }
        }
        return poList;
    }

    private Map<String, AdPlanStat> parseReport(String content, AdMediumAccount account, long time, PageModel pageModel)
            throws Exception {
        Map<String, AdPlanStat> adPlanStatMap = new HashMap<>();
        JsonObject jsonResult = StringUtil.jsonParser.parse(content).getAsJsonObject();
        int totalNum = StringUtil.convertInt(jsonResult.get("iTotalDisplayRecords").getAsString(), 0);
        if (totalNum == 0) {
            ExceptionHandler.throwException(ExceptionEnum.SERVER_ERROR, "无投放计划数据, 或者数据解析异常");
        }
        JsonArray listData = jsonResult.get("data").getAsJsonArray();
        if (listData.size() > 0) {
            for (JsonElement item : listData) {
                JsonObject order = item.getAsJsonObject();
                //计划ID
                String mPlanId = order.get("campaignid").getAsString();
                String planName = order.get("campaignidName").getAsString();
                int clickNum = StringUtil.getJsonIntValue(order, "clickNum");
                int showNum = StringUtil.getJsonIntValue(order, "showNum");
                //精度未知
                double cost = StringUtil.getJsonDoubleValue(order, "totalCost");
                AdPlanStat adPlanStat = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(),
                        account.getId());
                adPlanStat.setmPlanId(mPlanId);
                adPlanStat.setPlanName(planName);
                adPlanStat.setShowNum(showNum);
                adPlanStat.setClickNum(clickNum);
                adPlanStat.setCost(new BigDecimal(cost).multiply(new BigDecimal(100))
                        .setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
                adPlanStatMap.put(mPlanId, adPlanStat);
            }
        }
        return adPlanStatMap;
    }


    private Map<String, String> getHeaderMap(String cookie, String url) {
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Accept", "application/json, text/javascript, */*; q=0.01");
        headerMap.put("Accept-Encoding", "gzip, deflate");
        headerMap.put("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.6,en;q=0.4");
        headerMap.put("Connection", "keep-alive");
        headerMap.put("Cookie", cookie);
        headerMap.put("Host", "dsp.myzaker.com");
        headerMap.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.86 Safari/537.36");
        if (url.contains(URL_WITH))
            headerMap.put("X-Requested-With", "XMLHttpRequest");
        return headerMap;

    }
}


