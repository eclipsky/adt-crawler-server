package com.dataeye.ad.scheduler.job.zhihu;

import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.scheduler.job.CrawlerBaseExecutor;
import com.dataeye.ad.util.DateUtil;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * @author lingliqi
 * @create 2017-10-17 9:55
 */
public class ZhiHuJobDay implements Job {
    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        CrawlerBaseExecutor.addJob(new ZhiHuJob(DateUtil.unixTimestamp000() - Constant.ONEDAY));
    }
}
