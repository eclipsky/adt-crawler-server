package com.dataeye.ad.scheduler.job.youku;

import java.util.*;

import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.scheduler.job.CrawlerFilter;

import org.slf4j.Logger;

import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.constant.Constant.CrawlerPlatform;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.scheduler.job.youku.model.QueryEntity;
import com.dataeye.ad.scheduler.job.youku.model.QueryEntity.ExtLimit;
import com.dataeye.ad.util.CookieUtil;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class YoukuJob extends CrawlerBaseJob {

    private final static CrawlerPlatform platform = CrawlerPlatform.YOUKU;

    private final static Logger logger = ServerUtil.getLogger(platform);

    private final static String planUrl = "http://smartvideo.youku.com/cast/CastNReportController/search";

    private final static String accountUrl = "http://smartvideo.youku.com/financial/FinancialController/getCustomerFinancial";

    public YoukuJob() {
        this(DateUtil.unixTimestamp000());
    }

    public YoukuJob(long time) {
        super(time, platform);
    }

    public YoukuJob(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }


    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        int customerId = StringUtil.convertInt(CookieUtil.getYoukuCustomerId(cookie, account), 0);
        if (customerId <= 0) {
            ExceptionHandler.throwCookieException(ExceptionEnum.SKEY_NOT_EXISTS);
        }
        String queryFilter = generatorAccountFilter(customerId);
        // 请求
        String content = HttpUtil.getContentByPost(accountUrl, getHeaderMap(cookie), queryFilter);
        if (content.contains("CSRF")) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, content);
        }
        logger.info(content);
        // 解析
        AdAccountStat accountStat = parseAccount(content, account, time);
        // 入库
        DataProcessor.processAccountDay(accountStat);
    }

    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        String statDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        String cookie = account.getCookie();
        int pageId = 1;
        int pageSize = 20;
        PageModel pageModel = new PageModel(pageId, pageSize);
        String queryFilter = generatorFilter(statDate, pageModel);
        // 请求
        String content = HttpUtil.getContentByPost(planUrl, getHeaderMap(cookie), queryFilter);
        if (content.contains("CSRF")) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, content);
        }
        logger.info(content);
        // 解析
        List<AdPlanStat> list = parse(content, account, time, pageModel);
        // 入库
        DataProcessor.processPlanDay(list);
        int totalNum = pageModel.getTotalNum();
        totalNum -= pageSize;
        // 处理分页
        while (totalNum > 0) {
            pageId++;
            pageModel.setPageId(pageId);
            // 请求
            queryFilter = generatorFilter(statDate, pageModel);
            content = HttpUtil.getContentByPost(planUrl, getHeaderMap(cookie), queryFilter);
            logger.info(content);
            // 解析
            list = parse(content, account, time, pageModel);
            // 入库
            DataProcessor.processPlanDay(list);
            totalNum -= pageSize;
        }
    }

    private String generatorAccountFilter(int customerId) {
        JsonObject obj = new JsonObject();
        obj.addProperty("customerId", customerId);
        return obj.toString();
    }

    private String generatorFilter(String reportDate, PageModel page) {
        QueryEntity entity = new QueryEntity();
        ExtLimit extLimit = entity.new ExtLimit();
        entity.setExtLimit(extLimit);
        extLimit.setStart((page.getPageId() - 1) * page.getPageSize());
        extLimit.setLimit(page.getPageSize());
        extLimit.setSort("pv_sum");
        extLimit.setDir("desc");
        extLimit.setMaxSize(8);
        extLimit.setCurrentPage(page.getPageId()); // 无影响
        extLimit.setLimitOptions(new Integer[]{10, 20, 50, 100});
        extLimit.setAllPages(page.getTotalPages()); // 无影响
        entity.setStartDate(reportDate);
        entity.setEndDate(reportDate);
        return StringUtil.gson.toJson(entity);
    }

    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time) throws Exception {
        JsonObject result = StringUtil.jsonParser.parse(content).getAsJsonObject();
        boolean isOK = result.get("success").getAsBoolean();
        if (!isOK) {
            ExceptionHandler.throwException(ExceptionEnum.SERVER_ERROR, content);
        }
        AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                account.getId());
        JsonObject data = result.get("data").getAsJsonObject();
        int balance = StringUtil.convertBigDecimal(data.get("formatBalance").getAsString().replace(",", ""), 100, 0)
                .intValue();
        JsonElement costTodayEle = data.get("formatCostToday"); // 没消耗返回"null"，而不是0
        int costToday = 0;
        if (!costTodayEle.isJsonNull()) {
            costToday = StringUtil.convertBigDecimal(costTodayEle.getAsString().replace(",", ""), 100, 0).intValue();
        }
        accountStat.setBalance(balance);
        accountStat.setCostToday(costToday);
        return accountStat;
    }

    private List<AdPlanStat> parse(String content, AdMediumAccount account, long time, PageModel pageModel)
            throws Exception {
        List<AdPlanStat> resultList = new ArrayList<AdPlanStat>();
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        //如果返回错误提示，则抛出异常信息
        String resultStr = StringUtil.getJsonStrValue(resultJson, "result");
        if (resultStr.equals("error")) {
            ExceptionHandler.throwException(ExceptionEnum.SERVER_ERROR, StringUtil.getJsonStrValue(resultJson, "info"));
        }
        int totalNum = resultJson.get("total").getAsInt();
        pageModel.setTotalNum(totalNum);
        JsonArray plans = resultJson.getAsJsonArray("invdata");
        for (JsonElement ele : plans) {
            JsonObject obj = ele.getAsJsonObject();
            AdPlanStat adPlanDay = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(),
                    account.getId());
            resultList.add(adPlanDay);
            // 媒体计划ID
            String mPlanId = obj.get("castId").getAsInt() + "";
            // 媒体计划名
            String planName = obj.get("castName").getAsString();
            // 媒体计划出价
            int planBid = obj.get("singleMaxPrice").getAsInt();
            // 媒体计划出价策略
            String planBidStrategy = obj.get("payTypeStr").getAsString();
            // 媒体计划状态 1:进行中 0：已暂停 5：已归档
            String planStatus = obj.get("status").getAsString();
            // 媒体计划开关
            Integer planSwitch = null;
            if (Objects.equals(planStatus, "1")) {
                planSwitch = Constant.PlanSwitch.ON;
            } else if (Objects.equals(planStatus, "0")) {
                planSwitch = Constant.PlanSwitch.OFF;
            }
            // 消耗
            long totalCost = StringUtil.getJsonLongValue(obj, "total_price");
            int cost = StringUtil.convertBigDecimalWithDivide(totalCost + "", 1000, 0).intValue();
            // 曝光
            int showNum = StringUtil.getJsonIntValue(obj, "pv_sum");
            // 点击
            int clickNum = StringUtil.getJsonIntValue(obj, "click_sum");
            adPlanDay.setmPlanId(mPlanId);
            adPlanDay.setPlanName(planName);
            adPlanDay.setCost(cost);
            adPlanDay.setShowNum(showNum);
            adPlanDay.setClickNum(clickNum);
            adPlanDay.setBid(planBid);
            adPlanDay.setPlanBidStrategy(planBidStrategy);
            adPlanDay.setPlanStatus(planStatus);
            adPlanDay.setPlanSwitch(planSwitch);
        }
        return resultList;
    }

    private Map<String, String> getHeaderMap(String cookie) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Host", "smartvideo.youku.com");
        headerMap.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0");
        headerMap.put("Accept", "application/json, text/plain, */*");
        headerMap.put("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
        headerMap.put("Accept-Encoding", "gzip, deflate");
        headerMap.put("Content-Type", "application/json;charset=utf-8");
        headerMap.put("Referer", "http://smartvideo.youku.com/");
        headerMap.put("Cookie", cookie);
        headerMap.put("Connection", "keep-alive");
        // headerMap.put("Content-Length", "183");
        headerMap.put("Pragma", "no-cache");
        headerMap.put("Cache-Control", "no-cache");
        return headerMap;
    }

    public void codeCheck(int code, String content) throws Exception {
        // 如果正常查询 data为对象类型，如果异常，data为数组类型，这里要分开处理
        if (code != 0) {
            // 例如
            // code = 1000，"系统开小差啦，请刷新页面再试吧"
            // code = 1110，"自助平台登录已过期"
            // String msg = resultJson.get("msg").getAsString();
            if (code == 1110) {
                ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, content);
            } else {
                ExceptionHandler.throwException(ExceptionEnum.SERVER_ERROR, content);
            }
        }
    }
}
