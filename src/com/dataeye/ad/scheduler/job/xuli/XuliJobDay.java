package com.dataeye.ad.scheduler.job.xuli;

import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.scheduler.job.CrawlerBaseExecutor;
import com.dataeye.ad.util.DateUtil;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * @author lingliqi
 * @date 2018-01-05 13:13
 */
public class XuliJobDay implements Job {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        CrawlerBaseExecutor.addJob(new XuliJob(DateUtil.unixTimestamp000() - Constant.ONEDAY));

    }
}
