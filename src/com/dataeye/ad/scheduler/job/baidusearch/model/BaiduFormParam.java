package com.dataeye.ad.scheduler.job.baidusearch.model;

public class BaiduFormParam {

	private ReportInfo reportinfo;

	private int maxRecordNum;

	private int pageNo;

	private int pageSize;

	public ReportInfo getReportinfo() {
		return reportinfo;
	}

	public void setReportinfo(ReportInfo reportinfo) {
		this.reportinfo = reportinfo;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getMaxRecordNum() {
		return maxRecordNum;
	}

	public void setMaxRecordNum(int maxRecordNum) {
		this.maxRecordNum = maxRecordNum;
	}

}
