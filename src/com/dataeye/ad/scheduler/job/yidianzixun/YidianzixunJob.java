package com.dataeye.ad.scheduler.job.yidianzixun;

import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.scheduler.job.CrawlerFilter;
import com.dataeye.ad.scheduler.job.yidianzixun.model.LingxiGroupModel;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;
import org.slf4j.Logger;


import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Created by VitAs on 2017/6/26.
 */
public class YidianzixunJob extends CrawlerBaseJob {

    private final static Constant.CrawlerPlatform platform = Constant.CrawlerPlatform.YIDIANZIXUN;

    private final static Logger logger = ServerUtil.getLogger(platform);

    private final static String accountUrl = "http://lx.yidianzixun.com/home";

    private final static String groupUrl = "http://lx.yidianzixun.com/performance/index";

    private final static String planUrl = "http://lx.yidianzixun.com/plan?pid={0}";

    private final static String pastPlanUrl = "http://lx.yidianzixun.com/report/detail/bid?productId=2&uid=&" +
            "fromDate={0}&toDate={1}&planId=&ideaId=&platform=null&showType=null&templateId=null";

    private final static String BALANCE_LABEL = "余额";

    private List<LingxiGroupModel> groupModels;

    public YidianzixunJob() {
        this(DateUtil.unixTimestamp000());
    }

    public YidianzixunJob(long time) {
        super(time, platform);
    }

    public YidianzixunJob(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }


    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        String cookie = account.getCookie();
        // TODO: 2017/6/27  页面暂时获取不到分页相关的参数，待处理
        String content = "";
        List<AdPlanStat> poList = new ArrayList<>();
        if (DateUtil.unixTimestamp000() == time) {
            //实时请求
            //首先获取分组的id和name
            groupModels = groupSpider(time, account);
            //遍历分组；以分组的id为参数，获取投放计划
            if (!groupModels.isEmpty()) {
                for (LingxiGroupModel model : groupModels) {
                    String pid = model.getId();
                    content = HttpUtil.getContentByGet(HttpUtil.urlParamReplace(planUrl, pid),
                            getHeaderMap(cookie));
                    //解析数据
                    poList = parsePlan(content, account, time, model);
                    // 入库
                    if (!poList.isEmpty()) {
                        DataProcessor.processPlanDay(poList);
                    }
                }
            }
        } else {
            // TODO 历史报表请求平台上已经修改接口。后期需要跟进
            //由于请求中的时间参数为同一天时，数据的显示区间是按小时汇总的。
            //所以默认时间开始节点为时间参数的前一天
            String fromDate = DateUtil.getTimeString(time - Constant.ONEDAY, "yyyy-MM-dd");
            //历史请求
            content = HttpUtil.getContentByGet(HttpUtil.urlParamReplace(pastPlanUrl, fromDate, reportDate),
                    getHeaderMap(cookie));
            logger.info(content);
            //解析数据
            poList = parsePlanPast(content, account, time);
            // 入库
            if (!poList.isEmpty()) {
                DataProcessor.processPlanDay(poList);
            }
        }


    }

    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        //请求
        String content = HttpUtil.getContentByGet(accountUrl, getHeaderMap(cookie));
        logger.info(content);
        //解析返回的html页面
        AdAccountStat adAccountStat = parseAccount(content, account, time);
        // 入库
        DataProcessor.processAccountDay(adAccountStat);

    }


    /**
     * 模拟分组数据请求
     *
     * @param time
     * @param account
     * @return
     * @throws Exception
     */
    private List<LingxiGroupModel> groupSpider(long time, AdMediumAccount account) throws Exception {
        List<LingxiGroupModel> modelList = new ArrayList<>();
        String cookie = account.getCookie();
        //请求
        String content = HttpUtil.getContentByGet(groupUrl, getHeaderMap(cookie));
        logger.info(content);
        //解析返回的html页面
        modelList = parseGroup(content, account, time);
        return modelList;
    }


    /**
     * 通过响应页面内容解析分组信息
     *
     * @param content
     * @param account
     * @param time
     * @return
     * @throws Exception
     */
    private List<LingxiGroupModel> parseGroup(String content, AdMediumAccount account, long time)
            throws Exception {
        List<LingxiGroupModel> modelList = new ArrayList<>();
        Document doc = Jsoup.parse(content);
        Elements elements = doc.select(".listForm>table>tbody>tr");
        if (!elements.isEmpty()) {
            for (Element item : elements) {
                //无数据时的处理 通过属性进行判断
                Attributes attrs = item.attributes();
                if (attrs.size() != 0) {
                    String id = item.select("span").attr("data-pid");
                    String name = item.select("a").text();
                    LingxiGroupModel model = new LingxiGroupModel(id, name);
                    modelList.add(model);
                }
            }
        }
        return modelList;

    }


    /**
     * 实时投放计划解析
     *
     * @param content
     * @param account
     * @param time
     * @param model
     * @return
     * @throws Exception
     */
    private List<AdPlanStat> parsePlan(String content, AdMediumAccount account, long time, LingxiGroupModel model)
            throws Exception {
        List<AdPlanStat> poList = new ArrayList<>();
        Document doc = Jsoup.parse(content);
        Elements trList = doc.select(".trCount");
        if (trList != null && trList.size() > 0) {
            for (Element tr : trList) {
                AdPlanStat reportPO = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(),
                        account.getId());
                poList.add(reportPO);
                // 计划开关
                String switchStr = tr.select("td").get(2).select("span").attr("title");
                Integer planSwitch = Constant.PlanSwitch.OFF;
                if ("暂停".equals(switchStr)) {
                    planSwitch = Constant.PlanSwitch.ON;
                }
                // 计划id
                String mPlanId = tr.select("td").get(2).select("span").attr("data-aid");
                // 计划名称
                String planName = tr.select("td").get(3).select("a").text();
                // 分组id
                String groupId = model.getId();
                // 分组名称
                String groupName = model.getName();
                // 出价方式
                String planBidStrategy = tr.select("td").get(6).text();
                // 出价
                String bidPrice = tr.select("td").get(7).text().replace("￥", "").trim();
                int planBid = StringUtil.convertBigDecimal(bidPrice, 100, 0).intValue();
                //曝光数
                int showNum = StringUtil.convertInt(tr.select("td").get(8).text(), 0);
                //点击数
                int clickNum = StringUtil.convertInt(tr.select("td").get(9).text(), 0);
                String costStr = tr.select("td").get(11).text().replace("￥", "").trim();
                int cost = StringUtil.convertBigDecimal(costStr, 100, 0).intValue();
                // 计划状态
                String planStatus = tr.select("td").get(12).select("span").text();
                //补全映射类
                reportPO.setPlanSwitch(planSwitch);
                reportPO.setGroupName(groupName);
                reportPO.setGroupId(groupId);
                reportPO.setPlanName(planName);
                reportPO.setmPlanId(mPlanId);
                reportPO.setBid(planBid);
                reportPO.setPlanBidStrategy(planBidStrategy);
                reportPO.setClickNum(clickNum);
                reportPO.setCost(cost);
                reportPO.setShowNum(showNum);
                reportPO.setPlanStatus(planStatus);
            }
        }
        return poList;
    }


    /**
     * 解析历史投放计划
     *
     * @param content
     * @param account
     * @param time
     * @return
     * @throws Exception
     */
    private List<AdPlanStat> parsePlanPast(String content, AdMediumAccount account, long time)
            throws Exception {
        List<AdPlanStat> poList = new ArrayList<>();
        //通过Jsoup解析响应内容
        //页面解析分为两部分，存在两个数据源，
        //data-list包含着分组和计划的id、name
        //通过map<String,String>作为关联关系存储id和name，在解析data-rets时再同时补全映射类
        //data-rets包含着计划的点击量、曝光量、消耗
        Document doc = Jsoup.parse(content);
        //通过id获取投放计划的id和name
        Element eleId = doc.getElementById("planIds");
        //获取JSON对象
        String idsJson = eleId.attr("data-list");
        //投放组的id和name的集合
        JsonArray obj = StringUtil.jsonParser.parse(idsJson).getAsJsonArray();
        //初始化map对象存储组和计划的id、name
        HashMap<String, String> groupPlanMap = new HashMap<>();
        if (0 != obj.size()) {
            JsonArray idList = obj.get(0).getAsJsonObject().get("children").getAsJsonArray();
            if (0 != idList.size()) {
                for (JsonElement item : idList) {
                    JsonObject order = item.getAsJsonObject();
                    //投放组id
                    String groupId = order.get("id").getAsInt() + "";
                    //投放组名称
                    String groupName = order.get("name").getAsString();
                    //通过投放组获取子单元投放计划
                    JsonArray childList = order.get("children").getAsJsonArray();
                    if (0 != childList.size()) {
                        for (JsonElement child : childList) {
                            JsonObject childVO = child.getAsJsonObject();
                            //投放计划id
                            String mPlanId = childVO.get("id").getAsInt() + "";
                            //投放计划名称
                            String planName = childVO.get("name").getAsString();
                            //拼接key
                            String key = groupName + "#" + planName;
                            //拼接value
                            String value = groupId + "#" + mPlanId;
                            groupPlanMap.put(key, value);
                        }
                    }
                }
            }
        }

        //通过id获取到投放计划列表（不含id）
        Element element = doc.getElementById("list");
        //获取投放计划json对象
        String planJson = element.attr("data-rets");
        //反序列化json对象
        JsonArray planList = StringUtil.jsonParser.parse(planJson).getAsJsonArray();
        if (0 != planList.size()) {
            for (JsonElement item : planList) {
                JsonObject order = item.getAsJsonObject();
                //日期筛选
                String dateStr = order.get("日期").getAsString();
                Date date = new SimpleDateFormat("MM/dd/yyyy").parse(dateStr);
                String reportDate = DateUtil.getTimeString(time, "MM/dd/yyyy");
                Date currentDate = new SimpleDateFormat("MM/dd/yyyy").parse(reportDate);
                if (currentDate.compareTo(date) == 0) {
                    AdPlanStat reportPO = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(),
                            account.getId());
                    poList.add(reportPO);
                    //分组名称
                    String groupName = order.get("计划名称").getAsString();
                    // 计划名称
                    String planName = order.get("广告名称").getAsString();
                    // 点击数
                    int clickNum = StringUtil.getJsonIntValue(order, "点击量(次)");
                    // 消耗
                    String costStr = order.get("消费(元)").getAsString();
                    double cost = StringUtil.convertDouble(costStr, 0.00);
                    // 曝光
                    int showNum = StringUtil.getJsonIntValue(order, "曝光量(次)");
                    //获取分组id#计划id
                    String ids = groupPlanMap.get(groupName + "#" + planName);
                    //如果没有之前没有爬取到分组和计划的id及name字段，HashMap根据Name获取不到ID的值
                    //跳过，只存name
                    if (null != ids) {
                        String[] idArray = ids.split("#");
                        //分组id
                        reportPO.setGroupId(idArray[0]);
                        //计划id
                        reportPO.setmPlanId(idArray[1]);
                    }
                    reportPO.setGroupName(groupName);
                    reportPO.setPlanName(planName);
                    reportPO.setClickNum(clickNum);
                    //数据转换
                    reportPO.setCost(new BigDecimal(cost).multiply(new BigDecimal(100))
                            .setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
                    reportPO.setShowNum(showNum);
                }
            }
        }
        return poList;
    }

    /**
     * Jsoup解析响应内容获取账户余额
     *
     * @param content
     * @param account
     * @param time
     * @return
     * @throws Exception
     */
    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time) throws Exception {
        AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                account.getId());
        //jsoup解析html内容
        Document doc = Jsoup.parse(content);
        //获取页面的table标签内容
        Elements elements = doc.select(".table-overview>tbody>tr");
        if (!elements.isEmpty()) {
            for (Element element : elements) {
                String thLabel = element.select("th").text();
                String tdValue = element.select("td").text();
                if (BALANCE_LABEL.equals(thLabel)) {
                    String balanceStr = tdValue.replace("￥", "").replace(",", "").trim();
                    int balance = StringUtil.convertBigDecimal(balanceStr, 100, 0).intValue();
                    accountStat.setBalance(balance);
                }
            }
        }
        return accountStat;
    }

    /**
     * http请求头信息
     *
     * @param cookie
     * @return
     */
    private Map<String, String> getHeaderMap(String cookie) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        headerMap.put("Accept-Encoding", "gzip, deflate");
        headerMap.put("Accept-Language", "en,zh-CN;q=0.8,zh;q=0.6");
        headerMap.put("Connection", "keep-alive");
        headerMap.put("Cookie", cookie);
        headerMap.put("Host", "lx.yidianzixun.com");
        headerMap.put("User-Agent:", "ozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.86 Safari/537.36");
        return headerMap;
    }

}
