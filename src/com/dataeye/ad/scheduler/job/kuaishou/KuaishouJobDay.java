package com.dataeye.ad.scheduler.job.kuaishou;

import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.scheduler.job.CrawlerBaseExecutor;
import com.dataeye.ad.util.DateUtil;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * @author lingliqi
 * @date 2017-11-30 11:49
 */
public class KuaishouJobDay implements Job {
    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        CrawlerBaseExecutor.addJob(new KuaishouJob(DateUtil.unixTimestamp000() - Constant.ONEDAY));
    }


}
