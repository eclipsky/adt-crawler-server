package com.dataeye.ad.scheduler.job.sinafuyiold;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.scheduler.job.CrawlerBaseExecutor;
import com.dataeye.ad.util.DateUtil;

/**
 * 新浪扶翼按天统计爬虫
 * Created by huangzehai on 2017/3/30.
 */
public class SinaFuyiOldJobDay implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        CrawlerBaseExecutor.addJob(new SinaFuyiOldJob(DateUtil.unixTimestamp000() - Constant.ONEDAY));
    }
}
