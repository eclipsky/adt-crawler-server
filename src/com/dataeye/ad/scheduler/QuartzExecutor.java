package com.dataeye.ad.scheduler;

import org.quartz.JobDataMap;
import org.quartz.SchedulerException;

import com.dataeye.ad.scheduler.job.baiduinfo.BaiduInfoJob;
import com.dataeye.ad.scheduler.job.baiduinfo.BaiduInfoJobDay;
import com.dataeye.ad.scheduler.job.baidusearch.BaiduSearchJob;
import com.dataeye.ad.scheduler.job.baidusearch.BaiduSearchJobDay;
import com.dataeye.ad.scheduler.job.bihe.BiheJob;
import com.dataeye.ad.scheduler.job.bihe.BiheJobDay;
import com.dataeye.ad.scheduler.job.fenghuang.FengHuangJob;
import com.dataeye.ad.scheduler.job.fenghuang.FengHuangJobDay;
import com.dataeye.ad.scheduler.job.gdt.GDTJob;
import com.dataeye.ad.scheduler.job.gdt.GDTJobDay;
import com.dataeye.ad.scheduler.job.iqiyi.IQiYiJob;
import com.dataeye.ad.scheduler.job.iqiyi.IQiYiJobDay;
import com.dataeye.ad.scheduler.job.mta.MtaJob;
import com.dataeye.ad.scheduler.job.mta.MtaJobDay;
import com.dataeye.ad.scheduler.job.netease.NeteaseJob;
import com.dataeye.ad.scheduler.job.netease.NeteaseJobDay;
import com.dataeye.ad.scheduler.job.neteaseold.NeteaseOldJob;
import com.dataeye.ad.scheduler.job.neteaseold.NeteaseOldJobDay;
import com.dataeye.ad.scheduler.job.pinyou.PinyouJob;
import com.dataeye.ad.scheduler.job.pinyou.PinyouJobDay;
import com.dataeye.ad.scheduler.job.sinafuyiold.SinaFuyiOldJob;
import com.dataeye.ad.scheduler.job.sinafuyiold.SinaFuyiOldJobDay;
import com.dataeye.ad.scheduler.job.sm.SmJob;
import com.dataeye.ad.scheduler.job.sm.SmJobDay;
import com.dataeye.ad.scheduler.job.sogousearch.SogouSearchJob;
import com.dataeye.ad.scheduler.job.sogousearch.SogouSearchJobDay;
import com.dataeye.ad.scheduler.job.sohuhuisuan.SohuHuisuanJob;
import com.dataeye.ad.scheduler.job.sohuhuisuan.SohuHuisuanJobDay;
import com.dataeye.ad.scheduler.job.toutiao.ToutiaoJob;
import com.dataeye.ad.scheduler.job.toutiao.ToutiaoJobDay;
import com.dataeye.ad.scheduler.job.tui.TuiQQJob;
import com.dataeye.ad.scheduler.job.tui.TuiQQJobDay;
import com.dataeye.ad.scheduler.job.uc.UCJob;
import com.dataeye.ad.scheduler.job.uc.UCJobDay;
import com.dataeye.ad.scheduler.job.youku.YoukuJob;
import com.dataeye.ad.scheduler.job.youku.YoukuJobDay;

public class QuartzExecutor {

	/**
	 * pinyou 爬虫定时任务
	 *
	 * @throws SchedulerException
	 */
	public static void pinyou() throws SchedulerException {
		System.out.println("===QuartzExecutor pinyou Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("PinyouJob", PinyouJob.class, nonUseMap, "0 0/10 * * * ?", mainGroup);
		QuartzManager.start();
	}

	/**
	 * 天任务，补全前天数据
	 */
	public static void pinyouDay() throws SchedulerException {
		System.out.println("===QuartzExecutor pinyouDay Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("PinyouJobDay", PinyouJobDay.class, nonUseMap, "0 0 6 * * ?", mainGroup);
		QuartzManager.start();
	}

	/**
	 * toutiao 爬虫定时任务
	 *
	 * @throws SchedulerException
	 */
	public static void toutiao() throws SchedulerException {
		System.out.println("===QuartzExecutor toutiao Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("ToutiaoJob", ToutiaoJob.class, nonUseMap, "0 0/10 * * * ?", mainGroup);
		QuartzManager.start();
	}

	public static void toutiaoDay() throws SchedulerException {
		System.out.println("===QuartzExecutor toutiaoDay Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("ToutiaoJobDay", ToutiaoJobDay.class, nonUseMap, "00 07 06 * * ?", mainGroup);
		QuartzManager.start();
	}

	/**
	 * 百度搜索 爬虫定时任务
	 *
	 * @throws SchedulerException
	 */
	public static void baiduSearch() throws SchedulerException {
		System.out.println("===QuartzExecutor baiduSearch Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("BaiduSearchJob", BaiduSearchJob.class, nonUseMap, "0 0/10 * * * ?", mainGroup);
		QuartzManager.start();
	}

	public static void baiduSearchDay() throws SchedulerException {
		System.out.println("===QuartzExecutor baiduSearchDay Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("BaiduSearchJobDay", BaiduSearchJobDay.class, nonUseMap, "0 0 6 * * ?", mainGroup);
		QuartzManager.start();
	}

	/**
	 * 百度信息流 爬虫定时任务
	 *
	 * @throws SchedulerException
	 */
	public static void baiduInfo() throws SchedulerException {
		System.out.println("===QuartzExecutor baiduInfo Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("BaiduInfoJob", BaiduInfoJob.class, nonUseMap, "0 0/10 * * * ?", mainGroup);
		QuartzManager.start();
	}

	public static void baiduInfoDay() throws SchedulerException {
		System.out.println("===QuartzExecutor baiduSearchDay Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("BaiduInfoJobDay", BaiduInfoJobDay.class, nonUseMap, "0 0 6 * * ?", mainGroup);
		QuartzManager.start();
	}

	/**
	 * tuiqq 爬虫定时任务
	 *
	 * @throws SchedulerException
	 */
	public static void tuiqq() throws SchedulerException {
		System.out.println("===QuartzExecutor tuiQQ Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("TuiQQJob", TuiQQJob.class, nonUseMap, "0 0/10 * * * ?", mainGroup);
		QuartzManager.start();
	}

	public static void tuiqqDay() throws SchedulerException {
		System.out.println("===QuartzExecutor tuiQQDay Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("TuiQQJobDay", TuiQQJobDay.class, nonUseMap, "0 0 6 * * ?", mainGroup);
		QuartzManager.start();
	}

	/**
	 * mta 爬虫定时任务
	 *
	 * @throws SchedulerException
	 */
	public static void mta() throws SchedulerException {
		System.out.println("===QuartzExecutor mta Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("MtaJob", MtaJob.class, nonUseMap, "0 0/10 * * * ?", mainGroup);
		QuartzManager.start();
	}

	public static void mtaDay() throws SchedulerException {
		System.out.println("===QuartzExecutor mtaDay Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("MtaJobDay", MtaJobDay.class, nonUseMap, "0 0 6 * * ?", mainGroup);
		QuartzManager.start();
	}

	public static void bihe() throws SchedulerException {
		System.out.println("===QuartzExecutor bihe Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("BiheJob", BiheJob.class, nonUseMap, "0 0/10 * * * ?", mainGroup);
		QuartzManager.start();
	}

	public static void biheDay() throws SchedulerException {
		System.out.println("===QuartzExecutor biheDay Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("BiheJobDay", BiheJobDay.class, nonUseMap, "0 0 6 * * ?", mainGroup);
		QuartzManager.start();
	}

	public static void iqiyi() throws SchedulerException {
		System.out.println("===QuartzExecutor iQiYi Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("iQiYiJob", IQiYiJob.class, nonUseMap, "0 0/10 * * * ?", mainGroup);
		QuartzManager.start();
	}

	public static void iqiyiDay() throws SchedulerException {
		System.out.println("===QuartzExecutor iQiYiDay Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("iQiYiJobDay", IQiYiJobDay.class, nonUseMap, "0 0 6 * * ?", mainGroup);
		QuartzManager.start();
	}

	public static void uc() throws SchedulerException {
		System.out.println("===QuartzExecutor uc Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("UCJob", UCJob.class, nonUseMap, "0 0/10 * * * ?", mainGroup);
		QuartzManager.start();
	}

	public static void ucDay() throws SchedulerException {
		System.out.println("===QuartzExecutor ucDay Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("UCJobDay", UCJobDay.class, nonUseMap, "0 0 6 * * ?", mainGroup);
		QuartzManager.start();
	}

	public static void fenghuang() throws SchedulerException {
		System.out.println("===QuartzExecutor fenghuang Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("FengHuangJob", FengHuangJob.class, nonUseMap, "0 0/10 * * * ?", mainGroup);
		QuartzManager.start();
	}

	public static void fenghuangDay() throws SchedulerException {
		System.out.println("===QuartzExecutor fenghuangDay Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("FengHuangJobDay", FengHuangJobDay.class, nonUseMap, "0 0 6 * * ?", mainGroup);
		QuartzManager.start();
	}

	public static void sohu() throws SchedulerException {
		System.out.println("===QuartzExecutor sohuJob Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("SohuJob", SohuHuisuanJob.class, nonUseMap, "0 0/10 * * * ?", mainGroup);
		QuartzManager.start();
	}

	public static void sohuDay() throws SchedulerException {
		System.out.println("===QuartzExecutor sohuJobDay Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("SohuJobDay", SohuHuisuanJobDay.class, nonUseMap, "0 0 6 * * ?", mainGroup);
		QuartzManager.start();
	}

	public static void netease() throws SchedulerException {
		System.out.println("===QuartzExecutor neteaseJob Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("NeteaseJob", NeteaseJob.class, nonUseMap, "0 0/10 * * * ?", mainGroup);
		QuartzManager.start();
	}

	public static void neteaseDay() throws SchedulerException {
		System.out.println("===QuartzExecutor neteaseJobDay Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("NeteaseJobDay", NeteaseJobDay.class, nonUseMap, "0 0 6 * * ?", mainGroup);
		QuartzManager.start();
	}

	public static void neteaseOld() throws SchedulerException {
		System.out.println("===QuartzExecutor neteaseOldJob Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("NeteaseOldJob", NeteaseOldJob.class, nonUseMap, "0 0/10 * * * ?", mainGroup);
		QuartzManager.start();
	}

	public static void neteaseOldDay() throws SchedulerException {
		System.out.println("===QuartzExecutor neteaseOldJobDay Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("NeteaseOldJobDay", NeteaseOldJobDay.class, nonUseMap, "0 0 6 * * ?", mainGroup);
		QuartzManager.start();
	}

	public static void gdt() throws SchedulerException {
		System.out.println("===QuartzExecutor gdtJob Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("gdtJob", GDTJob.class, nonUseMap, "0 0/10 * * * ?", mainGroup);
		QuartzManager.start();
	}

	public static void gdtDay() throws SchedulerException {
		System.out.println("===QuartzExecutor gdtJobDay Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("GDTJobDay", GDTJobDay.class, nonUseMap, "0 0 6 * * ?", mainGroup);
		QuartzManager.start();
	}

	/**
	 * 神马搜索实时分析爬取任务
	 *
	 * @throws SchedulerException
	 */
	public static void sm() throws SchedulerException {
		System.out.println("===QuartzExecutor smJob Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("smJob", SmJob.class, nonUseMap, "0 0/10 * * * ?", mainGroup);
		QuartzManager.start();
	}

	/**
	 * 神马搜索按天统计爬取任务
	 *
	 * @throws SchedulerException
	 */
	public static void smDay() throws SchedulerException {
		System.out.println("===QuartzExecutor smJobDay Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("smJobDay", SmJobDay.class, nonUseMap, "0 0 6 * * ?", mainGroup);
		QuartzManager.start();
	}

	/**
	 * 新浪扶翼通实时分析爬取任务
	 *
	 * @throws SchedulerException
	 */
	public static void sinaFuyi() throws SchedulerException {
		System.out.println("===QuartzExecutor sinaJob Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("sinaFuyiJob", SinaFuyiOldJob.class, nonUseMap, "0 0/10 * * * ?", mainGroup);
		QuartzManager.start();
	}

	/**
	 * 新浪扶翼按天统计爬取任务
	 *
	 * @throws SchedulerException
	 */
	public static void sinaFuyiDay() throws SchedulerException {
		System.out.println("===QuartzExecutor sinaFuyiJobDay Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("sinaFuyiJobDay", SinaFuyiOldJobDay.class, nonUseMap, "0 0 6 * * ?", mainGroup);
		QuartzManager.start();
	}

	public static void sogouSearch() throws SchedulerException {
		System.out.println("===QuartzExecutor sogouSearchJob Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("sogouSearchJob", SogouSearchJob.class, nonUseMap, "0 0/10 * * * ?", mainGroup);
		QuartzManager.start();
	}

	public static void sogouSearchDay() throws SchedulerException {
		System.out.println("===QuartzExecutor sogouSearchJobDay Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("sogouSearchJobDay", SogouSearchJobDay.class, nonUseMap, "0 0 6 * * ?", mainGroup);
		QuartzManager.start();
	}

	public static void youku() throws SchedulerException {
		System.out.println("===QuartzExecutor youkuJob Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("youkuJob", YoukuJob.class, nonUseMap, "0 0/10 * * * ?", mainGroup);
		QuartzManager.start();
	}

	public static void youkuDay() throws SchedulerException {
		System.out.println("===QuartzExecutor youkuJobDay Run==========");
		long id = Thread.currentThread().getId();
		String mainGroup = "Main_Group_" + id;
		JobDataMap nonUseMap = new JobDataMap();
		// 在这里依次加入定时任务
		QuartzManager.addCronJob("youkuJobDay", YoukuJobDay.class, nonUseMap, "0 0 6 * * ?", mainGroup);
		QuartzManager.start();
	}

}
