package com.dataeye.ad.scheduler.job.xunlei;

import com.dataeye.ad.constant.Constant.*;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.scheduler.job.CrawlerFilter;
import com.dataeye.ad.util.*;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lingliqi
 * @date 2018-02-07 9:58
 */
public class XunleiJob extends CrawlerBaseJob {


    private final static CrawlerPlatform platform = CrawlerPlatform.XUNLEI;

    private final static Logger logger = ServerUtil.getLogger(platform);


    public XunleiJob() {
        this(DateUtil.unixTimestamp000());
    }

    public XunleiJob(long time) {
        super(time, platform);
    }

    public XunleiJob(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }

    private final static String accountUrl = "http://rest.biz.xunlei.com/console/user/statistics";

    private final static String planUrl = "http://rest.biz.xunlei.com/console/adverts?page={0}&date%5B%5D={1}&date%5B%5D={2}";

    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        String token = CookieUtil.getCookieValue(cookie, "advertiser-jwt-token");
        String content = HttpUtil.getContentByGet(accountUrl, getHeaderMapper(cookie, token));
        logger.info(content);
        AdAccountStat accountStat = parseAccount(content, account, time);
        DataProcessor.processAccountDay(accountStat);
    }

    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        String token = CookieUtil.getCookieValue(cookie, "advertiser-jwt-token");
        String statDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        int page = 1;
        int pageSize = 15;
        PageModel pageModel = new PageModel(page, pageSize);
        String url = HttpUtil.urlParamReplace(planUrl, page + "", statDate, statDate);
        String content = HttpUtil.getContentByGet(url, getHeaderMapper(cookie, token));
        logger.info(content);
        List<AdPlanStat> poList = new ArrayList<>();
        List<AdPlanStat> tempList = parsePlan(content, account, time, pageModel);
        poList.addAll(tempList);
        int totalNum = pageModel.getTotalNum();
        totalNum -= pageSize;
        while (totalNum > 0) {
            page++;
            // 请求
            url = HttpUtil.urlParamReplace(planUrl, page + "", statDate, statDate);
            content = HttpUtil.getContentByGet(url, getHeaderMapper(cookie, token));
            logger.info(content);
            // 解析
            tempList = parsePlan(content, account, time, pageModel);
            // 入库
            poList.addAll(tempList);
            totalNum -= pageSize;
        }
        DataProcessor.processPlanDay(poList);

    }

    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time) throws Exception {
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        String balance = resultJson.get("balance").getAsString();
        AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                account.getId());
        accountStat.setBalance(StringUtil.convertInt(balance, 0));
        return accountStat;
    }


    private List<AdPlanStat> parsePlan(String content, AdMediumAccount account, long time, PageModel pageModel) throws Exception {
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        int totalNum = resultJson.get("total").getAsInt();
        pageModel.setTotalNum(totalNum);
        JsonArray dataJson = resultJson.get("data").getAsJsonArray();
        List<AdPlanStat> poList = new ArrayList<>();
        if (null != dataJson && !dataJson.isJsonNull()) {
            for (JsonElement element : dataJson) {
                JsonObject order = element.getAsJsonObject();
                String mPlanId = order.get("id").getAsString();
                String planName = order.get("name").getAsString();
                JsonObject groupJson = order.get("group").getAsJsonObject();
                String planGroupId = groupJson.get("id").getAsString();
                String planGroupName = groupJson.get("name").getAsString();
                JsonObject bidJson = order.get("bid").getAsJsonObject();
                String bidStr = bidJson.get("unit_price").getAsString();
                int bid = StringUtil.convertBigDecimal(bidStr, 100, 0).intValue();
                // "1-1" CPC竞价
                String planBidStrategy = bidJson.get("billing_type").getAsString();
                JsonObject reportJson = order.get("data").getAsJsonObject();
                int clickNum = reportJson.get("clicks").getAsInt();
                int showNum = reportJson.get("impressions").getAsInt();
                int cost = StringUtil.convertBigDecimal(reportJson.get("costs").getAsString(), 100, 0).intValue();
                AdPlanStat adPlanStat = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(), account.getId());
                poList.add(adPlanStat);
                adPlanStat.setmPlanId(mPlanId);
                adPlanStat.setPlanName(planName);
                adPlanStat.setGroupId(planGroupId);
                adPlanStat.setGroupName(planGroupName);
                adPlanStat.setBid(bid);
                adPlanStat.setPlanBidStrategy(planBidStrategy);
                adPlanStat.setClickNum(clickNum);
                adPlanStat.setShowNum(showNum);
                adPlanStat.setCost(cost);
            }
        }
        return poList;

    }


    private Map<String, String> getHeaderMapper(String cookie, String token) {
        Map<String, String> headMap = new HashMap<>();
        headMap.put("cookie", cookie);
        headMap.put("Accept", "application/json, text/plain, */*");
        headMap.put("Accept-Encoding", "gzip, deflate");
        headMap.put("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.6,en;q=0.4");
        headMap.put("Connection", "keep-alive");
        headMap.put("Host", "rest.biz.xunlei.com");
        headMap.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36");
        headMap.put("Authorization", "Bearer " + token);
        return headMap;
    }

}
