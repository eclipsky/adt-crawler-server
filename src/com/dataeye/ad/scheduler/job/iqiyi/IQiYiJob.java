package com.dataeye.ad.scheduler.job.iqiyi;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.scheduler.job.CrawlerFilter;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;

import com.dataeye.ad.common.ServerException;
import com.dataeye.ad.constant.Constant.CrawlerPlatform;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.DBQuery;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.util.CookieUtil;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class IQiYiJob extends CrawlerBaseJob {

    private final static CrawlerPlatform platform = CrawlerPlatform.IQIYI;

    private final static Logger logger = ServerUtil.getLogger(platform);

    private static final String planViewColumn = "https://tuiguang.iqiyi.com/userViewedColumn/save";

    private static final String planUrlToday = "https://tuiguang.iqiyi.com/orderPlan/list?format=&max={0}&offset={1}";

    private static final String planGroupUrlPast = "https://tuiguang.iqiyi.com/report/index";

    private static final String planUrlPast = "https://tuiguang.iqiyi.com/report/fetch";

    private final static String accountUrl = "https://tuiguang.iqiyi.com/settlement/account";

    private final static Map<String, Integer> planIdCache = new ConcurrentHashMap<String, Integer>();

    public IQiYiJob() {
        this(DateUtil.unixTimestamp000());
    }

    public IQiYiJob(long time) {
        super(time, platform);
    }

    public IQiYiJob(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }

    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = CookieUtil.getCookieByAccount(account);
        CookieUtil.isCookieValid(cookie);
        if (time >= DateUtil.unixTimestamp000()) {
            planDataToday(time, account);
        } else {
            planDataPast(time, account);
        }
    }

    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        // 请求
        String content = HttpUtil.getContentByHttpsGet(accountUrl, getHeaderMap(account.getCookie()));
        logger.info(content);
        // 解析
        AdAccountStat accountStat = parseAccount(content, account, time);
        // 入库
        DataProcessor.processAccountDay(accountStat);
    }

    /**
     * <pre>
     * 今天实时数据
     * @author sam.xie
     * @date May 17, 2017 5:47:52 PM
     * @param time
     * @param account
     * @throws Exception
     */
    private void planDataToday(long time, AdMediumAccount account) throws Exception {
        // 实时报价页面可以自定义列，需要先设置才能确定字段顺序
        HttpUtil.getContentByHttpsPost(planViewColumn, getHeaderMap(account.getCookie()), getFormDataPlanColumns());
        int max = 100;
        int offset = 0;
        PageModel pageModel = new PageModel();
        // 请求
        String content = HttpUtil.getContentByHttpsGet(HttpUtil.urlParamReplace(planUrlToday, max + "", offset + ""),
                getHeaderMap(account.getCookie()));
        logger.info(content);
        // 解析
        List<AdPlanStat> poList = parsePlanToday(content, account, time, pageModel);
        // 入库
        DataProcessor.processPlanDay(poList);
        int totalPages = pageModel.getTotalPages();
        // 分页
        int pageIndex = 1;
        while (pageIndex < totalPages) {
            content = HttpUtil.getContentByHttpsGet(
                    HttpUtil.urlParamReplace(planUrlToday, max + "", offset + (pageIndex * max) + ""),
                    getHeaderMap(account.getCookie()));
            // 解析
            poList = parsePlanToday(content, account, time, pageModel);
            // 入库
            DataProcessor.processPlanDay(poList);
            pageIndex++;
        }

    }

    /**
     * <pre>
     * 历史数据
     * @author sam.xie
     * @date May 17, 2017 5:48:06 PM
     * @param time
     * @param account
     * @throws Exception
     */
    private void planDataPast(long time, AdMediumAccount account) throws Exception {
        List<String> planGroupList = getPlanGroup(account);
        String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        List<AdPlanStat> poList = new ArrayList<>();
        for (String id : planGroupList) {
            // 请求
            String content = HttpUtil.getContentByHttpsPost(planUrlPast, getPostHeaderMap(account.getCookie()),
                    getFormDataAdList(reportDate, reportDate, id));
            // 解析
            List<AdPlanStat> tempList = parsePlanPast(content, account, time);
            if (!tempList.isEmpty()) {
                logger.info(content);
                poList.addAll(tempList);
            }
        }
        // 入库
        DataProcessor.processPlanDay(poList);
    }
    /**
     * 获取历史广告系列列表
     *
     * @param account
     * @return
     * @throws Exception
     */
    private List<String> getPlanGroup(AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        String content = HttpUtil.getContentByGet(planGroupUrlPast, getHeaderMap(cookie));
        logger.info(content);
        return parsePlanGroupPast(content);
    }

    /**
     * 解析历史广告系列列表
     *
     * @param content
     * @return
     * @throws Exception
     */
    private List<String> parsePlanGroupPast(String content) throws Exception {
        Document doc = Jsoup.parse(content);
        Elements tr = doc.select(".qx-table-info > tbody > tr:first-child");
        if (null == tr || tr.isEmpty()) {
            return null;
        }
        Elements elements = tr.select("#orderGroupId > option");
        if (null == elements || elements.isEmpty()) {
            return null;
        }
        List<String> result = new ArrayList<>();
        for (Element element : elements) {
            String planGroupId = element.attr("value");
            if (null != planGroupId) {
                result.add(planGroupId);
            }
        }
        return result;
    }

    private List<AdPlanStat> parsePlanToday(String content, AdMediumAccount account, long time, PageModel pageModel)
            throws ServerException {
        List<AdPlanStat> poList = new ArrayList<AdPlanStat>();
        Document doc = Jsoup.parse(content);
        Element main = doc.select("div.qx-main-sheet").get(0);
        // 计划列表
        Elements planRows = main.select("table tr");
        for (int i = 1; i < planRows.size(); i++) { // 跳过表头
            Elements planRow = planRows.get(i).select("td");
            String mPlanId = planRow.get(1).text();
            String planName = planRow.get(2).attr("title");
            String planStatus = planRow.get(3).text();
            String planBid = planRow.get(5).text();
            String planBidStrategy = planRow.get(6).text();
            String groupName = planRow.get(7).text();
            String showNum = planRow.get(9).text();
            String clickNum = planRow.get(10).text();
            String cost = planRow.get(16).text();
            Elements elements = planRow.get(17).select("a");
            String switchStr = elements.get(0).text();
            Integer planSwitch = null;
            //如果状态为“广告系列已删除” 默认将开关设为空
            if (Objects.equals(switchStr, "开启")) {
                planSwitch = Constant.PlanSwitch.OFF;
            } else if (Objects.equals(switchStr, "暂停")) {
                planSwitch = Constant.PlanSwitch.ON;
            }
            AdPlanStat planPO = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(), account.getId());
            poList.add(planPO);
            planPO.setPlanName(planName);
            planPO.setmPlanId(mPlanId);
            planPO.setGroupName(groupName);
            planPO.setPlanStatus(planStatus);
            planPO.setPlanBidStrategy(planBidStrategy);
            planPO.setBid(StringUtil.convertBigDecimal(planBid, 100, 0).intValue());
            planPO.setShowNum(StringUtil.convertInt(showNum, 0));
            planPO.setCost(StringUtil.convertBigDecimal(cost, 100, 0).intValue());
            planPO.setClickNum(StringUtil.convertInt(clickNum, 0));
            planPO.setPlanSwitch(planSwitch);
        }
        // 解析分页
        Elements pages = main.select("div.text-center a.step");
        int totalPage = pages.size() + 1;
        pageModel.setTotalPages(totalPage);
        return poList;
    }

    private List<AdPlanStat> parsePlanPast(String content, AdMediumAccount account, long time) throws ServerException {
        List<AdPlanStat> poList = new ArrayList<AdPlanStat>();
        JsonArray jsonArray = StringUtil.jsonParser.parse(content).getAsJsonObject().get("data").getAsJsonArray();
        Iterator<JsonElement> iter = jsonArray.iterator();
        while (iter.hasNext()) {
            JsonElement element = iter.next();
            if (element.isJsonObject()) {
                JsonObject jsonData = element.getAsJsonObject();
                String planName = StringUtil.getJsonStrValue(jsonData, "order_plan_id");
                String groupName = StringUtil.getJsonStrValue(jsonData, "order_group_id");
                int click = StringUtil.getJsonIntValue(jsonData, "charged_clicks");
                int cost = (int) (100 * StringUtil.getJsonFloatValue(jsonData, "charged_fees"));
                int showNum = StringUtil.getJsonIntValue(jsonData, "charged_impressions");
                AdPlanStat planPO = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(),
                        account.getId());
                /**
                 * <pre/>
                 * 爱奇艺投放管理可以获取mPlanId，groupName等信息，而报表历史数据中没有这些字段，直接使用会破坏入库一致性，
                 * 这里先通过数据库补全mPlanId和相关字段才能保证后台数据准确性
                 */
                int planId = getPlanId(account.getCompanyId(), account.getPlatformId(), account.getId(), planName,
                        groupName);
                planPO.setPlanId(planId);
                if (planId <= 0) {
                    continue;
                }
                poList.add(planPO);
                planPO.setPlanName(planName);
                planPO.setGroupName(groupName);
                planPO.setClickNum(click);
                planPO.setCost(cost);
                planPO.setShowNum(showNum);
            }
        }
        return poList;
    }

    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time) throws Exception {
        Document doc = Jsoup.parse(content);
        String balanceStr = doc.getElementsByClass("qx-fee").get(0).html();
        int balance = StringUtil.convertBigDecimal(balanceStr, 100, 0).intValue();
        AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                account.getId());
        accountStat.setBalance(balance);
        return accountStat;
    }

    public List<NameValuePair> getFormDataPlanColumns() {
        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair(
                "columns",
                "name,operation,id,orderGroupName,status,adType,feeType,price,promotionGoal,impression,click,trueview,ctr,averageCpm,averageCpc,averageCpv,expense"));
        pairs.add(new BasicNameValuePair("type", "ORDERPLAN"));
        return pairs;
    }

    public List<NameValuePair> getFormDataAdList(String beginDate, String endDate, String planGroupId) {
        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("adEnv", "[]"));
        pairs.add(new BasicNameValuePair("adType", "[]"));
        pairs.add(new BasicNameValuePair("advertiser", "[]"));
        pairs.add(new BasicNameValuePair("creativeId", "[]"));
        pairs.add(new BasicNameValuePair("channel", "[]"));
        pairs.add(new BasicNameValuePair("dataColumn",
                "[\"IMPRESSIONS\",\"CLICKS\",\"TRUEVIEWS\",\"REVENUE\",\"CLICK_RATE\",\"AVG_CPM\",\"AVG_CPC\",\"AVG_CPV\"]"));
        pairs.add(new BasicNameValuePair("dynamicType", "[]"));
        pairs.add(new BasicNameValuePair("groupByItem", "[\"dt\",\"order_group_id\",\"order_plan_id\"]"));
        pairs.add(new BasicNameValuePair("orderGroupId", "[\"" + planGroupId + "\"]"));
        pairs.add(new BasicNameValuePair("orderPlanId", "[]"));
        pairs.add(new BasicNameValuePair("platform", "[]"));
        pairs.add(new BasicNameValuePair("startDate", beginDate));
        pairs.add(new BasicNameValuePair("endDate", endDate));

        return pairs;
    }

    private Map<String, String> getHeaderMap(String cookie) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Cookie", cookie);
        // request.setHeader("Host", "tuiguang.iqiyi.com");
        // request.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0");
        // request.setHeader("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
        // request.setHeader("Accept-Encoding", "gzip, deflate");
        // request.setHeader("Connection", "keep-alive");
        // request.setHeader("Content-Type", "application/x-www-form-urlencoded");
        return headerMap;
    }

    /**
     * Content-Type：application/x-www-form-urlencoded
     *
     * @param cookie
     * @return
     */
    private Map<String, String> getPostHeaderMap(String cookie) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Cookie", cookie);
        headerMap.put("Host", "tuiguang.iqiyi.com");
        headerMap.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0");
        headerMap.put("Accept", "*/*");
        headerMap.put("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
        headerMap.put("Accept-Encoding", "gzip, deflate");
        headerMap.put("Connection", "keep-alive");
        headerMap.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        return headerMap;
    }

    private int getPlanId(int companyId, int mediumId, int accountId, String planName, String groupName) {
        String cachedKey = companyId + "-" + mediumId + "-" + accountId + "-" + planName + "-" + groupName;
        Integer planId = planIdCache.get(cachedKey);
        if (planId == null || planId <= 0) {
            planId = DBQuery.getPlanIdForIQiYi(companyId, mediumId, accountId, planName, groupName);
            if (planId > 0) {
                planIdCache.put(cachedKey, planId);
            }
        }
        return planId;
    }
}
