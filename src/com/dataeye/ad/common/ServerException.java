package com.dataeye.ad.common;

/**
 * 抽象异常类
 *
 * @author sam.xie
 * @version 1.0
 * @date Feb 10, 2017 2:18:10 PM
 */
public class ServerException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ServerException(ExceptionEnum exception, String msg) {
        super(exception.getMsg() + " -> " + msg);
    }

    public ServerException(ExceptionEnum exception) {
        super(exception.getMsg());
    }

    public ServerException(String msg) {
        super(msg);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

}
