package com.dataeye.ad.scheduler.job.dongqiudi;

import com.dataeye.ad.constant.Constant.*;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.scheduler.job.CrawlerFilter;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;

import java.util.*;

/**
 * Created by VitAs on 2017/8/11.
 */
public class DongqiudiJob extends CrawlerBaseJob {

    private final static CrawlerPlatform platform = CrawlerPlatform.DONGQIUDI;

    private final static Logger logger = ServerUtil.getLogger(platform);

    private final static String accountUrl = "https://adm.dongqiudi.com/admin/statisticshomes?main_type=1";

    private final static String planUrl = "https://adm.dongqiudi.com/admin/units/bid?p={0}";

    private final static String reprotUrl = "https://adm.dongqiudi.com/admin/statisticsunits?main_type=23&" +
            "startTime={0}&endTime={1}&p={2}";

    public DongqiudiJob() {
        this(DateUtil.unixTimestamp000());
    }

    public DongqiudiJob(long time) {
        super(time, platform);
    }

    public DongqiudiJob(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }

    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        String content = HttpUtil.getContentByGet(accountUrl, getHeaderMap(cookie));
        logger.info(content);
        // 解析
        AdAccountStat accountStat = parseAccount(content, account, time);
        // 入库
        DataProcessor.processAccountDay(accountStat);

    }

    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        /*需要进行爬取时间判断，历史数据，则只请求报表接口
        * 待计划字典表入库逻辑修改后，再改动
        * 只有当实时爬取时，才需要请求投放计划单元接口，获取出价等信息
        */
        if (time >= DateUtil.unixTimestamp000()) {
            //通过报表后去有投放数据的投放单元
            Map<String, AdPlanStat> map = reportSpider(time, account);
            List<AdPlanStat> poList = new ArrayList<>();
            // 分页参数
            int pageId = 1;
            int pageSize = 20;
            PageModel pageModel = new PageModel(pageId, pageSize);
            //投放管理请求，获取所有投放计划的状态和出价
            String content = HttpUtil.getContentByGet(HttpUtil.urlParamReplace(planUrl, pageId + ""), getHeaderMap(cookie));
            logger.info(content);
            List<AdPlanStat> listOfOnePage = parsePlan(content, account, time, map, pageModel);
            poList.addAll(listOfOnePage);
            //处理分页
            int totalNum = pageModel.getTotalNum();
            totalNum -= pageSize;
            while (totalNum > 0) {
                pageId++;
                content = HttpUtil.getContentByGet(HttpUtil.urlParamReplace(planUrl, pageId + ""), getHeaderMap(cookie));
                logger.info(content);
                listOfOnePage = parsePlan(content, account, time, map, pageModel);
                poList.addAll(listOfOnePage);
                totalNum -= pageSize;
            }
            //入库
            DataProcessor.processPlanDay(poList);
        } else {
            List<AdPlanStat> poList = new ArrayList<>();
            //获取历史投放数据
            Map<String, AdPlanStat> map = reportSpider(time, account);
            if (null != map) {
                Collection<AdPlanStat> collection = map.values();
                Iterator<AdPlanStat> iterator = collection.iterator();
                while (iterator.hasNext()) {
                    poList.add(iterator.next());
                }
            }
            //入库
            DataProcessor.processPlanDay(poList);

        }


    }

    private Map<String, AdPlanStat> reportSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        Map<String, AdPlanStat> totalMap = new HashMap<>();
        //分页参数
        int pageId = 1;
        int pageSize = 20;
        PageModel pageModel = new PageModel(pageId, pageSize);
        String content = HttpUtil.getContentByGet(HttpUtil.urlParamReplace(
                reprotUrl, reportDate, reportDate, pageId + ""), getHeaderMap(cookie));
        logger.info(content);
        Map<String, AdPlanStat> partOfOnePageMap = parseReport(content, account, time, pageModel);
        totalMap.putAll(partOfOnePageMap);
        //处理分页
        int totalNum = pageModel.getTotalNum();
        totalNum -= pageSize;
        while (totalNum > 0) {
            pageId++;
            content = HttpUtil.getContentByGet(HttpUtil.urlParamReplace(
                    reprotUrl, reportDate, reportDate, pageId + ""), getHeaderMap(cookie));
            logger.info(content);
            partOfOnePageMap = parseReport(content, account, time, pageModel);
            totalMap.putAll(partOfOnePageMap);
            totalNum -= pageSize;
        }
        return totalMap;

    }

    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time)
            throws Exception {
        //解析Doc页面元素
        Document document = Jsoup.parse(content);
        Elements eles = document.getElementsByTag("table");
        AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                account.getId());
        if (eles.size() > 0) {
            for (Element element : eles) {
                String title = element.select("thead>tr>th:first-child").text();
                if (title.equals("账户余额(元)")) {
                    String balanceStr = element.select("tbody>tr>td:first-child").text();
                    int balance = StringUtil.convertBigDecimal(balanceStr, 100, 0).intValue();
                    accountStat.setBalance(balance);
                }
            }
        }
        return accountStat;
    }

    private List<AdPlanStat> parsePlan(String content, AdMediumAccount account, long time, Map<String, AdPlanStat> map, PageModel pageModel)
            throws Exception {
        List<AdPlanStat> poList = new ArrayList<>();
        //解析Doc页面元素
        Document document = Jsoup.parse(content);
        Elements eles = document.getElementsByTag("table");
        //分页参数获取
        Elements pageElement = document.getElementsByClass("box-footer");
        int totalNum = StringUtil.convertInt(pageElement.select("b").text(), 0);
        pageModel.setTotalNum(totalNum);
        if (eles.size() > 0) {
            Elements trs = eles.get(0).select(" tbody>tr");
            //由于包括表头，所以有数据时，至少大于1
            if (trs.size() > 1) {
                //删除表头
                trs.remove(0);
                for (Element element : trs) {
                    AdPlanStat reportPO = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(),
                            account.getId());
                    String mPlanId = element.select("td:nth-child(2)").text();
                    String planName = element.select("td:nth-child(3)").text();
                    String planStatus = element.select("td:nth-child(4)").text();
                    String planBidStrategy = element.select("td:nth-child(7)").text();
                    int bid = StringUtil.convertBigDecimal(element.select("td:nth-child(8)").text(), 100, 0).intValue();
                    reportPO.setmPlanId(mPlanId);
                    reportPO.setPlanName(planName);
                    reportPO.setPlanStatus(planStatus);
                    reportPO.setPlanBidStrategy(planBidStrategy);
                    reportPO.setBid(bid);
                    //补全AdPlanStat的消耗点击曝光
                    AdPlanStat adPlanStat = map.get(mPlanId);
                    if (null != adPlanStat) {
                        reportPO.setShowNum(adPlanStat.getShowNum());
                        reportPO.setClickNum(adPlanStat.getClickNum());
                        reportPO.setCost(adPlanStat.getCost());
                    }
                    poList.add(reportPO);
                }
            }
        }
        return poList;
    }


    private Map<String, AdPlanStat> parseReport(String content, AdMediumAccount account, long time, PageModel pageModel)
            throws Exception {
        Map<String, AdPlanStat> adPlanStatMap = new HashMap<>();
        //解析Doc页面元素
        Document document = Jsoup.parse(content);
        Elements eles = document.getElementsByTag("table");
        //分页参数获取
        Elements pageElement = document.getElementsByClass("box-footer");
        int totalNum = StringUtil.convertInt(pageElement.select("b").text(), 0);
        pageModel.setTotalNum(totalNum);
        if (eles.size() > 0) {
            Elements trs = eles.get(0).select(" tbody>tr");
            //由于包括表头，所以有数据时，至少大于1
            if (trs.size() > 1) {
                //删除表头
                trs.remove(0);
                for (Element element : trs) {
                    String mPlanId = element.select("td:first-child").text();
                    String planName = element.select("td:nth-child(2)").text();
                    int showNum = StringUtil.convertInt(element.select("td:nth-child(3)").text(), 0);
                    int click = StringUtil.convertInt(element.select("td:nth-child(4)").text(), 0);
                    int cost = StringUtil.convertBigDecimal(element.select("td:nth-child(6)").text(), 100, 0).intValue();
                    AdPlanStat reportPO = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(),
                            account.getId());
                    reportPO.setmPlanId(mPlanId);
                    reportPO.setPlanName(planName);
                    reportPO.setShowNum(showNum);
                    reportPO.setClickNum(click);
                    reportPO.setCost(cost);
                    adPlanStatMap.put(mPlanId, reportPO);
                }
            }
        }
        return adPlanStatMap;
    }

    private Map<String, String> getHeaderMap(String cookie) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        headerMap.put("Accept-Encoding", "gzip, deflate, br");
        headerMap.put("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.6,en;q=0.4");
        headerMap.put("Connection", "keep-alive");
        headerMap.put("Cookie", cookie);
        headerMap.put("Host", "adm.dongqiudi.com");
        headerMap.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.86 Safari/537.36");
        return headerMap;
    }
}
