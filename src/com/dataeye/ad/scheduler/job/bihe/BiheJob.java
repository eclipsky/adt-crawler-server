package com.dataeye.ad.scheduler.job.bihe;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dataeye.ad.scheduler.job.CrawlerFilter;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;

import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.constant.Constant.CrawlerPlatform;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.scheduler.job.bihe.model.BiheData;
import com.dataeye.ad.scheduler.job.bihe.model.BiheResponse;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;

public class BiheJob extends CrawlerBaseJob {

	private final static CrawlerPlatform platform = CrawlerPlatform.BEHE;

	private final static Logger logger = ServerUtil.getLogger(platform);

	private static final String planUrl = "http://uagain.behe.com/EffectReport/getERList/";

	public BiheJob() {
		this(DateUtil.unixTimestamp000());
	}

	public BiheJob(long time) {
		super(time, platform);
	}

	public BiheJob(long time, CrawlerFilter filter) {
		super(time, platform, filter);
	}

	@Override
	public void planSpider(long time, AdMediumAccount account) throws Exception {
		String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
		String cookie = account.getCookie();
		PageModel pageModel = new PageModel(1, 5);
		// 请求
		String content = HttpUtil.getContentByPost(planUrl, getHeaderMap(cookie),
				getFormDataAdList(reportDate, reportDate, pageModel));
		logger.info(content);
		// 解析
		List<AdPlanStat> poList = parse(content, account, time, pageModel);
		// 入库
		DataProcessor.processPlanDay(poList);
		// 分页
		int totalNum = pageModel.getTotalNum() - pageModel.getPageSize();
		while (totalNum > 0) {
			pageModel.setPageId(pageModel.getPageId() + 1);
			// 请求
			content = HttpUtil.getContentByPost(planUrl, getHeaderMap(cookie),
					getFormDataAdList(reportDate, reportDate, pageModel));
			logger.info(content);
			// 解析
			poList = parse(content, account, time, pageModel);
			// 入库
			DataProcessor.processPlanDay(poList);
			totalNum -= pageModel.getPageSize();
		}
	}

	@Override
	public void accountSpider(long time, AdMediumAccount account) throws Exception {
		// TODO Auto-generated method stub

	}

	private List<AdPlanStat> parse(String content, AdMediumAccount account, long time, PageModel pageModel)
			throws Exception {
		if (content.contains("\"status\":\"exit\"")) {
			ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, content);
		}
		List<AdPlanStat> poList = new ArrayList<AdPlanStat>();
		if (content.contains("\"result\":false")) {
			return poList;
		}
		BiheResponse response = Constant.GSON.fromJson(content, BiheResponse.class);
		pageModel.setTotalNum(StringUtil.convertInt(response.getTotalCount(), 0));
		List<BiheData> list = response.getResult();
		for (BiheData item : list) {
			String planName = item.getName();
			int click = StringUtil.convertInt(item.getClick(), 0);
			int cost = getCost(item.getSpend()); // 消耗转换为分
			int showNum = StringUtil.convertInt(item.getView(), 0);
			// if (cost > 0 || showNum > 0) {// 过滤完全无消耗无曝光数据，转移到入库阶段过滤
			AdPlanStat reportPO = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(), account.getId());
			poList.add(reportPO);
			reportPO.setPlanName(planName);
			reportPO.setClickNum(click);
			reportPO.setCost(cost); // 消耗转换为分
			reportPO.setShowNum(showNum);
			// }

		}
		return poList;
	}

	private int getCost(String spend) {
		try {
			return new BigDecimal(Double.parseDouble(spend) / 10000).setScale(0, BigDecimal.ROUND_HALF_UP).intValue(); // 这里转换为分
		} catch (Exception e) {
			return 0;
		}
	}

	public List<NameValuePair> getFormDataAdList(String startDate, String endDate, PageModel pageModel) {
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("page", pageModel.getPageId() + ""));
		pairs.add(new BasicNameValuePair("reportDate", startDate.replace('-', '/') + " - " + endDate.replace('-', '/')));
		pairs.add(new BasicNameValuePair("sortRule", "asc"));
		pairs.add(new BasicNameValuePair("sortField", "ad_order_id"));
		pairs.add(new BasicNameValuePair("listType", "order"));
		pairs.add(new BasicNameValuePair("reportType", "order"));
		pairs.add(new BasicNameValuePair("startDate", startDate));
		pairs.add(new BasicNameValuePair("endDate", endDate));
		pairs.add(new BasicNameValuePair("limit", pageModel.getPageSize() + ""));
		pairs.add(new BasicNameValuePair("conditions[search][putStatus]", "5"));
		pairs.add(new BasicNameValuePair("conditions[search][keyword]", ""));
		return pairs;
	}

	private Map<String, String> getHeaderMap(String cookie) {
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("Cookie", cookie);
		headerMap.put("Host", "uagain.behe.com");
		headerMap.put("Origin", "http://uagain.behe.com");
		headerMap.put("Referer", "http://uagain.behe.com/EffectReport/order/type/all");
		headerMap
				.put("User-Agent",
						"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
		headerMap.put("X-Requested-With", "XMLHttpRequest");
		return headerMap;
	}
}
