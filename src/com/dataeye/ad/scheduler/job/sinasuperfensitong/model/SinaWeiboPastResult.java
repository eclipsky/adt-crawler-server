package com.dataeye.ad.scheduler.job.sinasuperfensitong.model;

import java.util.List;

public class SinaWeiboPastResult {
	private int page;

	private int pageSize;

	private int pageTotal;

	private int total;

	private SinaWeiboPlanPastVo list;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageTotal() {
		return pageTotal;
	}

	public void setPageTotal(int pageTotal) {
		this.pageTotal = pageTotal;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public SinaWeiboPlanPastVo getList() {
		return list;
	}

	public void setList(SinaWeiboPlanPastVo list) {
		this.list = list;
	}

}
