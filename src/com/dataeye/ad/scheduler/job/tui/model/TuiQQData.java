package com.dataeye.ad.scheduler.job.tui.model;

import java.util.List;


public class TuiQQData {

	private TuiqqTotal total;

	private String update_time;

	private List<TuiQQItem> list;

	private TuiQQPage page_info;

	public TuiqqTotal getTotal() {
		return total;
	}

	public void setTotal(TuiqqTotal total) {
		this.total = total;
	}

	public String getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}

	public List<TuiQQItem> getList() {
		return list;
	}

	public void setList(List<TuiQQItem> list) {
		this.list = list;
	}

	public TuiQQPage getPage_info() {
		return page_info;
	}

	public void setPage_info(TuiQQPage page_info) {
		this.page_info = page_info;
	}

}

