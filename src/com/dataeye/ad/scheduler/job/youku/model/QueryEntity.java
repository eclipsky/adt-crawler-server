package com.dataeye.ad.scheduler.job.youku.model;

public class QueryEntity {
	private ExtLimit extLimit;
	private String startDate;
	private String endDate;

	// private String status;
	// private Integer oppsiteStatus;
	// private String castStartDateSta;
	// private String castStartDateEnd;
	// private String castEndDateSta;
	// private String castEndDateEnd;
	// private String outOfBudget;

	public ExtLimit getExtLimit() {
		return extLimit;
	}

	public void setExtLimit(ExtLimit extLimit) {
		this.extLimit = extLimit;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public class ExtLimit {
		private Integer start;
		private Integer limit;
		private String sort;
		private String dir;
		private Integer maxSize;
		private Integer currentPage;
		private Integer[] limitOptions;
		private Integer allPages;

		public Integer getStart() {
			return start;
		}

		public void setStart(Integer start) {
			this.start = start;
		}

		public Integer getLimit() {
			return limit;
		}

		public void setLimit(Integer limit) {
			this.limit = limit;
		}

		public String getSort() {
			return sort;
		}

		public void setSort(String sort) {
			this.sort = sort;
		}

		public String getDir() {
			return dir;
		}

		public void setDir(String dir) {
			this.dir = dir;
		}

		public Integer getMaxSize() {
			return maxSize;
		}

		public void setMaxSize(Integer maxSize) {
			this.maxSize = maxSize;
		}

		public Integer getCurrentPage() {
			return currentPage;
		}

		public void setCurrentPage(Integer currentPage) {
			this.currentPage = currentPage;
		}

		public Integer[] getLimitOptions() {
			return limitOptions;
		}

		public void setLimitOptions(Integer[] limitOptions) {
			this.limitOptions = limitOptions;
		}

		public Integer getAllPages() {
			return allPages;
		}

		public void setAllPages(Integer allPages) {
			this.allPages = allPages;
		}

	}

}
