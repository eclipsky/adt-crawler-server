package com.dataeye.ad.scheduler.job.toutiao.model;

import java.util.List;

public class ToutiaoTable {

	private ToutiaoPagination pagination;

	private List<ToutiaoAdData> ad_data;

	public ToutiaoPagination getPagination() {
		return pagination;
	}

	public void setPagination(ToutiaoPagination pagination) {
		this.pagination = pagination;
	}

	public List<ToutiaoAdData> getAd_data() {
		return ad_data;
	}

	public void setAd_data(List<ToutiaoAdData> ad_data) {
		this.ad_data = ad_data;
	}

}
