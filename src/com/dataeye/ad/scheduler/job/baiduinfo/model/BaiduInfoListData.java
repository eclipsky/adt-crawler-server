package com.dataeye.ad.scheduler.job.baiduinfo.model;
@Deprecated
public class BaiduInfoListData {

	private long unitid;

	private String unitname;

	private int shows;

	private int clks;

	private float paysum;

	private float bid;

	private String planid;	
	
	private String planname;	
	
	private int bidtype; // 1-CPC 3-OCPC
	
	private int pausestat; // 0-启动，1-暂停

	private String ocpc;
	
	public long getUnitid() {
		return unitid;
	}

	public void setUnitid(long unitid) {
		this.unitid = unitid;
	}

	public String getUnitname() {
		return unitname;
	}

	public void setUnitname(String unitname) {
		this.unitname = unitname;
	}

	public int getShows() {
		return shows;
	}

	public void setShows(int shows) {
		this.shows = shows;
	}

	public int getClks() {
		return clks;
	}

	public void setClks(int clks) {
		this.clks = clks;
	}

	public float getPaysum() {
		return paysum;
	}

	public void setPaysum(float paysum) {
		this.paysum = paysum;
	}

	public float getBid() {
		return bid;
	}

	public void setBid(float bid) {
		this.bid = bid;
	}

	public String getPlanid() {
		return planid;
	}

	public void setPlanid(String planid) {
		this.planid = planid;
	}

	public String getPlanname() {
		return planname;
	}

	public void setPlanname(String planname) {
		this.planname = planname;
	}

	public int getBidtype() {
		return bidtype;
	}

	public void setBidtype(int bidtype) {
		this.bidtype = bidtype;
	}

	public int getPausestat() {
		return pausestat;
	}

	public void setPausestat(int pausestat) {
		this.pausestat = pausestat;
	}

	public String getOcpc() {
		return ocpc;
	}

	public void setOcpc(String ocpc) {
		this.ocpc = ocpc;
	}
}
