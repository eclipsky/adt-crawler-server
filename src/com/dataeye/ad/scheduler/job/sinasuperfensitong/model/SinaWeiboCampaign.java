package com.dataeye.ad.scheduler.job.sinasuperfensitong.model;

public class SinaWeiboCampaign {
	private int id;

	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
