package com.dataeye.ad.scheduler.job.uc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dataeye.ad.scheduler.job.CrawlerFilter;

import com.dataeye.ad.util.*;
import org.slf4j.Logger;

import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.common.ServerException;
import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.constant.Constant.CrawlerPlatform;
import com.dataeye.ad.constant.Constant.PlanSwitch;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.scheduler.job.uc.model.UcDataItem;
import com.dataeye.ad.scheduler.job.uc.model.UcResponse;
import com.google.gson.JsonObject;

/**
 * UC爬虫任务
 *
 * @author sam.xie
 * @version 1.0
 * @date Feb 25, 2017 4:01:19 PM
 */
public class UCJob extends CrawlerBaseJob {

    private final static CrawlerPlatform platform = CrawlerPlatform.UC;

    private final static Logger logger = ServerUtil.getLogger(platform);

    private final static String PLAN_URL_SUFFIX = "a_c_d";

    private final static String planUrl = "https://e.uc.cn/ad/web/main/unit/list?" +
            "pageNo={0}&pageSize={1}&startDate={2}&endDate={3}&" +
            "state=-1&chargeType=-1&generalizeType=-1&adResourceId=-1&filterShowType=1&filterShowValue=0&" +
            "filterClickType=1&filterClickValue=0&filterConsumeType=1&filterConsumeValue=0&" +
            "filterCtrType=1&filterCtrValue=0&asc=true&userId={4}&a_c_d={5}";

    private final static String accountUrl = "https://e.uc.cn/ad/web/init/operator?userId={0}&_t=1492400659356&token=";

    private final static String timeUrl = "https://e.uc.cn/ad/web/init/time?userId=0&a_c_d={0}";

    public UCJob() {
        this(DateUtil.unixTimestamp000());
    }

    public UCJob(long time) {
        super(time, platform);
    }

    public UCJob(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }


    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        String reportDate = DateUtil.getTimeString(time, "yyyyMMdd");
        String cookie = account.getCookie();
        String userId = UCUtil.getUserId(account);
        int pageId = 1;
        int pageSize = 50;
        /**
         * ******************************************
         *          请求校验参数，通过XXTEA加密
         * Param1：当前时间+服务器时间容差（通过time接口）
         * Param2：UserId
         *
         * ******************************************
         */
        int diff = parseTime(cookie);
        String a_c_d = XXTEAUtil.encryptToBase64StringURLEncode((DateUtil.nowDate().getTime() + diff) + "", userId + "");
        // 请求
        String content = HttpUtil.getContentByHttpsGet(
                HttpUtil.urlParamReplace(planUrl, pageId + "", pageSize + "", reportDate, reportDate, userId, a_c_d),
                getHeaderMap(cookie, planUrl));
        logger.info(content);
        PageModel pageModel = new PageModel(1, pageSize);
        // 解析
        List<AdPlanStat> planList = parse(content, account, time, pageModel);
        // 入库
        DataProcessor.processPlanDay(planList);
        // 处理分页请求
        int totalNum = pageModel.getTotalNum();
        totalNum -= pageSize;
        while (totalNum > 0) {
            pageId += 1;
            // 请求
            content = HttpUtil.getContentByHttpsGet(
                    HttpUtil.urlParamReplace(planUrl, pageId + "", pageSize + "", reportDate, reportDate, userId, a_c_d),
                    getHeaderMap(cookie, planUrl));
            logger.info(content);
            // 解析
            planList = parse(content, account, time, pageModel);
            // 入库
            DataProcessor.processPlanDay(planList);
            totalNum -= pageSize;
        }
    }

    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        String userId = UCUtil.getUserId(account);
        // 请求
        String content = HttpUtil.getContentByHttpsGet(HttpUtil.urlParamReplace(accountUrl, userId),
                getHeaderMap(cookie, accountUrl));
        logger.info(content);
        // 解析
        AdAccountStat accountStat = parseAccount(content, account, time);
        DataProcessor.processAccountDay(accountStat);
    }

    private int parseTime(String cookie) throws Exception {
        long currentTime = DateUtil.nowDate().getTime();
        String a_c_d = XXTEAUtil.encryptToBase64StringURLEncode(currentTime + "", "0");
        String content = HttpUtil.getContentByHttpsGet(HttpUtil.urlParamReplace(timeUrl, a_c_d), getHeaderMap(cookie, timeUrl));
        logger.info(content);
        long time = StringUtil.jsonParser.parse(content).getAsJsonObject().get("data").getAsJsonObject().get("time").getAsLong();
        return new Long(time - currentTime).intValue();
    }
    /**
     * <pre>
     * 解析响应内容，封装成标准的PO对象
     * @author sam.xie
     * @date Feb 15, 2017 3:12:29 PM
     * @param content
     * @param account
     * @param time
     * @param pageModel
     * @throws ServerException
     */
    private List<AdPlanStat> parse(String content, AdMediumAccount account, long time, PageModel pageModel)
            throws ServerException {
        List<AdPlanStat> planList = new ArrayList<AdPlanStat>();
        UcResponse model = Constant.GSON.fromJson(content, UcResponse.class);
        int status = model.getStatus();
        if (status != 0) {
            ExceptionHandler.throwException(ExceptionEnum.COOKIE_EXPIRE);
        }
        pageModel.setTotalNum(model.getData().getTotalCount());
        for (UcDataItem item : model.getData().getList()) {
            AdPlanStat plan = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(), account.getId());
            planList.add(plan);
            // 媒体计划ID
            String mPlanId = item.getUnitId() + "";
            // 媒体计划名
            String planName = item.getUnitName();
            // 媒体计划分组ID
            String groupId = item.getPackId() + "";
            // 媒体计划分组名
            String groupName = item.getPackName() + "";
            // 媒体计划出价
            int planBid = StringUtil.convertBigDecimal(item.getBid(), 100, 0).intValue();
            // 媒体计划出价策略
            String planBidStrategy = item.getChargeTypeString();
            // 媒体计划状态
            int planStatusRaw = item.getUnitState(); // 0-启动，1-暂停
            String planStatus = "";
            switch (planStatusRaw) {
                case 0:
                    planStatus = "启动";
                    break;
                case 1:
                    planStatus = "暂停";
                    break;
                default:
                    planStatus = "" + planStatus;
            }
            // 媒体计划开关
            boolean planSwitchRaw = item.isUnitPaused();
            int planSwitch = planSwitchRaw ? PlanSwitch.OFF : PlanSwitch.ON;
            // 消耗
            int totalPay = (int) (StringUtil.convertFloat(item.getConsume(), 0) * 100);// 转换为分
            // 曝光
            int showNum = item.getShow();
            // 点击
            int clickNum = item.getClick();
            plan.setmPlanId(mPlanId);
            plan.setPlanName(planName);
            plan.setCost(totalPay);
            plan.setShowNum(showNum);
            plan.setClickNum(clickNum);
            plan.setBid(planBid);
            plan.setPlanBidStrategy(planBidStrategy);
            plan.setGroupId(groupId);
            plan.setGroupName(groupName);
            plan.setPlanStatus(planStatus);
            plan.setPlanSwitch(planSwitch);
        }
        return planList;
    }

    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time) throws Exception {
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        int status = resultJson.get("status").getAsInt();
        if (status != 0) {
            if (status == 919) {
                ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, content);
            } else {
                ExceptionHandler.throwException(ExceptionEnum.SERVER_ERROR, content);
            }
        }
        String balanceStr = resultJson.get("data").getAsJsonObject().get("balance").getAsString();
        int balance = StringUtil.convertBigDecimal(balanceStr, 100, 0).intValue();
        AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                account.getId());
        accountStat.setBalance(balance);
        return accountStat;
    }

    private Map<String, String> getHeaderMap(String cookie, String url) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap
                .put("User-Agent",
                        "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
        headerMap.put("Host", "e.uc.cn");
        headerMap.put("Referer", "https://e.uc.cn/ad/static/index.html");
        headerMap.put("Connection", "keep-alive");
        headerMap.put("Upgrade-Insecure-Requests", "1");
        headerMap.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        headerMap.put("x-requested-with", "XMLHttpRequest");
        headerMap.put("Cookie", cookie);
        if (url.contains(PLAN_URL_SUFFIX)) {
            headerMap.put("uc-csrf-token", CookieUtil.getCookieValue(cookie, "UC-CSRF-TOKEN"));
        }
        return headerMap;
    }

}
