package com.dataeye.ad.scheduler.job.baidusearch.model;

/**
 * Created by VitAs on 2017/7/6.
 */
public class BaiduGroupRequest {

    private final String[] fields = {"planid", "pausestat", "planname", "shows", "clks", "paysum", "trans", "avgprice",
            "plandynamicideastat", "acctdynamicideastat", "mPriceFactor", "planstat", "remarketingstat", "deviceprefer",
            "wregion", "qrstat1", "phonetrans", "allipblackcnt", "clkrate", "wbudget", "plancyc", "showprob", "allnegativecnt",
            "showpay", "adtype", "acctdynamictagsublinkstat", "acctdynamictitlestat", "acctdynamichotredirectstat",
            "plandynamictagsublinkstat", "plandynamictitlestat", "plandynamichotredirectstat", "plandynamictitlelist",
            "plandynamictitleabstract", "acctdynamictitlelist", "acctdynamictitleabstract", "acctscenarizedstatus",
            "planscenarizedphrasestatus", "atstatus", "atbid", "atbudget", "bidprefer", "pcpricefactor", "inpeople",
            "remarketingPauseStat", "robotAPlanState", "urlstatparam"};

    private int pageNo;

    private int pageSize;

    private String startTime;

    private String endTime;

    private LevelCond levelCond;


    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public LevelCond getLevelCond() {
        return levelCond;
    }

    public void setLevelCond(LevelCond levelCond) {
        this.levelCond = levelCond;
    }

    public BaiduGroupRequest() {
    }

    public BaiduGroupRequest(int pageNo, int pageSize, String startTime, String endTime, LevelCond levelCond) {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        this.startTime = startTime;
        this.endTime = endTime;
        this.levelCond = levelCond;
    }

}
