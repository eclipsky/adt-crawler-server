package com.dataeye.ad.scheduler.job.mta.model;

import java.util.List;

public class MtaModel {

	private List<MtaData> data;

	private MtaPage page;

	public List<MtaData> getData() {
		return data;
	}

	public void setData(List<MtaData> data) {
		this.data = data;
	}

	public MtaPage getPage() {
		return page;
	}

	public void setPage(MtaPage page) {
		this.page = page;
	}

}

