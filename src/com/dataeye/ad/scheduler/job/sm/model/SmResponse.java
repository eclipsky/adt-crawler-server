package com.dataeye.ad.scheduler.job.sm.model;

/**
 * Created by VitAs on 2017/6/30.
 */
public class SmResponse {

    private BizParam bizParam;

    public BizParam getBizParam() {
        return bizParam;
    }

    public void setBizParam(BizParam bizParam) {
        this.bizParam = bizParam;
    }

    public SmResponse(BizParam bizParam) {
        this.bizParam = bizParam;
    }
}
