package com.dataeye.ad.cluster;

import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.Watcher.Event.KeeperState;
import org.apache.zookeeper.ZooKeeper;
import org.slf4j.Logger;

import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;
import com.dataeye.util.log.DELogger;

public class ZKClient {

    protected static Logger logger = DELogger.getLogger("zookeeper");

    private static ZooKeeper zookeeper;

    private static CountDownLatch connectedSemaphore;

    private static String CONNECT_STRING = "10.1.2.240:2181,10.1.2.241:2181,10.1.2.242:2181";

    private static int SESSION_TIMEOUT = 6000;

    public static ZooKeeper getZK() {
        if (zookeeper == null) {
            synchronized (ZKClient.class) {
                if (zookeeper == null) {
                    connectedSemaphore = new CountDownLatch(1);
                    try {
                        Properties props = ServerUtil.getConfig("cluster.properties");
                        String connectString = props.getProperty("zookeeper.connect", CONNECT_STRING);
                        String sessionTimeoutStr = props.getProperty("zookeeper.session.timeout.ms");
                        int sessionTimeout = StringUtil.convertInt(sessionTimeoutStr, SESSION_TIMEOUT);
                        zookeeper = new ZooKeeper(connectString, sessionTimeout, new SessionWatcher());
                        connectedSemaphore.await(30, TimeUnit.SECONDS);
                    } catch (Exception e) {
                        logger.error("get zkClient error", e);
                    } finally {
                        connectedSemaphore = null;
                    }
                }
            }
        }
        return zookeeper;
    }

    public static void closeZK() {
        if (null != zookeeper) {
            try {
                zookeeper.close();
                zookeeper = null;
                connectedSemaphore = null;
            } catch (InterruptedException e) {
                logger.error("close zkClient error", e);
            }
        }
    }

    static class SessionWatcher implements Watcher {
        public void process(WatchedEvent event) {
            logger.info(event.toString());
            if (event.getState() == KeeperState.SyncConnected) {
                if (connectedSemaphore != null) {
                    connectedSemaphore.countDown();
                }
            } else if (event.getState() == KeeperState.Expired) {
                logger.info("session expired. now rebuilding...");
                closeZK();
                getZK();
            } else {
                // TODO
            }
        }
    }

    public static void main(String[] args) throws Exception {
        ZooKeeper zk = ZKClient.getZK();
        long sessionId = zk.getSessionId();

        // close the old connection
        new ZooKeeper("10.1.2.240:2181", 3000, null, sessionId, zk.getSessionPasswd()).close();
        Thread.sleep(10000L);

        // rebuild a new session
        long newSessionid = ZKClient.getZK().getSessionId();

        // check the new session
        String status = newSessionid != sessionId ? "OK" : "FAIL";
        System.out.println(sessionId + "--->" + newSessionid + ", status: " + status);

        // close the client
        ZKClient.getZK().close();
        Thread.currentThread().join();
    }

}
