package com.dataeye.ad.scheduler.job.gdt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.dataeye.ad.pipeline.DBQuery;
import com.dataeye.ad.pipeline.po.*;
import com.dataeye.ad.scheduler.job.CrawlerFilter;

import com.dataeye.ad.util.*;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;

import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.constant.Constant.CrawlerPlatform;
import com.dataeye.ad.constant.Constant.PlanSwitch;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DBPipeline;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class GDTJob extends CrawlerBaseJob {

    private final static String regex = "(-|_)?[a-zA-Z0-9]{6}$";

    private static Pattern pattern = Pattern.compile(regex);

    private final static CrawlerPlatform platform = CrawlerPlatform.TENCENT_GDT;

    private final static Logger logger = ServerUtil.getLogger(platform);

    private final static String planUrl = "http://e.qq.com/ec/api.php?mod=report&act=adlist&g_tk={0}&d_p=0.42743274644848983&callback=frameElement.callback&script&g_tk={1}";

    private final static String accountUrl = "http://e.qq.com/ec/api.php?mod=account&act=dashboard&owner={0}&owner={1}&unicode=true&g_tk={2}";

    private static String reportOnly = "1";
    
    public GDTJob() {
        this(DateUtil.unixTimestamp000());
    }

    public GDTJob(long time) {
        super(time, platform);
    }

    public GDTJob(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }


    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        String statDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        String cookie = account.getCookie();
        String g_tk = QQUtil.getGTK(cookie);
        String owner = QQUtil.getOwner(account, cookie, g_tk);
        // 获取计划总数
        int pageId = 1;
        int pageSize = 50; // 正式环境改为50
        PageModel pageModel = new PageModel(pageId, pageSize);
        // 请求
        String content = HttpUtil.getContentByPost(HttpUtil.urlParamReplace(planUrl, g_tk, g_tk), getHeaderMap(cookie),
                getFormDataAdList(pageId, pageSize, owner, statDate, statDate));
        logger.info(content);
        // 解析
        List<AdPlanStat> poList = parse(content, account, time, pageModel);
        // 计划数为0，直接跳过
        int totalNum = pageModel.getTotalNum();
        // 入库
        DataProcessor.processPlanDay(poList);
        totalNum -= pageSize;
        while (totalNum > 0) {
            pageId++;
            // 请求
            content = HttpUtil.getContentByPost(HttpUtil.urlParamReplace(planUrl, g_tk, g_tk), getHeaderMap(cookie),
                    getFormDataAdList(pageId, pageSize, owner, statDate, statDate));
            logger.info(content);
            // 解析
            poList = parse(content, account, time, pageModel);
            // 入库
            DataProcessor.processPlanDay(poList);
            totalNum -= pageSize;
        }
    }

    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        String g_tk = QQUtil.getGTK(cookie);
        String owner = QQUtil.getOwner(account, cookie, g_tk);
        // 请求
        String content = HttpUtil.getContentByGet(HttpUtil.urlParamReplace(accountUrl, owner, owner, g_tk),
                getHeaderMap(cookie));
        logger.info(content);
        // 解析
        AdAccountStat accountStat = parseAccount(content, account, time);
        // 入库
        DataProcessor.processAccountDay(accountStat);
    }

    /**
     * <pre>
     * 解析响应内容，封装成标准的PO对象
     * @author sam.xie
     * @date Feb 15, 2017 3:12:29 PM
     * @param time
     * @throws Exception
     */
    private List<AdPlanStat> parse(String content, AdMediumAccount account, long time, PageModel pageModel)
            throws Exception {
        Document document = Jsoup.parse(content);
        String callback = document.getElementsByTag("body").get(0).getElementsByTag("script").toString();
        String result = callback.replace(
                "<script type=\"text/javascript\">document.domain='qq.com';frameElement.callback(", "").replace(
                ");</script>", "");

        List<AdPlanStat> poList = new ArrayList<AdPlanStat>();
        // 返回结果集中类型不统一，同一个字段可能为字符串也可能为对象还可能为数字
        JsonObject resultJson = StringUtil.jsonParser.parse(result).getAsJsonObject();
        int ret = resultJson.get("ret").getAsInt();
        // 检查状态码
        retCheck(ret, result);
        JsonArray list = resultJson.get("data").getAsJsonObject().get("list").getAsJsonArray();
        JsonObject conf = resultJson.get("data").getAsJsonObject().getAsJsonObject("conf").getAsJsonObject();
        int totalNum = conf.get("totalnum").getAsInt();
        pageModel.setTotalNum(totalNum);
        for (JsonElement item : list) {
            AdPlanStat reportPO = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(), account.getId());
            poList.add(reportPO);
            JsonObject order = item.getAsJsonObject();
            int clickNum = StringUtil.getJsonIntValue(order, "validclickcount");
            int cost = (int) (StringUtil.getJsonIntValue(order, "cost")); // 已经是分为单位
            int showNum = StringUtil.getJsonIntValue(order, "viewcount");
            // 下载量
            int downloadTimes = StringUtil.getJsonIntValue(order, "download");
            // 激活数
            int activeNum = StringUtil.getJsonIntValue(order, "activated_count");
            // 注册数
            int registerNum = StringUtil.getJsonIntValue(order, "register_count");
            // 媒体计划ID
            String mPlanId = StringUtil.getJsonStrValue(order,"orderid");
            // 媒体计划名
            String planName = StringUtil.getJsonStrValue(order,"ordername");
            // 媒体计划分组ID
            String groupId = StringUtil.getJsonStrValue(order,"campaignid");
            // 媒体计划分组名
            String groupName = StringUtil.getJsonStrValue(order,"campaignname");
            // 媒体计划出价
            int planBid = StringUtil.getJsonIntValue(order, "price");// 已经是分为单位
            // 媒体计划出价策略
            String planBidStrategy = StringUtil.getJsonIntValue(order, "cost_type") + "";
            // 媒体计划状态
            String planStatus = StringUtil.getJsonIntValue(order, "status") + "";
            // 媒体计划开关
            int planSwitchRaw = StringUtil.getJsonIntValue(order, "raw_status");
            Integer planSwitch = null;
            if (planSwitchRaw == 1) {
                planSwitch = PlanSwitch.ON;
            } else if (planSwitchRaw == 10) {
                planSwitch = PlanSwitch.OFF;
            }
            if (GDTUtil.shouldHandleDownload(account)) {
                // 落地页实体类
                AdPageStat adPageStat = new AdPageStat(DateUtil.fromUnixtimestamp(time), account.getCompanyId());
                adPageStat.setH5AppId(account.getId());
                adPageStat.setH5PageId(mPlanId);
                adPageStat.setPageName(planName);
                adPageStat.setRemark("GDT_" + account.getId());
                // 下载数
                adPageStat.setDownloadTimes(downloadTimes);
                // 页面到达数
                adPageStat.setUv(clickNum);
                // 落地页数据入库，同时得到pageId
                DBPipeline.insertPageDay(adPageStat);
                reportPO.setPageId(adPageStat.getPageId());
            }
            if (GDTUtil.shouldHandleActive(account)) {
                // 激活和注册量的指标，从ad_pkg_*中获取
                AdPkgStat adPkgStat = new AdPkgStat(DateUtil.fromUnixtimestamp(time), account.getCompanyId());
                adPkgStat.setCPAppId(account.getId());
                adPkgStat.setCPPkgId(mPlanId);
                adPkgStat.setPkgName(planName);
                adPkgStat.setRemark("GDT_" + account.getId());
                // 激活
                adPkgStat.setActiveNum(activeNum);
                // 注册
                adPkgStat.setRegisterNum(registerNum);
                // 包数据入库，同时得到pkgId
                DBPipeline.insertPkgDay(adPkgStat);
                reportPO.setPkgId(adPkgStat.getPkgId());
            }
            Matcher matcher = pattern.matcher(planName);
            String campaignId = "";
            if (matcher.find()) {
                campaignId = matcher.group(0).replace("-", "").replace("_", "").trim();
            }
            int productId = DBQuery.getProductIdForGDT(campaignId);
            if (productId > 0) {
                reportPO.setProductId(productId);
            }
            // 投放计划实体类
            reportPO.setmPlanId(mPlanId);
            reportPO.setPlanName(planName);
            reportPO.setClickNum(clickNum);
            reportPO.setCost(cost);
            reportPO.setShowNum(showNum);
            reportPO.setBid(planBid);
            reportPO.setPlanBidStrategy(planBidStrategy);
            reportPO.setGroupId(groupId);
            reportPO.setGroupName(groupName);
            reportPO.setPlanStatus(planStatus);
            reportPO.setPlanSwitch(planSwitch);
        }
        return poList;
    }

    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time) throws Exception {
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        int ret = resultJson.get("ret").getAsInt();
        retCheck(ret, content);
        JsonArray accountArray = resultJson.get("data").getAsJsonObject().get("accounts").getAsJsonArray();
        // 现金账户 & 虚拟账户
        for (JsonElement ele : accountArray) {
            JsonObject obj = ele.getAsJsonObject();
            int appId = obj.get("app_id").getAsInt();
            if (appId == 0) { // 现金账户
                int balance = StringUtil.getJsonIntValue(obj, "balance");
                int costToday = StringUtil.getJsonIntValue(obj, "daily_cost");
                AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                        account.getId());
                accountStat.setBalance(balance);
                accountStat.setCostToday(costToday);
                return accountStat;
            }
        }
        return null;
    }

    public List<NameValuePair> getFormDataAdList(int page, int pageSize, String owner, String beginDate, String endDate) {
        reportOnly = ConfigFromDB.getConfig("gdt_report_only", "1");
        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("qzreferrer", "http://e.qq.com/atlas/" + owner + "/report/order"));
        pairs.add(new BasicNameValuePair("datetype", "1"));
        pairs.add(new BasicNameValuePair("format", "json"));
        pairs.add(new BasicNameValuePair("page", page + ""));
        pairs.add(new BasicNameValuePair("pagesize", pageSize + ""));
        pairs.add(new BasicNameValuePair("sdate", beginDate));
        pairs.add(new BasicNameValuePair("edate", endDate));
        pairs.add(new BasicNameValuePair("status", "0"));
        pairs.add(new BasicNameValuePair("_select_status", "999"));
        pairs.add(new BasicNameValuePair("daterange", beginDate + "+至+" + endDate));
        pairs.add(new BasicNameValuePair("searchtype", ""));
        pairs.add(new BasicNameValuePair("searchname", ""));
        pairs.add(new BasicNameValuePair("reportonly", reportOnly)); // 1表示过滤无数据的广告，0表示所有
        pairs.add(new BasicNameValuePair("fastdate", "custom"));
        pairs.add(new BasicNameValuePair("callback", "frameElement.callback&script"));
        pairs.add(new BasicNameValuePair("owner", owner));
        pairs.add(new BasicNameValuePair("product_type", ""));
        pairs.add(new BasicNameValuePair("product_id", ""));
        pairs.add(new BasicNameValuePair("time_rpt", "1"));
        return pairs;
    }

    private Map<String, String> getHeaderMap(String cookie) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Cookie", cookie);
        headerMap.put("Host", "e.qq.com");
        headerMap.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0");
        headerMap.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        headerMap.put("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
        headerMap.put("Accept-Encoding", "gzip, deflate");
        headerMap.put("Connection", "keep-alive");
        headerMap.put("Upgrade-Insecure-Requests", "1");
        headerMap.put("Content-Type", "application/x-www-form-urlencoded");
        return headerMap;
    }

    private void retCheck(int ret, String result) throws Exception {
        result = StringUtil.convertUnicode2String(result);
        if (ret != 0) {
            // {"ret":4512,"msg":"反CSRF验证失败"}
            // {"ret":4501,"msg":"请先登录"}
            // {"ret":4904,"msg":"登录态过期"}
            if (ret == 4512 || ret == 4501 || ret == 4904) {
                ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, result);
            } else {
                // {"ret":4003,"msg":"抱歉，不支持查询90天以前的数据","data":""}
                // {"ret":4903,"msg":"登录态校验失败，请稍后重试"}
                ExceptionHandler.throwException(ExceptionEnum.REQUEST_ERROR, result);
            }
        }
    }
}
