package com.dataeye.ad.scheduler.job.fenghuang;

import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.constant.Constant.CrawlerPlatform;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.scheduler.job.CrawlerFilter;
import com.dataeye.ad.scheduler.job.fenghuang.model.PlanModel;
import com.dataeye.ad.scheduler.job.fenghuang.model.ReportModel;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FengHuangJob extends CrawlerBaseJob {

    private final static CrawlerPlatform platform = CrawlerPlatform.FENGHUANG;

    private final static Logger logger = ServerUtil.getLogger(platform);

    private static final String planUrl = "http://customer.feather.ifeng.com/api/advertising/getListPage";

    private static final String reportUrl = "http://customer.feather.ifeng.com/api/report/adList";

    private final static String accountUrl = "http://customer.feather.ifeng.com/api/customer/balance";

    public FengHuangJob() {
        this(DateUtil.unixTimestamp000());
    }

    public FengHuangJob(long time) {
        super(time, platform);
    }

    public FengHuangJob(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }

    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        List<AdPlanStat> poList = new ArrayList<>();
        //
        if (DateUtil.unixTimestamp000() == time) {
            poList = currentPlanSpider(time, account);
        } else {
            poList = pastReportSpider(time, account);
        }
        // 入库
        DataProcessor.processPlanDay(poList);
    }

    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        String token = account.getCookie();
        // 请求
        String content = HttpUtil.getContentByGet(accountUrl, getHeaderMap(token));
        logger.info(content);
        // 解析
        AdAccountStat accountStat = parseAccount(content, account, time);
        // 入库
        DataProcessor.processAccountDay(accountStat);
    }

    /**
     * 广告报表数据
     *
     * @param time
     * @param account
     * @throws Exception
     */
    private List<AdPlanStat> pastReportSpider(long time, AdMediumAccount account) throws Exception {
        List<AdPlanStat> poList = new ArrayList<>();
        String token = account.getCookie();
        int pageNum = 1;
        int pageSize = 100;
        // 请求报表数据
        String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        PageModel pageModel = new PageModel(pageNum, pageSize);
        // 请求参数
        ReportModel reportModel = new ReportModel(reportDate, reportDate, pageNum, pageSize);
        String content = HttpUtil.getContentByPost(reportUrl, getHeaderMap(token), StringUtil.gson.toJson(reportModel));
        logger.info(content);
        List<AdPlanStat> tempList = parseReport(content, account, time, pageModel);
        poList.addAll(tempList);
        int totalNum = pageModel.getTotalNum();
        totalNum -= pageSize;
        while (totalNum > 0) {
            pageNum++;
            reportModel.setPageNum(pageNum);
            content = HttpUtil.getContentByPost(reportUrl, getHeaderMap(token), StringUtil.gson.toJson(reportModel));
            logger.info(content);
            tempList = parseReport(content, account, time, pageModel);
            poList.addAll(tempList);
            totalNum -= pageSize;
        }
        return poList;
    }

    /**
     * 实时数据出价
     *
     * @param time
     * @param account
     * @return
     * @throws Exception
     */
    private List<AdPlanStat> currentPlanSpider(long time, AdMediumAccount account) throws Exception {
        List<AdPlanStat> poList = new ArrayList<>();
        String token = account.getCookie();
        int pageNum = 1;
        int pageSize = 100;
        PageModel pageModel = new PageModel(pageNum, pageSize);
        // 请求参数
        PlanModel planModel = new PlanModel(pageNum, pageSize);
        String content = HttpUtil.getContentByPost(planUrl, getHeaderMap(token), StringUtil.gson.toJson(planModel));
        logger.info(content);
        List<AdPlanStat> tempList = parsePlan(content, account, time, pageModel);
        poList.addAll(tempList);
        int totalNum = pageModel.getTotalNum();
        totalNum -= pageSize;
        while (totalNum > 0) {
            pageNum++;
            planModel.setPageNum(pageNum);
            content = HttpUtil.getContentByPost(planUrl, getHeaderMap(token), StringUtil.gson.toJson(planModel));
            logger.info(content);
            tempList = parsePlan(content, account, time, pageModel);
            poList.addAll(tempList);
            totalNum -= pageSize;
        }
        return poList;
    }

    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time) throws Exception {
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        int code = resultJson.get("status").getAsInt();
        if (code != 0) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, resultJson.get("msg").getAsString());
        }
        int balance = StringUtil.convertBigDecimal(resultJson.get("data").getAsJsonObject().get("balance").getAsString(), 100, 0).intValue();
        AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                account.getId());
        accountStat.setBalance(balance);
        return accountStat;
    }


    private List<AdPlanStat> parseReport(String content, AdMediumAccount account, long time, PageModel pageModel)
            throws Exception {
        List<AdPlanStat> poList = new ArrayList<>();
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        int code = resultJson.get("status").getAsInt();
        if (code != 0) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, resultJson.get("msg").getAsString());
        }
        JsonObject dataJson = resultJson.get("data").getAsJsonObject();
        pageModel.setTotalNum(dataJson.get("total").getAsInt());
        JsonElement listElement = dataJson.get("list");
        if (null == listElement || listElement.isJsonNull()) {
            return null;
        }
        JsonArray listJson = listElement.getAsJsonArray();
        for (JsonElement ele : listJson) {
            JsonObject obj = ele.getAsJsonObject();
            int clickNum = StringUtil.getJsonIntValue(obj, "clickCount");
            int showNum = StringUtil.getJsonIntValue(obj, "implCount");
            int cost = StringUtil.convertBigDecimal(StringUtil.getJsonStrValue(obj, "expenditureCount"), 100, 0).intValue();
            String mPlanId = obj.get("adId").getAsString();
            String planName = obj.get("adName").getAsString();
            String planGroupName = obj.get("adGroupName").getAsString();
            String planGroupId = obj.get("adGroupId").getAsString();
            AdPlanStat reportPO = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(),
                    account.getId());
            reportPO.setmPlanId(mPlanId);
            reportPO.setPlanName(planName);
            reportPO.setClickNum(clickNum);
            reportPO.setShowNum(showNum);
            reportPO.setCost(cost);
            reportPO.setGroupId(planGroupId);
            reportPO.setGroupName(planGroupName);
            poList.add(reportPO);
        }
        return poList;
    }


    private List<AdPlanStat> parsePlan(String content, AdMediumAccount account, long time, PageModel pageModel)
            throws Exception {
        List<AdPlanStat> poList = new ArrayList<>();
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        int code = resultJson.get("status").getAsInt();
        if (code != 0) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, resultJson.get("msg").getAsString());
        }
        JsonObject dataJson = resultJson.get("data").getAsJsonObject();
        pageModel.setTotalNum(dataJson.get("total").getAsInt());
        JsonElement listElement = dataJson.get("list");
        if (null == listElement || listElement.isJsonNull()) {
            return null;
        }
        JsonArray listJson = listElement.getAsJsonArray();
        for (JsonElement ele : listJson) {
            JsonObject obj = ele.getAsJsonObject();
            String mPlanId = obj.get("id").getAsString();
            String planName = obj.get("name").getAsString();
            String planGroupName = obj.get("adGroupName").getAsString();
            String planGroupId = obj.get("adGroupId").getAsString();
            int clickNum = StringUtil.getJsonIntValue(obj, "todayClick");
            int showNum = StringUtil.getJsonIntValue(obj, "todayImpl");
            int cost = StringUtil.convertBigDecimal(StringUtil.getJsonStrValue(obj, "todayConsume"), 100, 0).intValue();
            int bid = StringUtil.convertBigDecimal(StringUtil.getJsonStrValue(obj, "unitPrice"), 100, 0).intValue();
            AdPlanStat reportPO = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(),
                    account.getId());
            reportPO.setmPlanId(mPlanId);
            reportPO.setPlanName(planName);
            reportPO.setClickNum(clickNum);
            reportPO.setShowNum(showNum);
            reportPO.setCost(cost);
            reportPO.setGroupId(planGroupId);
            reportPO.setGroupName(planGroupName);
            reportPO.setBid(bid);
            poList.add(reportPO);
        }
        return poList;
    }

    private Map<String, String> getHeaderMap(String token) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("x-auth-token", token);
        headerMap.put("Host", "customer.feather.ifeng.com");
        headerMap.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0");
        headerMap.put("Accept", "application/json, text/plain, */*");
        headerMap.put("Accept-Encoding", "gzip, deflate");
        headerMap.put("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.6,en;q=0.4");
        headerMap.put("Connection", "keep-alive");
        headerMap.put("content-type", "application/json");
        return headerMap;
    }

}
