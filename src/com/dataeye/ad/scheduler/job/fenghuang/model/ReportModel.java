package com.dataeye.ad.scheduler.job.fenghuang.model;

import com.dataeye.ad.util.DateUtil;
import scala.collection.parallel.mutable.ParArray;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lingliqi
 * @date 2018-01-02 16:49
 */
public class ReportModel {

    private int pageNum;

    private int pageSize;

    private List<String> period;

    private final int type = 1;

    private List<Integer> platformType;

    private List<Integer> creativeType;

    private String startTime;

    private String endTime;


    public ReportModel(String startTime, String endTime, int pageNum, int pageSize) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        this.period = new ArrayList<>();
        this.period.add(DateUtil.getUTCDate(sdf.parse(startTime)));
        this.period.add(DateUtil.getUTCDate(sdf.parse(startTime)));
        this.platformType = new ArrayList<>();
        this.platformType.add(0);
        this.platformType.add(1);
        this.platformType.add(2);
        this.creativeType = new ArrayList<>();
        this.creativeType.add(1);
        this.creativeType.add(2);
        this.creativeType.add(4);
        this.creativeType.add(5);
        this.startTime = startTime;
        this.endTime = endTime;
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }

    public List<String> getPeriod() {
        return period;
    }

    public void setPeriod(List<String> period) {
        this.period = period;
    }

    public int getType() {
        return type;
    }

    public List<Integer> getPlatformType() {
        return platformType;
    }

    public void setPlatformType(List<Integer> platformType) {
        this.platformType = platformType;
    }

    public List<Integer> getCreativeType() {
        return creativeType;
    }

    public void setCreativeType(List<Integer> creativeType) {
        this.creativeType = creativeType;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
