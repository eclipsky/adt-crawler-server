package com.dataeye.ad.scheduler.job.baiduinfo.model;

public class BaiduInfoFilter {
	private String field;
	private String op;
	private String[] values;

	public BaiduInfoFilter() {

	}

	public BaiduInfoFilter(String field, String op, String[] values) {
		this.field = field;
		this.op = op;
		this.values = values;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getOp() {
		return op;
	}

	public void setOp(String op) {
		this.op = op;
	}

	public String[] getValues() {
		return values;
	}

	public void setValues(String[] values) {
		this.values = values;
	}

}
