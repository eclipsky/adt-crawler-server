package com.dataeye.ad.scheduler;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;
import org.slf4j.Logger;

import com.qq.jutil.util.Pair;
import com.xunlei.util.Log;

public class QuartzManager {
	private final static Logger logger = Log.getLogger("scheduler");
	private static SchedulerFactory sf = new StdSchedulerFactory();
	private static Scheduler scheduler;
	public static final String JOB_GROUP_NAME = "REFRESH_JOB_GROUP";
	public static final String TRIGGER_GROUP_NAME = "REFRESH_JOB_GROUP";

	static {
		try {
			scheduler = sf.getScheduler();
			logger.info("QuartzManager init ... ");
		} catch (SchedulerException e) {
			logger.error("QuartzManager start error", e);
		}
	}
    
	public static void addCrawlerJobs(Map<Class<? extends Job>, String> jobMap) throws SchedulerException {
		for (Entry<Class<? extends Job>, String> entry : jobMap.entrySet()) {
			try {
				Class<? extends Job> clazz = entry.getKey();
				String cronExpr = entry.getValue();
				System.out.println("==================== QuartzJob Run:" + clazz.getSimpleName() + " -> " + cronExpr);
				long id = Thread.currentThread().getId();
				String mainGroup = "Main_Group_" + id;
				JobDataMap nonUseMap = new JobDataMap();
				QuartzManager.addCronJob(clazz.getSimpleName(), clazz, nonUseMap, cronExpr, mainGroup);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void addCronJob(String jobName, Class<? extends Job> jobClass, JobDataMap jobDataMap,
			String cronExpression, String groupName) throws SchedulerException {
		try {
			JobDetail jobDetail = JobBuilder.newJob(jobClass).withIdentity(jobName, groupName).setJobData(jobDataMap)
					.build();
			CronTrigger cronTrigger = TriggerBuilder.newTrigger().withIdentity(jobName, groupName)
					.withSchedule(CronScheduleBuilder.cronSchedule(cronExpression)).build();
			scheduler.scheduleJob(jobDetail, cronTrigger);
		} catch (SchedulerException e) {
			logger.error("QuartzManager addCronJob error,triggerOrJobName=" + jobName + ",cronExpression="
					+ cronExpression, e);
			throw e;
		}
	}

	/**
	 * 增加某个job，不会覆盖
	 * 
	 * @param jobName
	 * @param jobClass
	 * @param jobDataMap
	 * @param cronExpression
	 * @throws SchedulerException
	 */
	public static void addCronJob(String jobName, Class<? extends Job> jobClass, JobDataMap jobDataMap,
			String cronExpression) throws SchedulerException {
		try {
			JobDetail jobDetail = JobBuilder.newJob(jobClass).withIdentity(jobName, JOB_GROUP_NAME)
					.setJobData(jobDataMap).build();
			CronTrigger cronTrigger = TriggerBuilder.newTrigger().withIdentity(jobName, TRIGGER_GROUP_NAME)
					.withSchedule(CronScheduleBuilder.cronSchedule(cronExpression)).build();
			scheduler.scheduleJob(jobDetail, cronTrigger);
		} catch (SchedulerException e) {
			logger.error("QuartzManager addCronJob error,triggerOrJobName=" + jobName + ",cronExpression="
					+ cronExpression, e);
			throw e;
		}
	}

	public static void start() throws SchedulerException {
		scheduler.start();
		logger.info("QuartzManager start ... ");
	}

	public static void standby() throws SchedulerException {
		scheduler.standby();
		logger.info("QuartzManager standby ... ");
	}

	public static void shutdown() throws SchedulerException {
		scheduler.shutdown(true);
		logger.info("QuartzManager shuting down ... ");
	}

    public static void pause() throws SchedulerException {
        scheduler.pauseAll();
        logger.info("QuartzManager pause ... ");
    }

    public static void resume() throws SchedulerException {
        scheduler.resumeAll();
        logger.info("QuartzManager resume ... ");
    }
    
	/**
	 * 修改某个job的触发条件
	 * 
	 * @param triggerOrJobName
	 * @param cronExpression
	 * @throws SchedulerException
	 */
	public static void modifyCronTrigger(String triggerOrJobName, String cronExpression) throws SchedulerException {

		try {
			CronTrigger cronTrigger = TriggerBuilder.newTrigger().withIdentity(triggerOrJobName, TRIGGER_GROUP_NAME)
					.withSchedule(CronScheduleBuilder.cronSchedule(cronExpression)).build();
			scheduler.rescheduleJob(new TriggerKey(triggerOrJobName, TRIGGER_GROUP_NAME), cronTrigger);
		} catch (SchedulerException e) {
			logger.error("QuartzManager modifyCronTrigger error,triggerOrJobName=" + triggerOrJobName
					+ ",cronExpression=" + cronExpression, e);
			throw e;
		}
	}

	/**
	 * 移除某个job
	 * 
	 * @param triggerOrJobName
	 * @param cronExpression
	 * @throws SchedulerException
	 */
	public static void delCronJob(String triggerOrJobName, String cronExpression) throws SchedulerException {
		try {
			TriggerKey triggerKey = new TriggerKey(triggerOrJobName, TRIGGER_GROUP_NAME);
			scheduler.pauseTrigger(triggerKey);// 停止触发器
			scheduler.unscheduleJob(triggerKey);// 移除触发器
			scheduler.deleteJob(new JobKey(triggerOrJobName, JOB_GROUP_NAME));// 删除任务
		} catch (SchedulerException e) {
			logger.error("QuartzManager delCronJob error,triggerOrJobName=" + triggerOrJobName + ",cronExpression="
					+ cronExpression, e);
			throw e;
		}
	}

	/**
	 * 获取所有的job
	 * 
	 * @return
	 * @throws SchedulerException
	 */
	public static List<Pair<JobDetail, Trigger>> listAllJobs() throws SchedulerException {
		Set<JobKey> jobSet = scheduler.getJobKeys(GroupMatcher.jobGroupEquals(JOB_GROUP_NAME));
		List<Pair<JobDetail, Trigger>> list = new ArrayList<Pair<JobDetail, Trigger>>();
		for (JobKey jobKey : jobSet) {
			JobDetail jobDetail = scheduler.getJobDetail(jobKey);
			Trigger trigger = scheduler.getTrigger(new TriggerKey(jobKey.getName(), TRIGGER_GROUP_NAME));
			list.add(Pair.makePair(jobDetail, trigger));
		}
		return list;
	}

	public final static void main(String[] args) throws SchedulerException, InterruptedException {
		/*
		 * QuartzManager.addCronJob("test1", NoticeJob.class, new JobDataMap(), "* * * * * ?");
		 */
		// QuartzManager.addCronJob("test222", NoticeJob.class, new JobDataMap(),
		// "0/2 * * * * ?");
		QuartzManager.start();
		Thread.sleep(6000);
		/*
		 * QuartzManager.modifyCronTrigger("test1", "5 * * * * ?"); List<Pair<JobDetail, Trigger>> list = listAllJobs();
		 * for (Pair<JobDetail, Trigger> pair : list) { System.out.println(pair.getFirst().getKey());
		 * System.out.println(((CronTrigger) pair.getSecond()) .getCronExpression()); }
		 */
	}
}
