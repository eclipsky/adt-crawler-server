package com.dataeye.ad.scheduler.job.toutiao.model;

public class ToutiaoStatData {

    private String activate;

    private String show;

    private String convert_rate;

    private String convert_cost;

    private String click_install;

    private String click_call_cost;

    private String download_cost;

    private String show_full;

    private String click_cost;

    private String click;

    private String send;

    private String click_start;

    private String click_call_rate;

    private String download_finish;

    private String download_finish_rate;

    private String stat_time;

    private String click_start_rate;

    private String click_start_cost;

    private String click_call;

    private String convert;

    private String ecpm;

    private String ctr;

    private String stat_cost;

    private String activate_cost;

    private String activate_rate;

    private String install_finish;


    /**
     * @return the activate
     */
    public String getActivate() {
        return activate;
    }

    /**
     * @param activate the activate to set
     */
    public void setActivate(String activate) {
        this.activate = activate;
    }

    /**
     * @return the show
     */
    public String getShow() {
        return show;
    }

    /**
     * @param show the show to set
     */
    public void setShow(String show) {
        this.show = show;
    }

    /**
     * @return the convert_rate
     */
    public String getConvert_rate() {
        return convert_rate;
    }

    /**
     * @param convert_rate the convert_rate to set
     */
    public void setConvert_rate(String convert_rate) {
        this.convert_rate = convert_rate;
    }

    /**
     * @return the convert_cost
     */
    public String getConvert_cost() {
        return convert_cost;
    }

    /**
     * @param convert_cost the convert_cost to set
     */
    public void setConvert_cost(String convert_cost) {
        this.convert_cost = convert_cost;
    }

    /**
     * @return the click_install
     */
    public String getClick_install() {
        return click_install;
    }

    /**
     * @param click_install the click_install to set
     */
    public void setClick_install(String click_install) {
        this.click_install = click_install;
    }

    /**
     * @return the click_call_cost
     */
    public String getClick_call_cost() {
        return click_call_cost;
    }

    /**
     * @param click_call_cost the click_call_cost to set
     */
    public void setClick_call_cost(String click_call_cost) {
        this.click_call_cost = click_call_cost;
    }

    /**
     * @return the download_cost
     */
    public String getDownload_cost() {
        return download_cost;
    }

    /**
     * @param download_cost the download_cost to set
     */
    public void setDownload_cost(String download_cost) {
        this.download_cost = download_cost;
    }

    /**
     * @return the show_full
     */
    public String getShow_full() {
        return show_full;
    }

    /**
     * @param show_full the show_full to set
     */
    public void setShow_full(String show_full) {
        this.show_full = show_full;
    }

    /**
     * @return the click_cost
     */
    public String getClick_cost() {
        return click_cost;
    }

    /**
     * @param click_cost the click_cost to set
     */
    public void setClick_cost(String click_cost) {
        this.click_cost = click_cost;
    }

    /**
     * @return the click
     */
    public String getClick() {
        return click;
    }

    /**
     * @param click the click to set
     */
    public void setClick(String click) {
        this.click = click;
    }

    /**
     * @return the send
     */
    public String getSend() {
        return send;
    }

    /**
     * @param send the send to set
     */
    public void setSend(String send) {
        this.send = send;
    }

    /**
     * @return the click_start
     */
    public String getClick_start() {
        return click_start;
    }

    /**
     * @param click_start the click_start to set
     */
    public void setClick_start(String click_start) {
        this.click_start = click_start;
    }

    /**
     * @return the click_call_rate
     */
    public String getClick_call_rate() {
        return click_call_rate;
    }

    /**
     * @param click_call_rate the click_call_rate to set
     */
    public void setClick_call_rate(String click_call_rate) {
        this.click_call_rate = click_call_rate;
    }

    /**
     * @return the download_finish
     */
    public String getDownload_finish() {
        return download_finish;
    }

    /**
     * @param download_finish the download_finish to set
     */
    public void setDownload_finish(String download_finish) {
        this.download_finish = download_finish;
    }

    /**
     * @return the download_finish_rate
     */
    public String getDownload_finish_rate() {
        return download_finish_rate;
    }

    /**
     * @param download_finish_rate the download_finish_rate to set
     */
    public void setDownload_finish_rate(String download_finish_rate) {
        this.download_finish_rate = download_finish_rate;
    }

    /**
     * @return the stat_time
     */
    public String getStat_time() {
        return stat_time;
    }

    /**
     * @param stat_time the stat_time to set
     */
    public void setStat_time(String stat_time) {
        this.stat_time = stat_time;
    }

    /**
     * @return the click_start_rate
     */
    public String getClick_start_rate() {
        return click_start_rate;
    }

    /**
     * @param click_start_rate the click_start_rate to set
     */
    public void setClick_start_rate(String click_start_rate) {
        this.click_start_rate = click_start_rate;
    }

    /**
     * @return the click_start_cost
     */
    public String getClick_start_cost() {
        return click_start_cost;
    }

    /**
     * @param click_start_cost the click_start_cost to set
     */
    public void setClick_start_cost(String click_start_cost) {
        this.click_start_cost = click_start_cost;
    }

    /**
     * @return the click_call
     */
    public String getClick_call() {
        return click_call;
    }

    /**
     * @param click_call the click_call to set
     */
    public void setClick_call(String click_call) {
        this.click_call = click_call;
    }

    /**
     * @return the convert
     */
    public String getConvert() {
        return convert;
    }

    /**
     * @param convert the convert to set
     */
    public void setConvert(String convert) {
        this.convert = convert;
    }

    /**
     * @return the ecpm
     */
    public String getEcpm() {
        return ecpm;
    }

    /**
     * @param ecpm the ecpm to set
     */
    public void setEcpm(String ecpm) {
        this.ecpm = ecpm;
    }

    /**
     * @return the ctr
     */
    public String getCtr() {
        return ctr;
    }

    /**
     * @param ctr the ctr to set
     */
    public void setCtr(String ctr) {
        this.ctr = ctr;
    }

    /**
     * @return the stat_cost
     */
    public String getStat_cost() {
        return stat_cost;
    }

    /**
     * @param stat_cost the stat_cost to set
     */
    public void setStat_cost(String stat_cost) {
        this.stat_cost = stat_cost;
    }

    /**
     * @return the activate_cost
     */
    public String getActivate_cost() {
        return activate_cost;
    }

    /**
     * @param activate_cost the activate_cost to set
     */
    public void setActivate_cost(String activate_cost) {
        this.activate_cost = activate_cost;
    }

    /**
     * @return the activate_rate
     */
    public String getActivate_rate() {
        return activate_rate;
    }

    /**
     * @param activate_rate the activate_rate to set
     */
    public void setActivate_rate(String activate_rate) {
        this.activate_rate = activate_rate;
    }

    /**
     * @return the install_finish
     */
    public String getInstall_finish() {
        return install_finish;
    }

    /**
     * @param install_finish the install_finish to set
     */
    public void setInstall_finish(String install_finish) {
        this.install_finish = install_finish;
    }


}
