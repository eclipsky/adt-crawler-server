package com.dataeye.ad.scheduler.job.xunlei;


import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.scheduler.job.CrawlerBaseExecutor;
import com.dataeye.ad.util.DateUtil;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * @author lingliqi
 * @date 2018-02-07 9:58
 */
public class XunleiJobDay implements Job{

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        CrawlerBaseExecutor.addJob(new XunleiJob(DateUtil.unixTimestamp000() - Constant.ONEDAY));

    }
}
