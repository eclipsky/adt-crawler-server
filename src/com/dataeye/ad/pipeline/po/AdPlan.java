package com.dataeye.ad.pipeline.po;

public class AdPlan extends AdBase {

	private int mediumId;

	private int accountId;

	private String planName;

	private String mPlanId; // 媒体平台计划ID（区别ADT自增的planId）

	private String groupId; // 媒体平台计划组ID

	private String groupName; // 媒体平台计划组名称

	private Integer bid; // 媒体平台计划报价（单位：分）

	private String planBidStrategy; // 媒体平台计划报价策略（CPC，CPM等）

	private String planStatus; // 媒体平台计划状态（正在投放，已完成等）

	private Integer planSwitch; // 媒体平台计划开关（1-开，0-关）
	
    private String planExtend; // 扩展字段
    
    private String landingPage; // 落地页url
    
	public int getMediumId() {
		return mediumId;
	}

	public void setMediumId(int mediumId) {
		this.mediumId = mediumId;
	}

	public String getmPlanId() {
		return mPlanId;
	}

	public void setmPlanId(String mPlanId) {
		this.mPlanId = mPlanId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Integer getBid() {
		return bid;
	}

	public void setBid(Integer bid) {
		this.bid = bid;
	}

	public String getPlanBidStrategy() {
		return planBidStrategy;
	}

	public void setPlanBidStrategy(String planBidStrategy) {
		this.planBidStrategy = planBidStrategy;
	}

	public String getPlanStatus() {
		return planStatus;
	}

	public void setPlanStatus(String planStatus) {
		this.planStatus = planStatus;
	}

	public Integer getPlanSwitch() {
		return planSwitch;
	}

	public void setPlanSwitch(Integer planSwitch) {
		this.planSwitch = planSwitch;
	}

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

    public String getPlanExtend() {
        return planExtend;
    }

    public void setPlanExtend(String planExtend) {
        this.planExtend = planExtend;
    }

    public String getLandingPage() {
        return landingPage;
    }

    public void setLandingPage(String landingPage) {
        this.landingPage = landingPage;
    }

}
