package com.dataeye.ad.scheduler.job;

import java.util.ArrayList;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;

import com.dataeye.ad.cluster.ClusterNode;
import com.dataeye.ad.common.CookieException;
import com.dataeye.ad.constant.Constant.CrawlerPage;
import com.dataeye.ad.constant.Constant.CrawlerPlatform;
import com.dataeye.ad.constant.Constant.LoginStatus;
import com.dataeye.ad.pipeline.DBQuery;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.util.CookieUtil;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;

/**
 * 抽象爬虫类，封装基本的逻辑
 * 
 * @author sam.xie
 * @date Apr 11, 2017 3:23:49 PM
 * @version 1.0
 */
abstract public class CrawlerBaseJob implements Job, Runnable {

    private long time;

    private CrawlerPlatform platform;

    private CrawlerFilter filter;

    private Logger logger;

    private boolean needFilter = true;

    public CrawlerBaseJob(long time, CrawlerPlatform platform) {
        this.time = time;
        this.platform = platform;
        this.logger = ServerUtil.getLogger(platform);
    }

    public CrawlerBaseJob(long time, CrawlerPlatform platform, CrawlerFilter filter) {
        this(time, platform);
        this.filter = filter;
    }

    @Override
    public void execute(JobExecutionContext arg0) {
        process(time, platform, filter);
    }

    @Override
    public void run() {
        needFilter = false;
        process(time, platform, filter);
    }

    /**
     * <pre/>
     * 根据平台，过滤条件实体类filter爬取
     * 如果定时任务触发，需要考虑帐号过滤（集群状态下，节点只处理部分账户）
     * 如果数据恢复触发，就不用考虑账户过滤
     * 
     * @param time
     * @param platform
     * @param filter
     */
    public void process(long time, CrawlerPlatform platform, CrawlerFilter filter) {
        List<AdMediumAccount> accountList = DBQuery.getMediumAccountList(platform.getId(), filter);
        filterAccount(accountList);
        processByAccountList(time, accountList);
    }

    private void processByAccountList(long time, List<AdMediumAccount> accountList) {
        String statDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        logger.info("====================" + platform.getName() + ":" + statDate + "，爬虫启动====================");
        for (AdMediumAccount account : accountList) {
            try {
                if (account.getLoginStatus() != LoginStatus.HAS_LOGGED
                        && account.getLoginStatus() != LoginStatus.IS_LOGGING) {
                    logger.info(platform.getName() + ":" + statDate + ":" + account + "，账户未登录，爬虫跳过...");
                    continue;
                }
                logger.info(platform.getName() + ":" + statDate + ":" + account + "，爬虫开始...");

                // Cookie校验
                String cookie = CookieUtil.getCookieByAccount(account);
                CookieUtil.isCookieValid(cookie);

                // 执行爬取
                spider(time, account);

                logger.info(platform.getName() + ":" + statDate + ":" + account + "，爬虫结束...");
                // CookieUtil.changeLoginStatus(LoginStatus.HAS_LOGGED, account);
                ServerUtil.crawlerMailControlClear(account);

            } catch (Exception e) {
                // Cookie异常需要重新登录
                if (e instanceof CookieException) {
                    CookieUtil.changeLoginStatus(LoginStatus.NOT_LOGGED, account);
                }
                logger.error(platform.getName() + "账户：" + account + "，爬取异常！" + ServerUtil.printStackTrace(e));
                // update by sam at 2017.06.16 爬取帐号多了之后，会有各种网络连接超时异常，这里跳过，不再发送邮件
                // if ((e instanceof SocketTimeoutException) || (e instanceof ConnectTimeoutException)) {
                // // TODO 网络异常统计，阈值告警
                // } else {
                // }
                // 连续失败N次才会发送邮件
                // if(platform == CrawlerPlatform.UC && e.getMessage().contains("HTTP/1.1 403 Forbidden")){
                // /**
                // * update by sam at 2017.12.26，UC阿里汇川，模拟请求还有待根治，这里先过滤此类异常
                // */
                // // TODO
                // }else{
                boolean allowSend = ServerUtil.crawlerMailControl(account);
                if (allowSend) {
                    ServerUtil.sendAlarmMail(platform.getName(), account + "\n" + ServerUtil.printStackTrace(e));
                }
                // }
            }
        }
        logger.info("====================" + platform.getName() + ":" + statDate + "，爬取完成====================");
    }

    public void spider(long time, AdMediumAccount account) throws Exception {
        // 爬取账户余额
        if (DateUtil.unixTimestamp000() == time) {
            logger.info("1.账户余额...");
            accountSpider(time, account);
        }
        int pageType = account.getPageType();
        if (pageType == CrawlerPage.PLAN_GROUP) {
            // 爬取计划组
            logger.info("2.投放计划组...");
            planGroupSpider(time, account);
        } else {
            // 爬取计划
            logger.info("2.投放计划...");
            planSpider(time, account);
        }
    }

    public abstract void accountSpider(long time, AdMediumAccount account) throws Exception;

    public abstract void planSpider(long time, AdMediumAccount account) throws Exception;

    public void planGroupSpider(long time, AdMediumAccount account) throws Exception {

    };

    /**
     * <pre>
     * 账户过滤兼容standalone和cluster模式
     * @author sam.xie
     * @date Aug 25, 2017 11:14:46 AM
     * @param accountList
     */
    private void filterAccount(List<AdMediumAccount> accountList) {
        int clusterMode = StringUtil.convertInt(ServerUtil.getConfigProperties().getProperty("clusterMode"), 0);
        if (clusterMode <= 0) {
            return;
        }
        if (!needFilter)
            return;
        // 按集群节点过滤待处理媒体账户
        List<AdMediumAccount> nodeHandleAccounts = new ArrayList<AdMediumAccount>();
        int mod = 0;
        int size = 1;
        if (null != ClusterNode.getNodes()) {
            mod = ClusterNode.getMod();
            size = ClusterNode.getNodes().size();
        }
        // 集群节点数大于1才需要做负载均衡
        if (size > 1) {
            for (AdMediumAccount account : accountList) {
                if (account.getId() % size == mod) {
                    nodeHandleAccounts.add(account);
                }
            }
            accountList.retainAll(nodeHandleAccounts);
        }
    }
}
