package com.dataeye.ad.scheduler.job.uc.model;

public class UcDataItem {

	private String time;

	private String stamp;

	private int show;

	private int click;

	private String consume;

	private String ctr;

	private String acp;

	private String cpm;

	private String actualConsume;

	private String refund;

	private int activate;

	private String aap;

	private String userId;

	private String userName;

	private int packId;

	private String packName;

	private String packBudget;

	private boolean packDeleted;

	private int state;

	private boolean allSchedule;

	private boolean packPaused;

	private int unitId;

	private String unitName;

	private int platform;

	private int styleTypeId;

	private String styleTypeName;

	private int generalizeType;

	private String generalizeTypeName;

	private int chargeType;

	private String unitBudget;

	private String bid;

	private int unitState;

	private boolean unitDeleted;

	private boolean unitPaused;

	private String chargeTypeString;

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getStamp() {
		return stamp;
	}

	public void setStamp(String stamp) {
		this.stamp = stamp;
	}

	public int getShow() {
		return show;
	}

	public void setShow(int show) {
		this.show = show;
	}

	public int getClick() {
		return click;
	}

	public void setClick(int click) {
		this.click = click;
	}

	public String getConsume() {
		return consume;
	}

	public void setConsume(String consume) {
		this.consume = consume;
	}

	public String getCtr() {
		return ctr;
	}

	public void setCtr(String ctr) {
		this.ctr = ctr;
	}

	public String getAcp() {
		return acp;
	}

	public void setAcp(String acp) {
		this.acp = acp;
	}

	public String getCpm() {
		return cpm;
	}

	public void setCpm(String cpm) {
		this.cpm = cpm;
	}

	public String getActualConsume() {
		return actualConsume;
	}

	public void setActualConsume(String actualConsume) {
		this.actualConsume = actualConsume;
	}

	public String getRefund() {
		return refund;
	}

	public void setRefund(String refund) {
		this.refund = refund;
	}

	public int getActivate() {
		return activate;
	}

	public void setActivate(int activate) {
		this.activate = activate;
	}

	public String getAap() {
		return aap;
	}

	public void setAap(String aap) {
		this.aap = aap;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getPackId() {
		return packId;
	}

	public void setPackId(int packId) {
		this.packId = packId;
	}

	public String getPackName() {
		return packName;
	}

	public void setPackName(String packName) {
		this.packName = packName;
	}

	public String getPackBudget() {
		return packBudget;
	}

	public void setPackBudget(String packBudget) {
		this.packBudget = packBudget;
	}

	public boolean isPackDeleted() {
		return packDeleted;
	}

	public void setPackDeleted(boolean packDeleted) {
		this.packDeleted = packDeleted;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public boolean isAllSchedule() {
		return allSchedule;
	}

	public void setAllSchedule(boolean allSchedule) {
		this.allSchedule = allSchedule;
	}

	public boolean isPackPaused() {
		return packPaused;
	}

	public void setPackPaused(boolean packPaused) {
		this.packPaused = packPaused;
	}

	public int getUnitId() {
		return unitId;
	}

	public void setUnitId(int unitId) {
		this.unitId = unitId;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public int getPlatform() {
		return platform;
	}

	public void setPlatform(int platform) {
		this.platform = platform;
	}

	public int getStyleTypeId() {
		return styleTypeId;
	}

	public void setStyleTypeId(int styleTypeId) {
		this.styleTypeId = styleTypeId;
	}

	public String getStyleTypeName() {
		return styleTypeName;
	}

	public void setStyleTypeName(String styleTypeName) {
		this.styleTypeName = styleTypeName;
	}

	public int getGeneralizeType() {
		return generalizeType;
	}

	public void setGeneralizeType(int generalizeType) {
		this.generalizeType = generalizeType;
	}

	public String getGeneralizeTypeName() {
		return generalizeTypeName;
	}

	public void setGeneralizeTypeName(String generalizeTypeName) {
		this.generalizeTypeName = generalizeTypeName;
	}

	public int getChargeType() {
		return chargeType;
	}

	public void setChargeType(int chargeType) {
		this.chargeType = chargeType;
	}

	public String getUnitBudget() {
		return unitBudget;
	}

	public void setUnitBudget(String unitBudget) {
		this.unitBudget = unitBudget;
	}

	public String getBid() {
		return bid;
	}

	public void setBid(String bid) {
		this.bid = bid;
	}

	public int getUnitState() {
		return unitState;
	}

	public void setUnitState(int unitState) {
		this.unitState = unitState;
	}

	public boolean isUnitDeleted() {
		return unitDeleted;
	}

	public void setUnitDeleted(boolean unitDeleted) {
		this.unitDeleted = unitDeleted;
	}

	public boolean isUnitPaused() {
		return unitPaused;
	}

	public void setUnitPaused(boolean unitPaused) {
		this.unitPaused = unitPaused;
	}

	public String getChargeTypeString() {
		return chargeTypeString;
	}

	public void setChargeTypeString(String chargeTypeString) {
		this.chargeTypeString = chargeTypeString;
	}

}
