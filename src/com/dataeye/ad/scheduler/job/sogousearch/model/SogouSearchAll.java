package com.dataeye.ad.scheduler.job.sogousearch.model;

import java.math.BigDecimal;

public class SogouSearchAll {
	private String startDate;

	private int cpcConsumeCount;

	private long cpcPlanId;

	private int click;

	private String endDate;

	private String cpcPlanName;

	private BigDecimal consume;

	private int pv;

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public int getCpcConsumeCount() {
		return cpcConsumeCount;
	}

	public void setCpcConsumeCount(int cpcConsumeCount) {
		this.cpcConsumeCount = cpcConsumeCount;
	}

	public long getCpcPlanId() {
		return cpcPlanId;
	}

	public void setCpcPlanId(long cpcPlanId) {
		this.cpcPlanId = cpcPlanId;
	}

	public int getClick() {
		return click;
	}

	public void setClick(int click) {
		this.click = click;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getCpcPlanName() {
		return cpcPlanName;
	}

	public void setCpcPlanName(String cpcPlanName) {
		this.cpcPlanName = cpcPlanName;
	}

	public BigDecimal getConsume() {
		return consume;
	}

	public void setConsume(BigDecimal consume) {
		this.consume = consume;
	}

	public int getPv() {
		return pv;
	}

	public void setPv(int pv) {
		this.pv = pv;
	}

}
