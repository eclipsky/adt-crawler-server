package com.dataeye.ad.scheduler.job.fenghuang.model;

/**
 * @author lingliqi
 * @date 2018-01-02 17:58
 */
public class PlanModel {

    private int pageNum;

    private int pageSize;

    private final String name = "";
    private final String id = "";
    private final String adStatus = "";
    private final int isHide = 0;
    private final boolean unitPrice = true;
    private final boolean status = true;
    private final boolean createTime = true;
    private final boolean todayImpl = true;
    private final boolean todayClick = true;
    private final boolean todayConsume = true;
    private final boolean todayClickRatio = false;
    private final boolean todayECPM = false;
    private final boolean todayCPC = false;
    private final boolean histImpl = false;
    private final boolean histClick = false;
    private final boolean histConsume = false;
    private final boolean histClickRatio = false;
    private final boolean histECPM = false;
    private final boolean histCPC = false;

    public PlanModel(int pageNum, int pageSize) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getAdStatus() {
        return adStatus;
    }

    public int getIsHide() {
        return isHide;
    }

    public boolean isUnitPrice() {
        return unitPrice;
    }

    public boolean isStatus() {
        return status;
    }

    public boolean isCreateTime() {
        return createTime;
    }

    public boolean isTodayImpl() {
        return todayImpl;
    }

    public boolean isTodayClick() {
        return todayClick;
    }

    public boolean isTodayConsume() {
        return todayConsume;
    }

    public boolean isTodayClickRatio() {
        return todayClickRatio;
    }

    public boolean isTodayECPM() {
        return todayECPM;
    }

    public boolean isTodayCPC() {
        return todayCPC;
    }

    public boolean isHistImpl() {
        return histImpl;
    }

    public boolean isHistClick() {
        return histClick;
    }

    public boolean isHistConsume() {
        return histConsume;
    }

    public boolean isHistClickRatio() {
        return histClickRatio;
    }

    public boolean isHistECPM() {
        return histECPM;
    }

    public boolean isHistCPC() {
        return histCPC;
    }
}
