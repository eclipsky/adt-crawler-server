package com.dataeye.ad.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import org.apache.http.HttpHost;
import org.apache.http.auth.AUTH;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.message.BasicHeader;
import org.slf4j.Logger;

import com.dataeye.ad.constant.Constant.CrawlerPlatform;
import com.dataeye.ad.constant.Constant.Table;
import com.dataeye.util.log.DELogger;
import com.xunlei.jdbc.JdbcTemplate;
import com.xunlei.jdbc.RowCallbackHandler;

public class ProxyUtil {

	/**
	 * <pre>
	 * 获取IP代理配置
	 * @author sam.xie
	 * @date May 16, 2017 6:47:57 PM
	 * @return
	 */
	public static void init() {
		// 先从数据库获取，再读取配置

	}

	public Set<IpProxy> getFromDB() {
		JdbcTemplate jdbcTemplate = DBClient.getJdbcTemplateDeAdt();
		final Set<IpProxy> proxySet = new HashSet<IpProxy>();
		String sql = "select ip, port from " + Table.IP_PROXY + " where 1 = 1";
		try {
			jdbcTemplate.query(sql, new RowCallbackHandler() {
				@Override
				public void processRow(ResultSet rs) throws SQLException {
					String host = rs.getString(1);
					String port = rs.getString(2);
					proxySet.add(new IpProxy(host, port));
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}

		return proxySet;
	}

	/**
	 * <pre>
	 * 从配置文件获取IP
	 * @author sam.xie
	 * @date May 17, 2017 10:07:02 AM
	 * @return
	 */
	public Set<IpProxy> getFromConfig() {
		String proxys = ServerUtil.getConfigProperties().getProperty("ip_proxy");
		final Set<IpProxy> proxySet = new HashSet<IpProxy>();
		String[] proxyArr = proxys.split(";");
		for (String proxy : proxyArr) {
			String[] items = proxy.split(":");
			String host = items[0];
			String port = items[1];
			proxySet.add(new IpProxy(host, port));
		}
		return proxySet;
	}

	public static Logger getLogger(CrawlerPlatform platform) {
		return DELogger.getLogger("spider_" + platform.getLabel());
	}

	public static void main(String[] args) {
		String url = "http://www.baidu.com";
		HttpClient client = HttpUtil.getHttpClient();
		String proxyHost = "211.149.159.44";
		int proxyPort = 16816;
		String proxyUser = "sam";
		String proxyPass = "79ng64e1";
		HttpHost proxy = new HttpHost(proxyHost, proxyPort);
		client.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
		
		HttpGet get = new HttpGet(url);
//		String content = HttpUtil.getContentByGet(url, null);
		
		// 代理认证
		
		// RequestConfig config = RequestConfig.
	}
}
