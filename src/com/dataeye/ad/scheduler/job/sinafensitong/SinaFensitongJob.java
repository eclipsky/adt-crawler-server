package com.dataeye.ad.scheduler.job.sinafensitong;

import java.math.BigDecimal;
import java.util.*;

import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.scheduler.job.CrawlerFilter;
import com.dataeye.ad.scheduler.job.sinafensitong.model.SinaPlanModel;

import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;

import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.common.ServerException;
import com.dataeye.ad.constant.Constant.CrawlerPlatform;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;
import com.google.gson.*;


/**
 * 新浪粉丝通爬取任务.
 */
public class SinaFensitongJob extends CrawlerBaseJob {

    private final static CrawlerPlatform platform = CrawlerPlatform.SINA_FENSITONG;

    private final static Logger logger = ServerUtil.getLogger(platform);

    private final static String accountUrl = "http://bp.biz.weibo.com/bid/";

    private final static String groupUrl = "http://bp.biz.weibo.com/bid/groupajax/grouplist";

    private final static String modelUrl = "http://bp.biz.weibo.com/bid/groupajax/orderlist";

    private final static String planUrl = "http://bp.biz.weibo.com/bid/order/detail?orderId={0}&groupId={1}";

    private final static String pastPlanUrl = "http://bp.biz.weibo.com/bid/reporting/order";

    public SinaFensitongJob() {
        this(DateUtil.unixTimestamp000());
    }

    public SinaFensitongJob(long time) {
        super(time, platform);
    }

    public SinaFensitongJob(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }


    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        String cookie = account.getCookie();
        String content = "";
        List<AdPlanStat> poList = new ArrayList<>();
        if (DateUtil.unixTimestamp000() == time) {
            //实时请求
            //首先获取分组的id
            List<String> groupIds = groupSpider(cookie);
            //遍历分组
            if (groupIds != null) {
                for (String groupId : groupIds) {
                    //获取计划列表
                    List<SinaPlanModel> modelList = modelSpider(cookie, groupId);
                    //遍历计划列表
                    if (modelList != null) {
                        for (SinaPlanModel model : modelList) {
                            //获取详细计划单元
                            AdPlanStat adPlanStat = planItemSpider(time, account, model);
                            poList.add(adPlanStat);
                        }
                    }
                }
                if (!poList.isEmpty()) {
                    DataProcessor.processPlanDay(poList);
                }
            }
        } else {
            // 历史数据
            // 获取计划总数
            int page = 1;
            int pageSize = 30; // 正式环境改为50
            PageModel pageModel = new PageModel(page, pageSize);
            // 请求
            content = HttpUtil.getContentByPost(pastPlanUrl, getHeaderMap(cookie),
                    getFormData(page, pageSize, reportDate, reportDate));
            logger.info(content);
            // 解析
            poList = parsePastPlan(content, account, time, pageModel);
            // 计划数为0，直接跳过
            // 入库
            DataProcessor.processPlanDay(poList);
            // 爬取剩余页面
            page = 2;
            while (page <= pageModel.getTotalPages()) {
                // 请求
                content = HttpUtil.getContentByPost(pastPlanUrl, getHeaderMap(cookie),
                        getFormData(page, pageSize, reportDate, reportDate));
                logger.info(content);
                // 解析
                poList = parsePastPlan(content, account, time, pageModel);
                // 入库
                DataProcessor.processPlanDay(poList);
                page++;
            }
        }
    }

    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        // 请求
        String content = HttpUtil.getContentByGet(accountUrl, getHeaderMap(cookie));
        logger.info(content);
        // 解析
        AdAccountStat accountStat = parseAccount(content, account, time);
        // 入库
        DataProcessor.processAccountDay(accountStat);

    }

    /**
     * 解析账户余额响应
     *
     * @param content
     * @param account
     * @param time
     * @return
     * @throws Exception
     */
    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time) throws Exception {
        Document doc = Jsoup.parse(content);
        Element ele = doc.select("div.main_cont").first().select("div.account").first().select("div.cont").first();
        Elements nums = ele.select("a[href]");

        int balance = StringUtil.convertBigDecimal(nums.get(0).html(), 100, 0).intValue();
        int costToday = StringUtil.convertBigDecimal(nums.get(1).html(), 100, 0).intValue();
        AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                account.getId());
        accountStat.setBalance(balance);
        accountStat.setCostToday(costToday);
        return accountStat;
    }

    /**
     * 解析分组请求，获取所有分组ID的集合
     *
     * @return
     * @throws Exception
     */
    private List<String> groupSpider(String cookie) throws Exception {
        List<String> idList = new ArrayList<>();
        int pageNum = 1;
        int pageSize = 5;
        PageModel pageModel = new PageModel(pageNum, pageSize);
        //请求
        String content = HttpUtil.getContentByPost(groupUrl, getHeaderMap(cookie), getGroupParams(pageNum, pageSize));
        logger.info(content);
        //解析分组数据
        List<String> groupIdsOfOnePage = parseGroup(content, pageModel);
        idList.addAll(groupIdsOfOnePage);
        pageNum = 2;
        while (pageNum <= pageModel.getTotalPages()) {
            content = HttpUtil.getContentByPost(groupUrl, getHeaderMap(cookie), getGroupParams(pageNum, pageSize));
            logger.info(content);
            groupIdsOfOnePage = parseGroup(content, pageModel);
            idList.addAll(groupIdsOfOnePage);
            pageNum++;
        }
        return idList;


    }

    /**
     * 解析计划列表请求
     *
     * @param cookie
     * @return
     * @throws Exception
     */
    private List<SinaPlanModel> modelSpider(String cookie, String groupId) throws Exception {
        List<SinaPlanModel> planList = new ArrayList<>();
        int pageNum = 1;
        int pageSize = 5;
        PageModel pageModel = new PageModel(pageNum, pageSize);
        //请求
        String content = HttpUtil.getContentByPost(modelUrl, getHeaderMap(cookie), getModelParams(groupId, pageNum, pageSize));
        logger.info(content);
        //解析计划列表数据
        List<SinaPlanModel> modelListOfOnePage = parseModel(content, pageModel, groupId);
        planList.addAll(modelListOfOnePage);
        pageNum = 2;
        while (pageNum <= pageModel.getTotalPages()) {
            content = HttpUtil.getContentByPost(modelUrl, getHeaderMap(cookie), getModelParams(groupId, pageNum, pageSize));
            logger.info(content);
            //解析计划列表数据
            modelListOfOnePage = parseModel(content, pageModel, groupId);
            planList.addAll(modelListOfOnePage);
            pageNum++;
        }
        return planList;

    }

    /**
     * 解析详细计划单元请求
     *
     * @param time
     * @param account
     * @param sinaPlanModel
     * @return
     * @throws Exception
     */
    private AdPlanStat planItemSpider(long time, AdMediumAccount account, SinaPlanModel sinaPlanModel) throws Exception {
        String planId = sinaPlanModel.getId();
        String groupId = sinaPlanModel.getGroupId();
        String url = HttpUtil.urlParamReplace(planUrl, planId, groupId);
        String content = HttpUtil.getContentByGet(url, getHeaderMap(account.getCookie()));
        logger.info(content);
        AdPlanStat adPlanStat = parsePlan(content, account, sinaPlanModel);
        return adPlanStat;
    }

    /**
     * <pre>
     * 解析历史数据响应内容
     * @author sam.xie
     * @date Feb 15, 2017 3:12:29 PM
     * @param time
     * @throws ServerException
     */
    private List<AdPlanStat> parsePastPlan(String response, AdMediumAccount account, long time, PageModel pageModel)
            throws ServerException {
        List<AdPlanStat> poList = new ArrayList<>();
        // 返回结果集中类型不统一，同一个字段可能为字符串也可能为对象还可能为数字
        JsonObject jsonObj = StringUtil.jsonParser.parse(response).getAsJsonObject();
        int status = jsonObj.get("status").getAsInt();
        if (status != 0) {
            ExceptionHandler.throwException(ExceptionEnum.REQUEST_ERROR, response);
        } else {
            // 提取分页信息
            JsonObject data = jsonObj.get("data").getAsJsonObject();
            pageModel.setTotalNum(data.get("total").getAsInt());
            pageModel.setPageSize(data.get("pageSize").getAsInt());
            pageModel.setTotalPages(data.get("totalPages").getAsInt());

            // 解析报告
            JsonArray reports = jsonObj.get("data").getAsJsonObject().get("result").getAsJsonArray();
            if (reports != null) {
                for (JsonElement item : reports) {
                    JsonObject order = item.getAsJsonObject();
                    // 排除平均值和总计行
                    if (order.get("order_id") != null) {
                        // 点击数
                        int clickNum = StringUtil.getJsonIntValue(order, "click");
                        // 消耗
                        String cost = StringUtil.getJsonStrValue(order, "consume");
                        // 曝光数
                        int showNum = StringUtil.getJsonIntValue(order, "pv");
                        // 计划名称
                        String planName = order.get("order_name").getAsString();
                        // 计划id
                        String mPlanId = order.get("order_id").getAsString();
                        AdPlanStat reportPO = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(),
                                account.getId());
                        poList.add(reportPO);
                        reportPO.setClickNum(clickNum);
                        // 由单位元转成分
                        reportPO.setCost(new BigDecimal(cost).multiply(new BigDecimal(100))
                                .setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
                        reportPO.setShowNum(showNum);
                        reportPO.setPlanName(planName);
                        reportPO.setmPlanId(mPlanId);
                    }
                }
            }
        }
        return poList;
    }

    /**
     * 构建历史计划请求表单数据
     *
     * @param page
     * @param pageSize
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<NameValuePair> getFormData(int page, int pageSize, String beginDate, String endDate) {
        List<NameValuePair> pairs = new ArrayList<>();
        pairs.add(new BasicNameValuePair("order_name", ""));
        pairs.add(new BasicNameValuePair("group_name", ""));
        pairs.add(new BasicNameValuePair("settle_type", "-1"));
        pairs.add(new BasicNameValuePair("start_time", beginDate));
        pairs.add(new BasicNameValuePair("end_time", endDate));
        pairs.add(new BasicNameValuePair("feedUid", ""));
        // pairs.add(new BasicNameValuePair("customer_id", "0"));
        pairs.add(new BasicNameValuePair("page", String.valueOf(page)));
        pairs.add(new BasicNameValuePair("pageSize", String.valueOf(pageSize)));
        return pairs;
    }

    /**
     * 构建分组列表请求参数
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    private List<NameValuePair> getGroupParams(int pageNum, int pageSize) {
        List<NameValuePair> pairs = new ArrayList<>();
        pairs.add(new BasicNameValuePair("groupName", ""));
        pairs.add(new BasicNameValuePair("groupStatus", "-1"));
        pairs.add(new BasicNameValuePair("display", "0"));
        pairs.add(new BasicNameValuePair("page", String.valueOf(pageNum)));
        pairs.add(new BasicNameValuePair("pageSize", String.valueOf(pageSize)));
        return pairs;
    }

    /**
     * 构建计划列表请求参数
     *
     * @param groupId
     * @param pageNum
     * @param pageSize
     * @return
     */
    private List<NameValuePair> getModelParams(String groupId, int pageNum, int pageSize) {
        List<NameValuePair> pairs = new ArrayList<>();
        pairs.add(new BasicNameValuePair("groupId", groupId));
        pairs.add(new BasicNameValuePair("page", String.valueOf(pageNum)));
        pairs.add(new BasicNameValuePair("pageSize", String.valueOf(pageSize)));
        pairs.add(new BasicNameValuePair("orderName", ""));
        pairs.add(new BasicNameValuePair("orderStart", ""));
        pairs.add(new BasicNameValuePair("orderEnd", ""));
        pairs.add(new BasicNameValuePair("orderType", "-1"));
        pairs.add(new BasicNameValuePair("orderStatus", "-1"));
        return pairs;
    }

    /**
     * 解析投放分组响应
     *
     * @param content
     * @param pageModel
     * @return
     * @throws Exception
     */
    private List<String> parseGroup(String content, PageModel pageModel) throws Exception {
        List<String> groupIds = new ArrayList<>();
        // 返回结果集中类型不统一，同一个字段可能为字符串也可能为对象还可能为数字
        JsonObject jsonObj = StringUtil.jsonParser.parse(content).getAsJsonObject();
        int status = jsonObj.get("status").getAsInt();
        if (status == 0) {
            //提取分页信息
            JsonObject data = jsonObj.get("data").getAsJsonObject();
            pageModel.setTotalNum(data.get("total").getAsInt());
            pageModel.setPageSize(data.get("pageSize").getAsInt());
            pageModel.setTotalPages(data.get("totalPages").getAsInt());

            JsonArray groups = jsonObj.get("data").getAsJsonObject().get("result").getAsJsonArray();
            if (groups != null) {
                for (JsonElement item : groups) {
                    JsonObject group = item.getAsJsonObject();
                    String id = group.get("id").getAsString();
                    if (null != id) {
                        groupIds.add(id);
                    }
                }
            }
        } else {
            ExceptionHandler.throwException(ExceptionEnum.REQUEST_ERROR, content);
        }
        return groupIds;

    }

    /**
     * 解析计划列表响应，返回SinaPlanModel类型的初步计划单元数据
     *
     * @param content
     * @param pageModel
     * @return
     * @throws Exception
     */
    private List<SinaPlanModel> parseModel(String content, PageModel pageModel, String groupId) throws Exception {
        List<SinaPlanModel> modelList = new ArrayList<>();
        // 返回结果集中类型不统一，同一个字段可能为字符串也可能为对象还可能为数字
        JsonObject jsonObj = StringUtil.jsonParser.parse(content).getAsJsonObject();
        int status = jsonObj.get("status").getAsInt();
        if (status == 0) {
            //提取分页信息
            JsonObject data = jsonObj.get("data").getAsJsonObject();
            pageModel.setTotalNum(data.get("total").getAsInt());
            pageModel.setPageSize(data.get("pageSize").getAsInt());
            pageModel.setTotalPages(data.get("totalPages").getAsInt());
            JsonArray groups = jsonObj.get("data").getAsJsonObject().get("result").getAsJsonArray();
            if (groups != null) {
                for (JsonElement item : groups) {
                    JsonObject group = item.getAsJsonObject();
                    //计划ID
                    String id = group.get("id").getAsString();
                    //计划出价
                    String price = group.get("price").getAsString();//0.01的格式
                    //计划状态
                    String deliveryStatus = group.get("delivery_status").getAsString();
                    SinaPlanModel sinaPlanModel = new SinaPlanModel(id, groupId, price, deliveryStatus);
                    modelList.add(sinaPlanModel);
                }
            }
        } else {
            ExceptionHandler.throwException(ExceptionEnum.REQUEST_ERROR, content);
        }
        return modelList;

    }

    /**
     * 解析详细计划单元，返回计划单元对应的映射类
     *
     * @param content
     * @param account
     * @param sinaPlanModel
     * @return
     * @throws Exception
     */
    private AdPlanStat parsePlan(String content, AdMediumAccount account, SinaPlanModel sinaPlanModel) throws Exception {
        // 构建计划统计对象
        AdPlanStat planPO = new AdPlanStat(DateUtil.unixTimestamp000(), account.getCompanyId(), account.getPlatformId(),
                account.getId());
        Document document = Jsoup.parse(content);
        // 选中今日概览的指标
        Elements elements = document.select("div.ovData[node-type=dataReviewNode] table.ovTable span.ovValue ");
        if (elements == null) {
            return null;
        }
        // 收集指标
        List<String> indicators = new ArrayList<>();
        Iterator it = elements.iterator();
        while (it.hasNext()) {
            Element element = (Element) it.next();
            indicators.add(StringUtils.trim(element.text()));
        }
        if (indicators.size() != 6) {
            return null;
        }
        // 获取计划名称
        elements = document.select("#mainWrap div.pd_item table.pd_table tr:first-child td:first-child ");
        if (elements != null) {
            String text = elements.text();
            if (StringUtils.isNotBlank(text)) {
                String planName = StringUtils.trim(StringUtils.substringAfter(text, "计划名称："));
                //计划名称
                planPO.setPlanName(planName);
            }
        }
        // 获取出价方式
        elements = document.select("#mainWrap div.pd_item table.pd_table tr:first-child td:nth-child(2) ");
        if (null != elements) {
            String text = elements.text();
            if (StringUtils.isNotBlank(text)) {
                String bidStrategy = StringUtils.trim(StringUtils.substringAfter(text, "竞价方式："));
                //出价方式
                planPO.setPlanBidStrategy(bidStrategy);
            }
        }
        //计划ID
        planPO.setmPlanId(sinaPlanModel.getId());
        //计划出价
        double bidPrice = StringUtil.convertDouble(sinaPlanModel.getPrice(), 0.00);
        planPO.setBid(new BigDecimal(bidPrice).multiply(new BigDecimal(100))
                .setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
        // 计划状态
        planPO.setPlanStatus(sinaPlanModel.getDeliveryStatus());
        // 点击量
        planPO.setClickNum(Integer.valueOf(StringUtils.defaultIfEmpty(indicators.get(2).replace(",", ""), "0")));
        // 消耗  单位由元转成分
        planPO.setCost(new BigDecimal(100)
                .multiply(new BigDecimal(StringUtils.defaultIfEmpty(indicators.get(0).replace(",", ""), "0")))
                .setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
        //曝光量
        planPO.setShowNum(Integer.valueOf(StringUtils.defaultIfEmpty(indicators.get(1).replace(",", ""), "0")));

        return planPO;
    }

    /**
     * 设置请求头
     *
     * @param cookie
     */
    private Map<String, String> getHeaderMap(String cookie) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Cookie", cookie);
        headerMap.put("Host", "bp.biz.weibo.com");
        headerMap.put("Origin", "bp.biz.weibo.com");
        headerMap.put("Referer", "http://bp.biz.weibo.com/bid/data/order");
        headerMap
                .put("User-Agent",
                        "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36");
        headerMap.put("X-Requested-With", "XMLHttpRequest");
        headerMap.put("Accept", "application/json, text/javascript, */*; q=0.01");
        headerMap.put("Accept-Language", "zh-CN,zh;q=0.8");
        headerMap.put("Accept-Encoding", "gzip, deflate");
        headerMap.put("Connection", "keep-alive");
        headerMap.put("Upgrade-Insecure-Requests", "1");
        headerMap.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        return headerMap;
    }

}
