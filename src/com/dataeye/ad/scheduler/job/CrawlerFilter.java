package com.dataeye.ad.scheduler.job;

/**
 * 过滤参数实体类 Created by VitAs on 2017/7/18.
 */
public class CrawlerFilter {

	private Integer accountId;

	private Integer companyId;

	private String account;

	public CrawlerFilter() {
	}

	public CrawlerFilter(Integer accountId, Integer companyId) {
		this.accountId = accountId;
		this.companyId = companyId;
	}

	public CrawlerFilter(Integer accountId, Integer companyId, String account) {
		this(accountId, companyId);
		this.account = account;
	}

	public Integer getAccountId() {
		return accountId;
	}

	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

}
