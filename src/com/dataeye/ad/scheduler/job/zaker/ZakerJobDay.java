package com.dataeye.ad.scheduler.job.zaker;

import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.scheduler.job.CrawlerBaseExecutor;
import com.dataeye.ad.util.DateUtil;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Created by VitAs on 2017/8/8.
 */
public class ZakerJobDay implements Job {

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        CrawlerBaseExecutor.addJob(new ZakerJob(DateUtil.unixTimestamp000() - Constant.ONEDAY));

    }
}
