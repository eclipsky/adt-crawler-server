package com.dataeye.ad.scheduler.job.netease.model;

/**
 * 网易易效新平台请求参数扩展值
 * Created by luzhuyou 2017/04/14
 */
public class ExtLimit {
    private int start;
    private int limit;
    private String sort;
    private String dir;
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getDir() {
		return dir;
	}
	public void setDir(String dir) {
		this.dir = dir;
	}
	@Override
	public String toString() {
		return "NeteaseNewRequest [start=" + start + ", limit=" + limit
				+ ", sort=" + sort + ", dir=" + dir + "]";
	}
}

