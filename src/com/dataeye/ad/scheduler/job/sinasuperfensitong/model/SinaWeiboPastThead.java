package com.dataeye.ad.scheduler.job.sinasuperfensitong.model;

public class SinaWeiboPastThead {
	
	private String date; //时间
	
	private String ad; //广告计划
	
	private String pv; // 曝光

	private String consume; // 花费

	private String trafficBhv; // 导流数（点击）

	private String bhv; // 互动

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getAd() {
		return ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public String getPv() {
		return pv;
	}

	public void setPv(String pv) {
		this.pv = pv;
	}

	public String getConsume() {
		return consume;
	}

	public void setConsume(String consume) {
		this.consume = consume;
	}

	public String getTrafficBhv() {
		return trafficBhv;
	}

	public void setTrafficBhv(String trafficBhv) {
		this.trafficBhv = trafficBhv;
	}

	public String getBhv() {
		return bhv;
	}

	public void setBhv(String bhv) {
		this.bhv = bhv;
	}
	
	

}
