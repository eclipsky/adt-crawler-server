package com.dataeye.ad.pipeline.po;

public class AdPkg extends AdBase {

	private String pkgName;

	private String cpAppId;

	private String cpPkgId;

	public String getPkgName() {
		return pkgName;
	}

	public void setPkgName(String pkgName) {
		this.pkgName = pkgName;
	}

	public String getCpAppId() {
		return cpAppId;
	}

	public void setCpAppId(String cpAppId) {
		this.cpAppId = cpAppId;
	}

	public String getCpPkgId() {
		return cpPkgId;
	}

	public void setCpPkgId(String cpPkgId) {
		this.cpPkgId = cpPkgId;
	}

}
