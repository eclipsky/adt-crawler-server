package com.dataeye.ad.scheduler.job.mta.model;

import java.util.List;

public class MtaDownloadModel {

	private List<MtaDownloadData> data;

	private MtaPage page;

	public List<MtaDownloadData> getData() {
		return data;
	}

	public void setData(List<MtaDownloadData> data) {
		this.data = data;
	}

	public MtaPage getPage() {
		return page;
	}

	public void setPage(MtaPage page) {
		this.page = page;
	}

}

