package com.dataeye.ad.scheduler.job.baidusearch.model;

public class BaiduResponse {

	private int status;

	private BaiduModel data;

	private BaiduError error;

	private BaiduErrorCode errorCode;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public BaiduModel getData() {
		return data;
	}

	public void setData(BaiduModel data) {
		this.data = data;
	}

	public BaiduError getError() {
		return error;
	}

	public void setError(BaiduError error) {
		this.error = error;
	}

	public BaiduErrorCode getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(BaiduErrorCode errorCode) {
		this.errorCode = errorCode;
	}

}
