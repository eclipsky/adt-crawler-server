package com.dataeye.ad.scheduler.job.sinasuperfensitong;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.dataeye.ad.scheduler.job.CrawlerFilter;

import org.slf4j.Logger;

import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.constant.Constant.CrawlerPlatform;
import com.dataeye.ad.constant.Constant.PlanSwitch;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.DBQuery;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.scheduler.job.sinasuperfensitong.model.AdAccountStatResponse;
import com.dataeye.ad.scheduler.job.sinasuperfensitong.model.AdAccountStatResult;
import com.dataeye.ad.scheduler.job.sinasuperfensitong.model.SinaWeiboPastResponse;
import com.dataeye.ad.scheduler.job.sinasuperfensitong.model.SinaWeiboPastResult;
import com.dataeye.ad.scheduler.job.sinasuperfensitong.model.SinaWeiboPlan;
import com.dataeye.ad.scheduler.job.sinasuperfensitong.model.SinaWeiboPlanPastTbody;
import com.dataeye.ad.scheduler.job.sinasuperfensitong.model.SinaWeiboPlanPastVo;
import com.dataeye.ad.scheduler.job.sinasuperfensitong.model.SinaWeiboResponse;
import com.dataeye.ad.scheduler.job.sinasuperfensitong.model.SinaWeiboResult;
import com.dataeye.ad.util.CookieUtil;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;

public class SinaSuperFensitongJob extends CrawlerBaseJob {

	private final static CrawlerPlatform platform = CrawlerPlatform.SINA_SUPERFENSITONG;

	private final static Logger logger = ServerUtil.getLogger(platform);

	private final static String planUrl = "http://ad.weibo.com/aj/ad/list?customer_id={0}&reportData=1&pageSize={1}&page={2}";

	private final static String accountUrl = "http://ad.weibo.com/aj/widget/banlance?customer_id={0}";

	private final static String planUrlPast = "http://ad.weibo.com/aj/data/getEffectTable?customer_id={0}&startTime={1}&endTime={2}&rows={3}&page={4}&field[]=pv&field[]=consume&field[]=traffic_bhv&field[]=bhv&granularity[]=ad&granularity[]=date&dimension[]=ad&orderBy[]=date&orderMode=desc&tableType=effect&mergeType=normal";

	private final static Map<String, Integer> planIdCache = new ConcurrentHashMap<String, Integer>();

	public SinaSuperFensitongJob() {
		this(DateUtil.unixTimestamp000());
	}

	public SinaSuperFensitongJob(long time) {
		super(time, platform);
	}

	public SinaSuperFensitongJob(long time, CrawlerFilter filter) {
		super(time, platform, filter);
	}


	@Override
	public void accountSpider(long time, AdMediumAccount account) throws Exception {
		String cookie = account.getCookie();
		String customerId = CookieUtil.getSinaWeiboAdCustomerId(cookie, account);
		account.setSkey(customerId);
		if (StringUtil.isEmpty(customerId)) {
			ExceptionHandler.throwCookieException(ExceptionEnum.SKEY_NOT_EXISTS, "customer_id不存在");
		}
		// 请求
		String content = HttpUtil.getContentByGet(HttpUtil.urlParamReplace(accountUrl, customerId),
				getHeaderMap(cookie));
		logger.info(content);
		// 解析
		AdAccountStat accountStat = parseAccount(content, account, time);
		// 入库
		DataProcessor.processAccountDay(accountStat);
	}

	@Override
	public void planSpider(long time, AdMediumAccount account) throws Exception {
		String cookie = account.getCookie();
		String customerId = CookieUtil.getSinaWeiboAdCustomerId(cookie, account);
		if (StringUtil.isEmpty(customerId)) {
			ExceptionHandler.throwCookieException(ExceptionEnum.SKEY_NOT_EXISTS, "customer_id不存在");
		}
		account.setSkey(customerId);
		if (time >= DateUtil.unixTimestamp000()) {
			planToday(time, account);
		} else {
			planPast(time, account);
		}

	}

	private void planToday(long time, AdMediumAccount account) throws Exception {
		String cookie = account.getCookie();
		String customerId = account.getSkey();
		int pageId = 1;
		int pageSize = 10; // 正式环境改为50
		PageModel pageModel = new PageModel(pageId, pageSize);
		// 请求
		String content = HttpUtil.getContentByGet(
				HttpUtil.urlParamReplace(planUrl, customerId, pageSize + "", pageId + ""), getHeaderMap(cookie));
		logger.info(content);
		// 解析
		List<AdPlanStat> poList = parsePlan(content, account, time, pageModel);
		int totalNum = pageModel.getTotalNum();
		// 入库
		DataProcessor.processPlanDay(poList);
		totalNum -= pageSize;
		while (totalNum > 0) {
			pageId++;
			// 请求
			content = HttpUtil.getContentByGet(
					HttpUtil.urlParamReplace(planUrl, customerId, pageSize + "", pageId + ""), getHeaderMap(cookie));
			logger.info(content);
			// 解析
			poList = parsePlan(content, account, time, pageModel);
			// 入库
			DataProcessor.processPlanDay(poList);
			totalNum -= pageSize;
		}
	}

	private void planPast(long time, AdMediumAccount account) throws Exception {
		String statDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
		String cookie = account.getCookie();
		String customerId = account.getSkey();
		int pageId = 1;
		int pageSize = 10;
		PageModel pageModel = new PageModel(pageId, pageSize);
		// 请求
		String content = HttpUtil.getContentByGet(
				HttpUtil.urlParamReplace(planUrlPast, customerId, statDate, statDate, pageSize + "", pageId + ""),
				getHeaderMap(cookie));
		logger.info(content);
		// 解析
		List<AdPlanStat> poList = parsePlanPast(content, account, time, pageModel);
		int totalNum = pageModel.getTotalNum();
		// 入库
		DataProcessor.processPlanDay(poList);
		totalNum -= pageSize;
		while (totalNum > 0) {
			pageId++;
			// 请求
			content = HttpUtil.getContentByGet(
					HttpUtil.urlParamReplace(planUrlPast, customerId, statDate, statDate, pageSize + "", pageId + ""),
					getHeaderMap(cookie));
			logger.info(content);
			// 解析
			poList = parsePlanPast(content, account, time, pageModel);
			// 入库
			DataProcessor.processPlanDay(poList);
			totalNum -= pageSize;
		}
	}

	/**
	 * <pre>
	 * 解析响应内容，封装成标准的PO对象
	 * @author sam.xie
	 * @date Feb 15, 2017 3:12:29 PM
	 * @param accountID
	 * @param jdbcTemplate
	 * @param list
	 * @param time
	 * @throws Exception
	 */
	private List<AdPlanStat> parsePlan(String content, AdMediumAccount account, long time, PageModel pageModel)
			throws Exception {
		SinaWeiboResponse response = StringUtil.gson.fromJson(content, SinaWeiboResponse.class);
		SinaWeiboResult result = response.getResult();
		pageModel.setTotalPages(result.getPageTotal());
		pageModel.setPageSize(result.getPageSize());
		pageModel.setTotalNum(result.getTotal());
		List<AdPlanStat> poList = new ArrayList<AdPlanStat>();
		for (SinaWeiboPlan plan : result.getList()) {
			AdPlanStat reportPO = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(), account.getId());
			poList.add(reportPO);
			int clickNum = plan.getReport().getTrafficBhv();
			int cost = plan.getReport().getConsume() * 100;
			int showNum = StringUtil.convertInt(plan.getReport().getPv(), 0);
			// 媒体计划ID
			String mPlanId = plan.getId() + "";
			// 媒体计划名
			String planName = plan.getName();
			// 媒体计划分组ID
			String groupId = plan.getCampaign().getId() + "";
			// 媒体计划分组名
			String groupName = plan.getCampaign().getName();
			// 媒体计划出价
			int planBid = StringUtil.convertBigDecimal(plan.getBidAmount(), 100, 0).intValue();
			// 媒体计划出价策略
			String planBidStrategy = plan.getBillingType() + "";
			// 媒体计划状态
			String planStatus = plan.getEffectiveStatusStr();
			// 媒体计划开关
			int planSwitchRaw = plan.getConfiguredStatus();
			Integer planSwitch = planSwitchRaw == 1 ? PlanSwitch.ON : PlanSwitch.OFF;

			reportPO.setmPlanId(mPlanId);
			reportPO.setPlanName(planName);
			reportPO.setClickNum(clickNum);
			reportPO.setCost(cost);
			reportPO.setShowNum(showNum);
			reportPO.setBid(planBid);
			reportPO.setPlanBidStrategy(planBidStrategy);
			reportPO.setGroupId(groupId);
			reportPO.setGroupName(groupName);
			reportPO.setPlanStatus(planStatus);
			reportPO.setPlanSwitch(planSwitch);
		}
		return poList;
	}

	private Map<String, String> getHeaderMap(String cookie) {
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("Cookie", cookie);
		headerMap.put("Host", "ad.weibo.com");
		headerMap.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0");
		headerMap.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		headerMap.put("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
		headerMap.put("Accept-Encoding", "gzip, deflate");
		headerMap.put("Referer", "http://ad.weibo.com/ad/index/manage?customer_id=6000816904");
		headerMap.put("Connection", "keep-alive");
		headerMap.put("X-Requested-With", "XMLHttpRequest");
		return headerMap;
	}

	/**
	 * 解析相应内容
	 * 
	 * */
	public List<AdPlanStat> parsePlanPast(String content, AdMediumAccount account, long time, PageModel pageModel) {
		List<AdPlanStat> poList = new ArrayList<AdPlanStat>();
		SinaWeiboPastResponse response = StringUtil.gson.fromJson(content, SinaWeiboPastResponse.class);
		// int code = response.getCode();
		SinaWeiboPastResult result = response.getResult();
		SinaWeiboPlanPastVo resultList = result.getList();
		pageModel.setTotalPages(result.getPageTotal());
		pageModel.setPageSize(result.getPageSize());
		pageModel.setTotalNum(result.getTotal());
		for (SinaWeiboPlanPastTbody plan : resultList.getTbody()) {
			AdPlanStat planPO = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(), account.getId());

			int clickNum = plan.getTrafficBhv();
			int cost = StringUtil.convertInt(plan.getConsume() * 100 + "", 0);
			int showNum = plan.getPv();
			// 媒体计划ID
			String mPlanId = plan.getAdId() + "";

			planPO.setClickNum(clickNum);
			planPO.setCost(cost);
			planPO.setShowNum(showNum);
			planPO.setmPlanId(mPlanId);

			/**
			 * 超级粉丝通历史报表数据不能获取到group_id，groupName等信息，这里先通过数据库补全mPlanId和相关字段才能保证后台数据准确性
			 * 
			 * */

			int planId = getPlanId(account.getCompanyId(), account.getPlatformId(), account.getId(), mPlanId);
			planPO.setPlanId(planId);
			if (planId <= 0) {
				continue;
			}
			poList.add(planPO);
		}
		return poList;
	}

	private int getPlanId(int companyId, int mediumId, int accountId, String mPlanId) {
		String cachedKey = companyId + "-" + mediumId + "-" + accountId + "-" + mPlanId;
		Integer planId = planIdCache.get(cachedKey);
		if (planId == null || planId <= 0) {
			planId = DBQuery.getPlanIdForSinaWeiboAd(companyId, mediumId, accountId, mPlanId);
			if (planId > 0) {
				planIdCache.put(cachedKey, planId);
			}
		}
		return planId;
	}

	/**
	 * 解析媒体账户余额相关统计信息
	 * 
	 * **/
	public AdAccountStat parseAccount(String content, AdMediumAccount account, long time) {
		AdAccountStatResponse response = StringUtil.gson.fromJson(content, AdAccountStatResponse.class);
		AdAccountStatResult result = response.getResult();
		AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
				account.getId());
		int balance = StringUtil.convertInt(result.getAccountBalance() * 100 + "", 0);
		int costToday = StringUtil.convertInt(result.getRealUse() * 100 + "", 0);
		accountStat.setBalance(balance);
		accountStat.setCostToday(costToday);
		return accountStat;
	}
}
