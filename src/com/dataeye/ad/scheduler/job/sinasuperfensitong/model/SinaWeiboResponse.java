package com.dataeye.ad.scheduler.job.sinasuperfensitong.model;

public class SinaWeiboResponse {
	private int code;

	private SinaWeiboResult result;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public SinaWeiboResult getResult() {
		return result;
	}

	public void setResult(SinaWeiboResult result) {
		this.result = result;
	}

}
