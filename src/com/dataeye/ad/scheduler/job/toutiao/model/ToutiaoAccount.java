package com.dataeye.ad.scheduler.job.toutiao.model;

public class ToutiaoAccount {

	private long advertiser_id;

	private String advertiser_name;

	private String balance;

	private String today_cost;

	public long getAdvertiser_id() {
		return advertiser_id;
	}

	public void setAdvertiser_id(long advertiser_id) {
		this.advertiser_id = advertiser_id;
	}

	public String getAdvertiser_name() {
		return advertiser_name;
	}

	public void setAdvertiser_name(String advertiser_name) {
		this.advertiser_name = advertiser_name;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getToday_cost() {
		return today_cost;
	}

	public void setToday_cost(String today_cost) {
		this.today_cost = today_cost;
	}

}
