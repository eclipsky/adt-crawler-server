package com.dataeye.ad.pipeline.po;

import java.util.Calendar;
import java.util.Date;

/**
 * 统计表基础类
 * 
 * @author sam.xie
 * @date Mar 6, 2017 5:07:15 PM
 * @version 1.0
 */
public abstract class AdStatBase {

	/**
	 * <pre/>
	 * 事件 
	 * ACCOUNT：媒体账户 
	 * PLAN：计划统计 
	 * PAGE：落地页
	 */
	private String event;

	private Date statDate;

	private int companyId;

	private Date updateTime;

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public Date getStatDate() {
		return statDate;
	}

	public void setStatDate(Date statDate) {
		this.statDate = statDate;
		this.updateTime = Calendar.getInstance().getTime();
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

}
