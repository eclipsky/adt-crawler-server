package com.dataeye.ad.common;

/**
 * 异常处理器
 * 
 * @author sam.xie
 * @date Feb 10, 2017 2:27:10 PM
 * @version 1.0
 */
public class ExceptionHandler {

	public static void throwException(String message) throws ServerException {
		throw new ServerException(message);
	}

	public static void throwException(ExceptionEnum exception) throws ServerException {
		throw new ServerException(exception);
	}

	public static void throwException(ExceptionEnum exception, String msg) throws ServerException {
		throw new ServerException(exception, msg);
	}

	public static void throwCookieException(ExceptionEnum exception) throws CookieException {
		throw new CookieException(exception);
	}

	public static void throwCookieException(ExceptionEnum exception, String msg) throws CookieException {
		throw new CookieException(exception, msg);
	}

}
