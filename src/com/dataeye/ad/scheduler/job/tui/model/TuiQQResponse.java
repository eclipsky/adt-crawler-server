package com.dataeye.ad.scheduler.job.tui.model;


public class TuiQQResponse {

	private int code;

	private String msg;

	private TuiQQData data;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public TuiQQData getData() {
		return data;
	}

	public void setData(TuiQQData data) {
		this.data = data;
	}

}
