package com.dataeye.ad.scheduler.job.toutiao.model;

public class ToutiaoAccountResponse {

	private String status;

	private ToutiaoAccount data;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ToutiaoAccount getData() {
		return data;
	}

	public void setData(ToutiaoAccount data) {
		this.data = data;
	}

}
