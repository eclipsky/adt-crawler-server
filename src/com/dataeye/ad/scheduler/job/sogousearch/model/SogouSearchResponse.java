package com.dataeye.ad.scheduler.job.sogousearch.model;

public class SogouSearchResponse {
	private String flag;

	private String[] msg;

	private SogouSearchData[] data;

	private SogouSearchAjaxpc[] ajaxpc;

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String[] getMsg() {
		return msg;
	}

	public void setMsg(String[] msg) {
		this.msg = msg;
	}

	public SogouSearchData[] getData() {
		return data;
	}

	public void setData(SogouSearchData[] data) {
		this.data = data;
	}

	public SogouSearchAjaxpc[] getAjaxpc() {
		return ajaxpc;
	}

	public void setAjaxpc(SogouSearchAjaxpc[] ajaxpc) {
		this.ajaxpc = ajaxpc;
	}

}
