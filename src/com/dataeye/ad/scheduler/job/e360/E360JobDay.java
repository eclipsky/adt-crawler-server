package com.dataeye.ad.scheduler.job.e360;

import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.scheduler.job.CrawlerBaseExecutor;
import com.dataeye.ad.util.DateUtil;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Created by VitAs on 2017/7/10.
 */
public class E360JobDay implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        CrawlerBaseExecutor.addJob(new E360Job(DateUtil.unixTimestamp000() - Constant.ONEDAY));
    }
}
