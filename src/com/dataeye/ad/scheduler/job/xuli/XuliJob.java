package com.dataeye.ad.scheduler.job.xuli;

import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.scheduler.job.CrawlerFilter;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;

import javax.print.Doc;
import java.util.*;

/**
 * @author lingliqi
 * @date 2018-01-05 9:46
 */
public class XuliJob extends CrawlerBaseJob {

    private final static Constant.CrawlerPlatform platform = Constant.CrawlerPlatform.XULI;

    private final static Logger logger = ServerUtil.getLogger(platform);

    private final static String planUrl = "http://www.xuli.com/?action=advertiser&opt=planList&planid=&ads_type=&plat=0&page={0}";

    private final static String reportUrl = "http://www.xuli.com/?action=advertiser&opt=main&export=0&dateopt=&start_date={0}&end_date={1}&planid=&ads_type=&plat=0";

    private final static String accountUrl = "http://www.xuli.com/?action=advertiser&opt=main";

    public XuliJob() {
        this(DateUtil.unixTimestamp000());
    }

    public XuliJob(long time) {
        super(time, platform);
    }

    public XuliJob(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }

    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        String content = HttpUtil.getContentByGet(accountUrl, getHeaderMap(cookie));
        logger.info(content);
        AdAccountStat adAccountStat = parseAccount(content, account, time);
        DataProcessor.processAccountDay(adAccountStat);

    }

    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        if (DateUtil.unixTimestamp000() == time) {
            List<AdPlanStat> poList = new ArrayList<>();
            Map<String, AdPlanStat> statMap = reportSpider(time, account);
            // 实时请求
            String cookie = account.getCookie();
            int page = 1;
            String content = HttpUtil.getContentByGet(HttpUtil.urlParamReplace(planUrl, page + ""), getHeaderMap(cookie));
            logger.info(content);
            List<AdPlanStat> tempList = parsePlan(content, account, time, statMap);
            poList.addAll(tempList);
            while (true) {
                page++;
                content = HttpUtil.getContentByGet(HttpUtil.urlParamReplace(planUrl, page + ""), getHeaderMap(cookie));
                logger.info(content);
                tempList = parsePlan(content, account, time, statMap);
                if (tempList.isEmpty()) {
                    break;
                }
                poList.addAll(tempList);
            }
            DataProcessor.processPlanDay(poList);
        } else {
            // 历史数据
            Map<String, AdPlanStat> statMap = reportSpider(time, account);
            DataProcessor.processPlanDay(new ArrayList<>(statMap.values()));
        }

    }

    private Map<String, AdPlanStat> reportSpider(long time, AdMediumAccount account) throws Exception {
        String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        String cookie = account.getCookie();
        String content = HttpUtil.getContentByGet(HttpUtil.urlParamReplace(reportUrl, reportDate, reportDate), getHeaderMap(cookie));
        logger.info(content);
        Map<String, AdPlanStat> statMap = parseReport(content, account, time);
        return statMap;
    }

    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time) throws Exception {
        AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                account.getId());
        //jsoup解析html内容
        Document doc = Jsoup.parse(content);
        Elements elements = doc.select(".top_sele>ul:nth-child(2)>li");
        if (elements.text().contains("余额")) {
            String balanceStr = elements.text().replace("账号余额：￥", "").replace(",", "").trim();
            int balance = StringUtil.convertBigDecimal(balanceStr, 100, 0).intValue();
            accountStat.setBalance(balance);
        }
        return accountStat;
    }

    private Map<String, AdPlanStat> parseReport(String content, AdMediumAccount account, long time) throws Exception {
        Map<String, AdPlanStat> statMap = new HashMap<>();
        Document doc = Jsoup.parse(content);
        Elements elements = doc.select("#tab_content_0>.datalist>tbody>tr");
        // 去除表头和表脚
        elements.remove(0);
        elements.remove(elements.size() - 1);
        for (Element ele : elements) {
            AdPlanStat reportPO = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(),
                    account.getId());
            String mPlanId = ele.select("td:first-child").text();
            String planName = ele.select("td:nth-child(3)").text();
            int showNum = StringUtil.convertInt(ele.select("td:nth-child(7)").text(), 0);
            int clickNum = StringUtil.convertInt(ele.select("td:nth-child(8)").text(), 0);
            int cost = StringUtil.convertBigDecimal(ele.select("td:nth-child(10)").text(), 100, 0).intValue();
            reportPO.setmPlanId(mPlanId);
            reportPO.setPlanName(planName);
            reportPO.setClickNum(clickNum);
            reportPO.setShowNum(showNum);
            reportPO.setCost(cost);
            statMap.put(mPlanId, reportPO);
        }
        return statMap;
    }

    private List<AdPlanStat> parsePlan(String content, AdMediumAccount account, long time, Map<String, AdPlanStat> statMap) throws Exception {
        List<AdPlanStat> poList = new ArrayList<>();
        Document doc = Jsoup.parse(content);
        Elements elements = doc.select(".datalist>tbody>tr");
        elements.remove(0);
        if (!elements.isEmpty()) {
            for (Element ele : elements) {
                AdPlanStat adPlanStat = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(),
                        account.getId());
                poList.add(adPlanStat);
                String mPlanId = ele.select("td:nth-child(2)").text();
                String planName = ele.select("td:nth-child(3)").text();
                String bidStr = ele.select("td:nth-child(6)").text().replace("元/1000IP", "").replace(",", "").trim();
                int bid = StringUtil.convertBigDecimal(bidStr, 100, 0).intValue();
                String planStatus = ele.select("td:nth-child(9)").text();
                adPlanStat.setmPlanId(mPlanId);
                adPlanStat.setPlanName(planName);
                adPlanStat.setBid(bid);
                adPlanStat.setPlanStatus(planStatus);
                AdPlanStat reportPO = statMap.get(mPlanId);
                if (reportPO != null) {
                    adPlanStat.setClickNum(reportPO.getClickNum());
                    adPlanStat.setShowNum(reportPO.getShowNum());
                    adPlanStat.setCost(reportPO.getCost());
                }
            }
        }
        return poList;
    }

    private Map<String, String> getHeaderMap(String cookie) {
        Map<String, String> headMap = new HashMap<>();
        headMap.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        headMap.put("Accept-Encoding", "gzip, deflate");
        headMap.put("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.6,en;q=0.4");
        headMap.put("Connection", "keep-alive");
        headMap.put("Cookie", cookie);
        headMap.put("Host", "www.xuli.com");
        headMap.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36");
        return headMap;
    }


}
