package com.dataeye.ad.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;

import com.dataeye.ad.constant.Constant.CrawlerPlatform;
import com.dataeye.ad.pipeline.po.AdAccountBase;
import com.dataeye.util.log.DELogger;

public class ServerUtil {

    private final static Logger logger = DELogger.getLogger("server_util");

    private final static Map<String, Integer> accountMailMap = new HashMap<String, Integer>();

    private final static int mailInterval = 6;

    public static String printStackTrace(Throwable e) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        e.printStackTrace(pw);
        if (pw != null) {
            pw.close();
        }
        if (sw != null) {
            try {
                sw.close();
            } catch (IOException e1) {
                // e1.printStackTrace();
            }
        }
        return sw.toString();
    }

    public static Properties getConfigProperties() {
        Properties properties = new Properties();
        try {
            properties.load(ClassLoader.getSystemResourceAsStream("config.properties"));
        } catch (IOException e) {
            logger.error("读取config.properties异常" + printStackTrace(e));
        }
        return properties;
    }

    public static Properties getConfig(String configFileName) {
        Properties properties = new Properties();
        try {
            properties.load(ClassLoader.getSystemResourceAsStream(configFileName));
        } catch (IOException e) {
            logger.error("配置文件读取异常：" + configFileName + printStackTrace(e));
        }
        return properties;
    }

    /**
     * 发送告警邮件
     */
    public static void sendAlarmMail(String subject, String message) {
        Map<String, String> param = new HashMap<String, String>();
        Properties props = getConfigProperties();
        String serverName = props.getProperty("server_name");
        String to = props.getProperty("alarm_email");
        param.put("to", to);
        if (!StringUtil.isEmpty(serverName)) {
            param.put("subject", subject + ":" + serverName);
        } else {
            InetAddress address = null;
            try {
                address = InetAddress.getLocalHost();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
            param.put("subject", subject + ":" + address);
        }
        param.put("content", message);
        try {
            HttpUtil.post("http://alarm.dataeye.com/alarm/mail", param);
        } catch (Exception e) {
            logger.error(printStackTrace(e));
        }
    }

    public static synchronized boolean crawlerMailControl(AdAccountBase account) {
        String accountName = account.getAccount();
        Integer count = accountMailMap.get(accountName);
        if (count == null) {
            count = 1;
        } else {
            count++;
        }
        accountMailMap.put(accountName, count);
        if (count >= mailInterval) {
            accountMailMap.remove(accountName);
            return true;
        } else {
            return false;
        }
    }

    public static void crawlerMailControlClear(AdAccountBase account) {
        accountMailMap.remove(account.getAccount());
    }

    public static Logger getLogger(CrawlerPlatform platform) {
        return DELogger.getLogger("spider_" + platform.getLabel());
    }

}
