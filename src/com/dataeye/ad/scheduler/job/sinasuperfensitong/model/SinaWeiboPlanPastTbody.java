package com.dataeye.ad.scheduler.job.sinasuperfensitong.model;

public class SinaWeiboPlanPastTbody {
	
	private String date; //时间
	
	private String ad; //广告计划
	
	private Integer pv;//曝光量
	
	private Float consume;//花费
	
	private Integer trafficBhv;//导流数
	
	private Integer adId;  //计划id

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getAd() {
		return ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public Integer getPv() {
		return pv;
	}

	public void setPv(Integer pv) {
		this.pv = pv;
	}

	public Float getConsume() {
		return consume;
	}

	public void setConsume(Float consume) {
		this.consume = consume;
	}

	public Integer getTrafficBhv() {
		return trafficBhv;
	}

	public void setTrafficBhv(Integer trafficBhv) {
		this.trafficBhv = trafficBhv;
	}

	public Integer getAdId() {
		return adId;
	}

	public void setAdId(Integer adId) {
		this.adId = adId;
	}

}
