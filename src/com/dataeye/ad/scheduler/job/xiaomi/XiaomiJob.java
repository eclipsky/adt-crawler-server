package com.dataeye.ad.scheduler.job.xiaomi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;

import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.common.ServerException;
import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.constant.Constant.CrawlerPlatform;
import com.dataeye.ad.constant.Constant.PlanSwitch;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.scheduler.job.CrawlerFilter;
import com.dataeye.ad.scheduler.job.xiaomi.model.XiaomiResponse;
import com.dataeye.ad.scheduler.job.xiaomi.model.XiaomiResponse.Result.Item;
import com.dataeye.ad.util.CookieUtil;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;
import com.google.gson.JsonObject;

/**
 * 小米爬虫任务
 * 
 * @author sam.xie
 * @date Jan 24, 2018 6:58:19 PM
 * @version 1.0
 */
public class XiaomiJob extends CrawlerBaseJob {

    private final static CrawlerPlatform platform = CrawlerPlatform.XIAOMI;

    private final static Logger logger = ServerUtil.getLogger(platform);

    private final static String planUrl = "http://e.mi.com/v2/creative/list?acceptProtocolFlag=true&accountId=0&accountType=0&contractType=0&{0}&serviceToken={1}&sdate={2}&edate={3}&page={4}&pagesize={5}&orderby=id&kw=&type=&status=&sortMode=0&_=1516787205294";

    private final static String accountUrl = "http://e.mi.com/v2/account/balance?acceptProtocolFlag=true&accountId=0&accountType=0&contractType=0&{0}&serviceToken={1}&_=1516791985897";

    private final static int pageSize = 100;

    public XiaomiJob() {
        this(DateUtil.unixTimestamp000());
    }

    public XiaomiJob(long time) {
        super(time, platform);
    }

    public XiaomiJob(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }

    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        String cookie = account.getCookie();
        String accountInfo = CookieUtil.getXiaomiAccountInfo(cookie, account);
        String serviceToken = CookieUtil.getXiaomiServiceToken(cookie);
        int pageId = 1;
        // 请求
        String content = HttpUtil.getContentByHttpsGet(HttpUtil.urlParamReplace(planUrl, accountInfo, serviceToken,
                reportDate, reportDate, pageId + "", pageSize + ""), getHeaderMap(cookie));
        logger.info(content);
        PageModel pageModel = new PageModel(1, pageSize);
        // 解析
        List<AdPlanStat> planList = parse(content, account, time, pageModel);
        // 入库
        DataProcessor.processPlanDay(planList);
        // 处理分页请求
        int totalNum = pageModel.getTotalNum();
        totalNum -= pageSize;
        while (totalNum > 0) {
            pageId += 1;
            // 请求
            content = HttpUtil.getContentByHttpsGet(HttpUtil.urlParamReplace(planUrl, accountInfo, serviceToken,
                    reportDate, reportDate, pageId + "", pageSize + ""), getHeaderMap(cookie));
            logger.info(content);
            // 解析
            planList = parse(content, account, time, pageModel);
            // 入库
            DataProcessor.processPlanDay(planList);
            totalNum -= pageSize;
        }
    }

    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        String accountInfo = CookieUtil.getXiaomiAccountInfo(cookie, account);
        String serviceToken = CookieUtil.getXiaomiServiceToken(cookie);
        // 请求
        String content = HttpUtil.getContentByHttpsGet(HttpUtil.urlParamReplace(accountUrl, accountInfo, serviceToken),
                getHeaderMap(cookie));
        logger.info(content);
        // 解析
        AdAccountStat accountStat = parseAccount(content, account, time);
        DataProcessor.processAccountDay(accountStat);
    }

    /**
     * <pre>
     * 解析响应内容，封装成标准的PO对象
     * @author sam.xie
     * @date Feb 15, 2017 3:12:29 PM
     * @param content
     * @param account
     * @param time
     * @param pageModel
     * @throws ServerException
     * @throws Exception 
     */
    private List<AdPlanStat> parse(String content, AdMediumAccount account, long time, PageModel pageModel)
            throws ServerException, Exception {
        List<AdPlanStat> planList = new ArrayList<AdPlanStat>();
        XiaomiResponse model = Constant.GSON.fromJson(content, XiaomiResponse.class);
        int code = model.getCode();
        if (code != 1) {
            if (code == 4000) {
                ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, content);
            } else {
                ExceptionHandler.throwException(ExceptionEnum.SERVER_ERROR, content);
            }
        }
        pageModel.setTotalNum(model.getResult().getConf().getTotalnum());
        if (pageModel.getTotalNum() <= 0) {
            return null;
        }
        for (Item item : model.getResult().getList()) {
            AdPlanStat plan = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(), account.getId());
            planList.add(plan);
            // 媒体计划ID
            String mPlanId = item.getId();
            // 媒体计划名
            String planName = item.getName();
            // 媒体计划分组ID
            String groupId = item.getGroupId();
            // 媒体计划分组名
            String groupName = item.getGroupName();
            // 媒体计划出价
            int planBid = StringUtil.convertBigDecimal(item.getBidding().getBid(), 100, 0).intValue();
            // 媒体计划出价策略
            String planBidStrategy = item.getBidding().getBillingTypeName();
            // 审核状态
            int assetCheckStatus = item.getAssetCheckStatus();
            // 开关状态
            int status = item.getStatus();
            String planStatus = "";
            int planSwitch = PlanSwitch.OFF;
            if (status == 2) {
                planStatus = "暂停中";
            } else {
                planSwitch = PlanSwitch.ON;
                switch (assetCheckStatus) {
                case 0:
                    planStatus = "已删除";
                    break;
                case 101:
                    planStatus = "有效";
                    break;
                case 102:
                    planStatus = "审核中";
                    break;
                case 103:
                    planStatus = "部分素材有效";
                    break;
                case 104:
                    planStatus = "审核未通过";
                    break;
                default:
                    planStatus = "" + planStatus;
                }
            }
            // 媒体计划开关
            // 消耗
            int totalPay = StringUtil.convertBigDecimal(item.getCost(), 100, 0).intValue();// 转换为分
            // 曝光
            int showNum = item.getViewSum();
            // 点击
            int clickNum = item.getClickSum();
            plan.setmPlanId(mPlanId);
            plan.setPlanName(planName);
            plan.setCost(totalPay);
            plan.setShowNum(showNum);
            plan.setClickNum(clickNum);
            plan.setBid(planBid);
            plan.setPlanBidStrategy(planBidStrategy);
            plan.setGroupId(groupId);
            plan.setGroupName(groupName);
            plan.setPlanStatus(planStatus);
            plan.setPlanSwitch(planSwitch);
        }
        return planList;
    }

    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time) throws Exception {
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        int code = resultJson.get("code").getAsInt();
        if (code != 1) {
            if (code == 4000) {
                ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, content);
            } else {
                ExceptionHandler.throwException(ExceptionEnum.SERVER_ERROR, content);
            }
        }
        String balanceStr = resultJson.get("result").getAsJsonObject().get("cash").getAsJsonObject().get("balance")
                .getAsString();
        int balance = StringUtil.convertBigDecimal(balanceStr, 100, 0).intValue();
        AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                account.getId());
        accountStat.setBalance(balance);
        return accountStat;
    }

    private Map<String, String> getHeaderMap(String cookie) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap
                .put("User-Agent",
                        "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
        headerMap.put("Host", "e.mi.com");
        headerMap.put("Referer", "http://e.mi.com/v2/dist/plan.html");
        headerMap.put("Connection", "keep-alive");
        headerMap.put("Upgrade-Insecure-Requests", "1");
        headerMap.put("Accept", "application/json, text/javascript, */*; q=0.01");
        headerMap.put("x-requested-with", "XMLHttpRequest");
        headerMap.put("Cookie", cookie);
        return headerMap;
    }

}
