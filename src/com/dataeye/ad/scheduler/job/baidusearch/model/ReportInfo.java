package com.dataeye.ad.scheduler.job.baidusearch.model;

public class ReportInfo {

	private int reportid;

	private String starttime;

	private String endtime;

	private int isrelativetime;

	private int relativetime;

	private int mtldim;

	private int idset;

	private int mtllevel;

	private int reporttype;

	private int platform;

	private String splitDim;

	private String dataitem;

	private int reporttag;

	private int reportcycle;

	private int timedim;

	private int firstday;

	private int ismail;

	private String mailaddr;

	private String sortlist;

	private String reportname;

	private int userid;

	private int reportlevel;

	private String rgtag;

	private String mtag;

	private String targetingType;

	private int totalCount;

	public int getReportid() {
		return reportid;
	}

	public void setReportid(int reportid) {
		this.reportid = reportid;
	}

	public String getStarttime() {
		return starttime;
	}

	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}

	public String getEndtime() {
		return endtime;
	}

	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}

	public int getIsrelativetime() {
		return isrelativetime;
	}

	public void setIsrelativetime(int isrelativetime) {
		this.isrelativetime = isrelativetime;
	}

	public int getRelativetime() {
		return relativetime;
	}

	public void setRelativetime(int relativetime) {
		this.relativetime = relativetime;
	}

	public int getMtldim() {
		return mtldim;
	}

	public void setMtldim(int mtldim) {
		this.mtldim = mtldim;
	}

	public int getIdset() {
		return idset;
	}

	public void setIdset(int idset) {
		this.idset = idset;
	}

	public int getMtllevel() {
		return mtllevel;
	}

	public void setMtllevel(int mtllevel) {
		this.mtllevel = mtllevel;
	}

	public int getReporttype() {
		return reporttype;
	}

	public void setReporttype(int reporttype) {
		this.reporttype = reporttype;
	}

	public int getPlatform() {
		return platform;
	}

	public void setPlatform(int platform) {
		this.platform = platform;
	}

	public String getSplitDim() {
		return splitDim;
	}

	public void setSplitDim(String splitDim) {
		this.splitDim = splitDim;
	}

	public String getDataitem() {
		return dataitem;
	}

	public void setDataitem(String dataitem) {
		this.dataitem = dataitem;
	}

	public int getReporttag() {
		return reporttag;
	}

	public void setReporttag(int reporttag) {
		this.reporttag = reporttag;
	}

	public int getReportcycle() {
		return reportcycle;
	}

	public void setReportcycle(int reportcycle) {
		this.reportcycle = reportcycle;
	}

	public int getTimedim() {
		return timedim;
	}

	public void setTimedim(int timedim) {
		this.timedim = timedim;
	}

	public int getFirstday() {
		return firstday;
	}

	public void setFirstday(int firstday) {
		this.firstday = firstday;
	}

	public int getIsmail() {
		return ismail;
	}

	public void setIsmail(int ismail) {
		this.ismail = ismail;
	}

	public String getMailaddr() {
		return mailaddr;
	}

	public void setMailaddr(String mailaddr) {
		this.mailaddr = mailaddr;
	}

	public String getSortlist() {
		return sortlist;
	}

	public void setSortlist(String sortlist) {
		this.sortlist = sortlist;
	}

	public String getReportname() {
		return reportname;
	}

	public void setReportname(String reportname) {
		this.reportname = reportname;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public int getReportlevel() {
		return reportlevel;
	}

	public void setReportlevel(int reportlevel) {
		this.reportlevel = reportlevel;
	}

	public String getRgtag() {
		return rgtag;
	}

	public void setRgtag(String rgtag) {
		this.rgtag = rgtag;
	}

	public String getMtag() {
		return mtag;
	}

	public void setMtag(String mtag) {
		this.mtag = mtag;
	}

	public String getTargetingType() {
		return targetingType;
	}

	public void setTargetingType(String targetingType) {
		this.targetingType = targetingType;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

}
