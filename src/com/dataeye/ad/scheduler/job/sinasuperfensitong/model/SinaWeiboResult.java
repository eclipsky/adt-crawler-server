package com.dataeye.ad.scheduler.job.sinasuperfensitong.model;

import java.util.List;

public class SinaWeiboResult {
	private int page;

	private int pageSize;

	private int pageTotal;

	private int total;

	private List<SinaWeiboPlan> list;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageTotal() {
		return pageTotal;
	}

	public void setPageTotal(int pageTotal) {
		this.pageTotal = pageTotal;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<SinaWeiboPlan> getList() {
		return list;
	}

	public void setList(List<SinaWeiboPlan> list) {
		this.list = list;
	}

}
