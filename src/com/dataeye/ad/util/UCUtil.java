package com.dataeye.ad.util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.slf4j.Logger;

import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.util.log.DELogger;
import com.google.gson.JsonObject;

public class UCUtil {

	private final static Logger logger = DELogger.getLogger("uc_util");

	public static String getUserId(AdMediumAccount account) throws Exception {
		String userId = account.getSkey();
		if (StringUtil.isEmpty(userId)) {
			// 调用接口，获取登录信息
			String url = "https://e.uc.cn/ad/web/init/operator?userId=0&_t=1489663953694&token=";
			String content = requestUserId(url, account.getCookie());
			JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
			int ret = resultJson.get("status").getAsInt();
			if (ret == 0) {
				userId = String.valueOf(resultJson.get("data").getAsJsonObject().get("id").getAsInt());
			}
			// 保存skey
			if (!StringUtil.isEmpty(userId)) {
				CookieUtil.updateSkey(userId, account);
			}
		}
		return userId;
	}

	private static String requestUserId(String url, String cookie) throws Exception {
		HttpClient httpClient = HttpUtil.getHttpClient();
		// 1. 获取计划数量
		HttpGet httpGet = new HttpGet(url);
		// 设置头字段
		setHeader(httpGet, cookie);
		// 发送请求
		HttpResponse response = httpClient.execute(httpGet);
		// 检查状态
		int statusCode = response.getStatusLine().getStatusCode();
		response.setEntity(response.getEntity());
		HttpEntity entity = response.getEntity();
		String content = HttpUtil.getContent(entity);
		logger.info(content);
		if (statusCode != 200) {
			ExceptionHandler.throwException(ExceptionEnum.COOKIE_EXPIRE);
		}
		return content;
	}

	private static void setHeader(HttpRequestBase method, String cookie) {
		method.setHeader("User-Agent",
				"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
		method.setHeader("Host", "e.uc.cn");
		method.setHeader("Referer", "https://e.uc.cn/ad/static/index.html");
		method.setHeader("Connection", "keep-alive");
		method.setHeader("Upgrade-Insecure-Requests", "1");
		method.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
		method.setHeader("x-requested-with", "XMLHttpRequest");
		method.setHeader("Cookie", cookie);
	}

	public static void main(String[] args) {
		String content = "document.domain='qq.com';jQuery112202943331780298203_1489483860205({\"ret\":0,\"msg\":\"\",\"data\":{\"uid\":162952,\"notice\":\"\",\"userlist\":[162952],\"loginlist\":[{\"uid\":162952,\"name\":\"深圳市佳游科技有限公司\",\"role\":\"广告主\",\"url\":\"http://e.qq.com/atlas/162952/\",\"isagency\":0}],\"isagency\":0,\"loguid\":2055460477,\"uname\":\"深圳市佳游科技有限公司\",\"isgdtuser\":1,\"status\":1,\"targettype\":25,\"homepage\":\"http://e.qq.com/atlas/\",\"isdev\":0,\"aduid\":162952}});";
		System.out.println(content.substring(content.indexOf("({") + 1, content.indexOf("})")));
	}
}
