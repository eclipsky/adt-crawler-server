package com.dataeye.ad.kafka;

import java.util.Random;

import kafka.producer.Partitioner;
import kafka.utils.VerifiableProperties;

/**
 * @see http://www.cnblogs.com/devos/p/3632115.html
 * @see kafka.producer.DefaultPartitioner
 */
public class EventPartitioner implements Partitioner {

    private Random random = new Random();

    public EventPartitioner(VerifiableProperties props) {

    }

    public int partition(Object key, int a_numPartitions) {
        // int partition = 0;
        // partition = Integer.parseInt((String)key) % a_numPartitions;
        // return partition;
        // [0, a_numPartitions)
        return random.nextInt(a_numPartitions);
    }

}
