package com.dataeye.ad.scheduler.job.kuaishou;

import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.constant.Constant.*;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.scheduler.job.CrawlerFilter;
import com.dataeye.ad.util.*;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lingliqi
 * @date 2017-11-30 11:48
 */
public class KuaishouJob extends CrawlerBaseJob {

    private final static CrawlerPlatform platform = CrawlerPlatform.KUAISHOU;

    private final static Logger logger = ServerUtil.getLogger(platform);

    private final static String planUrl = "https://ad.e.kuaishou.com/rest/dsp/control-panel/units?kuaishou.ad.dsp_ph={0}";

    private final static String accountUrl = "https://ad.e.kuaishou.com/rest/dsp/owner/info?kuaishou.ad.dsp_ph={0}";

    public KuaishouJob() {
        this(DateUtil.unixTimestamp000());
    }

    public KuaishouJob(long time) {
        super(time, platform);
    }

    public KuaishouJob(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }


    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        String dspPh = CookieUtil.getCookieValue(cookie, "kuaishou.ad.dsp_ph");
        if (StringUtil.isEmpty(dspPh)) {
            ExceptionHandler.throwCookieException(ExceptionEnum.SKEY_NOT_EXISTS);
        }
        String url = HttpUtil.urlParamReplace(accountUrl, dspPh);
        String content = HttpUtil.getContentByHttpsPost(url, getHeaderMap(cookie), "");
        logger.info(content);
        AdAccountStat adAccountStat = parseAccount(content, account, time);
        DataProcessor.processAccountDay(adAccountStat);
    }

    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        int page = 1;
        int pageSize = 50;
        String cookie = account.getCookie();
        String statDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        PageModel pageModel = new PageModel(page, pageSize);
        String dspPh = CookieUtil.getCookieValue(cookie, "kuaishou.ad.dsp_ph");
        if (StringUtil.isEmpty(dspPh)) {
            ExceptionHandler.throwCookieException(ExceptionEnum.SKEY_NOT_EXISTS);
        }
        String url = HttpUtil.urlParamReplace(planUrl, dspPh);
        String content = HttpUtil.getContentByHttpsPost(url, getHeaderMap(cookie), getPlanPostPair(page, pageSize, statDate));
        logger.info(content);
        List<AdPlanStat> poList = new ArrayList<>();
        List<AdPlanStat> partList = parsePlan(content, account, time, pageModel);
        poList.addAll(partList);
        int totalNum = pageModel.getTotalNum();
        totalNum -= pageSize;
        while (totalNum > 0) {
            page++;
            content = HttpUtil.getContentByHttpsPost(url, getHeaderMap(cookie), getPlanPostPair(page, pageSize, statDate));
            logger.info(content);
            partList = parsePlan(content, account, time, pageModel);
            poList.addAll(partList);
            totalNum -= pageSize;
        }
        DataProcessor.processPlanDay(poList);
    }


    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time) throws Exception {
        AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                account.getId());
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        int result = resultJson.get("result").getAsInt();
        if (result != 1) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE);
        }
        JsonObject user = resultJson.get("userDspAccount").getAsJsonObject();
        // 51,719.34元 - 51719339
        int balance = StringUtil.convertBigDecimalWithDivide(user.get("balance").getAsString(), 10, 0).intValue();
        accountStat.setBalance(balance);
        return accountStat;
    }

    private List<AdPlanStat> parsePlan(String content, AdMediumAccount account, long time, PageModel pageModel)
            throws Exception {
        List<AdPlanStat> poList = new ArrayList<>();
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        int result = resultJson.get("result").getAsInt();
        if (result != 1) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE);
        }
        int totalNum = resultJson.get("pageInfo").getAsJsonObject().get("totalCount").getAsInt();
        pageModel.setTotalNum(totalNum);
        JsonArray unitJson = resultJson.get("units").getAsJsonArray();
        for (JsonElement element : unitJson) {
            JsonObject order = element.getAsJsonObject();
            String mplanId = order.get("id").getAsString();
            String planName = order.get("name").getAsString();
            String groupId = order.get("campaignId").getAsString();
            int clickNum = StringUtil.getJsonIntValue(order, "click");
            int showNum = StringUtil.getJsonIntValue(order, "impression");
            int cost = StringUtil.convertBigDecimalWithDivide(order.get("charge").getAsString(), 10, 0).intValue();
            int bid = StringUtil.convertBigDecimalWithDivide(order.get("bid").getAsString(), 10, 0).intValue();
            // 2-CPC
            String planBidStrategy = order.get("bidType").getAsString();
            String planStatus = order.get("viewStatusReason").getAsString();
            AdPlanStat reportPO = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(),
                    account.getId());
            poList.add(reportPO);
            reportPO.setmPlanId(mplanId);
            reportPO.setPlanName(planName);
            reportPO.setGroupId(groupId);
            reportPO.setCost(cost);
            reportPO.setClickNum(clickNum);
            reportPO.setShowNum(showNum);
            reportPO.setBid(bid);
            reportPO.setPlanStatus(planStatus);
            reportPO.setPlanBidStrategy(planBidStrategy);
        }
        return poList;
    }

    private Map<String, String> getHeaderMap(String cookie) {
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Accept", "application/json");
        headerMap.put("Accept-Encoding", "gzip, deflate, br");
        headerMap.put("Accept-Language", "zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7");
        headerMap.put("Connection", "keep-alive");
        headerMap.put("content-type", "application/json");
        headerMap.put("Cookie", cookie);
        headerMap.put("Host", "ad.e.kuaishou.com");
        headerMap.put("Cache-Control", "no-cache");
        headerMap.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0");
        return headerMap;
    }

    private String getPlanPostPair(int page, int pageSize, String statDate) {
        Map<String, String> postMap = new HashMap<>();
        postMap.put("pageNum", page + "");
        postMap.put("pageSize", pageSize + "");
        postMap.put("reportStartDay", statDate);
        postMap.put("reportEndDay", statDate);
        return StringUtil.gson.toJson(postMap);
    }

}
