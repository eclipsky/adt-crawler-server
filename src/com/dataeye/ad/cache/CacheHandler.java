package com.dataeye.ad.cache;

import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;

import com.dataeye.ad.pipeline.po.AdPlanRelation;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.util.log.DELogger;

/**
 * 清理过期缓存
 * 
 * @author sam.xie
 * @date Mar 6, 2017 11:57:22 AM
 * @version 1.0
 */
public class CacheHandler {

	private final static Logger logger = DELogger.getLogger("cache_log");

	private static final int LIMIT_SECONDS = 86400; // 秒，默认24小时
	private static final int LIMIT_TIMES = 10000; // 默认10次
	private static final int CACHE_SECONDS = 300; // 秒

	/** 缓存关联规则 */
	private static final ConcurrentHashMap<String, CacheObject<AdPlanRelation>> RELATION_CHACHE = new ConcurrentHashMap<String, CacheObject<AdPlanRelation>>();

	/** 缓存媒体折扣率 */
	private static final ConcurrentHashMap<String, CacheObject<Float>> DISCOUNT_CACHE = new ConcurrentHashMap<String, CacheObject<Float>>();

	/** 访问对象池 */
	private static final ConcurrentHashMap<String, AccessObject> ACCESS_POOL = new ConcurrentHashMap<String, AccessObject>();

	/** 用于清理过期缓存 */
	private static ScheduledThreadPoolExecutor CACHE_SCHEDULER = new ScheduledThreadPoolExecutor(1);

	/**
	 * 开启线程清理过期缓存
	 * 
	 * @author sam.xie
	 * @date 2014年11月24日 下午3:56:44
	 */
	public static void cleanCacheJob() {
		CACHE_SCHEDULER.scheduleWithFixedDelay(new Runnable() {
			@Override
			public void run() {
				logger.info(">>>>>>>>>>start clean cache, count: " + RELATION_CHACHE.size());
				if (RELATION_CHACHE.size() == 0) {
					try { // 没有要清理的缓存，睡眠5秒
						Thread.sleep(5 * 1000);
					} catch (InterruptedException e) {

					}
				}
				for (Entry<String, CacheObject<AdPlanRelation>> entry : RELATION_CHACHE.entrySet()) {
					String key = entry.getKey();
					CacheObject<AdPlanRelation> value = entry.getValue();
					if (value.expire()) {
						RELATION_CHACHE.remove(key);
						logger.info(">>>>>>>>>>clean cache: " + key);
					}
				}
			}

		}, CACHE_SECONDS, CACHE_SECONDS, TimeUnit.SECONDS); // 5秒钟清理一次
	}

	public static CacheObject get(String key) {
		// 先查询缓存
		CacheObject cacheObject = RELATION_CHACHE.get(key);
		return cacheObject;
		// int now = DateUtil.unixTimestamp();
		// // 有缓存且没过期
		// if (null != cacheObject && cacheObject.getExpireTime() > now) {
		// logger.info(">>>>>>>>>>hit cache: " + key);
		// return cacheObject.getCacheValue();
		// } else {
		// // 缓存不存在或者已过期，就从数据库取对象，重新设置缓存
		// OpenAuthDao openAuthDao = BeanUtil.getTypedBean(SpringBootstrap.getContext(), OpenAuthDao.class);
		// OpenAuthVO vo;
		// try {
		// vo = openAuthDao.getAuth(key);
		// } catch (Exception e) {
		// return null;
		// }
		//
		// // 数据库不存在直接返回null
		// if (null == vo) {
		// return null;
		// } else {
		// // 数据库存在就写缓存，并返回
		// if (null == cacheObject) {
		// cacheObject = new CacheObject();
		// }
		// cacheObject.setCacheKey(key);
		// cacheObject.setCacheValue(vo);
		// cacheObject.setCreateTime(now);
		// cacheObject.setExpireTime(now + CACHE_SECONDS);
		// RELATION_CHACHE.put(key, cacheObject);
		// logger.info(">>>>>>>>>>add cache：" + key);
		// return vo;
		// }
		// }
	}

	public static void remove(String key) {
		RELATION_CHACHE.remove(key);
	}

	public static boolean isAccessble(String key) {
		// 先查询访问对象
		AccessObject accessObject = ACCESS_POOL.get(key);

		if (null == accessObject) {
			accessObject = new AccessObject();
			accessObject.setAccessKey(key);
			accessObject.setCreateTime(DateUtil.unixTimestamp());
			accessObject.incrementAccessTimes();// 自增一次
			ACCESS_POOL.put(key, accessObject);
			return true;
		} else {
			int now = DateUtil.unixTimestamp();
			int createTime = accessObject.getCreateTime();
			int accessTimes = accessObject.getAccessTimes();
			if (now - createTime <= LIMIT_SECONDS && accessTimes >= LIMIT_TIMES) {
				// 如果与首次访问时间差不大于LIMIT_SECONDS，且已经访问次数不少于LIMIT_TIMES，就是请求太频繁
				logger.info(">>>>>>>>>>request too often ! openID:" + key);
				return false;
			} else if (now - createTime > LIMIT_SECONDS) {
				// 当前请求时间已经在首次访问1秒钟之后，请求正常，同时需要重置访问计时
				accessObject.setCreateTime(now);// 重置时间
				accessObject.setAccessTimes(1);// 重置次数
				return true;
			} else {
				accessObject.incrementAccessTimes(); // 自增一次
				return true;
			}
		}

	}

	public static void shudown() {
		if (CACHE_SCHEDULER != null) {
			CACHE_SCHEDULER.shutdown();
		}
	}
}
