package com.dataeye.ad.pipeline.po;

import java.util.Date;

/**
 * <pre/>
 * 基础类
 * 
 * @author sam.xie
 * @date Mar 6, 2017 4:36:15 PM
 * @version 1.0
 */
public abstract class AdBase {

	private int id;

	private int companyId;

	private Date createTime;

	private Date updateTime;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

}
