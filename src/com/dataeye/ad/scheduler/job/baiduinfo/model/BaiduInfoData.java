package com.dataeye.ad.scheduler.job.baiduinfo.model;

import java.util.List;
@Deprecated
public class BaiduInfoData {

	private int totalCount;

	private List<BaiduInfoListData> listData;

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public List<BaiduInfoListData> getListData() {
		return listData;
	}

	public void setListData(List<BaiduInfoListData> listData) {
		this.listData = listData;
	}

}
