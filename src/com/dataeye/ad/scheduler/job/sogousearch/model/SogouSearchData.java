package com.dataeye.ad.scheduler.job.sogousearch.model;

public class SogouSearchData {
	private SogouSearchCpcReport[] cpcReportList;

	public SogouSearchCpcReport[] getCpcReportList() {
		return cpcReportList;
	}

	public void setCpcReportList(SogouSearchCpcReport[] cpcReportList) {
		this.cpcReportList = cpcReportList;
	}

}
