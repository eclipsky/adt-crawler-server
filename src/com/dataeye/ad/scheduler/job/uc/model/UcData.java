package com.dataeye.ad.scheduler.job.uc.model;

import java.util.List;

public class UcData {

	private int pageNo;
	
	private int pageSize;
	
	private int totalCount;
	
	private int totalPageCount;
	
	private List<UcDataItem> list;

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getTotalPageCount() {
		return totalPageCount;
	}

	public void setTotalPageCount(int totalPageCount) {
		this.totalPageCount = totalPageCount;
	}

	public List<UcDataItem> getList() {
		return list;
	}

	public void setList(List<UcDataItem> list) {
		this.list = list;
	}

}
