package com.dataeye.ad.tmp.qq.pc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import org.slf4j.Logger;

import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.StringUtil;
import com.dataeye.util.log.DELogger;

public class QunCrawler {
    public static String[] keywords = {"梦塔防", "D10", "众神争霸", "霸三国", "无尽战区", "战意", "暗黑血统2",
            "跑跑卡丁车", "极品飞车OL", "欢乐斗地主，腾讯", "炉石传说", "攒局麻将", "超神战记", "乖离性百万亚瑟王", "三国杀", "天天德州", "秘境对决", "巫师之昆特牌",
            "球球大作战", "劲舞团3", "初音速", "我的世界", "泡泡堂", "恋与制作人" };
//    public static String[] keywords = { "荒野行动", "穿越火线", "枪战王者", "终结者2", "审判日", "光荣使命", "绝地求生", "小米枪战", "使命召唤OL",
//        "穿越火线", "逆战", "全民突击", "全民枪战", "战舰世界", "机动战士敢达OL", "逆战", "枪神纪", "生化战场", "战争前线", "战地之王", "突击英雄", "虚幻争霸",
//        "炫斗之王", "火影忍者", "fifa online3", "最强NBA", "NBALIVE", "NBA2K OL", "自由足球", "青春篮球", "英雄联盟", "全民超神", "弹弹堂",
//        "泡泡堂", "守望先锋", "梦三国2", "坦克世界", "风暴英雄", "枪火游侠", "王者荣耀", "CS:GO", "英魂之刃", "300英雄", "神之浩劫", "DOTA2", "星际争霸2",
//        "DNF", "虚荣", "乱斗西游2", "自由之战", "混沌与秩序之英雄战歌", "指尖刀塔", "梦塔防", "D10", "众神争霸", "霸三国", "无尽战区", "战意", "暗黑血统2",
//        "跑跑卡丁车", "极品飞车OL", "欢乐斗地主，腾讯", "炉石传说", "攒局麻将", "超神战记", "乖离性百万亚瑟王", "三国杀", "天天德州", "秘境对决", "巫师之昆特牌",
//        "球球大作战", "劲舞团3", "初音速", "我的世界", "泡泡堂", "恋与制作人" };
    public static final String URL = "http://qun.qq.com/cgi-bin/group_search/pc_group_search";
    public static final String COOKIE = "RK=kPS4UlS0Xs; pac_uid=1_65307160; pgv_pvi=5853125632; pt2gguin=o0065307160; ptisp=ctc; ptcz=ed04dcf7e84ddd31ece09fc241d24657ff29f859592114f132d53ed1ab365eda; pgv_info=ssid=s8585272389; pgv_pvid=1800670600; uin=o65307160; skey=ZuUWtl7HUh; p_skey=O4BzwZ4jahk4WbCZ3*zlDOPa42xtxfFUQ37Y5pTraZY_; p_uin=o65307160";
//    public static final Logger logger_r = DELogger.getLogger("qun_r");
//    public static final Logger logger_c = DELogger.getLogger("qun_c");

    public static void main(String[] args) throws Exception {
        QunCrawler qun = new QunCrawler();
        qun.spider();
    }

    public void spider() throws Exception {
        for (int i = 0; i < keywords.length; i++) {
            String keyword = keywords[i];
            Logger logger_r = DELogger.getLogger("qun_r_" + keyword);
            Logger logger_c = DELogger.getLogger("qun_c_" + keyword);
            System.out.println(">>>>> " + keyword);
            logger_r.info("开始:" + keyword + ".....");
            int page = 0;
            int endflag = 0;
            int num = 0;
            try {
                while (endflag == 0) {
                    String content = HttpUtil.getContentByPost(URL, getHeaderMap(COOKIE), getFormData(keyword, page));
                    logger_c.info(content);
                    Response qun = StringUtil.gson.fromJson(content, Response.class);
                    endflag = qun.getEndflag();
                    int errorcode = qun.getErrorcode();
                    if(errorcode > 0){
                        break;
                    }
                    if (null != qun.getGroup_list()) {
                        for (Group group : qun.getGroup_list()) {
                            logger_r.info(group.getCode() + "");
                            num++;
                        }
                    }
                    Thread.sleep(5000 + (System.currentTimeMillis() % 1000) * 5);
                    page++;

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            logger_r.info("结束:" + keyword + ", num=" + num);
            Thread.sleep(20000);
        }
    }

    public void parser() {

    }

    private List<NameValuePair> getFormData(String keyword, int page) {
        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("k", "交友"));
        pairs.add(new BasicNameValuePair("n", "8"));
        pairs.add(new BasicNameValuePair("st", "1"));
        pairs.add(new BasicNameValuePair("iso", "0"));
        pairs.add(new BasicNameValuePair("src", "1"));
        pairs.add(new BasicNameValuePair("v", "5545"));
        pairs.add(new BasicNameValuePair("bkn", "260244604"));
        pairs.add(new BasicNameValuePair("isRecommend", "false"));
        pairs.add(new BasicNameValuePair("city_id", "0"));
        pairs.add(new BasicNameValuePair("from", "1"));
        pairs.add(new BasicNameValuePair("keyword", keyword));
        pairs.add(new BasicNameValuePair("sort", "0"));
        pairs.add(new BasicNameValuePair("wantnum", "24"));
        pairs.add(new BasicNameValuePair("page", page + ""));
        pairs.add(new BasicNameValuePair("ldw", "260244604"));
        return pairs;
    }

    private Map<String, String> getHeaderMap(String cookie) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Host", "qun.qq.com");
        headerMap.put("Connection", "keep-alive");
        headerMap.put("Accept", "application/json, text/javascript, */*; q=0.01");
        headerMap.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        headerMap.put("Origin", "http://find.qq.com");
        headerMap
                .put("User-Agent",
                        "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) QQ/8.9.5.22062 Chrome/43.0.2357.134 Safari/537.36 QBCore/3.43.716.400 QQBrowser/9.0.2524.400");
        headerMap.put("Referer",
                "http://find.qq.com/index.html?version=1&im_version=5545&width=910&height=610&search_target=0");
        headerMap.put("Accept-Encoding", "gzip, deflate");
        headerMap.put("Accept-Language", "en-US,en;q=0.8");
        headerMap.put("Cookie", COOKIE);
        return headerMap;
    }

}

class Response {
    private int ec;
    private int endflag;
    private int exact;
    private int errorcode;
    private int gTotal;
    private List<Group> group_list;

    public int getEc() {
        return ec;
    }

    public void setEc(int ec) {
        this.ec = ec;
    }

    public int getEndflag() {
        return endflag;
    }

    public void setEndflag(int endflag) {
        this.endflag = endflag;
    }

    public int getExact() {
        return exact;
    }

    public void setExact(int exact) {
        this.exact = exact;
    }

    public int getErrorcode() {
        return errorcode;
    }

    public void setErrorcode(int errorcode) {
        this.errorcode = errorcode;
    }

    public int getgTotal() {
        return gTotal;
    }

    public void setgTotal(int gTotal) {
        this.gTotal = gTotal;
    }

    public List<Group> getGroup_list() {
        return group_list;
    }

    public void setGroup_list(List<Group> group_list) {
        this.group_list = group_list;
    }

}

class Group {
    private int code;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

}
