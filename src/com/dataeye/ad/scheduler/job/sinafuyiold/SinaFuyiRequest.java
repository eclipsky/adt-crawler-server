package com.dataeye.ad.scheduler.job.sinafuyiold;

/**
 * Created by huangzehai on 2017/3/29.
 */
public class SinaFuyiRequest {
    private String startDate;
    private String endDate;
    private String byDay = "1";// 0 - 合计 1 - 按天
    private String pageSize;
    private String pageNo;
    private String bidtype = "-1";
    private String webtype = "-1";
    private String postype = "-1";
    private String period = "-1";

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getByDay() {
        return byDay;
    }

    public void setByDay(String byDay) {
        this.byDay = byDay;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getPageNo() {
        return pageNo;
    }

    public void setPageNo(String pageNo) {
        this.pageNo = pageNo;
    }

    public String getBidtype() {
        return bidtype;
    }

    public void setBidtype(String bidtype) {
        this.bidtype = bidtype;
    }

    public String getWebtype() {
        return webtype;
    }

    public void setWebtype(String webtype) {
        this.webtype = webtype;
    }

    public String getPostype() {
        return postype;
    }

    public void setPostype(String postype) {
        this.postype = postype;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    @Override
    public String toString() {
        return "SinaRequest{" +
                "startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", byDay='" + byDay + '\'' +
                ", pageSize='" + pageSize + '\'' +
                ", pageNo='" + pageNo + '\'' +
                ", bidtype='" + bidtype + '\'' +
                ", webtype='" + webtype + '\'' +
                ", postype='" + postype + '\'' +
                ", period='" + period + '\'' +
                '}';
    }
}

