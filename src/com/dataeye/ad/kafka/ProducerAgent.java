package com.dataeye.ad.kafka;

import java.util.List;
import java.util.Properties;

import com.dataeye.ad.util.ServerUtil;

import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;

public class ProducerAgent {

	private static Producer<String, String> producer = null;

	private static final String KAFKA_CONFIG = "kafka.properties";

	public static String topic;

	static {
		init();
	}

	private ProducerAgent() {
	}

	private static void init() {
		Properties properties = ServerUtil.getConfig(KAFKA_CONFIG);
		ProducerConfig config = new ProducerConfig(properties);
		producer = new Producer<String, String>(config);
		topic = properties.getProperty("kafka.topic");
	}

	public static void send(List<KeyedMessage<String, String>> list) {
		producer.send(list);
	}

	public static void send(KeyedMessage<String, String> msg) {
		producer.send(msg);
	}

	public static void send(String topic, String key, String msg) {
		producer.send(new KeyedMessage<String, String>(topic, key, msg));
	}

	public static void send(String key, String msg) {
		producer.send(new KeyedMessage<String, String>(topic, key, msg));
	}
}
