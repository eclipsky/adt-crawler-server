package com.dataeye.ad.scheduler.job.sinasuperfensitong.model;

public class SinaWeiboReport {
	private String pv; // 曝光

	private String bhv; // 互动

	private int trafficBhv; // 导流数（点击）

	private int consume; // 花费

	public String getPv() {
		return pv;
	}

	public void setPv(String pv) {
		this.pv = pv;
	}

	public String getBhv() {
		return bhv;
	}

	public void setBhv(String bhv) {
		this.bhv = bhv;
	}

	public int getTrafficBhv() {
		return trafficBhv;
	}

	public void setTrafficBhv(int trafficBhv) {
		this.trafficBhv = trafficBhv;
	}

	public int getConsume() {
		return consume;
	}

	public void setConsume(int consume) {
		this.consume = consume;
	}

}
