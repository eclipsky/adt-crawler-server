package com.dataeye.ad.scheduler.job.autohome;

import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.constant.Constant.CrawlerPlatform;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.scheduler.job.CrawlerFilter;
import com.dataeye.ad.scheduler.job.autohome.model.AutoHomeModel;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.slf4j.Logger;

import java.util.*;


/**
 * Created by VitAs on 2017/6/15.
 */
public class AutoHomeJob extends CrawlerBaseJob {

    private final static CrawlerPlatform platform = Constant.CrawlerPlatform.AUTOHOME;

    private final static Logger logger = ServerUtil.getLogger(platform);

    private final static String planUrl =
            "https://dsp.autohome.com.cn/autodx-advert/adunits/list?" +
                    "customerId={0}&accountId={1}&pageNum={2}&pageSize={3}&startTime={4}&endTime={5}&productType=1";

    private final static String listAccountUrl =
            "https://dsp.autohome.com.cn/autodx-admin/user/overview?" +
                    "startTime={0}&endTime={1}&pageNum={2}&pageSize={3}";

    //爬取余额时的客户代理公司集合
    private List<AutoHomeModel> autoHomeModelList;

    public AutoHomeJob() {
        this(DateUtil.unixTimestamp000());
    }

    public AutoHomeJob(long time) {
        super(time, platform);
    }

    public AutoHomeJob(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }

    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        String dateTime = DateUtil.getTimeString(time, "yyyy/MM/dd");
        String dateTimeURLEncode = HttpUtil.urlEncode(dateTime);
        //分页参数，pageSize可选值为20，50，100，
        //默认pageSize为50
        int pageNum = 1;
        int pageSize = 50;
        PageModel pageModel = new PageModel(pageNum, pageSize);
        //  需要根据爬取余额时的list<AutoHomeModel>遍历，爬取所有代理公司的投放计划
        if (null == autoHomeModelList) {
            autoHomeModelList = getListByUrl(time, account);
        }
        for (AutoHomeModel model : autoHomeModelList) {
            //爬取投放计划的参数
            int customerId = model.getCustomerId();
            int accountId = model.getAccountId();
            int status = model.getStatus();
            //代理公司状态生效中
            if (status == 1) {
                String content = HttpUtil.getContentByHttpsGet(
                        HttpUtil.urlParamReplace(planUrl, Integer.toString(customerId), Integer.toString(accountId),
                                Integer.toString(pageNum), Integer.toString(pageSize), dateTimeURLEncode, dateTimeURLEncode),
                        getHeaderMap(cookie));
                logger.info(content);
                //解析
                List<AdPlanStat> poList = parsePlan(content, account, time, pageModel);
                int totalNum = pageModel.getTotalNum();
                //入库
                DataProcessor.processPlanDay(poList);
                totalNum -= pageSize;
                while (totalNum > 0) {
                    pageNum++;
                    // 请求
                    content = HttpUtil.getContentByHttpsGet(
                            HttpUtil.urlParamReplace(planUrl, Integer.toString(customerId), Integer.toString(accountId),
                                    Integer.toString(pageNum), Integer.toString(pageSize), dateTimeURLEncode, dateTimeURLEncode),
                            getHeaderMap(cookie));
                    logger.info(content);
                    // 解析
                    poList = parsePlan(content, account, time, pageModel);
                    // 入库
                    DataProcessor.processPlanDay(poList);
                    totalNum -= pageSize;
                }

            }

        }
    }


    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        //解析
        autoHomeModelList = getListByUrl(time, account);
        AdAccountStat accountStat = parseAccount(autoHomeModelList, account, time);
        //入库
        DataProcessor.processAccountDay(accountStat);

    }


    /**
     * 通过请求URL获取List<AutoHomeModel>集合
     *
     * @param time
     * @param account
     * @return
     * @throws Exception
     */
    private List<AutoHomeModel> getListByUrl(long time, AdMediumAccount account) throws Exception {
        List<AutoHomeModel> poList = new ArrayList<AutoHomeModel>();
        String cookie = account.getCookie();
        String dateTime = DateUtil.getTimeString(time, "yyyy-MM-dd");
        String dateTimeURLEncode = HttpUtil.urlEncode(dateTime);
        int pageNum = 1;
        int pageSize = 50;
        PageModel pageModel = new PageModel(pageNum, pageSize);
        //请求
        String content = HttpUtil.getContentByHttpsGet(
                HttpUtil.urlParamReplace(listAccountUrl, dateTimeURLEncode, dateTimeURLEncode,
                        Integer.toString(pageNum), Integer.toString(pageSize)),
                getHeaderMap(cookie));
        logger.info(content);
        poList = getListByJson(content);
        return poList;
    }

    /**
     * 解析账户余额请求响应内容，json -->> List<AutoHomeModel>
     *
     * @param content
     * @return
     * @throws Exception
     */
    private List<AutoHomeModel> getListByJson(String content) throws Exception {
        List<AutoHomeModel> poList = new ArrayList<AutoHomeModel>();
        //第一步，将json整体进行解析
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        //第二步，检查状态码 0-成功 127-未登录系统
        int status = resultJson.get("status").getAsInt();
        statusCheck(status, content);
        //第三步,获取resultJson对象中的list集合
        JsonArray list = resultJson.get("data").getAsJsonObject().get("list").getAsJsonArray();
        //第四步，获取list集合的总数
        int totalNum = resultJson.get("data").getAsJsonObject().get("total").getAsInt();
        //第五步，生成对应的实力类映射集合
        for (JsonElement item : list) {
            AutoHomeModel autoHomeModel = new AutoHomeModel();
            poList.add(autoHomeModel);
            JsonObject order = item.getAsJsonObject();
            //客户id
            int customerId = order.get("customerId").getAsInt();
            //代理公司id
            int accountId = order.get("accountId").getAsInt();
            //公司状态 1-正在生效中
            int ItemStatus = order.get("status").getAsInt();
            //余额
            int balance = order.get("balance").getAsInt();
            autoHomeModel.setCustomerId(customerId);
            autoHomeModel.setAccountId(accountId);
            autoHomeModel.setStatus(ItemStatus);
            autoHomeModel.setBalance(balance);
        }
        return poList;
    }


    /**
     * 解析投放计划的响应内容
     *
     * @param content
     * @param account
     * @param time
     * @param pageModel
     * @return
     * @throws Exception
     */
    private List<AdPlanStat> parsePlan(String content, AdMediumAccount account, long time, PageModel pageModel)
            throws Exception {
        List<AdPlanStat> poList = new ArrayList<>();
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        int status = resultJson.get("status").getAsInt();
        //检查状态码
        statusCheck(status, content);
        JsonArray list = resultJson.get("data").getAsJsonObject().get("list").getAsJsonArray();
        int totalNum = resultJson.get("data").getAsJsonObject().get("total").getAsInt();
        pageModel.setTotalNum(totalNum);
        for (JsonElement item : list) {
            AdPlanStat adPlanStat = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(), account.getId());
            poList.add(adPlanStat);
            JsonObject order = item.getAsJsonObject();
            //  解析投放计划响应内容
            int clickNum = StringUtil.getJsonIntValue(order, "clickNum");
            int showNum = StringUtil.getJsonIntValue(order, "exposeNum");
            int cost = StringUtil.convertInt(order.get("consumed").getAsString(),0);
            // 媒体计划ID
            String mPlanId = order.get("id").getAsInt() + "";
            // 媒体计划名
            String planName = order.get("name").getAsString();
            // 媒体计划分组ID
            String groupId = order.get("campaignId").getAsInt() + "";
            // 媒体计划分组名
            String groupName = order.get("campaignName").getAsString();
            // 媒体计划出价
            int planBid = StringUtil.getJsonIntValue(order, "bidPrice");// 已经是分为单位
            // 媒体计划状态 1：有效 2：已下线 3:已暂停 104：有效_客户余额不足 999：已删除
            String planStatus = order.get("status").getAsString();
            // 媒体计划开关
            Integer planSwitch = null;
            if (null != planStatus && !planStatus.equals("")) {
                char[] charArray = planStatus.toCharArray();
                if (charArray[0] == '1') {
                    planSwitch = Constant.PlanSwitch.ON;
                } else if (charArray[0] == '3') {
                    planSwitch = Constant.PlanSwitch.OFF;
                }
            }
            adPlanStat.setmPlanId(mPlanId);
            adPlanStat.setPlanName(planName);
            adPlanStat.setGroupId(groupId);
            adPlanStat.setGroupName(groupName);
            adPlanStat.setBid(planBid);
            adPlanStat.setClickNum(clickNum);
            adPlanStat.setShowNum(showNum);
            adPlanStat.setCost(cost);
            adPlanStat.setPlanStatus(planStatus);
            adPlanStat.setPlanSwitch(planSwitch);
        }
        return poList;

    }

    /**
     * 根据初步解析JSON得到的对象，计算余额之和，返回入库的媒体帐号对象
     *
     * @param modelList
     * @param account
     * @param time
     * @return
     * @throws Exception
     */
    private AdAccountStat parseAccount(List<AutoHomeModel> modelList, AdMediumAccount account, long time)
            throws Exception {
        int sum = 0;
        for (AutoHomeModel item : modelList) {
            int status = item.getStatus();
            //当status=1时，代理公司为生效状态
            // 计算余额之和
            if (status == 1) {
                int balance = item.getBalance();
                sum += balance;
                AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                        account.getId());
                accountStat.setBalance(sum);
                return accountStat;
            }
        }
        return null;
    }


    /***
     * 请求登录状态检查
     * @param status
     * @param content
     * @throws Exception
     */
    private void statusCheck(int status, String content) throws Exception {
        content = StringUtil.convertUnicode2String(content);
        if (status != 0) {
            // {"status":127,"msg":"请先登录"}
            if (status == 127) {
                ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, content);
            } else {
                ExceptionHandler.throwException(ExceptionEnum.REQUEST_ERROR, content);
            }
        }
    }

    /***
     * https请求头信息
     * @param cookie
     * @return
     */
    private Map<String, String> getHeaderMap(String cookie) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Accept", "application/json");
        headerMap.put("Accept-Encoding", "gzip, deflate, br");
        headerMap.put("Accept-Language", "en,zh-CN;q=0.8,zh;q=0.6");
        headerMap.put("Connection", "keep-alive");
        headerMap.put("Content-Type", "application/x-www-form-urlencoded");
        headerMap.put("Cookie", cookie);
        headerMap.put("Host", "dsp.autohome.com.cn");
        headerMap.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0");
        headerMap.put("x-requested-with", "fetch");
        return headerMap;
    }

}
