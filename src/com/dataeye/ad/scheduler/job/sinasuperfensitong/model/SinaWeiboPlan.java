package com.dataeye.ad.scheduler.job.sinasuperfensitong.model;

public class SinaWeiboPlan {

	private int id; // 计划ID

	private String name; // 计划名

	private int effectiveStatus; // 状态

	private String effectiveStatusStr; // 状态

	private int configuredStatus; // 开关

	private String bidAmount; // 报价

	private int billingType; // 报价类型？不确定

	private SinaWeiboCampaign campaign; // 广告系列

	private SinaWeiboReport report;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getEffectiveStatus() {
		return effectiveStatus;
	}

	public void setEffectiveStatus(int effectiveStatus) {
		this.effectiveStatus = effectiveStatus;
	}

	public String getEffectiveStatusStr() {
		return effectiveStatusStr;
	}

	public void setEffectiveStatusStr(String effectiveStatusStr) {
		this.effectiveStatusStr = effectiveStatusStr;
	}

	public String getBidAmount() {
		return bidAmount;
	}

	public void setBidAmount(String bidAmount) {
		this.bidAmount = bidAmount;
	}

	public int getBillingType() {
		return billingType;
	}

	public void setBillingType(int billingType) {
		this.billingType = billingType;
	}

	public SinaWeiboCampaign getCampaign() {
		return campaign;
	}

	public void setCampaign(SinaWeiboCampaign campaign) {
		this.campaign = campaign;
	}

	public SinaWeiboReport getReport() {
		return report;
	}

	public void setReport(SinaWeiboReport report) {
		this.report = report;
	}

	public int getConfiguredStatus() {
		return configuredStatus;
	}

	public void setConfiguredStatus(int configuredStatus) {
		this.configuredStatus = configuredStatus;
	}

}
