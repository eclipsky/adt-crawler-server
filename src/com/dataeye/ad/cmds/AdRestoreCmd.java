package com.dataeye.ad.cmds;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.springframework.stereotype.Service;

import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.constant.Constant.CrawlerPlatform;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.scheduler.job.CrawlerFilter;
import com.dataeye.ad.scheduler.job.autohome.AutoHomeJob;
import com.dataeye.ad.scheduler.job.baidubaiyi.BaiduBaiyiJob;
import com.dataeye.ad.scheduler.job.baiduinfo.BaiduInfoJob;
import com.dataeye.ad.scheduler.job.baidusearch.BaiduSearchJob;
import com.dataeye.ad.scheduler.job.bihe.BiheJob;
import com.dataeye.ad.scheduler.job.dongqiudi.DongqiudiJob;
import com.dataeye.ad.scheduler.job.e360.E360Job;
import com.dataeye.ad.scheduler.job.fenghuang.FengHuangJob;
import com.dataeye.ad.scheduler.job.gdt.GDTJob;
import com.dataeye.ad.scheduler.job.iqiyi.IQiYiJob;
import com.dataeye.ad.scheduler.job.kuaishou.KuaishouJob;
import com.dataeye.ad.scheduler.job.mta.MtaJob;
import com.dataeye.ad.scheduler.job.netease.NeteaseJob;
import com.dataeye.ad.scheduler.job.neteaseold.NeteaseOldJob;
import com.dataeye.ad.scheduler.job.pinyou.PinyouJob;
import com.dataeye.ad.scheduler.job.sinafensitong.SinaFensitongJob;
import com.dataeye.ad.scheduler.job.sinafuyi.SinaFuyiJob;
import com.dataeye.ad.scheduler.job.sinafuyiold.SinaFuyiOldJob;
import com.dataeye.ad.scheduler.job.sinasuperfensitong.SinaSuperFensitongJob;
import com.dataeye.ad.scheduler.job.sm.SmJob;
import com.dataeye.ad.scheduler.job.sogousearch.SogouSearchJob;
import com.dataeye.ad.scheduler.job.sohuhuisuan.SohuHuisuanJob;
import com.dataeye.ad.scheduler.job.sohuxinpinsuan.SohuXinpinsuanJob;
import com.dataeye.ad.scheduler.job.toutiao.ToutiaoJob;
import com.dataeye.ad.scheduler.job.tui.TuiQQJob;
import com.dataeye.ad.scheduler.job.uc.UCJob;
import com.dataeye.ad.scheduler.job.weixin.WeixinJob;
import com.dataeye.ad.scheduler.job.xuli.XuliJob;
import com.dataeye.ad.scheduler.job.xunlei.XunleiJob;
import com.dataeye.ad.scheduler.job.yidianzixun.YidianzixunJob;
import com.dataeye.ad.scheduler.job.youku.YoukuJob;
import com.dataeye.ad.scheduler.job.zaker.ZakerJob;
import com.dataeye.ad.scheduler.job.zhihu.ZhiHuJob;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.util.log.DELogger;
import com.qq.jutil.string.StringUtil;
import com.xunlei.netty.httpserver.cmd.BaseCmd;
import com.xunlei.netty.httpserver.cmd.CmdMapper;
import com.xunlei.netty.httpserver.component.XLHttpRequest;
import com.xunlei.netty.httpserver.component.XLHttpResponse;
import com.xunlei.netty.httpserver.spring.SpringBootstrap;

/**
 * 用于数据临时恢复的接口
 */
@Service
public class AdRestoreCmd extends BaseCmd {

    private final static Logger logger = DELogger.getLogger("ad_restore");
    private static ExecutorService executor = Executors.newFixedThreadPool(5);

    /**
     * pinyou数据恢复
     *
     * @param request
     * @param response
     */
    @CmdMapper("/ad/restore/pinyou")
    public Object restorePinyou(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new PinyouJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    @CmdMapper("/ad/restore/toutiao")
    public Object restoreToutiao(XLHttpRequest request, XLHttpResponse response) {
        try {

            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new ToutiaoJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    @CmdMapper("/ad/restore/tuiqq")
    public Object restoreTuiQQ(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new TuiQQJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    @CmdMapper("/ad/restore/baidusearch")
    public Object restoreBaiduSearch(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new BaiduSearchJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    @CmdMapper("/ad/restore/baiduinfo")
    public Object restoreBaiduInfo(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new BaiduInfoJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    @CmdMapper("/ad/restore/mta")
    public Object restoreMta(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new MtaJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    @CmdMapper("/ad/restore/bihe")
    public Object restoreBihe(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new BiheJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    @CmdMapper("/ad/restore/iqiyi")
    public Object restoreAiqiyi(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new IQiYiJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    @CmdMapper("/ad/restore/uc")
    public Object restoreUc(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new UCJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    @CmdMapper("/ad/restore/fenghuang")
    public Object restoreFenghuang(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new FengHuangJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    @CmdMapper("/ad/restore/sohu")
    public Object restoreSohu(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new SohuHuisuanJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    @CmdMapper("/ad/restore/netease")
    public Object restoreNetease(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new NeteaseJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    @CmdMapper("/ad/restore/neteaseold")
    public Object restoreNeteaseOld(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new NeteaseOldJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    @CmdMapper("/ad/restore/gdt")
    public Object restoreGDT(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new GDTJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    /**
     * 新浪微博广告联盟
     *
     * @param request
     * @param response
     * @return
     */
    @CmdMapper("/ad/restore/sinaweiboadalliance")
    public Object restoreSinaWeiboAdAlliance(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new SinaFensitongJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    /**
     * 新浪微博超级粉丝通
     *
     * @param request
     * @param response
     * @return
     */
    @CmdMapper("/ad/restore/sinaweibosuperfensitong")
    public Object restoreSinaWeiboSuperFensitong(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new SinaSuperFensitongJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    /**
     * 新浪扶翼旧版
     *
     * @param request
     * @param response
     * @return
     */
    @CmdMapper("/ad/restore/sinafuyiold")
    public Object restoreSinaFuyiOld(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new SinaFuyiOldJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    /**
     * 神马搜索
     *
     * @param request
     * @param response
     * @return
     */
    @CmdMapper("/ad/restore/sm")
    public Object restoreSm(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new SmJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    @CmdMapper("/ad/restore/sogousearch")
    public Object restoreSogousearch(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new SogouSearchJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    @CmdMapper("/ad/restore/youku")
    public Object restoreYouku(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new YoukuJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    /**
     * 汽车之家
     *
     * @param request
     * @param response
     * @return
     */
    @CmdMapper("/ad/restore/autohome")
    public Object restoreAutoHome(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new AutoHomeJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    /**
     * 一点资讯
     *
     * @param request
     * @param response
     * @return
     */
    @CmdMapper("/ad/restore/yidianzixun")
    public Object restoreYidianzixun(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new YidianzixunJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    /**
     * 360点睛
     *
     * @param request
     * @param response
     * @return
     */
    @CmdMapper("/ad/restore/e360")
    public Object restoreE360(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new E360Job(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    /**
     * 百度百意移动DSP
     *
     * @param request
     * @param response
     * @return
     */
    @CmdMapper("/ad/restore/baidubaiyi")
    public Object restoreBaiduBaiyi(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new BaiduBaiyiJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    /**
     * zaker
     *
     * @param request
     * @param response
     * @return
     */
    @CmdMapper("/ad/restore/zaker")
    public Object restoreZaker(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new ZakerJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    /**
     * 懂球帝
     *
     * @param request
     * @param response
     * @return
     */
    @CmdMapper("/ad/restore/dongqiudi")
    public Object restoreDongqiudi(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new DongqiudiJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    /**
     * 搜狐新品算
     *
     * @param request
     * @param response
     * @return
     */
    @CmdMapper("/ad/restore/sohuads")
    public Object restoreSohuAds(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new SohuXinpinsuanJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    /**
     * 新浪扶翼
     *
     * @param request
     * @param response
     * @return
     */
    @CmdMapper("/ad/restore/sinafuyi")
    public Object restoreSinaFuyi(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new SinaFuyiJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    /**
     * 知乎
     *
     * @param request
     * @param response
     * @return
     */
    @CmdMapper("/ad/restore/zhihu")
    public Object restoreZhiHu(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new ZhiHuJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    /**
     * 快手
     *
     * @param request
     * @param response
     * @return
     */
    @CmdMapper("/ad/restore/kuaishou")
    public Object restoreKuaishou(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new KuaishouJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    /**
     * 微信
     *
     * @param request
     * @param response
     * @return
     */
    @CmdMapper("/ad/restore/weixin")
    public Object restoreWeixin(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new WeixinJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    /**
     * 旭力网盟
     *
     * @param request
     * @param response
     * @return
     */
    @CmdMapper("/ad/restore/xuli")
    public Object restoreXuli(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new XuliJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    /**
     * 迅雷
     *
     * @param request
     * @param response
     * @return
     */
    @CmdMapper("/ad/restore/xunlei")
    public Object restoreXunlei(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                executor.submit(new XunleiJob(i));
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail";
        }
        return "success";
    }

    @CmdMapper("/ad/platform/get")
    public String getMediumAll(XLHttpRequest request, XLHttpResponse response) {
        StringBuilder builder = new StringBuilder();
        try {
            builder.append("----------媒体平台列表----------\n");
            for (Constant.CrawlerPlatform platform : Constant.CrawlerPlatform.values()) {
                builder.append("ID: ");
                builder.append(platform.getId());
                builder.append(" NAME: ");
                builder.append(platform.getName());
                builder.append(" LABEL: ");
                builder.append(platform.getLabel());
                builder.append("\n");
            }
            builder.append("----------END----------");
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail, " + ServerUtil.printStackTrace(e);
        }
        return builder.toString();

    }

    @CmdMapper("/ad/restore")
    public Object restore(XLHttpRequest request, XLHttpResponse response) {
        try {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            Integer mediumId = StringUtil.convertInt(request.getParameter("mediumId"), 0);
            Integer companyId = StringUtil.convertInt(request.getParameter("companyId"), 0);
            Integer accountId = StringUtil.convertInt(request.getParameter("accountId"), 0);
            companyId = companyId == 0 ? null : companyId;
            accountId = accountId == 0 ? null : accountId;
            CrawlerFilter filter = new CrawlerFilter(accountId, companyId);
            long startTime = DateUtil.getUnixTimestampFromyyyyMMdd(startDate);
            long endTime = DateUtil.getUnixTimestampFromyyyyMMdd(endDate);
            Class<? extends CrawlerBaseJob> jobClass = CrawlerPlatform.getPlatformMap().get(mediumId).getJobClass();
            if (jobClass == null) {
                return "fail, jobClass is undefined";
            }
            for (long i = startTime; i <= endTime; i += Constant.ONEDAY) {
                CrawlerBaseJob job = jobClass.getConstructor(long.class, CrawlerFilter.class).newInstance(i, filter);
                executor.submit(job);
            }
        } catch (Exception e) {
            logger.error(ServerUtil.printStackTrace(e));
            return "fail, " + ServerUtil.printStackTrace(e);
        }
        return "success";
    }
}
