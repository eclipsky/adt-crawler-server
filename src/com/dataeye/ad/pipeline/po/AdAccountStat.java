package com.dataeye.ad.pipeline.po;

import com.dataeye.ad.util.DateUtil;

/**
 * 账户统计表（余额等）
 * 
 * @author sam.xie
 * @date Apr 11, 2017 4:47:10 PM
 * @version 1.0
 */
public class AdAccountStat extends AdStatBase {
	private int mediumId;
	private int mediumAccountId;
	private Integer balance; // 转换为分
	private Integer costToday; // 转换为分

	public AdAccountStat(long timesamp, int companyId, int mediumId, int mediumAccountId) {
		this();
		this.setStatDate(DateUtil.fromUnixtimestamp(timesamp));
		this.setCompanyId(companyId);
		this.setMediumId(mediumId);
		this.setMediumAccountId(mediumAccountId);
	}

	public AdAccountStat(long timesamp, AdMediumAccount account) {
		this();
		this.setStatDate(DateUtil.fromUnixtimestamp(timesamp));
		this.setCompanyId(account.getCompanyId());
		this.setMediumId(account.getPlatformId());
		this.setMediumAccountId(account.getId());
	}

	private AdAccountStat() {
		this.balance = null;
		this.costToday = null;
	}

	public int getMediumId() {
		return mediumId;
	}

	public void setMediumId(int mediumId) {
		this.mediumId = mediumId;
	}

	public int getMediumAccountId() {
		return mediumAccountId;
	}

	public void setMediumAccountId(int mediumAccountId) {
		this.mediumAccountId = mediumAccountId;
	}

	public Integer getBalance() {
		return balance;
	}

	public void setBalance(Integer balance) {
		this.balance = balance;
	}

	public Integer getCostToday() {
		return costToday;
	}

	public void setCostToday(Integer costToday) {
		this.costToday = costToday;
	}

}
