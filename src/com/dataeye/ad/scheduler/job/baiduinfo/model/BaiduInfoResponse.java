package com.dataeye.ad.scheduler.job.baiduinfo.model;

@Deprecated
public class BaiduInfoResponse {

	private int status;

	private BaiduInfoData data;

	private String error;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public BaiduInfoData getData() {
		return data;
	}

	public void setData(BaiduInfoData data) {
		this.data = data;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
