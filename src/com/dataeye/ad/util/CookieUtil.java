package com.dataeye.ad.util;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.dataeye.ad.common.CookieException;
import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.constant.Constant.PlatformType;
import com.dataeye.ad.constant.Constant.Table;
import com.dataeye.ad.pipeline.po.AdAccountBase;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.xunlei.jdbc.JdbcTemplate;

public class CookieUtil {

    private static JdbcTemplate jdbcTemplate = DBClient.getJdbcTemplateDeAdt();

    /**
     * <pre>
     * 通过帐号信息获取最新cookie
     * @author sam.xie
     * @date Mar 7, 2017 5:11:16 PM
     * @param account
     * @return
     */
    public static String getCookieByAccount(AdAccountBase account) {
        if (StringUtil.isEmpty(account.getCookie())) {
            try {
                // 获取最新Cookie
                String sql = "select cookie from " + Table.AD_CRAWLER_COOKIE
                        + " where type = ? and platform_id = ? and account = ? order by create_time desc limit 1";
                String cookie = jdbcTemplate.queryForString(sql, account.getPlatformType(), account.getPlatformId(),
                        account.getAccount());
                if (!StringUtil.isEmpty(cookie)) {
                    if (account.getPlatformType() == PlatformType.H5) {
                        updateH5Cookie(cookie, account);
                    }
                    updateMediumCookie(cookie, account);
                }
                return cookie;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        } else {
            return account.getCookie();
        }
    }

    public static void isCookieValid(String cookie) throws CookieException {
        if (StringUtil.isEmpty(cookie)) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_NOT_EXISTS);
        }
    }

    /**
     * <pre>
     * 切换媒体帐号登录态
     * @author sam.xie
     * @date Mar 14, 2017 1:57:03 PM
     * @param status
     * @param account
     */
    public static void changeLoginStatus(int status, AdAccountBase account) {
        String sql = "update " + Table.AC_MEDIUM_ACCOUNT
                + " set login_status = ?,  update_time = ? where medium_account_id = ?";
        if (account.getPlatformType() == PlatformType.H5) {
            sql = "update " + Table.AC_H5_ACCOUNT + " set login_status = ?, update_time = ? where h5_account_id = ?";
        }
        jdbcTemplate.execute(sql, status, DateUtil.nowDate(), account.getId());
    }

    /**
     * <pre>
     * 更新skey
     * @author sam.xie
     * @date Mar 14, 2017 6:54:06 PM
     * @param skey
     * @param account
     */
    public static void updateSkey(String skey, AdAccountBase account) {
        String sql = "update " + Table.AC_MEDIUM_ACCOUNT + " set skey = ? where medium_account_id = ?";
        jdbcTemplate.execute(sql, skey, account.getId());
    }

    /**
     * <pre>
     * 更新cookie
     * @author sam.xie
     * @date Mar 14, 2017 6:54:19 PM
     * @param cookie
     * @param account
     */
    public static void updateMediumCookie(String cookie, AdAccountBase account) {
        String sql = "update " + Table.AC_MEDIUM_ACCOUNT + " set cookie = ? where medium_account_id = ?";
        jdbcTemplate.execute(sql, cookie, account.getId());
    }

    /**
     * <pre>
     * 更新H5平台Cookie
     * @author sam.xie
     * @date Mar 15, 2017 3:49:09 PM
     * @param cookie
     * @param account
     */
    public static void updateH5Cookie(String cookie, AdAccountBase account) {
        String sql = "update " + Table.AC_H5_ACCOUNT + " set cookie = ? where h5_account_id = ?";
        jdbcTemplate.execute(sql, cookie, account.getId());
    }

    public static String getBaiduToken(String cookie) {
        return getCookieValue(cookie, "__cas__st__3");
    }

    public static String getBaiduUserId(String cookie) {
        String userId = getCookieValue(cookie, "SAMPLING_USER_ID");
        if (StringUtil.isEmpty(userId)) {
            userId = getCookieValue(cookie, "__cas__id__3");
        }
        return userId;
    }

    public static String getXiaomiServiceToken(String cookie) {
        return getCookieValue(cookie, "serviceToken");
    }

    public static String getXiaomiAccountInfo(String cookie, AdMediumAccount account) {
        if (account != null && StringUtils.isNotBlank(account.getSkey())) {
            return account.getSkey();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("customerId=" + getCookieValue(cookie, "customerId"));
        sb.append("&subAccountId=" + getCookieValue(cookie, "subAccountId"));
        sb.append("&subAccountType=" + getCookieValue(cookie, "subAccountType"));
        sb.append("&xiaomiId=" + getCookieValue(cookie, "xiaomiId"));
        CookieUtil.updateSkey(sb.toString(), account);
        return sb.toString();
    }
    
    public static final String getSmUid(String cookie, AdMediumAccount account) {
        if (account != null && StringUtils.isNotBlank(account.getSkey())) {
            return account.getSkey();
        }
        String uid = getCookieValue(cookie, "uid");
        if (!StringUtil.isEmpty(uid)) {
            CookieUtil.updateSkey(uid, account);
            return uid;
        } else {
            return null;
        }
    }

    public static final String getSinaWeiboAdCustomerId(String cookie, AdMediumAccount account) {
        if (account != null && StringUtils.isNotBlank(account.getSkey())) {
            return account.getSkey();
        }
        String uid = getCookieValue(cookie, "$$userId");
        if (!StringUtil.isEmpty(uid)) {
            CookieUtil.updateSkey(uid, account);
            return uid;
        } else {
            return null;
        }
    }

    public static final String getYoukuCustomerId(String cookie, AdMediumAccount account) {
        if (account != null && StringUtils.isNotBlank(account.getSkey())) {
            return account.getSkey();
        }
        // Skey可能存在与userId或者dspCustomerId中
        // 先取dspCustomerId
        String uid = getCookieValue(cookie, "dspCustomerUserId");
        // 如果不为空，更新Skey到account中
        if (!StringUtil.isEmpty(uid)) {
            CookieUtil.updateSkey(uid, account);
            return uid;
        } else {
            // 如果为空，取userId
            uid = getCookieValue(cookie, "userId");
        }
        if (!StringUtil.isEmpty(uid)) {
            CookieUtil.updateSkey(uid, account);
            return uid;
        } else {
            return null;
        }
    }

    /**
     * 从cookie中获取知乎账号id
     * @param str
     * @return
     */
    public static String getZhiHuUserId(String str) {
        String regex = "advertiser/(.*?)/";
        Pattern pattern = Pattern.compile(regex);// 匹配的模式
        Matcher m = pattern.matcher(str);
        while (m.find()) {
            return m.group(1);
        }
        return null;
    }

    public static String cleanToutiaoCookie(String rawCookie) {
        /**
         * openedMenu=3%2C11; _ga=GA1.2.1462946268.1486433004; login_flag=163fa6a979f8715d5ee40fb4eb838a07;
         * sessionid=104c5bcbf30e683db3247bf0bc6e713e; sid_tt=104c5bcbf30e683db3247bf0bc6e713e;
         * sid_guard="104c5bcbf30e683db3247bf0bc6e713e|1488694311|2592000|Tue\054 04-Apr-2017 06:11:51 GMT";
         * csrftoken=iCzUyDaDaToR485IqCwNIF7DlGm8J9Hu; part=stable; Hm_lvt_85c76e21c62dcb584da211941caf4e0a=1488694315;
         * Hm_lpvt_85c76e21c62dcb584da211941caf4e0a=1488694315
         */
        String _ga = getCookieValue(rawCookie, "_ga");
        String sessionid = getCookieValue(rawCookie, "sessionid");
        return "_ga=" + _ga + "; sessionid=" + sessionid;
    }

    private static Map<String, String> getCookieMap(String cookies) {
        Map<String, String> cookieMap = new HashMap<>();
        String[] cookieArr = cookies.split(";");
        for (String cookie : cookieArr) {
            String[] kv = cookie.split("=");
            String key = kv[0].trim();
            String value = "";
            if (kv.length == 2) {
                value = kv[1].trim();
            }
            cookieMap.put(key, value);
        }
        return cookieMap;
    }

    public static String getCookieValue(String cookies, String field) {
        return getCookieMap(cookies).get(field);
    }

    public static void main(String[] args) throws Exception {

    }

}
