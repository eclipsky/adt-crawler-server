package com.dataeye.ad.scheduler.job.sohuxinpinsuan;

import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.constant.Constant.*;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DBPipeline;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.scheduler.job.CrawlerFilter;
import com.dataeye.ad.util.*;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by VitAs on 2017/8/14.
 */
public class SohuXinpinsuanJob extends CrawlerBaseJob {

    private final static CrawlerPlatform platform = CrawlerPlatform.SOHU_XINPINSUAN;

    private final static Logger logger = ServerUtil.getLogger(platform);

    private final static String accountUrl = "http://ads.sohu.com/home/statisticsAndOthers";

    private final static String planUrl = "http://ads.sohu.com/manage/groupList";

    private final static String authUrl = "http://ads.sohu.com/user/auth";

    //签名失效
    private final static int SIGN_INVALID = 9004;

    //令牌过期
    private final static int TOKEN_EXPIRE = 9002;

    //登陆过期
    private final static int COOKIE_EXPIRE = 9005;

    //刷新token的失败次数，
    //最多允许失败3次，当刷新成功后，重置为0
    private static int refreshCount = 0;

    private final static int MAX_REFRESH_NUM = 3;


    public SohuXinpinsuanJob() {
        this(DateUtil.unixTimestamp000());
    }

    public SohuXinpinsuanJob(long time) {
        super(time, platform);
    }

    public SohuXinpinsuanJob(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }

    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        // 从cookie中获取用于请求验证的token
        String cookie = account.getCookie();
        String webToken = CookieUtil.getCookieValue(cookie, "hui-token");
        String uip = CookieUtil.getCookieValue(cookie, "uip");
        String tokenDecode = HttpUtil.urlDecode(webToken);
        //请求
        String content = HttpUtil.getContentByPost(accountUrl, getHeadMap(tokenDecode, uip), new Gson().toJson(new HashMap<String, String>()));
        logger.info(content);
        AdAccountStat accountStat = parseAccount(content, account, time);
        //入库
        DataProcessor.processAccountDay(accountStat);
    }

    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        // 从cookie中获取用于请求验证的token
        String cookie = account.getCookie();
        String webToken = CookieUtil.getCookieValue(cookie, "hui-token");
        String uip = CookieUtil.getCookieValue(cookie, "uip");
        String tokenDecode = HttpUtil.urlDecode(webToken);
        String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        int page = 1;
        int pageSize = 200;
        PageModel pageModel = new PageModel(page, pageSize);
        String content = HttpUtil.getContentByPost(planUrl, getHeadMap(tokenDecode, uip), getPostParams(reportDate, page, pageSize));
        logger.info(content);
        List<AdPlanStat> poList = parsePlan(content, account, time, pageModel);
        // 入库
        DataProcessor.processPlanDay(poList);
        //处理分页
        int totalNum = pageModel.getTotalNum();
        totalNum -= pageSize;
        while (totalNum > 0) {
            page++;
            content = HttpUtil.getContentByPost(planUrl, getHeadMap(tokenDecode, uip), getPostParams(reportDate, page, pageSize));
            logger.info(content);
            poList = parsePlan(content, account, time, pageModel);
            // 入库
            DataProcessor.processPlanDay(poList);
            totalNum -= pageSize;
        }
    }

    /**
     * 当前Token失效时，刷新Token接口
     *
     * @param account
     * @param seed
     * @return
     * @throws Exception
     */
    private boolean refreshToken(AdMediumAccount account, String seed) throws Exception {
        //将token和seed进行MD5加密
        //组成seed参数，进行auth接口请求
        //获取新的token值
        //从cookie中获取用于请求验证的token
        //存入ad_medium_account表中
        String cookie = account.getCookie();
        String webToken = CookieUtil.getCookieValue(cookie, "hui-token");
        String uip = CookieUtil.getCookieValue(cookie, "uip");
        String tokenDecode = HttpUtil.urlDecode(webToken);
        //请求
        String content = HttpUtil.getContentByPost(authUrl, getHeadMap(tokenDecode, uip), getAuthParams(tokenDecode, seed));
        logger.info(content);
        String token = parseAuth(content, account);
        if (null != token) {
            //解析
            account.setCookie(token);
            //入库更新Ad_medium_account表
            DBPipeline.updateAccountCookie(account);
            return true;
        }
        return false;


    }

    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time)
            throws Exception {
        JsonObject jsonResult = StringUtil.jsonParser.parse(content).getAsJsonObject();
        //响应状态判断
        int code = jsonResult.get("code").getAsInt();
        //提示令牌过期，则刷新token
        if (code == TOKEN_EXPIRE) {
            JsonObject jsonObject = jsonResult.get("data").getAsJsonObject();
            String seed = jsonObject.get("seed").getAsString();
            //调用auth接口刷新token
            //重新调用spideraccount爬取余额
            boolean isRefresh = refreshToken(account, seed);
            if (isRefresh) {
                logger.info(" ................ 『成功刷新Token』  ................ ");
                accountSpider(time, account);
                return null;
            }
        } else if (code == COOKIE_EXPIRE) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, jsonResult.get("msg").getAsString());
        } else if (code != 0) {
            ExceptionHandler.throwException(jsonResult.get("msg").getAsString());
        }
        JsonObject jsonObject = jsonResult.get("data").getAsJsonObject();
        AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                account.getId());
        if (null != jsonObject) {
            String balanceStr = StringUtil.getJsonStrValue(jsonObject, "balance");
            int balance = StringUtil.convertBigDecimalWithDivide(balanceStr, 1000, 0).intValue();
            String costDayStr = jsonObject.get("cost").getAsString();
            int costDay = StringUtil.convertBigDecimalWithDivide(costDayStr, 1000, 0).intValue();
            accountStat.setBalance(balance);
            accountStat.setCostToday(costDay);
        }
        return accountStat;

    }

    private List<AdPlanStat> parsePlan(String content, AdMediumAccount account, long time, PageModel pageModel)
            throws Exception {
        List<AdPlanStat> poList = new ArrayList<>();
        JsonObject jsonResult = StringUtil.jsonParser.parse(content).getAsJsonObject();
        //响应状态判断
        int code = jsonResult.get("code").getAsInt();
        //提示服务过期，则刷新token
        if (code == TOKEN_EXPIRE) {
            JsonObject jsonObject = jsonResult.get("data").getAsJsonObject();
            String seed = jsonObject.get("seed").getAsString();
            //调用auth接口刷新token
            //重新调用spiderplan爬取计划
            boolean isRefresh = refreshToken(account, seed);
            if (isRefresh) {
                logger.info(" ................ 『成功刷新Token』  ................ ");
                planSpider(time, account);
                return null;
            }
        } else if (code == COOKIE_EXPIRE) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, jsonResult.get("msg").getAsString());
        } else if (code != 0) {
            ExceptionHandler.throwException(jsonResult.get("msg").getAsString());
        }
        JsonObject jsonObject = jsonResult.get("data").getAsJsonObject();
        if (jsonObject != null) {
            int totalNum = StringUtil.getJsonIntValue(jsonObject, "total");
            pageModel.setTotalNum(totalNum);
            JsonElement list = jsonObject.get("list");
            if (null == list || list.isJsonNull()) {
                return null;
            }
            JsonArray dataList = list.getAsJsonArray();
            if (dataList.size() > 0) {
                for (JsonElement element : dataList) {
                    AdPlanStat reportPO = new AdPlanStat(time, account);
                    poList.add(reportPO);
                    JsonObject order = element.getAsJsonObject();
                    //点击
                    int clickNum = StringUtil.getJsonIntValue(order, "hits");
                    //曝光
                    int showNum = StringUtil.getJsonIntValue(order, "showNum");
                    //消耗
                    String costStr = StringUtil.getJsonStrValue(order, "consume");
                    int cost = StringUtil.convertBigDecimalWithDivide(costStr, 1000, 0).intValue();
                    //计划ID
                    String mplanId = order.get("adgroupId").getAsString();
                    //计划名称
                    String planName = order.get("adgroupName").getAsString();
                    //分组ID
                    String groupId = order.get("campaignId").getAsString();
                    //分组名称
                    String groupNmae = order.get("campaignName").getAsString();
                    //计划出价
                    String bidStr = StringUtil.getJsonStrValue(order, "bid");
                    int bid = StringUtil.convertBigDecimalWithDivide(bidStr, 1000, 0).intValue();
                    //出价策略
                    int planBidStrategy = StringUtil.getJsonIntValue(order, "biddingType");
                    if (planBidStrategy == 3)
                        reportPO.setPlanBidStrategy("CPC");
                    else if (planBidStrategy == 4)
                        reportPO.setPlanBidStrategy("dCPM");
                    reportPO.setCost(cost);
                    reportPO.setClickNum(clickNum);
                    reportPO.setShowNum(showNum);
                    reportPO.setmPlanId(mplanId);
                    reportPO.setPlanName(planName);
                    reportPO.setGroupId(groupId);
                    reportPO.setGroupName(groupNmae);
                    reportPO.setBid(bid);
                }
            }
        }
        return poList;
    }


    private String parseAuth(String content, AdMediumAccount account)
            throws Exception {
        JsonObject jsonResult = StringUtil.jsonParser.parse(content).getAsJsonObject();
        //响应状态判断
        int code = jsonResult.get("code").getAsInt();
        //如果状态码为无效签名;且刷新次数小于三次;再次执行刷新token
        while (code == SIGN_INVALID && refreshCount <= MAX_REFRESH_NUM) {
            //计数自增
            refreshCount++;
            //获取seed
            String seed = jsonResult.get("data").getAsJsonObject().get("seed").getAsString();
            //从原有cookie中获取到token字段
            String cookie = account.getCookie();
            String webToken = CookieUtil.getCookieValue(cookie, "hui-token");
            String uip = CookieUtil.getCookieValue(cookie, "uip");
            String tokenDecode = HttpUtil.urlDecode(webToken);
            //再次发送请求
            content = HttpUtil.getContentByPost(authUrl, getHeadMap(tokenDecode, uip), getAuthParams(tokenDecode, seed));
            logger.info(content);
            jsonResult = StringUtil.jsonParser.parse(content).getAsJsonObject();
            //获取返回码
            code = jsonResult.get("code").getAsInt();
            if (code == 0) {
                refreshCount = 0;
                break;
            }
        }
        //超过三次刷新,也会抛出异常信息
        if (code != 0) {
            ExceptionHandler.throwException(jsonResult.get("msg").getAsString());
        }
        StringBuilder sb = new StringBuilder("hui-token=");
        JsonObject jsonObject = jsonResult.get("data").getAsJsonObject();
        String token = jsonObject.get("webToken").getAsString();
        sb.append(HttpUtil.urlEncode(token));
        sb.append(";uip=");
        sb.append(CookieUtil.getCookieValue(account.getCookie(), "uip"));
        return sb.toString();

    }

    private String getPostParams(String reportDate, int page, int pageSize) {
        Map<String, String> map = new HashMap<>();
        map.put("startDate", reportDate);
        map.put("endDate", reportDate);
        map.put("page", page + "");
        map.put("rows", pageSize + "");
        String pairsJson = new Gson().toJson(map);
        return pairsJson;
    }

    private String getAuthParams(String token, String seed) {
        Map<String, String> map = new HashMap<>();
        String sign = EncodeUtils.getMD5Str(token + seed.substring(seed.length() - 8));
        map.put("sign", sign);
        logger.info(" >>>>>>>   『token: " + token + "』   『seed: " + seed + "』   『sign: " + sign + "』   <<<<<<<");
        String mapJson = new Gson().toJson(map);
        return mapJson;
    }

    private Map<String, String> getHeadMap(String cookie, String uip) {
        Map<String, String> headMap = new HashMap<>();
        headMap.put("Host", "ads.sohu.com");
        headMap.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:54.0) Gecko/20100101 Firefox/54.0");
        headMap.put("Accept", "*/*");
        headMap.put("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
        headMap.put("Accept-Encoding", "gzip, deflate");
        headMap.put("uip", uip);
        headMap.put("x-token", cookie);
        headMap.put("Connection", "keep-alive");
        headMap.put("Referer", "http://ads.sohu.com/");
        headMap.put("Content-Type", "application/json");
        headMap.put("Origin", "http://ads.sohu.com");
        return headMap;
    }
}
