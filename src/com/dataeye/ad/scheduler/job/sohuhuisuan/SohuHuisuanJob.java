package com.dataeye.ad.scheduler.job.sohuhuisuan;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DBPipeline;
import com.dataeye.ad.scheduler.job.CrawlerFilter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;

import com.dataeye.ad.common.ServerException;
import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.constant.Constant.CrawlerPlatform;
import com.dataeye.ad.constant.Constant.PlanSwitch;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;

public class SohuHuisuanJob extends CrawlerBaseJob {

    private final static CrawlerPlatform platform = CrawlerPlatform.SOHU_HUISUAN;

    private final static Logger logger = ServerUtil.getLogger(platform);

    private final static String planUrl = "http://hui.sohu.com/titan/adgroup/list?report_first={0}&start_date={1}&end_date={2}&view=list&page={3}";

    private final static String accountUrl = "http://hui.sohu.com/titan/payment/list";

    public SohuHuisuanJob() {
        this(DateUtil.unixTimestamp000());
    }

    public SohuHuisuanJob(long time) {
        super(time, platform);
    }

    public SohuHuisuanJob(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }


    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        // 请求
        String content = HttpUtil.getContentByGet(accountUrl, getHeaderMap(account.getCookie()));
        logger.info(content);
        // 解析
        AdAccountStat accountStat = parseAccount(content, account, time);
        // 入库
        DataProcessor.processAccountDay(accountStat);
    }

    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        String filterNoData = "1"; // 1表示过滤无数据的记录
        // 2018.03.01 由于平台的实时数据第二天会进行校准，补跑数据时会先重置历史数据统计表
        if (DateUtil.unixTimestamp000() != time) {
            DBPipeline.reSetPlanStatDay(account.getId(),reportDate);
        }
        int pageId = 1;
        int pageSize = 10;
        PageModel pageModel = new PageModel(pageId, pageSize);
        // 请求
        String content = HttpUtil.getContentByGet(
                HttpUtil.urlParamReplace(planUrl, filterNoData, reportDate, reportDate, pageId + ""),
                getHeaderMap(account.getCookie()));
        logger.info(content);
        // 解析
        List<AdPlanStat> poList = parsePlan(content, account, time, pageModel);
        // 入库
        DataProcessor.processPlanDay(poList);
        int totalPage = pageModel.getTotalPages();
        while (totalPage > pageId) {
            pageId++;
            content = HttpUtil.getContentByGet(
                    HttpUtil.urlParamReplace(planUrl, filterNoData, reportDate, reportDate, pageId + ""),
                    getHeaderMap(account.getCookie()));
            logger.info(content);
            poList = parsePlan(content, account, time, pageModel);
            // 入库
            DataProcessor.processPlanDay(poList);
        }
    }

    private List<AdPlanStat> parsePlan(String content, AdMediumAccount account, long time, PageModel pageModel) throws ServerException {
        List<AdPlanStat> poList = new ArrayList<AdPlanStat>();
        Document doc = Jsoup.parse(content);
        // 分页统计
        int totalPages = 1;
        Elements pageEle = doc.select(".pagination-box>ul>span:last-child>a");
        String pageLabel = "尾页";
        if (pageLabel.equals(pageEle.text())) {
            String[] lines = StringUtil.split(pageEle.attr("data"), "&page=");
            totalPages = StringUtil.convertInt(lines[lines.length - 1], 1);
        }
        pageModel.setTotalPages(totalPages);
        Elements elements = doc.getElementsByTag("table").get(0).getElementsByTag("tbody").get(0)
                .getElementsByTag("tr");
        for (Element element : elements) {
            Elements tds = element.getElementsByTag("td");
            int i = 0;
            String planName = "";
            int showNum = 0;
            int clickNum = 0;
            int cost = 0;
            int bid = 0;
            String bidStrategy = null;
            String planStatus = null;
            Integer planSwitch = null;
            String mPlanId = "";
            String groupName = "";
            String groupId = "";
            for (Element td : tds) {
                if (td != null) {
                    if (i == 1) {
                        // 计划
                        String aId = td.getElementsByTag("a").get(0).id();
                        mPlanId = aId.split("_")[3];
                        planName = td.getElementsByTag("a").get(0).text();
                    }
                    if (i == 2) {
                        // 计划组
                        String href = td.getElementsByTag("a").get(0).attr("href");
                        groupId = href.split("id=")[1];
                        groupName = td.getElementsByTag("a").get(0).text();
                    }
                    if (i == 3) {
                        int size = td.getElementsByClass("status-btn-open").size();
                        // 计划开关
                        planSwitch = size > 0 ? Constant.PlanSwitch.ON : PlanSwitch.OFF;
                    } else if (i == 4) {
                        // 计划状态
                        planStatus = td.getElementsByTag("span").text();
                    } else if (i == 7) {
                        // 曝光
                        showNum = StringUtil.convertInt(td.text().replaceAll(",", ""), 0);
                    } else if (i == 8) {
                        // 点击
                        clickNum = StringUtil.convertInt(td.text().replaceAll(",", ""), 0);
                    } else if (i == 10) {
                        // 花费
                        cost = (int) (100 * StringUtil.convertFloat(td.text().replaceAll(",", ""), 0));
                    } else if (i == 11) {
                        String bidItem = td.getElementsByTag("span").get(0).text();
                        // 出价策略
                        bidStrategy = bidItem.replaceAll("[0-9.]", "").trim();
                        // 出价
                        String bidStr = bidItem.replaceAll("[A-Za-z]", "").trim();
                        bid = (int) (100 * StringUtil.convertFloat(bidStr, 0));
                    }
                    i++;
                }

            }
            if (!StringUtil.isEmpty(planName)) {
                AdPlanStat po = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(), account.getId());
                poList.add(po);
                po.setClickNum(clickNum);
                po.setShowNum(showNum);
                po.setCost(cost);
                po.setBid(bid);
                po.setPlanBidStrategy(bidStrategy);
                po.setmPlanId(mPlanId);
                po.setPlanName(planName);
                po.setGroupId(groupId);
                po.setGroupName(groupName);
                po.setPlanStatus(planStatus);
                po.setPlanSwitch(planSwitch);
            }
        }
        return poList;
    }

    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time) throws Exception {
        Document doc = Jsoup.parse(content);
        Elements elements = doc.getElementsByClass("balance_s").get(0).getElementsByTag("span");
        String balanceStr = elements.get(0).html().trim().replace(",", "");
        String costTodayStr = elements.get(2).html().trim().replace(",", "");
        int balance = StringUtil.convertBigDecimal(balanceStr, 100, 0).intValue();
        int costToday = StringUtil.convertBigDecimal(costTodayStr, 100, 0).intValue();
        AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                account.getId());
        accountStat.setBalance(balance);
        accountStat.setCostToday(costToday);
        return accountStat;
    }

    private Map<String, String> getHeaderMap(String cookie) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Cookie", cookie);
        headerMap.put("Referer", "http://hui.sohu.com/titan/adgroup/list");
        headerMap.put("Host", "hui.sohu.com");
        headerMap.put("X-Requested-With", "XMLHttpRequest");
        headerMap
                .put("User-Agent",
                        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
        return headerMap;
    }

}
