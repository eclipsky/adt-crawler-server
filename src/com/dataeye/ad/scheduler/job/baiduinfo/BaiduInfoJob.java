package com.dataeye.ad.scheduler.job.baiduinfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dataeye.ad.scheduler.job.CrawlerFilter;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import kafka.utils.Json;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;

import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.constant.Constant.CrawlerPlatform;
import com.dataeye.ad.constant.Constant.PlanSwitch;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.scheduler.job.baiduinfo.model.BaiduInfoFilter;
import com.dataeye.ad.scheduler.job.baiduinfo.model.BaiduInfoForm;
import com.dataeye.ad.scheduler.job.baiduinfo.model.BaiduInfoListData;
import com.dataeye.ad.scheduler.job.baiduinfo.model.BaiduInfoResponse;
import com.dataeye.ad.scheduler.job.baiduinfo.model.LevelCond;
import com.dataeye.ad.util.CookieUtil;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;
import com.google.gson.JsonObject;

public class BaiduInfoJob extends CrawlerBaseJob {

    private final static CrawlerPlatform platform = CrawlerPlatform.BAIDU_INFO;

    private final static Logger logger = ServerUtil.getLogger(platform);

    private final static String planUrl = "http://feedads.baidu.com/nirvana/request.ajax?path=sirius/GET/unitlist";

    private final static String accountUrl = "http://feedads.baidu.com/nirvana/request.ajax?path=sirius/GET/userinfo";

    public BaiduInfoJob() {
        this(DateUtil.unixTimestamp000());
    }

    public BaiduInfoJob(long time) {
        super(time, platform);
    }

    public BaiduInfoJob(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }

    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        String cookie = account.getCookie();
        // 将可能导致获取到异常token的字符串（__cas__st__3=NLI; 和 __cas__st__3=0;）清除 add by luzhuyou 2017.04.14
        cookie = cookie.replace("__cas__st__3=NLI;", "").replace("__cas__st__3=0;", "");
        String userId = CookieUtil.getBaiduUserId(cookie);
        String token = CookieUtil.getBaiduToken(cookie);
        PageModel pageModel = new PageModel(1, 50);
        // 请求
        String content = HttpUtil.getContentByPost(planUrl, getHeaderMap(cookie),
                getFormDataAdList(userId, token, reportDate, reportDate, pageModel));
        logger.info(content);
        // 解析
        List<AdPlanStat> planList = parse(content, account, time, pageModel);
        // 入库
        DataProcessor.processPlanDay(planList);
        // 分页
        int totalNum = pageModel.getTotalNum();
        totalNum -= pageModel.getPageSize();
        while (totalNum > 0) {
            pageModel.setPageId(pageModel.getPageId() + 1);
            // 请求
            content = HttpUtil.getContentByPost(planUrl, getHeaderMap(cookie),
                    getFormDataAdList(userId, token, reportDate, reportDate, pageModel));
            logger.info(content);
            // 解析
            planList = parse(content, account, time, pageModel);
            // 入库
            DataProcessor.processPlanDay(planList);

            totalNum -= pageModel.getPageSize();
        }
    }

    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        // 将可能导致获取到异常token的字符串（__cas__st__3=NLI; 和 __cas__st__3=0;）清除 add by luzhuyou 2017.04.14
        cookie = cookie.replace("__cas__st__3=NLI;", "").replace("__cas__st__3=0;", "");
        String userId = CookieUtil.getBaiduUserId(cookie);
        String token = CookieUtil.getBaiduToken(cookie);
        // 请求
        String content = HttpUtil.getContentByPost(accountUrl, getHeaderMap(cookie),
                getAccountFormDataAdList(userId, token));
        logger.info(content);
        // 解析
        AdAccountStat accountStat = parseAccount(content, account, time);
        // 入库
        DataProcessor.processAccountDay(accountStat);
    }

    /**
     * <pre>
     * 获取表单列表
     * @author sam.xie
     * @date Mar 1, 2017 6:24:10 PM
     * @param userId
     * @param token
     * @param pageModel
     * @param startDate
     * @param endDate
     * @return
     */
    public List<NameValuePair> getFormDataAdList(String userId, String token, String startDate, String endDate,
                                                 PageModel pageModel) {
        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("params", generateParams(userId, startDate, endDate, pageModel)));
        pairs.add(new BasicNameValuePair("userid", userId));
        pairs.add(new BasicNameValuePair("token", token));
        pairs.add(new BasicNameValuePair("path", "sirius/GET/unitlist"));
        // pairs.add(new BasicNameValuePair("eventId", "4b534c46-d7ab-4468-7481-148913434334"));
        // pairs.add(new BasicNameValuePair("reqid", "4b534c46-1e83-49c3-bba6-148913434334"));
        return pairs;
    }

    public List<NameValuePair> getAccountFormDataAdList(String userId, String token) {
        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("params", "{}"));
        pairs.add(new BasicNameValuePair("userid", userId));
        pairs.add(new BasicNameValuePair("token", token));
        pairs.add(new BasicNameValuePair("path", "sirius/GET/userinfo"));
        // pairs.add(new BasicNameValuePair("eventId", "4b534c46-d7ab-4468-7481-148913434334"));
        // pairs.add(new BasicNameValuePair("reqid", "4b534c46-1e83-49c3-bba6-148913434334"));
        return pairs;
    }

    private String generateParams(String userId, String startDate, String endDate, PageModel pageModel) {
        BaiduInfoForm form = new BaiduInfoForm();
        LevelCond levelCond = new LevelCond();
        levelCond.setUserid(StringUtil.convertInt(userId, 0));
        // BaiduInfoFilter filter1 = new BaiduInfoFilter("unitstat", "in", new String[] { "0" });
        BaiduInfoFilter filter2 = new BaiduInfoFilter("bstype", "in", new String[]{"1"});
        BaiduInfoFilter[] filters = new BaiduInfoFilter[]{filter2};
        form.setFilters(filters);
        form.setFields(new String[]{"unitid", "unitname", "planid", "planname", "unitstat", "bidtype", "bid",
                "paysum", "clks", "shows", "ocpc"}); // 这个参数内容配置影响貌似不大
        form.setLevelCond(levelCond);
        form.setStartTime(startDate);
        form.setEndTime(endDate);
        form.setPageSize(pageModel.getPageSize());
        form.setPageNo(pageModel.getPageId());
        String result = Constant.GSON.toJson(form);
        return result;
    }

    /**
     * <pre>
     * 解析响应内容，封装成标准的PO对象
     * @author sam.xie
     * @date Feb 15, 2017 3:12:29 PM
     * @param account
     * @param pageModel
     * @param time
     * @throws Exception
     */
    private List<AdPlanStat> parse(String content, AdMediumAccount account, long time, PageModel pageModel)
            throws Exception {
        if (content.contains("\"redirect\":\"true\"")) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, content);
        }
        List<AdPlanStat> planList = new ArrayList<AdPlanStat>();
        if (StringUtil.isEmpty(content)) {
            return planList;
        }
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        if (resultJson == null) {
            return planList;
        }
        JsonObject dataJson = resultJson.get("data").getAsJsonObject();
        if (dataJson == null) {
            return planList;
        }
        int totalNum = StringUtil.getJsonIntValue(dataJson, "totalCount");
        pageModel.setTotalNum(totalNum); // 设置记录数
        JsonArray listData = dataJson.get("listData").getAsJsonArray();
        for (JsonElement item : listData) {
            AdPlanStat plan = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(), account.getId());
            planList.add(plan);
            JsonObject order = item.getAsJsonObject();
            // 媒体计划ID
            String mPlanId = order.get("unitid").getAsString();
            // 媒体计划名
            String planName = order.get("unitname").getAsString();
            // 媒体计划分组ID
            String groupId = order.get("planid").getAsString();
            // 媒体计划分组名
            String groupName = order.get("planname").getAsString();
            // 媒体计划出价 CPC出价/OCPC第一阶段出价
            float planBid = StringUtil.getJsonFloatValue(order, "bid");
            // 媒体计划出价策略  1-CPC   3-OCPC
            int planBidStrategy = order.get("bidtype").getAsInt();
            String planExtend = "";
            if (planBidStrategy == 3) {
                JsonElement ocpcElement = order.get("ocpc");
                if (!ocpcElement.isJsonNull()) {
                    JsonObject ocpcJson = ocpcElement.getAsJsonObject();
                    int ocpcLevel = ocpcJson.get("ocpcLevel").getAsInt();
                    // 1-OCPC第一阶段   2-OCPC第二阶段
                    if (ocpcLevel == 2) {
                        planBid = StringUtil.getJsonFloatValue(ocpcJson, "ocpcBid");
                    }
                    planExtend = ocpcElement.toString();
                }
            }
            // 媒体计划状态，注意这里取的是"pausestat"
            String planStatus = order.get("pausestat").getAsString();
            // 媒体计划开关
            int planSwitchRaw = order.get("pausestat").getAsInt();
            int planSwitch = planSwitchRaw == 0 ? PlanSwitch.ON : PlanSwitch.OFF;
            float cost = StringUtil.getJsonFloatValue(order, "paysum");
            int shows = StringUtil.getJsonIntValue(order, "shows");
            int clickNum = StringUtil.getJsonIntValue(order, "clks");
            plan.setmPlanId(mPlanId);
            plan.setPlanName(planName);
            plan.setCost(StringUtil.convertBigDecimal(cost + "", 100, 0).intValue()); // 转换为分
            plan.setShowNum(shows);
            plan.setClickNum(clickNum);
            plan.setBid(StringUtil.convertBigDecimal(planBid + "", 100, 0).intValue()); // 转换为分
            plan.setPlanBidStrategy(planBidStrategy + "");
            plan.setGroupId(groupId);
            plan.setGroupName(groupName);
            plan.setPlanStatus(planStatus);
            plan.setPlanSwitch(planSwitch);
            plan.setPlanExtend(planExtend);
        }
        return planList;
    }

    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time) throws Exception {
        if (content.contains("\"redirect\":\"true\"")) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, content);
        }
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        String balanceStr = resultJson.get("data").getAsJsonObject().get("balance").getAsString();
        int balance = StringUtil.convertBigDecimal(balanceStr, 100, 0).intValue();
        AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                account.getId());
        accountStat.setBalance(balance);
        return accountStat;
    }

    private Map<String, String> getHeaderMap(String cookie) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Cookie", cookie);
        headerMap
                .put("User-Agent",
                        "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
        headerMap.put("Host", "feedads.baidu.com");
        headerMap.put("Connection", "keep-alive");
        headerMap.put("Accept", "*/*");
        headerMap.put("Accept-Encoding", "gzip, deflate");
        headerMap.put("Content-Type", "application/x-www-form-urlencoded");
        return headerMap;
    }
}
