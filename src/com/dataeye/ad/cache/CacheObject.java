package com.dataeye.ad.cache;

import java.util.concurrent.atomic.AtomicInteger;

import com.dataeye.ad.util.DateUtil;

/**
 * 通用缓存对象，设置过期时间
 * 
 * @author sam.xie
 * @date Mar 6, 2017 11:59:18 AM
 * @version 1.0
 * @param <T>
 */
public class CacheObject<T> {

	/** 缓存对象 */
	private T cacheValue;

	/** 对象加入时间 */
	private int createTime;

	/** 对象过期时间 */
	private int expireTime;

	/** 对象访问次数 */
	private AtomicInteger accessTimes = new AtomicInteger(0);

	public boolean expire() {
		int now = DateUtil.unixTimestamp();
		if (now > expireTime) {
			return true;
		}
		return false;
	}

	public T getCacheValue() {
		return cacheValue;
	}

	public void setCacheValue(T cacheValue) {
		this.cacheValue = cacheValue;
	}

	public int getCreateTime() {
		return createTime;
	}

	public void setCreateTime(int createTime) {
		this.createTime = createTime;
	}

	public int getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(int expireTime) {
		this.expireTime = expireTime;
	}

	public AtomicInteger getAccessTimes() {
		return accessTimes;
	}

	public void setAccessTimes(AtomicInteger accessTimes) {
		this.accessTimes = accessTimes;
	}

}
