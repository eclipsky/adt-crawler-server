package com.dataeye.ad.scheduler.job.yidianzixun.model;

/**
 * Created by VitAs on 2017/6/27.
 */
public class LingxiGroupModel {

    private String id;

    private String name;


    public LingxiGroupModel(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
