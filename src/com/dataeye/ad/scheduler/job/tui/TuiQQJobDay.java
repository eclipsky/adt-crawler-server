package com.dataeye.ad.scheduler.job.tui;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.scheduler.job.CrawlerBaseExecutor;
import com.dataeye.ad.util.DateUtil;

public class TuiQQJobDay implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		CrawlerBaseExecutor.addJob(new TuiQQJob(DateUtil.unixTimestamp000() - Constant.ONEDAY));
	}

}
