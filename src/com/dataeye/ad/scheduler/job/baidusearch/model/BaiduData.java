package com.dataeye.ad.scheduler.job.baidusearch.model;

public class BaiduData {
	private String date;

	private int shows;

	private int clks;

	private float paysum;

	private PlanInfo planinfo;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getShows() {
		return shows;
	}

	public void setShows(int shows) {
		this.shows = shows;
	}

	public int getClks() {
		return clks;
	}

	public void setClks(int clks) {
		this.clks = clks;
	}

	public float getPaysum() {
		return paysum;
	}

	public void setPaysum(float paysum) {
		this.paysum = paysum;
	}

	public PlanInfo getPlaninfo() {
		return planinfo;
	}

	public void setPlaninfo(PlanInfo planinfo) {
		this.planinfo = planinfo;
	}

}
