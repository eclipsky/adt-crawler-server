package com.dataeye.ad.scheduler.job.mta;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;

import com.dataeye.ad.cluster.ClusterNode;
import com.dataeye.ad.common.CookieException;
import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.constant.Constant.CrawlerPlatform;
import com.dataeye.ad.constant.Constant.LoginStatus;
import com.dataeye.ad.pipeline.DBQuery;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountWithApp;
import com.dataeye.ad.pipeline.po.AdPageStat;
import com.dataeye.ad.scheduler.job.mta.model.MtaData;
import com.dataeye.ad.scheduler.job.mta.model.MtaDownloadData;
import com.dataeye.ad.scheduler.job.mta.model.MtaDownloadModel;
import com.dataeye.ad.scheduler.job.mta.model.MtaModel;
import com.dataeye.ad.util.ConfigFromDB;
import com.dataeye.ad.util.CookieUtil;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;

/**
 * MTA爬虫，落地页，下载页等数据
 * 
 * @author sam.xie
 * @date Feb 22, 2017 2:56:17 PM
 * @version 1.0
 */
public class MtaJob extends Thread implements Job {

    private final static CrawlerPlatform platform = CrawlerPlatform.TENCENT_MTA;

    private final static Logger logger = ServerUtil.getLogger(platform);

    private final static String landPageUrl = "http://mta.qq.com/h5/visitor/ctr_page/get_table_realtime?start_date={0}&end_date={1}&need_compare=0&start_compare_date=&end_compare_date=&app_id={2}&search_page=&rnd=1479108232653&ajax=1";

    private final static String downloadUrl = "http://mta.qq.com/h5/visitor/ctr_button_stat/get_table_realtime?start_date={0}&end_date={1}&need_compare=0&start_compare_date=&end_compare_date=&app_id={2}&rnd=1482225778535&ajax=1";

    private long time;

    private boolean needFilter = true;

    public MtaJob() {
    }

    public MtaJob(long time) {
        this.time = time;
    }

    @Override
    public void run() {
        needFilter = false;
        process(this.time);
    }

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        process(DateUtil.unixTimestamp000());
    }

    private void process(long time) {
        String statDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        List<AdAccountWithApp> accountList = DBQuery.getH5AppList(CrawlerPlatform.TENCENT_MTA.getId());
        filterAccount(accountList);
        logger.info("====================" + platform.getName() + ":" + statDate + "，爬虫启动====================");
        for (AdAccountWithApp account : accountList) {
            try {
                if (account.getLoginStatus() != LoginStatus.HAS_LOGGED
                        && account.getLoginStatus() != LoginStatus.IS_LOGGING) {
                    logger.info(platform.getName() + ":" + statDate + ":" + account + "，账户未登录，爬虫跳过...");
                    continue;
                }
                logger.info(platform.getName() + ":" + statDate + ":" + account + "，爬虫开始...");
                String cookie = CookieUtil.getCookieByAccount(account);
                CookieUtil.isCookieValid(cookie);
                String appId = account.getAppCode();
                // 1. 爬取页面排行中的PV，UV
                logger.info(platform.getName() + ":" + statDate + ":" + account + "，开始爬取PV和UV数据...");
                String content = HttpUtil.getContentByGet(
                        HttpUtil.urlParamReplace(landPageUrl, statDate, statDate, appId), getHeaderMap(cookie));
                logger.info(content);
                Collection<AdPageStat> pageDataList = parseLandPage(content, account, time);
                DataProcessor.processPageDay(pageDataList);

                // 2. 爬取自定义页中的下载次数
                logger.info(platform.getName() + ":" + statDate + ":" + account + "，开始爬取DownloadTimes数据...");
                content = HttpUtil.getContentByGet(HttpUtil.urlParamReplace(downloadUrl, statDate, statDate, appId),
                        getHeaderMap(cookie));
                logger.info(content);
                List<AdPageStat> pageDownloadList = parseDownload(content, account, time);
                DataProcessor.processPageDownloadDay(pageDownloadList);

                // 更新登陆状态
                // CookieUtil.changeLoginStatus(LoginStatus.HAS_LOGGED, account);
                logger.info(platform.getName() + ":" + statDate + ":" + account + "，爬虫结束...");
                ServerUtil.crawlerMailControlClear(account);
            } catch (Exception e) {
                // Cookie异常需要重新登录
                if (e instanceof CookieException) {
                    CookieUtil.changeLoginStatus(LoginStatus.NOT_LOGGED, account);
                }
                logger.error(platform.getName() + "账户：" + account + "，爬取异常！" + ServerUtil.printStackTrace(e));
                // 连续失败N次才会发送邮件
                boolean allowSend = ServerUtil.crawlerMailControl(account);
                if (allowSend) {
                    ServerUtil.sendAlarmMail(platform.getName(), account + "\n" + ServerUtil.printStackTrace(e));
                }
            }
        }
        logger.info("====================" + platform.getName() + ":" + statDate + "，爬取完成====================");
    }

    /**
     * 登录页面PV UV等
     */
    private Collection<AdPageStat> parseLandPage(String content, AdAccountWithApp account, long time) throws Exception {
        if (content.toLowerCase().startsWith("<!doctype html>")) {
            if (content.contains("登录超时")) {
                ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE);
            } else {
                ExceptionHandler.throwException(ExceptionEnum.SERVER_ERROR);
            }
        }
        MtaModel result = Constant.GSON.fromJson(content, MtaModel.class);
        List<MtaData> list = result.getData();
        Map<String, AdPageStat> map = new HashMap<String, AdPageStat>();

        for (MtaData data : list) {
            String pageUrl = cleanUrl(account, data.getUrl());
            if (!StringUtil.isEmpty(pageUrl)) { // 非空才处理
                AdPageStat po = map.get(pageUrl);
                if (null == po) {
                    po = new AdPageStat(DateUtil.fromUnixtimestamp(time), account.getCompanyId(),
                            account.getPlatformId(), account.getAppId());
                    po.setPageName(pageUrl);
                    /**
                     * update by sam.xie at 20170707 ad_pkg表做改造，使用h5_page_id作为主键，这里用pageName填充h5_page_id
                     */
                    po.setH5PageId(po.getPageName());
                    po.setPv(StringUtil.convertInt(data.getPv(), 0));
                    po.setUv(StringUtil.convertInt(data.getUv(), 0));
                    po.setUniqueClickNum(po.getUv());
                } else {
                    po.setPv(po.getPv() + StringUtil.convertInt(data.getPv(), 0));
                    po.setUv(po.getUv() + StringUtil.convertInt(data.getUv(), 0));
                    po.setUniqueClickNum(po.getUv());
                }
                if (po.getCompanyId() == 2) {
                    /**
                     * <pre>
                     * 到达率原本是由uv/媒体点击数，数据库的uv字段也兼容了tracking上报的到达数
                     * 现在优点到达率要用pv/媒体点击数，临时方案如下：mta的pv，pv，uv分别入库数据库字段pv，uv，unique_click_num
                     * fuck youdian! brain damage!
                     */
                    po.setUv(po.getPv());
                }
                map.put(pageUrl, po);
            }
        }
        return map.values();
    }

    /**
     * 下载点击或跳转统计
     */
    private List<AdPageStat> parseDownload(String content, AdAccountWithApp account, long time) throws Exception {
        if (content.toLowerCase().startsWith("<!doctype html>")) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE);
        }
        MtaDownloadModel result = Constant.GSON.fromJson(content, MtaDownloadModel.class);
        List<AdPageStat> list = new ArrayList<AdPageStat>();
        for (MtaDownloadData data : result.getData()) {
            String urlName = data.getUrl().replace("<div class=" + "\"b-all" + "\">", "").replace("</div>", "");
            if (urlName.startsWith("download")) {
                AdPageStat po = new AdPageStat(DateUtil.fromUnixtimestamp(time), account.getCompanyId(),
                        account.getPlatformId(), account.getAppId());
                po.setDownloadTimes(StringUtil.convertInt(data.getUv(), 0));
                po.setPageName(urlName.toLowerCase());// 统一为小写
                /**
                 * update by sam.xie at 20170707 ad_pkg表做改造，使用h5_page_id作为主键，这里用pageName填充h5_page_id
                 */
                po.setH5PageId(po.getPageName());
                list.add(po);
            }
        }
        return list;
    }

    private Map<String, String> getHeaderMap(String cookie) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Cookie", cookie);
        headerMap.put("Host", "mta.qq.com");
        headerMap.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0");
        headerMap.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        headerMap.put("X-Requested-With", "XMLHttpRequest");
        return headerMap;
    }

    private String cleanUrl(AdAccountWithApp account, String rawUrl) {
        // 转为小写
        String pageUrl = rawUrl.replace("<div class=" + "\"b-all" + "\">", "").replace("</div>", "").toLowerCase();
        // 统一过滤
        if (pageUrl.startsWith("/") || pageUrl.contains("__motz_")) {
            return null;
        }
        if (account.getCompanyId() == 2 || account.getCompanyId() == 10) {
            /**
             * <pre/>
             * 优点 && DE
             * 
             * 合法连接：
             * res.digitcube.net/ads-page/swipe/index-aibei_twzw_41H.html
             * res.youyousm.com
             * tttf.qdhdkj.com
             * 
             * 几种异常连接，有的直接去掉，有的需要统一化处理
             * 	res.digitcube.net/ads-page/swipe/index-aibei_twzw_50p.html__motz_
             * 	yd.ddquw.com/ads-page/swipe/index-aibei_twzw_39x.html_%7csrc_%e5%b0%8f%e5%b0%8f%e6%88%98%e4%ba%89%e6%94%bb%e7%95%a5%7csa_ib
             *  /c:/users/administrator/desktop/%e5%a1%94%e7%8e%8b%e4%b9%8b%e7%8e%8b/vanggame-twzw42b.html
             * /blank
             * resde1.wodou2015.com/ads-page/swipe/index-aibei_twzw_77f.html
             * 
             */
            // if (pageUrl.startsWith("res.digitcube.net") || pageUrl.startsWith("resde.wodou2015.com")) {
            if (pageUrl.endsWith(".html")
                    && !pageUrl.startsWith("m.ccplay.cc")
                    && !pageUrl.contains("10086")
                    && (pageUrl.startsWith("res.digitcube.net/") || pageUrl.startsWith("res")
                            || pageUrl.startsWith("tttf.qdhdkj.com") || isUrlValid(pageUrl))) { // 以".html"结尾，且不以"/"开始
                int index = pageUrl.indexOf(".html");
                return pageUrl.substring(0, index + ".html".length());
            } else {
                return null;
            }
        }
        // 其他原样返回
        return pageUrl;
    }

    /**
     * <pre>
     * 从数据库配置获取合法连接
     * @author sam.xie
     * @date Feb 28, 2018 1:48:18 PM
     * @param pageUrl
     * @return
     */
    private boolean isUrlValid(String pageUrl) {
        String whiteList = ConfigFromDB.getConfig("mta_valid_host");
        if (StringUtil.isEmpty(whiteList)) {
            return false;
        }
        String[] urlArray = whiteList.split(",");
        for (String url : urlArray) {
            if (pageUrl.startsWith(url.trim())) {
                return true;
            }
        }
        return false;
    }

    /**
     * <pre>
     * 账户过滤兼容standalone和cluster模式
     * @author sam.xie
     * @date Aug 25, 2017 11:14:46 AM
     * @param accountList
     */
    private void filterAccount(List<AdAccountWithApp> accountList) {
        int clusterMode = StringUtil.convertInt(ServerUtil.getConfigProperties().getProperty("clusterMode"), 0);
        if (clusterMode <= 0) {
            return;
        }
        if (!needFilter)
            return;
        List<AdAccountWithApp> nodeHandleAccounts = new ArrayList<AdAccountWithApp>();
        int mod = 0;
        int size = 1;
        if (null != ClusterNode.getNodes()) {
            mod = ClusterNode.getMod();
            size = ClusterNode.getNodes().size();
        }
        if (size > 1) {
            for (AdAccountWithApp account : accountList) {
                if (account.getAppId() % size == mod) {
                    nodeHandleAccounts.add(account);
                }
            }
            accountList.retainAll(nodeHandleAccounts);
        }
    }

    public static void main(String[] args) {
        String content = "<HTML>";
        System.out.println(content.toLowerCase());
        System.out.println(content);
    }
}
