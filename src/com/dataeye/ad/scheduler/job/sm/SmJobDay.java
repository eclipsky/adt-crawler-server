package com.dataeye.ad.scheduler.job.sm;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.scheduler.job.CrawlerBaseExecutor;
import com.dataeye.ad.util.DateUtil;

/**
 * Created by huangzehai on 2017/3/30.
 */
public class SmJobDay implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		CrawlerBaseExecutor.addJob(new SmJob(DateUtil.unixTimestamp000() - Constant.ONEDAY));
	}

}
