package com.dataeye.ad.scheduler.job.baiduinfo.model;

public class BaiduInfoForm {

	private LevelCond levelCond;

	private String[] fields;

	private String startTime;

	private String endTime;

	private int pageSize;

	private int pageNo;

	private BaiduInfoFilter[] filters;

	public LevelCond getLevelCond() {
		return levelCond;
	}

	public void setLevelCond(LevelCond levelCond) {
		this.levelCond = levelCond;
	}

	public String[] getFields() {
		return fields;
	}

	public void setFields(String[] fields) {
		this.fields = fields;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public BaiduInfoFilter[] getFilters() {
		return filters;
	}

	public void setFilters(BaiduInfoFilter[] filters) {
		this.filters = filters;
	}

}
