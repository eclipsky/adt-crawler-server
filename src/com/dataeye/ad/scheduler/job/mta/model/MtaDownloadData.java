package com.dataeye.ad.scheduler.job.mta.model;

public class MtaDownloadData {

	private String url;

	private String pv;

	private String uv;

	private String vv;

	private String iv;

	private String online_time;

	private String bounce_rate;

	private String detail;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getPv() {
		return pv;
	}

	public void setPv(String pv) {
		this.pv = pv;
	}

	public String getUv() {
		return uv;
	}

	public void setUv(String uv) {
		this.uv = uv;
	}

	public String getVv() {
		return vv;
	}

	public void setVv(String vv) {
		this.vv = vv;
	}

	public String getIv() {
		return iv;
	}

	public void setIv(String iv) {
		this.iv = iv;
	}

	public String getOnline_time() {
		return online_time;
	}

	public void setOnline_time(String online_time) {
		this.online_time = online_time;
	}

	public String getBounce_rate() {
		return bounce_rate;
	}

	public void setBounce_rate(String bounce_rate) {
		this.bounce_rate = bounce_rate;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

}