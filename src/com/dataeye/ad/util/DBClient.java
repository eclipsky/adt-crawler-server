package com.dataeye.ad.util;

import org.slf4j.Logger;

import com.dataeye.util.log.DELogger;
import com.xunlei.jdbc.JdbcTemplate;
import com.xunlei.netty.httpserver.spring.BeanUtil;
import com.xunlei.netty.httpserver.spring.SpringBootstrap;

public class DBClient {

	private final static Logger logger = DELogger.getLogger("db_client");

	public static JdbcTemplate getJdbcTemplateDeAdt() {
		JdbcTemplate jdbcTemplateDeAdt = null;
		try {
			jdbcTemplateDeAdt = BeanUtil.getTypedBean(SpringBootstrap.getContext(), "jdbcTemplateDeAdt");
		} catch (Exception e) {
			logger.error("jdbcTemplate Exceptiong:" + e.getMessage());
		}

		if (null == jdbcTemplateDeAdt) {
			logger.error("callBackUrl", "jdbcTemplateDeAdt is null,return");
		}
		return jdbcTemplateDeAdt;
	}
}
