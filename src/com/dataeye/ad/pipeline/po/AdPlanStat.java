package com.dataeye.ad.pipeline.po;

import java.math.BigDecimal;

import com.dataeye.ad.util.DateUtil;

/**
 * 计划统计表
 *
 * @author sam.xie
 * @version 1.0
 * @date Mar 6, 2017 5:04:08 PM
 */
public class AdPlanStat extends AdStatBase {
    private int planId;
    private String planName;
    private int mediumId;
    private int mediumAccountId;
    private int pageId;
    private int pkgId;
    private int cost; // 转换为分
    private int showNum;
    private int clickNum;
    private BigDecimal discountRate;
    private String mPlanId; // 媒体平台计划ID（区别ADT自增的planId）
    private String groupId; // 媒体平台计划组ID
    private String groupName; // 媒体平台计划组名称
    private Integer bid; // 媒体平台计划报价（单位：分）
    private String planBidStrategy; // 媒体平台计划报价策略（CPC，CPM等）
    private String planStatus; // 媒体平台计划状态（正在投放，已完成等）
    private Integer planSwitch; // 媒体平台计划开关（1-开，0-关）
    private String planExtend; // 扩展字段
    private Integer pageType; // 页面类型（计划|计划组）
    private String landingPage; // 落地页url
    /**
     * 推广产品id
     **/
    private int productId;

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public AdPlanStat(long timesamp, int companyId, int mediumId, int accountId) {
        this();
        this.setStatDate(DateUtil.fromUnixtimestamp(timesamp));
        this.setCompanyId(companyId);
        this.setMediumId(mediumId);
        this.setMediumAccountId(accountId);
    }

    public AdPlanStat(long timesamp, AdMediumAccount account) {
        this();
        this.setStatDate(DateUtil.fromUnixtimestamp(timesamp));
        this.setCompanyId(account.getCompanyId());
        this.setMediumId(account.getPlatformId());
        this.setMediumAccountId(account.getId());
        this.setPageType(account.getPageType());
    }

    private AdPlanStat() {
        this.mPlanId = "";
        this.groupId = "";
        this.groupName = "";
        this.cost = 0;
        this.showNum = 0;
        this.clickNum = 0;
        this.discountRate = BigDecimal.ZERO; // 0就是没有返点（折扣）
        this.bid = null; // 获取不到出价
        this.planBidStrategy = "";
        this.planExtend = "";
        this.landingPage = ""; // 落地页
    }

    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int planId) {
        this.planId = planId;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public int getMediumId() {
        return mediumId;
    }

    public void setMediumId(int mediumId) {
        this.mediumId = mediumId;
    }

    public int getMediumAccountId() {
        return mediumAccountId;
    }

    public void setMediumAccountId(int mediumAccountId) {
        this.mediumAccountId = mediumAccountId;
    }

    public int getPageId() {
        return pageId;
    }

    public void setPageId(int pageId) {
        this.pageId = pageId;
    }

    public int getPkgId() {
        return pkgId;
    }

    public void setPkgId(int pkgId) {
        this.pkgId = pkgId;
    }

    public String getmPlanId() {
        return mPlanId;
    }

    public void setmPlanId(String mPlanId) {
        this.mPlanId = mPlanId;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getShowNum() {
        return showNum;
    }

    public void setShowNum(int showNum) {
        this.showNum = showNum;
    }

    public int getClickNum() {
        return clickNum;
    }

    public void setClickNum(int clickNum) {
        this.clickNum = clickNum;
    }

    public BigDecimal getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(BigDecimal discountRate) {
        this.discountRate = discountRate;
    }

    public Integer getBid() {
        return bid;
    }

    public void setBid(Integer bid) {
        this.bid = bid;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getPlanBidStrategy() {
        return planBidStrategy;
    }

    public void setPlanBidStrategy(String planBidStrategy) {
        this.planBidStrategy = planBidStrategy;
    }

    public String getPlanStatus() {
        return planStatus;
    }

    public void setPlanStatus(String planStatus) {
        this.planStatus = planStatus;
    }

    public Integer getPlanSwitch() {
        return planSwitch;
    }

    public void setPlanSwitch(Integer planSwitch) {
        this.planSwitch = planSwitch;
    }

    public String getPlanExtend() {
        return planExtend;
    }

    public void setPlanExtend(String planExtend) {
        this.planExtend = planExtend;
    }

    public Integer getPageType() {
        return pageType;
    }

    public void setPageType(Integer pageType) {
        this.pageType = pageType;
    }

    public String getLandingPage() {
        return landingPage;
    }

    public void setLandingPage(String landingPage) {
        this.landingPage = landingPage;
    }

}
