package com.dataeye.ad.scheduler.job.tui.model;

import java.math.BigDecimal;


public class TuiqqTotal {

	private int cost;

	private int pv;

	private int click;

	private int active;

	private int download;

	private int action;

	private String adgroup_id;

	private int eCPC;

	private int eCPM;

	private int cpa;

	private BigDecimal ctr;

	private int cvr;

	private int creative_count;

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getPv() {
		return pv;
	}

	public void setPv(int pv) {
		this.pv = pv;
	}

	public int getClick() {
		return click;
	}

	public void setClick(int click) {
		this.click = click;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public int getDownload() {
		return download;
	}

	public void setDownload(int download) {
		this.download = download;
	}

	public int getAction() {
		return action;
	}

	public void setAction(int action) {
		this.action = action;
	}

	public String getAdgroup_id() {
		return adgroup_id;
	}

	public void setAdgroup_id(String adgroup_id) {
		this.adgroup_id = adgroup_id;
	}

	public int geteCPC() {
		return eCPC;
	}

	public void seteCPC(int eCPC) {
		this.eCPC = eCPC;
	}

	public int geteCPM() {
		return eCPM;
	}

	public void seteCPM(int eCPM) {
		this.eCPM = eCPM;
	}

	public int getCpa() {
		return cpa;
	}

	public void setCpa(int cpa) {
		this.cpa = cpa;
	}

	public BigDecimal getCtr() {
		return ctr;
	}

	public void setCtr(BigDecimal ctr) {
		this.ctr = ctr;
	}

	public int getCvr() {
		return cvr;
	}

	public void setCvr(int cvr) {
		this.cvr = cvr;
	}

	public int getCreative_count() {
		return creative_count;
	}

	public void setCreative_count(int creative_count) {
		this.creative_count = creative_count;
	}

}
