package com.dataeye.ad.scheduler.job.sinafensitong;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.scheduler.job.CrawlerBaseExecutor;
import com.dataeye.ad.util.DateUtil;

/**
 * Created by huangzehai on 2017/3/28.
 */
public class SinaFensitongJobDay implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		CrawlerBaseExecutor.addJob(new SinaFensitongJob(DateUtil.unixTimestamp000() - Constant.ONEDAY));
	}
}
