package com.dataeye.ad.kafka;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import kafka.consumer.ConsumerConfig;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;

import com.dataeye.ad.util.ServerUtil;
import com.qq.jutil.string.StringUtil;

/**
 * @see https://cwiki.apache.org/confluence/display/KAFKA/Consumer+Group+Example
 */
public class ConsumerAgent {
    private final ConsumerConnector consumer;
    private static final String KAFKA_CONFIG = "kafka.properties";

    public ConsumerAgent(Properties props) {
        this.consumer = kafka.consumer.Consumer.createJavaConsumerConnector(new ConsumerConfig(props));
    }

    private void run(String topic, int a_numThreads) {
        Map<String, Integer> topicCountMap = new HashMap<String, Integer>();
        topicCountMap.put(topic, new Integer(a_numThreads));
        Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap = consumer.createMessageStreams(topicCountMap);
        List<KafkaStream<byte[], byte[]>> streams = consumerMap.get(topic);
        ExecutorService executor = Executors.newFixedThreadPool(a_numThreads);
        int threadNumber = 0;
        for (final KafkaStream<byte[], byte[]> stream : streams) {
            executor.submit(new ConsumerRunner(stream, threadNumber));
            threadNumber++;
        }
    }

    public void shutdown() {
        if (consumer != null)
            consumer.shutdown();
    }

    public static void start() {
        Properties properties = ServerUtil.getConfig(KAFKA_CONFIG);
        String topic = properties.getProperty("kafka.topic", "adtCrawlerTopic");
        int threads = StringUtil.convertInt(properties.getProperty("consumer.thread"), 6);
        ConsumerAgent consumer = new ConsumerAgent(properties);
        consumer.run(topic, threads);
    }

    public static void main(String[] args) {
        start();
    }
}
