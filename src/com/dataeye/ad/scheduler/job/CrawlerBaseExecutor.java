package com.dataeye.ad.scheduler.job;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CrawlerBaseExecutor {

	private static ExecutorService executor = Executors.newFixedThreadPool(20);

	
	public static void addJob(CrawlerBaseJob job) {
		executor.submit(job);
	}
}
