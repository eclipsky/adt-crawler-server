package com.dataeye.ad.kafka;

import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;

import org.slf4j.Logger;

import com.dataeye.ad.constant.Constant.KafkaEvent;
import com.dataeye.ad.pipeline.DBPipeline;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdPageStat;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.util.StringUtil;
import com.dataeye.util.log.DELogger;

public class ConsumerRunner implements Runnable {

    private KafkaStream<byte[], byte[]> m_stream;
    private int m_threadNumber;
    private static final Logger logger = DELogger.getLogger("consumer");

    public ConsumerRunner(KafkaStream<byte[], byte[]> a_stream, int a_threadNumber) {
        m_threadNumber = a_threadNumber;
        m_stream = a_stream;
    }

    public void run() {
        logger.info("consumer-" + m_threadNumber + " is starting...");
        ConsumerIterator<byte[], byte[]> it = m_stream.iterator();
        while (it.hasNext()) {
            String msg = new String(it.next().message());
            logger.info(msg);
            try {
                String event = StringUtil.jsonParser.parse(msg).getAsJsonObject().get("event").getAsString();
                if (KafkaEvent.EVENT_ACCOUNT.equals(event)) {
                    DBPipeline.insertAccountDay(StringUtil.gson.fromJson(msg, AdAccountStat.class));
                } else if (KafkaEvent.EVENT_PLAN.equals(event)) {
                    DBPipeline.insertPlanDay(StringUtil.gson.fromJson(msg, AdPlanStat.class));
                } else if (KafkaEvent.EVENT_PAGE.equals(event)) {
                    DBPipeline.insertPageDay(StringUtil.gson.fromJson(msg, AdPageStat.class));
                } else {
                    // TODO
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        logger.info("Shutting down Thread: " + m_threadNumber);
    }
}
