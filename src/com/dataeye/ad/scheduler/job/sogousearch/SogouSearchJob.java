package com.dataeye.ad.scheduler.job.sogousearch;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dataeye.ad.scheduler.job.CrawlerFilter;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;

import com.dataeye.ad.common.CookieException;
import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.common.ServerException;
import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.constant.Constant.CrawlerPlatform;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.scheduler.job.sogousearch.model.SogouSearchAjaxpc;
import com.dataeye.ad.scheduler.job.sogousearch.model.SogouSearchCpcReport;
import com.dataeye.ad.scheduler.job.sogousearch.model.SogouSearchData;
import com.dataeye.ad.scheduler.job.sogousearch.model.SogouSearchResponse;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;
import com.google.gson.JsonObject;

public class SogouSearchJob extends CrawlerBaseJob {

    private final static CrawlerPlatform platform = CrawlerPlatform.SOGOU_SEARCH;

    private final static Logger logger = ServerUtil.getLogger(platform);

    private static final String pastGroupUrl = "http://xuri.p4p.sogou.com/report/queryPlanReport.action";

    private static final String groupUrl = "http://xuri.p4p.sogou.com/manage/showplanlist.action";

    private static final String planUrl = "http://xuri.p4p.sogou.com/manage/showgrouplist.action";

    private static final String pastPlanUrl = "http://xuri.p4p.sogou.com/report/queryGroupReport.action";

    private final static String accountUrl = "http://xuri.p4p.sogou.com/profile/init.action";

    public SogouSearchJob() {
        this(DateUtil.unixTimestamp000());
    }

    public SogouSearchJob(long time) {
        super(time, platform);
    }

    public SogouSearchJob(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }


    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        // 2017/6/20 由于爬取当天数据和爬取历史数据的请求不是同一路径，所以需要对time参数进行判断
        String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        String cookie = account.getCookie();
        // 获取计划总数
        int pageId = 1;
        int pageSize = 200;
        PageModel pageModel = new PageModel(pageId, pageSize);
        // 请求
        String content = "";
        List<AdPlanStat> poList = new ArrayList<AdPlanStat>();
        //如果时间为当天
        if (DateUtil.unixTimestamp000() == time) {
            content = HttpUtil.getContentByPost(planUrl, getHeaderMap(cookie),
                    getFormDataAdList(time, pageId, pageSize, reportDate, reportDate));
            logger.info(content);
            // 解析
            poList = parsePlan(content, account, time, pageModel);
        } else {
            content = HttpUtil.getContentByPost(pastPlanUrl, getHeaderMap(cookie),
                    getFormDataAdList(time, pageId, pageSize, reportDate, reportDate));
            logger.info(content);
            // 解析
            poList = parsePlanPast(content, account, time, pageModel);
        }
        int totalNum = pageModel.getTotalNum();
        // 入库
        DataProcessor.processPlanDay(poList);
        totalNum -= pageSize;
        while (totalNum > 0) {
            pageId++;
            // 请求
            //如果时间为当天
            if (DateUtil.unixTimestamp000() == time) {
                content = HttpUtil.getContentByPost(planUrl, getHeaderMap(cookie),
                        getFormDataAdList(time, pageId, pageSize, reportDate, reportDate));
                logger.info(content);
                // 解析
                poList = parsePlan(content, account, time, pageModel);
            } else {
                content = HttpUtil.getContentByPost(pastPlanUrl, getHeaderMap(cookie),
                        getFormDataAdList(time, pageId, pageSize, reportDate, reportDate));
                logger.info(content);
                // 解析
                poList = parsePlanPast(content, account, time, pageModel);
            }
            // 入库
            DataProcessor.processPlanDay(poList);
            totalNum -= pageSize;
        }
    }

    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        // 请求
        String content = HttpUtil.getContentByGet(accountUrl, getHeaderMap(cookie));
        logger.info(content);
        // 解析
        AdAccountStat accountStat = parseAccount(content, account, time);
        // 入库
        DataProcessor.processAccountDay(accountStat);
    }

    @Override
    public void planGroupSpider(long time, AdMediumAccount account) throws Exception {
        String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        String cookie = account.getCookie();
        // 获取计划总数
        int pageId = 1;
        int pageSize = 200;
        PageModel pageModel = new PageModel(pageId, pageSize);
        String content = "";
        List<AdPlanStat> poList = new ArrayList<AdPlanStat>();
        if (DateUtil.unixTimestamp000() == time) {
            // 请求
            content = HttpUtil.getContentByPost(groupUrl, getHeaderMap(cookie),
                    getFormDataAdList(time, pageId, pageSize, reportDate, reportDate));
            logger.info(content);
            // 解析
            poList = parseGroup(content, account, time, pageModel);
        } else {
            // 请求
            content = HttpUtil.getContentByPost(pastGroupUrl, getHeaderMap(cookie),
                    getFormDataAdList(time, pageId, pageSize, reportDate, reportDate));
            logger.info(content);
            // 解析
            poList = parseGroupPast(content, account, time, pageModel);
        }
        // 计划数为0，直接跳过
        int totalNum = pageModel.getTotalNum();
        // 入库
        DataProcessor.processPlanDay(poList);
        totalNum -= pageSize;
        while (totalNum > 0) {
            pageId++;
            if (DateUtil.unixTimestamp000() == time) {
                // 请求
                content = HttpUtil.getContentByPost(groupUrl, getHeaderMap(cookie),
                        getFormDataAdList(time, pageId, pageSize, reportDate, reportDate));
                logger.info(content);
                // 解析
                poList = parseGroup(content, account, time, pageModel);
            } else {
                // 请求
                content = HttpUtil.getContentByPost(pastGroupUrl, getHeaderMap(cookie),
                        getFormDataAdList(time, pageId, pageSize, reportDate, reportDate));
                logger.info(content);
                // 解析
                poList = parseGroupPast(content, account, time, pageModel);
            }
            // 入库
            DataProcessor.processPlanDay(poList);
            totalNum -= pageSize;
        }
    }

    /**
     * <pre>
     * 解析响应内容，封装成标准的PO对象
     * @author sam.xie
     * @date Feb 15, 2017 3:12:29 PM
     * @throws ServerException
     * @throws CookieException
     */
    private List<AdPlanStat> parseGroupPast(String content, AdMediumAccount account, long time, PageModel pageModel)
            throws ServerException, CookieException {
        SogouSearchResponse response = Constant.GSON.fromJson(content, SogouSearchResponse.class);
        if (!"0".equals(response.getFlag())) {
            ExceptionHandler.throwException(ExceptionEnum.REQUEST_ERROR, Constant.GSON.toJson(response.getMsg()));
        }
        SogouSearchAjaxpc ajaxpc = response.getAjaxpc()[0];
        pageModel.setTotalNum(ajaxpc.getRecordCount()); // 设置总记录数
        List<AdPlanStat> poList = new ArrayList<AdPlanStat>();
        for (SogouSearchData item : response.getData()) {
            for (SogouSearchCpcReport report : item.getCpcReportList()) {
                AdPlanStat reportPO = new AdPlanStat(time, account);
                poList.add(reportPO);
                int clickNum = report.getAll().getClick();
                int showNum = report.getAll().getPv();
                int cost = report.getAll().getConsume().multiply(new BigDecimal(100)).intValue();
                String mPlanId = report.getAll().getCpcPlanId() + "";
                String planName = report.getAll().getCpcPlanName();
                reportPO.setClickNum(clickNum);
                reportPO.setCost(cost);
                reportPO.setShowNum(showNum);
                reportPO.setPlanName(planName);
                reportPO.setmPlanId(mPlanId);
            }
        }
        return poList;
    }

    /**
     * 爬取当天的分组数据解析
     *
     * @param content
     * @param account
     * @param time
     * @param pageModel
     * @return
     * @throws ServerException
     * @throws CookieException
     */
    private List<AdPlanStat> parseGroup(String content, AdMediumAccount account, long time, PageModel pageModel)
            throws ServerException, CookieException {
        List<AdPlanStat> poList = new ArrayList<AdPlanStat>();
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        if (!"0".equals(resultJson.get("flag").getAsString())) {
            ExceptionHandler.throwException(ExceptionEnum.REQUEST_ERROR, resultJson.get("msg").getAsString());
        }
        int totalNum = resultJson.get("ajaxpc").getAsJsonArray().get(0).getAsJsonObject().get("recordCount").getAsInt();
        pageModel.setTotalNum(totalNum);
        JsonArray list = resultJson.get("data").getAsJsonArray();
        for (JsonElement item : list) {
            AdPlanStat adPlanStat = new AdPlanStat(time, account);
            poList.add(adPlanStat);
            JsonObject order = item.getAsJsonObject();
            // 解析投放计划响应内容
            int clickNum = StringUtil.getJsonIntValue(order, "click");
            int showNum = StringUtil.getJsonIntValue(order, "pv");
            int cost = StringUtil.getJsonIntValue(order, "spend");
            // 媒体计划分组ID
            String mPlanId = order.get("cpcPlanId").getAsInt() + "";
            // 媒体计划分组名
            String planName = order.get("planName").getAsString();
            adPlanStat.setmPlanId(mPlanId);
            adPlanStat.setPlanName(planName);
            adPlanStat.setClickNum(clickNum);
            adPlanStat.setShowNum(showNum);
            adPlanStat.setCost(cost);
        }
        return poList;
    }

    /**
     * 爬取当天的计划数据内容解析
     *
     * @param content
     * @param account
     * @param time
     * @param pageModel
     * @return
     * @throws ServerException
     * @throws CookieException
     */
    private List<AdPlanStat> parsePlan(String content, AdMediumAccount account, long time, PageModel pageModel)
            throws ServerException, CookieException {
        List<AdPlanStat> poList = new ArrayList<AdPlanStat>();
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        if (!"0".equals(resultJson.get("flag").getAsString())) {
            ExceptionHandler.throwException(ExceptionEnum.REQUEST_ERROR, resultJson.get("msg").getAsString());
        }
        int totalNum = resultJson.get("ajaxpc").getAsJsonArray().get(0).getAsJsonObject().get("recordCount").getAsInt();
        pageModel.setTotalNum(totalNum);
        JsonArray list = resultJson.get("data").getAsJsonArray();
        for (JsonElement item : list) {
            AdPlanStat adPlanStat = new AdPlanStat(time, account);
            poList.add(adPlanStat);
            JsonObject order = item.getAsJsonObject();
            // 解析投放计划响应内容
            int clickNum = StringUtil.getJsonIntValue(order, "click");
            int showNum = StringUtil.getJsonIntValue(order, "pv");
            int cost = StringUtil.getJsonIntValue(order, "spend");
            // 媒体计划ID
            String mPlanId = order.get("cpcgrpid").getAsInt() + "";
            // 媒体计划名
            String planName = order.get("groupName").getAsString();
            // 媒体计划分组ID
            String groupId = order.get("cpcPlanId").getAsInt() + "";
            // 媒体计划分组名
            String groupName = order.get("planName").getAsString();
            // 媒体计划出价
            String groupPrice = order.get("groupprice").getAsString();
            // 媒体计划状态  正常 暂停 推广组暂停
            String planStatus = order.get("status").getAsString();
            // 媒体计划开关
            Integer planSwitch = null;
            // 0- 开启 1- 暂停
            int switchNum = order.get("isPause").getAsInt();
            if (switchNum == 0) {
                planSwitch = Constant.PlanSwitch.ON;
            } else if (switchNum == 1) {
                planSwitch = Constant.PlanSwitch.OFF;
            }
            int planBid = StringUtil.convertBigDecimal(groupPrice, 100, 0).intValue();
            adPlanStat.setmPlanId(mPlanId);
            adPlanStat.setPlanName(planName);
            adPlanStat.setGroupId(groupId);
            adPlanStat.setGroupName(groupName);
            adPlanStat.setBid(planBid);
            adPlanStat.setClickNum(clickNum);
            adPlanStat.setShowNum(showNum);
            adPlanStat.setCost(cost);
            adPlanStat.setPlanStatus(planStatus);
            adPlanStat.setPlanSwitch(planSwitch);
        }

        return poList;
    }

    /**
     * 爬取历史的投放计划内容解析
     *
     * @param content
     * @param account
     * @param time
     * @param pageModel
     * @return
     * @throws ServerException
     * @throws CookieException
     */
    private List<AdPlanStat> parsePlanPast(String content, AdMediumAccount account, long time, PageModel pageModel)
            throws ServerException, CookieException {
        List<AdPlanStat> poList = new ArrayList<AdPlanStat>();
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        if (!"0".equals(resultJson.get("flag").getAsString())) {
            ExceptionHandler.throwException(ExceptionEnum.REQUEST_ERROR, resultJson.get("msg").getAsString());
        }
        int totalNum = resultJson.get("ajaxpc").getAsJsonArray().get(0).getAsJsonObject().get("recordCount").getAsInt();
        pageModel.setTotalNum(totalNum);
        JsonArray list = resultJson.get("data").getAsJsonArray().get(0).getAsJsonObject().get("cpcReportList").getAsJsonArray();
        for (JsonElement item : list) {
            AdPlanStat adPlanStat = new AdPlanStat(time, account);
            poList.add(adPlanStat);
            JsonObject order = item.getAsJsonObject().get("all").getAsJsonObject();
            // 解析投放计划响应内容
            int clickNum = StringUtil.getJsonIntValue(order, "click");
            int showNum = StringUtil.getJsonIntValue(order, "pv");
            int cost = StringUtil.convertBigDecimal(StringUtil.getJsonStrValue(order, "consume"), 100, 0).intValue();
            // 媒体计划ID
            String mPlanId = order.get("cpcGroupId").getAsInt() + "";
            // 媒体计划名
            String planName = order.get("cpcGroupName").getAsString();
            // 媒体计划分组ID
            String groupId = order.get("cpcPlanId").getAsInt() + "";
            // 媒体计划分组名
            String groupName = order.get("cpcPlanName").getAsString();
            adPlanStat.setmPlanId(mPlanId);
            adPlanStat.setPlanName(planName);
            adPlanStat.setGroupId(groupId);
            adPlanStat.setGroupName(groupName);
            adPlanStat.setClickNum(clickNum);
            adPlanStat.setShowNum(showNum);
            adPlanStat.setCost(cost);
        }

        return poList;


    }

    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time) throws Exception {
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        String balanceStr = resultJson.get("data").getAsJsonArray().get(0).getAsJsonObject()
                .get("cpcRemain").getAsString();
        int balance = StringUtil.convertBigDecimal(balanceStr, 100, 0).intValue();
        AdAccountStat accountStat = new AdAccountStat(time, account);
        accountStat.setBalance(balance);
        return accountStat;
    }


    /**
     * post请求参数解析
     *
     * @param time
     * @param pageId
     * @param pageSize
     * @param beginDate
     * @param endDate
     * @return
     */
    private List<NameValuePair> getFormDataAdList(long time, int pageId, int pageSize, String beginDate, String endDate) {
        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        if (time == DateUtil.unixTimestamp000()) { // 今日
            pairs.add(new BasicNameValuePair("queryDto.timeType", "1"));
            pairs.add(new BasicNameValuePair("queryDto.queryStatus", "-1"));
            pairs.add(new BasicNameValuePair("queryDto.matchType", "0"));
            pairs.add(new BasicNameValuePair("queryDto.searchInfo", ""));
            pairs.add(new BasicNameValuePair("queryDto.filterType", "0"));
        } else { // 历史数据（使用自定义事件）
            pairs.add(new BasicNameValuePair("reportQueryFormDto.timeType", "5"));
            pairs.add(new BasicNameValuePair("reportQueryFormDto.startDate", beginDate));
            pairs.add(new BasicNameValuePair("reportQueryFormDto.endDate", endDate));
            pairs.add(new BasicNameValuePair("reportQueryFormDto.deviceType", "0"));
            pairs.add(new BasicNameValuePair("reportQueryFormDto.sumType", "1"));
        }
        pairs.add(new BasicNameValuePair("pageNo", pageId + ""));
        pairs.add(new BasicNameValuePair("pageSize", pageSize + ""));
        return pairs;
    }


    private Map<String, String> getHeaderMap(String cookie) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Cookie", cookie);
        headerMap.put("Host", "xuri.p4p.sogou.com");
        headerMap.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.86 Safari/537.36");
        headerMap.put("Accept", "*/*");
        headerMap.put("Accept-Language", "en,zh-CN;q=0.8,zh;q=0.6");
        headerMap.put("Accept-Encoding", "gzip, deflate");
        headerMap.put("Connection", "keep-alive");
        headerMap.put("Referer", "http://xuri.p4p.sogou.com/cpcadindex/init.action");
        headerMap.put("Content-Type", "application/x-www-form-urlencoded");
        headerMap.put("Sogou-Hash", "#datareport/plan/list");
        headerMap.put("Sogou-Request-Type", "XMLHTTPRequest");
        return headerMap;
    }

}
