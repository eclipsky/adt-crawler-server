package com.dataeye.ad.scheduler.job.tui.model;

public class TuiQQItem {

	private String adgroup_id;

	private String adgroup_name;

	private String campaign_id;

	private String campaign_name;

	private String product_type;

	private String channel_id;

	private String unit_price;

	private String creative_template_id;

	private String creative_count;

	private long create_time;

	private long modify_time;

	private String category_id;

	private String price_mode;

	private ProductContent product_content;

	private String optimum_target;

	private String cost;

	private String pv;

	private String eCPM;

	private String eCPC;

	private String ctr;
	private String active;
	private String download;
	private String click;
	private String action;
	private String cpa;
	private String cvr;
	private String status;
	private int validate_creative_count;

	private String operation_status;

	private String creative_template_name;

	private boolean is_editable;

	public String getAdgroup_id() {
		return adgroup_id;
	}

	public void setAdgroup_id(String adgroup_id) {
		this.adgroup_id = adgroup_id;
	}

	public String getAdgroup_name() {
		return adgroup_name;
	}

	public void setAdgroup_name(String adgroup_name) {
		this.adgroup_name = adgroup_name;
	}

	public String getCampaign_id() {
		return campaign_id;
	}

	public void setCampaign_id(String campaign_id) {
		this.campaign_id = campaign_id;
	}

	public String getCampaign_name() {
		return campaign_name;
	}

	public void setCampaign_name(String campaign_name) {
		this.campaign_name = campaign_name;
	}

	public String getProduct_type() {
		return product_type;
	}

	public void setProduct_type(String product_type) {
		this.product_type = product_type;
	}

	public String getChannel_id() {
		return channel_id;
	}

	public void setChannel_id(String channel_id) {
		this.channel_id = channel_id;
	}

	public String getUnit_price() {
		return unit_price;
	}

	public void setUnit_price(String unit_price) {
		this.unit_price = unit_price;
	}

	public String getCreative_template_id() {
		return creative_template_id;
	}

	public void setCreative_template_id(String creative_template_id) {
		this.creative_template_id = creative_template_id;
	}

	public String getCreative_count() {
		return creative_count;
	}

	public void setCreative_count(String creative_count) {
		this.creative_count = creative_count;
	}

	public long getCreate_time() {
		return create_time;
	}

	public void setCreate_time(long create_time) {
		this.create_time = create_time;
	}

	public long getModify_time() {
		return modify_time;
	}

	public void setModify_time(long modify_time) {
		this.modify_time = modify_time;
	}

	public String getCategory_id() {
		return category_id;
	}

	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}

	public String getPrice_mode() {
		return price_mode;
	}

	public void setPrice_mode(String price_mode) {
		this.price_mode = price_mode;
	}

	public String getOptimum_target() {
		return optimum_target;
	}

	public void setOptimum_target(String optimum_target) {
		this.optimum_target = optimum_target;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getPv() {
		return pv;
	}

	public void setPv(String pv) {
		this.pv = pv;
	}

	public String geteCPM() {
		return eCPM;
	}

	public void seteCPM(String eCPM) {
		this.eCPM = eCPM;
	}

	public String geteCPC() {
		return eCPC;
	}

	public void seteCPC(String eCPC) {
		this.eCPC = eCPC;
	}

	public String getCtr() {
		return ctr;
	}

	public void setCtr(String ctr) {
		this.ctr = ctr;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getDownload() {
		return download;
	}

	public void setDownload(String download) {
		this.download = download;
	}

	public String getClick() {
		return click;
	}

	public void setClick(String click) {
		this.click = click;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getCpa() {
		return cpa;
	}

	public void setCpa(String cpa) {
		this.cpa = cpa;
	}

	public String getCvr() {
		return cvr;
	}

	public void setCvr(String cvr) {
		this.cvr = cvr;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getValidate_creative_count() {
		return validate_creative_count;
	}

	public void setValidate_creative_count(int validate_creative_count) {
		this.validate_creative_count = validate_creative_count;
	}

	public String getOperation_status() {
		return operation_status;
	}

	public void setOperation_status(String operation_status) {
		this.operation_status = operation_status;
	}

	public String getCreative_template_name() {
		return creative_template_name;
	}

	public void setCreative_template_name(String creative_template_name) {
		this.creative_template_name = creative_template_name;
	}

	public boolean isIs_editable() {
		return is_editable;
	}

	public void setIs_editable(boolean is_editable) {
		this.is_editable = is_editable;
	}

	public ProductContent getProduct_content() {
		return product_content;
	}

	public void setProduct_content(ProductContent product_content) {
		this.product_content = product_content;
	}

	public class ProductContent {

		private String product_click_url;

		public String getProduct_click_url() {
			return product_click_url;
		}

		public void setProduct_click_url(String product_click_url) {
			this.product_click_url = product_click_url;
		}

	}
}
