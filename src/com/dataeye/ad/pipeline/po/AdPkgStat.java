package com.dataeye.ad.pipeline.po;

import java.util.Date;

public class AdPkgStat extends AdStatBase {
    private int pkgId;
    private int CPAppId;
    private String CPPkgId;
    private String pkgName;
    private int downloadTimes;
    private int activeNum;
    private int registerNum;
    private int loginNum;
    private int newPayNum;
    private float newPayAmount;
    private int totalPayTimes;
    private int totalPayNum;
    private float totalPayAmount;
    private String pageName;
    private String remark;

    public AdPkgStat() {

    }

    public AdPkgStat(Date statDate, int companyId) {
        this.setStatDate(statDate);
        this.setCompanyId(companyId);
    }

    public AdPkgStat(Date statDate, int companyId, String pkgName, int downloadTimes) {
        this(statDate, companyId);
        this.pkgName = pkgName;
        this.downloadTimes = downloadTimes;
    }

    public int getPkgId() {
        return pkgId;
    }

    public void setPkgId(int pkgId) {
        this.pkgId = pkgId;
    }

    public int getCPAppId() {
        return CPAppId;
    }

    public void setCPAppId(int CPAppId) {
        this.CPAppId = CPAppId;
    }

    public String getCPPkgId() {
        return CPPkgId;
    }

    public void setCPPkgId(String CPPkgId) {
        this.CPPkgId = CPPkgId;
    }

    public String getPkgName() {
        return pkgName;
    }

    public void setPkgName(String pkgName) {
        this.pkgName = pkgName;
    }

    public int getDownloadTimes() {
        return downloadTimes;
    }

    public void setDownloadTimes(int downloadTimes) {
        this.downloadTimes = downloadTimes;
    }

    public int getActiveNum() {
        return activeNum;
    }

    public void setActiveNum(int activeNum) {
        this.activeNum = activeNum;
    }

    public int getRegisterNum() {
        return registerNum;
    }

    public void setRegisterNum(int registerNum) {
        this.registerNum = registerNum;
    }

    public int getLoginNum() {
        return loginNum;
    }

    public void setLoginNum(int loginNum) {
        this.loginNum = loginNum;
    }

    public int getNewPayNum() {
        return newPayNum;
    }

    public void setNewPayNum(int newPayNum) {
        this.newPayNum = newPayNum;
    }

    public float getNewPayAmount() {
        return newPayAmount;
    }

    public void setNewPayAmount(float newPayAmount) {
        this.newPayAmount = newPayAmount;
    }

    public int getTotalPayTimes() {
        return totalPayTimes;
    }

    public void setTotalPayTimes(int totalPayTimes) {
        this.totalPayTimes = totalPayTimes;
    }

    public int getTotalPayNum() {
        return totalPayNum;
    }

    public void setTotalPayNum(int totalPayNum) {
        this.totalPayNum = totalPayNum;
    }

    public float getTotalPayAmount() {
        return totalPayAmount;
    }

    public void setTotalPayAmount(float totalPayAmount) {
        this.totalPayAmount = totalPayAmount;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

}
