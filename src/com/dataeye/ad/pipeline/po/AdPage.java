package com.dataeye.ad.pipeline.po;

public class AdPage extends AdBase {

	private String pageName;

	private String h5AppId;

	private String h5PageId;

	public String getPageName() {
		return pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	public String getH5AppId() {
		return h5AppId;
	}

	public void setH5AppId(String h5AppId) {
		this.h5AppId = h5AppId;
	}

	public String getH5PageId() {
		return h5PageId;
	}

	public void setH5PageId(String h5PageId) {
		this.h5PageId = h5PageId;
	}

}
