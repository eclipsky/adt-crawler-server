package com.dataeye.ad.scheduler.job.baidusearch.model;



/**
 * Created by VitAs on 2017/7/3.
 */
public class BaiduRequest {

    private final String[] fields={"unitid","planid","pausestat","unitname","planname","unitbid","unitMPriceFactor",
            "shows","clks","paysum","trans","avgprice","unitstat","matchPriceFactor","matchPriceStatus","phonetrans",
            "clkrate","extbind","allnegativecnt","showpay","deviceprefer","mPriceFactor","adtype","finebidstatus",
            "atstatus","atbid","planatbid","bidprefer","pcpricefactor","planPcpricefactor"};

    private int pageNo;

    private int pageSize;

    private String startTime;

    private String endTime;

    private LevelCond levelCond;


    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public LevelCond getLevelCond() {
        return levelCond;
    }

    public void setLevelCond(LevelCond levelCond) {
        this.levelCond = levelCond;
    }

    public BaiduRequest() {
    }

    public BaiduRequest(int pageNo, int pageSize, String startTime, String endTime, LevelCond levelCond) {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        this.startTime = startTime;
        this.endTime = endTime;
        this.levelCond = levelCond;
    }

}
