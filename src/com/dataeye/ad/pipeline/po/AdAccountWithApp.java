package com.dataeye.ad.pipeline.po;

/**
 * 针对非媒体帐号应用
 * 
 * @author sam.xie
 * @date Mar 6, 2017 4:47:00 PM
 * @version 1.0
 */
public class AdAccountWithApp extends AdAccount {

    /**
     * ADT平台自增ID
     */
    private int appId;

    private String appName;

    /**
     * 平台内部的应用编号
     */
    private String appCode;

    public int getAppId() {
        return appId;
    }

    public void setAppId(int appId) {
        this.appId = appId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    @Override
    public String toString() {
        return "[company:" + getCompanyId() + ", appId:" + getAppId() + ", appName:" + getAppName() + ", id:" + getId()
                + ", account:" + getAccount() + "]";
    }
}
