package com.dataeye.ad.util;

import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.GzipDecompressingEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.common.ServerException;

public class HttpUtil {
    private static PoolingHttpClientConnectionManager connectionManager = null;

    private static final Object lock = new Object();

    private static CloseableHttpClient httpClient = null;

    private static boolean viaProxy = false;

    static {
        // 注册http和https协议支持
        Registry<ConnectionSocketFactory> connSocketFactoryRegistry = RegistryBuilder
                .<ConnectionSocketFactory>create().register("http", PlainConnectionSocketFactory.INSTANCE)
                .register("https", SSLConnectionSocketFactory.getSocketFactory()).build();
        // 初始化池管理器
        connectionManager = new PoolingHttpClientConnectionManager(connSocketFactoryRegistry);
        // 验证失效连接
        connectionManager.setValidateAfterInactivity(1000);
        // 连接池最大连接数
        connectionManager.setMaxTotal(200);
        // 连接池最大并发数
        connectionManager.setDefaultMaxPerRoute(20);
        if (StringUtil.convertInt(ServerUtil.getConfigProperties().getProperty("via_proxy"), 0) > 0) {
            viaProxy = true;
        }
    }

    /**
     * 默认的httpClient 支持http和https两种协议
     *
     * @return
     */
    public static CloseableHttpClient getHttpClient() {
        if (httpClient == null) {
            synchronized (lock) {
                if (httpClient == null) {
                    // 客户端配置
                    RequestConfig defaultConfig = RequestConfig.custom().setCookieSpec(CookieSpecs.IGNORE_COOKIES)
                            // 设置从连接池获取Connection 超时时间
                            .setConnectionRequestTimeout(5000)
                            // 设置自动重定向
                            .setRedirectsEnabled(false)
                            // 设置连接超时时间
                            .setConnectTimeout(5000)
                            // 请求获取数据的超时时间
                            .setSocketTimeout(30000).build();
                    if (viaProxy) {
                        // 代理连接
                        String proxyHost = ServerUtil.getConfigProperties().getProperty("proxy_host", "39.108.226.112");
                        int proxyPort = StringUtil.convertInt(ServerUtil.getConfigProperties()
                                .getProperty("proxy_port"), 9078);
                        String proxyUser = ServerUtil.getConfigProperties().getProperty("proxy_user", "de");
                        String proxyPwd = ServerUtil.getConfigProperties().getProperty("proxy_pwd", "de@2017@de");
                        HttpHost proxy = new HttpHost(proxyHost, proxyPort);
                        // 代理认证
                        CredentialsProvider credsProvider = new BasicCredentialsProvider();
                        credsProvider.setCredentials(new AuthScope(proxy.getHostName(), proxy.getPort()),
                                new UsernamePasswordCredentials(proxyUser, proxyPwd));
                        DefaultProxyRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxy);
                        httpClient = HttpClients.custom().setConnectionManager(connectionManager)
                                .setDefaultRequestConfig(defaultConfig).setRoutePlanner(routePlanner)
                                .setDefaultCredentialsProvider(credsProvider).build();
                    } else {
                        // 直接连接
                        httpClient = HttpClients.custom().setConnectionManager(connectionManager)
                                .setDefaultRequestConfig(defaultConfig).build();
                    }
                    return httpClient;
                }
            }
        }
        return httpClient;
    }

    public static String getContent(HttpEntity entity) {
        if (entity == null) {
            return null;
        }
        String content = null;
        try {
            Header contentEncoding = entity.getContentEncoding();
            if (null != contentEncoding && contentEncoding.getValue().toLowerCase().contains("gzip")) {
                content = EntityUtils.toString(new GzipDecompressingEntity(entity), "utf-8");
            } else {
                content = EntityUtils.toString(entity, "utf-8");
            }
        } catch (Exception e) {
            ExceptionHandler.throwException(ExceptionEnum.REQUEST_ERROR, ServerUtil.printStackTrace(e));
        } finally {
            // 确保数据流关闭，否则HttpClient将不能被后续请求使用
            try {
                entity.getContent().close();
            } catch (IllegalStateException | IOException e1) {
                e1.printStackTrace();
            }
        }
        return content;
    }

    /**
     * 根据参数构建完整url
     *
     * @param url
     * @param vars
     * @return
     */
    public static String urlParamReplace(String url, String... vars) {
        for (int i = 0; i < vars.length; i++) {
            url = url.replace("{" + i + "}", vars[i]);
        }
        return url;
    }

    /**
     * url转码
     *
     * @param url
     * @return
     */
    public static String urlEncode(String url) {
        try {
            return URLEncoder.encode(url, "utf-8");
        } catch (Exception e) {
            return null;
        }
    }

    public static String urlDecode(String url) {
        try {
            return URLDecoder.decode(url, "utf-8");
        } catch (Exception e) {
            return null;
        }
    }

    public static String get(String url) throws Exception {
        try {
            CloseableHttpClient httpClient = getHttpClient();
            HttpGet httpGet = new HttpGet(url);
            CloseableHttpResponse response = httpClient.execute(httpGet);
            HttpEntity httpEntity = response.getEntity();
            if (httpEntity != null) {
                byte[] bytes = EntityUtils.toByteArray(httpEntity);
                return new String(bytes, "utf-8");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String post(String url, Map<String, String> para) throws ServerException, IOException {
        CloseableHttpResponse httpResponse = null;
        try {
            CloseableHttpClient httpClient = getHttpClient();
            HttpPost httpPost = new HttpPost(url);
            List<NameValuePair> params = new ArrayList<>();
            if (para != null && para.size() > 0)
                for (Map.Entry<String, String> entry : para.entrySet()) {
                    params.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
                }
            // Post请求
            // 设置参数
            httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            // 发送请求
            httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {
                byte[] bytes = EntityUtils.toByteArray(httpEntity);
                return new String(bytes, "utf-8");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            httpResponse.close();
        }
        return null;
    }

    private static String getContentByGet(boolean isHttps, String url, Map<String, String> headerMap) throws Exception {
        CloseableHttpClient httpClient = getHttpClient();
        HttpGet httpGet = new HttpGet(url);
        setHeader(httpGet, headerMap);
        CloseableHttpResponse response = httpClient.execute(httpGet);
        return handleResponse(response);
    }

    /**
     * <pre>
     * 提交方式：x-www-form-urlencoded
     * @author sam.xie
     * @date Apr 24, 2017 4:32:41 PM
     * @param
     * @param url
     * @param headerMap
     * @param params
     * @return
     * @throws Exception
     */
    private static String getContentByPost(boolean isHttps, String url, Map<String, String> headerMap,
                                           List<NameValuePair> params) throws Exception {
        CloseableHttpClient httpClient = getHttpClient();
        HttpPost httpPost = new HttpPost(url);
        setHeader(httpPost, headerMap);
        httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
        CloseableHttpResponse response = httpClient.execute(httpPost);
        return handleResponse(response);

    }

    /**
     * <pre>
     * 提交方式：application/json
     * @author sam.xie
     * @date Apr 24, 2017 4:32:59 PM
     * @param
     * @param url
     * @param headerMap
     * @param params
     * @return
     * @throws Exception
     */
    private static String getContentByPost(boolean isHttps, String url, Map<String, String> headerMap, String params)
            throws Exception {
        CloseableHttpClient httpClient = getHttpClient();
        HttpPost httpPost = new HttpPost(url);
        setHeader(httpPost, headerMap);
        if (!StringUtil.isEmpty(params)) {
            httpPost.setEntity(new StringEntity(params, "UTF-8"));
        }
        CloseableHttpResponse httpResponse = httpClient.execute(httpPost);
        return handleResponse(httpResponse);

    }

    private static String getContentByPut(boolean isHttps, String url, Map<String, String> headerMap,
                                          List<NameValuePair> params) throws Exception {
        CloseableHttpClient httpClient = getHttpClient();
        HttpPut httpPut = new HttpPut(url);
        setHeader(httpPut, headerMap);
        httpPut.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
        CloseableHttpResponse response = httpClient.execute(httpPut);
        return handleResponse(response);

    }

    public static String getContentByGet(String url, Map<String, String> headerMap) throws Exception {
        return getContentByGet(false, url, headerMap);
    }

    public static String getContentByHttpsGet(String url, Map<String, String> headerMap) throws Exception {
        return getContentByGet(true, url, headerMap);
    }

    public static String getContentByPost(String url, Map<String, String> headerMap, List<NameValuePair> params)
            throws Exception {
        return getContentByPost(false, url, headerMap, params);
    }

    public static String getContentByHttpsPost(String url, Map<String, String> headerMap, List<NameValuePair> params)
            throws Exception {
        return getContentByPost(true, url, headerMap, params);
    }

    public static String getContentByPost(String url, Map<String, String> headerMap, String params) throws Exception {
        return getContentByPost(false, url, headerMap, params);
    }

    public static String getContentByHttpsPost(String url, Map<String, String> headerMap, String params)
            throws Exception {
        return getContentByPost(true, url, headerMap, params);
    }

    public static String getContentByPut(String url, Map<String, String> headerMap, List<NameValuePair> params)
            throws Exception {
        return getContentByPut(false, url, headerMap, params);
    }

    public static String getContentByHttpsPut(String url, Map<String, String> headerMap, List<NameValuePair> params)
            throws Exception {
        return getContentByPut(true, url, headerMap, params);
    }

    private static String handleResponse(CloseableHttpResponse response) throws Exception {
        try {
            int statusCode = response.getStatusLine().getStatusCode();
            String content = getContent(response.getEntity());
            if (statusCode != 200) {
                switch (statusCode) {
                    case 302:// 302跳转一般是因为登录失效跳转到登录页
                        ExceptionHandler.throwCookieException(ExceptionEnum.REDIRECT_302,
                                response.getFirstHeader("Location").toString());
                        break;
                    case 401:// 401未鉴权，使用ajax异步请求cookie失效
                        ExceptionHandler.throwCookieException(ExceptionEnum.REDIRECT_401, "[" + response.getStatusLine()
                                + "]:" + content);
                        break;
                    default:
                        ExceptionHandler.throwException(ExceptionEnum.SERVER_ERROR, "[" + response.getStatusLine() + "]:"
                                + content);
                }
            }
            return content;
        } finally {
            response.close();
        }
    }

    private static void setHeader(HttpRequestBase request, Map<String, String> headerMap) {
        for (Map.Entry<String, String> entry : headerMap.entrySet()) {
            request.setHeader(entry.getKey(), entry.getValue());
        }
    }

    public static void main(String[] args) {
        String url = "http://tui.qq.com/misapi/adgroup/select?filter=[{\"field\":\"ad_group_name\",\"operator\":\"CONTAINS\",\"value\":\"\"},{\"field\":\"status\",\"operator\":\"EQUALS\",\"value\":\"ADGROUP_STATUS_UN_DELETED\"},{\"field\":\"price_mode\",\"operator\":\"EQUALS\",\"value\":\"\"},{\"field\":\"product_type\",\"operator\":\"EQUALS\",\"value\":\"\"},{\"field\":\"begin_date\",\"operator\":\"EQUALS\",\"value\":\"2017-02-23\"},{\"field\":\"end_date\",\"operator\":\"EQUALS\",\"value\":\"9999-12-31\"},{\"field\":\"data_begin_date\",\"operator\":\"EQUALS\",\"value\":\"2017-02-23\"},{\"field\":\"data_end_date\",\"operator\":\"EQUALS\",\"value\":\"2017-02-23\"},{\"field\":\"is_filter_data\",\"operator\":\"EQUALS\",\"value\":\"\"}]&page=1&page_size=50&advertiser_id=10012572";
        System.out.println(urlEncode(url));

    }

}
