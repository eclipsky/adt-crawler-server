package com.dataeye.ad.cluster;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.Watcher.Event.EventType;
import org.apache.zookeeper.Watcher.Event.KeeperState;
import org.apache.zookeeper.ZooDefs.Ids;
import org.apache.zookeeper.ZooKeeper;
import org.slf4j.Logger;

import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;
import com.dataeye.util.log.DELogger;

public class ClusterNode {

    private static Logger logger = DELogger.getLogger("zookeeper");
    private static ZooKeeper zookeeper;
    private static CountDownLatch connectedSemaphore;
    private static String CONNECT_STRING = "10.1.2.240:2181,10.1.2.241:2181,10.1.2.242:2181";
    private static int SESSION_TIMEOUT = 6000;
    private static NodeInfo nodeInfo;
    private static List<String> nodes;
    private static String nodesPath;
    private static int mod;
    private static Watcher watcher;

    static {
        initNodeConfig();
        initZookeeper();
    }

    private static void initNodeConfig() {
        Properties props = ServerUtil.getConfig("cluster.properties");
        watcher = new NodeWatcher();
        nodesPath = props.getProperty("crawler.nodes.zookeeper.path", "/adt/crawler/nodes");
        String serverIdStr = props.getProperty("crawler.server.id");
        int serverId = StringUtil.convertInt(serverIdStr, 0);
        String serverName = props.getProperty("crawler.server.name");
        nodeInfo = new NodeInfo(serverId, serverName);
        nodes = new ArrayList<String>();
        nodes.add(serverIdStr);
    }

    public synchronized static void register() throws Exception {
        // register watcher
        getZK().getChildren(nodesPath, watcher);
        String path = nodesPath + "/" + nodeInfo.getId();
        String data = StringUtil.gson.toJson(nodeInfo);
        logger.info("register ephemeral node -> " + path);
        getZK().create(path, data.getBytes(), Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL);
    }

    private static void rebalance() throws Exception {
        // get all nodes
        nodes.clear();
        nodes.addAll(getZK().getChildren(nodesPath, watcher));
        // sort node, get position
        mod = getPosition(nodes, nodeInfo);
        logger.info("all nodes:" + nodes + ", size: " + nodes.size() + ", current node: " + nodeInfo.getId()
                + ", handle mod: " + mod + " (mod = medium_account_id % size)");
    }

    private static int getPosition(List<String> nodes, NodeInfo nodeInfo) {
        int pos = 0;
        int id = nodeInfo.getId();
        for (String node : nodes) {
            int nodeId = Integer.parseInt(node);
            if (nodeId < id) {
                pos++;
            }
        }
        return pos;
    }

    public static int getMod() {
        return mod;
    }

    public static List<String> getNodes() {
        return nodes;
    }

    public static synchronized void initZookeeper() {
        if (zookeeper == null) {
            connectedSemaphore = new CountDownLatch(1);
            try {
                Properties props = ServerUtil.getConfig("cluster.properties");
                String connectString = props.getProperty("zookeeper.connect", CONNECT_STRING);
                String sessionTimeoutStr = props.getProperty("zookeeper.session.timeout.ms");
                int sessionTimeout = StringUtil.convertInt(sessionTimeoutStr, SESSION_TIMEOUT);
                zookeeper = new ZooKeeper(connectString, sessionTimeout, watcher);
                connectedSemaphore.await(30, TimeUnit.SECONDS);
            } catch (Exception e) {
                logger.error("get zkClient error", e);
            } finally {
                connectedSemaphore = null;
            }
        }
    }

    public static synchronized ZooKeeper getZK() {
        if (zookeeper == null) {
            initZookeeper();
        }
        /*
         * while (!zookeeper.getState().isConnected()) { // 未连接，循环等待 }
         */
        return zookeeper;
    }

    public static synchronized void closeZooKeeper() {
        if (null != zookeeper) {
            try {
                zookeeper.close();
                zookeeper = null;
                connectedSemaphore = null;
            } catch (InterruptedException e) {
                logger.error("close zkClient error", e);
            }
        }
    }

    static class NodeWatcher implements Watcher {
        @Override
        public void process(WatchedEvent event) {
            logger.info(event.toString());
            if (event.getState() == KeeperState.SyncConnected) {
                if (connectedSemaphore != null) {
                    connectedSemaphore.countDown();
                }
                if (event.getType() == EventType.NodeChildrenChanged) {
                    if (nodesPath.equals(event.getPath())) {
                        logger.info("child nodes changed, prepared for rebalancing...");
                        try {
                            rebalance();
                        } catch (Exception e) {
                            logger.error("rebalance failed! ", e);
                        }
                    }
                }
            }
            if (event.getState() == KeeperState.Expired) {
                // session过期，重建
                logger.info("session expired. now rebuilding...");
                closeZooKeeper();
                initZookeeper();
                // 重建完，需要重新注册节点
                try {
                    register();
                } catch (Exception e) {
                    logger.error("register failed! " + e);
                }
            }
        }

        public static void main(String[] args) throws Exception {
            ClusterNode.register();
            Thread.currentThread().join();
        }
    }
}
