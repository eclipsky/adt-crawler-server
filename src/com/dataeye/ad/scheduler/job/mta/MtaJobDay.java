package com.dataeye.ad.scheduler.job.mta;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.util.DateUtil;

public class MtaJobDay implements Job {
	private static ExecutorService executor = Executors.newFixedThreadPool(2);

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		executor.submit(new MtaJob(DateUtil.unixTimestamp000() - Constant.ONEDAY));
	}

}
