package com.dataeye.ad.scheduler.job.baidusearch.model;

import java.util.List;

public class BaiduModel {

	private int totalCount;

	private int pageNo;

	private int pageSize;

	private List<BaiduData> DATA;

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public List<BaiduData> getDATA() {
		return DATA;
	}

	public void setDATA(List<BaiduData> dATA) {
		DATA = dATA;
	}
}
