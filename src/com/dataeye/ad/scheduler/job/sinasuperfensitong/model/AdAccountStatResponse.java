package com.dataeye.ad.scheduler.job.sinasuperfensitong.model;

public class AdAccountStatResponse {
	private int code;
	private AdAccountStatResult result;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public AdAccountStatResult getResult() {
		return result;
	}
	public void setResult(AdAccountStatResult result) {
		this.result = result;
	}
	
	

}
