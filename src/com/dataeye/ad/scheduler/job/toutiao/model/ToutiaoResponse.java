package com.dataeye.ad.scheduler.job.toutiao.model;

@Deprecated
public class ToutiaoResponse {

	private String status;

	private ToutiaoData data;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ToutiaoData getData() {
		return data;
	}

	public void setData(ToutiaoData data) {
		this.data = data;
	}

}
