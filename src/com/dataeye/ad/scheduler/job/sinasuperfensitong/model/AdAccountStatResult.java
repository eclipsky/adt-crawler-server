package com.dataeye.ad.scheduler.job.sinasuperfensitong.model;

public class AdAccountStatResult {
	private Float realUse;  //今日实时消耗
	private Float accountBalance;  //账户余额
	public Float getRealUse() {
		return realUse;
	}
	public void setRealUse(Float realUse) {
		this.realUse = realUse;
	}
	public Float getAccountBalance() {
		return accountBalance;
	}
	public void setAccountBalance(Float accountBalance) {
		this.accountBalance = accountBalance;
	}
	
	

}
