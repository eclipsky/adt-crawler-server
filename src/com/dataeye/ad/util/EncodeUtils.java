package com.dataeye.ad.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang.StringUtils;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * <pre/>
 * 编码工具类 
 * 1.将byte[]转为各种进制的字符串
 * 2.base 64 encode
 * 3.base 64 decode
 * 4.获取byte[]的md5值
 * 5.获取字符串md5值
 * 6.结合base64实现md5加密 7.AES加密
 * 8.AES加密为base 64 code
 * 9.AES解密
 * 10.将base 64 code AES解密
 * 11.md编码
 * 
 * @author louis 2017/02/07
 */
public class EncodeUtils {

    public static String DEFAULT_KEY = "SJ2DFIOWE234JL39";

    private static MessageDigest md = null;

    static {
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (Exception e) {

        }
    }

    public static void main(String[] args) throws Exception {
        String content = "abc123";
        System.out.println("加密前：" + content);

        System.out.println("AES加解密密钥：" + DEFAULT_KEY);

        String encrypt = aesEncrypt(content, DEFAULT_KEY);
        System.out.println("AES加密后：" + encrypt);

        String decrypt = aesDecrypt(encrypt, DEFAULT_KEY);
        System.out.println("AES解密后：" + decrypt);
        System.out.println(getMD5Str("wrlefl8Jw51hw4HCtcOgQcOJwobCrsO%2FWB7DnMKSw5pHw5LDjcOtw7MwalJqIwjDvxBwNTx2w6w3w4wow6Y2RMOdwpHDlwjDnwLDskw4woYowoM0wpzCgcOJw67CjcKXA8KOw7dvwqTDrDlDUsKjFQx9w6bCtsOLGcO8IVvCtyAvBsOyRUQRwqYOe8KSGMOYRQ00fMONdn%2FCgULDpFQkwplocQ%3D%3DO8LfqksG"));
    }

    public static String getMD5Str(String src) {
        // 使用指定的字节数组更新摘要。
        md.update(src.getBytes());
        // 通过执行诸如填充之类的最终操作完成哈希计算。
        byte b[] = md.digest();
        // 生成具体的md5密码到buf数组
        int i;
        StringBuffer buf = new StringBuffer("");
        for (int offset = 0; offset < b.length; offset++) {
            i = b[offset];
            if (i < 0)
                i += 256;
            if (i < 16)
                buf.append("0");
            buf.append(Integer.toHexString(i));
        }
        // System.out.println("32位: " + buf.toString());// 32位的加密
        // System.out.println("16位: " + buf.toString().substring(8, 24));// 16位的加密，其实就是32位加密后的截取
        return new String(buf.toString());
    }

    /**
     * 将byte[]转为各种进制的字符串
     * 
     * @param bytes
     *            byte[]
     * @param radix
     *            可以转换进制的范围，从Character.MIN_RADIX到Character.MAX_RADIX，超出范围后变为10进制
     * @return 转换后的字符串
     */
    public static String binary(byte[] bytes, int radix) {
        return new BigInteger(1, bytes).toString(radix);// 这里的1代表正数
    }

    /**
     * base 64 encode
     * 
     * @param bytes
     *            待编码的byte[]
     * @return 编码后的base 64 code
     */
    public static String base64Encode(byte[] bytes) {
        return new BASE64Encoder().encode(bytes);
    }

    /**
     * base 64 decode
     * 
     * @param base64Code
     *            待解码的base 64 code
     * @return 解码后的byte[]
     * @throws Exception
     */
    public static byte[] base64Decode(String base64Code) throws Exception {
        return StringUtils.isEmpty(base64Code) ? null : new BASE64Decoder().decodeBuffer(base64Code);
    }

    /**
     * AES加密
     * 
     * @param content
     *            待加密的内容
     * @param encryptKey
     *            加密密钥
     * @return 加密后的byte[]
     * @throws Exception
     */
    public static byte[] aesEncryptToBytes(String content, String encryptKey) throws Exception {
        // 被注释代码的方式，在window下可正常加密，linux下每次结果都会变，不满足需要，需要设置为ECB加密模式，同时指定随机数生成规则
        // KeyGenerator kgen = KeyGenerator.getInstance("AES");
        // kgen.init(128, new SecureRandom(encryptKey.getBytes()));
        //
        // Cipher cipher = Cipher.getInstance("AES");
        // cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(kgen.generateKey().getEncoded(), "AES"));
        //
        // return cipher.doFinal(content.getBytes("utf-8"));

        KeyGenerator kgen = KeyGenerator.getInstance("AES");

        // 需手动指定 SecureRandom 随机数生成规则
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        random.setSeed(encryptKey.getBytes());
        kgen.init(128, random);

        // 创建密码器（采用ECB加密模式）
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        byte[] byteContent = content.getBytes("utf-8");
        cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(kgen.generateKey().getEncoded(), "AES"));
        return cipher.doFinal(byteContent);
    }

    /**
     * AES加密为base 64 code
     * 
     * @param content
     *            待加密的内容
     * @param encryptKey
     *            加密密钥
     * @return 加密后的base 64 code
     * @throws Exception
     */
    public static String aesEncrypt(String content, String encryptKey) throws Exception {
        return base64Encode(aesEncryptToBytes(content, encryptKey));
    }

    /**
     * AES解密
     * 
     * @param encryptBytes
     *            待解密的byte[]
     * @param decryptKey
     *            解密密钥
     * @return 解密后的String
     * @throws Exception
     */
    public static String aesDecryptByBytes(byte[] encryptBytes, String decryptKey) throws Exception {
        // 被注释代码的方式，在window下可正常加密，linux下每次结果都会变，不满足需要，需要设置为ECB加密模式，同时指定随机数生成规则
        // KeyGenerator kgen = KeyGenerator.getInstance("AES");
        // kgen.init(128, new SecureRandom(decryptKey.getBytes()));
        //
        // Cipher cipher = Cipher.getInstance("AES");
        // cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(kgen.generateKey().getEncoded(), "AES"));
        // byte[] decryptBytes = cipher.doFinal(encryptBytes);

        KeyGenerator kgen = KeyGenerator.getInstance("AES");

        // 需手动指定 SecureRandom 随机数生成规则
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        random.setSeed(decryptKey.getBytes());
        kgen.init(128, random);

        // 创建密码器（采用ECB加密模式）
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(kgen.generateKey().getEncoded(), "AES"));
        byte[] result = cipher.doFinal(encryptBytes);
        return new String(result);
    }

    /**
     * 将base 64 code AES解密
     * 
     * @param encryptStr
     *            待解密的base 64 code
     * @param decryptKey
     *            解密密钥
     * @return 解密后的string
     * @throws Exception
     */
    public static String aesDecrypt(String encryptStr, String decryptKey) throws Exception {
        return StringUtils.isEmpty(encryptStr) ? null : aesDecryptByBytes(base64Decode(encryptStr), decryptKey);
    }

}