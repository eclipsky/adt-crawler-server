package com.dataeye.ad.constant;

public class PageModel {

	private int pageId;

	private int pageSize;

	private int totalNum;

	/**
	 * 总页数
	 */
	private int totalPages;

	public PageModel() {

	}

	public PageModel(int pageId, int pageSize) {
		this.pageId = pageId;
		this.pageSize = pageSize;
	}

	public int getPageId() {
		return pageId;
	}

	public void setPageId(int pageId) {
		this.pageId = pageId;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotalNum() {
		return totalNum;
	}

	public void setTotalNum(int totalNum) {
		this.totalNum = totalNum;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}
}
