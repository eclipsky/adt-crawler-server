package com.dataeye.ad.scheduler.job.toutiao.model;

public class ToutiaoPagination {

	private boolean has_more;

	private int total_count;

	private int page_count;

	private int limit;

	private int display_pagination_count;

	private int offset;

	private int page;

	private boolean use_offset;

	public boolean isHas_more() {
		return has_more;
	}

	public void setHas_more(boolean has_more) {
		this.has_more = has_more;
	}

	public int getTotal_count() {
		return total_count;
	}

	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}

	public int getPage_count() {
		return page_count;
	}

	public void setPage_count(int page_count) {
		this.page_count = page_count;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getDisplay_pagination_count() {
		return display_pagination_count;
	}

	public void setDisplay_pagination_count(int display_pagination_count) {
		this.display_pagination_count = display_pagination_count;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public boolean isUse_offset() {
		return use_offset;
	}

	public void setUse_offset(boolean use_offset) {
		this.use_offset = use_offset;
	}

}
