package com.dataeye.ad.scheduler.job.sinasuperfensitong.model;

public class SinaWeiboPastResponse {
	
	private int code;
	private SinaWeiboPastResult result;
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public SinaWeiboPastResult getResult() {
		return result;
	}
	public void setResult(SinaWeiboPastResult result) {
		this.result = result;
	}
	
	

}
