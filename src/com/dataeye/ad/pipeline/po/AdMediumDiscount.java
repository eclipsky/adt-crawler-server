package com.dataeye.ad.pipeline.po;

import java.math.BigDecimal;

/**
 * 媒体折扣率
 * 
 * @author sam.xie
 * @date Mar 6, 2017 8:58:37 PM
 * @version 1.0
 */
public class AdMediumDiscount extends AdBase {

	private BigDecimal discountRate;

	public BigDecimal getDiscountRate() {
		return discountRate;
	}

	public void setDiscountRate(BigDecimal discountRate) {
		this.discountRate = discountRate;
	}

}
