package com.dataeye.ad.cache;

import java.util.concurrent.atomic.AtomicInteger;

public class AccessObject {

	/** 访问key */
	private String accessKey;

	/** 首次访问时间 */
	private int createTime;

	/** 总共访问次数 */
	private AtomicInteger accessTimes = new AtomicInteger(0);

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	public int getCreateTime() {
		return createTime;
	}

	public void setCreateTime(int createTime) {
		this.createTime = createTime;
	}

	public int getAccessTimes() {
		return accessTimes.get();
	}

	public void setAccessTimes(int times) { // 每次访问自增一次
		this.accessTimes.set(times);
	}

	public void incrementAccessTimes() { // 每次访问自增一次
		this.accessTimes.incrementAndGet();
	}

}
