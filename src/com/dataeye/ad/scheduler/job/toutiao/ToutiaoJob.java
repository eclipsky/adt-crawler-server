package com.dataeye.ad.scheduler.job.toutiao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dataeye.ad.scheduler.job.CrawlerFilter;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.slf4j.Logger;

import com.dataeye.ad.common.ServerException;
import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.constant.Constant.CrawlerPlatform;
import com.dataeye.ad.constant.Constant.PlanSwitch;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.scheduler.job.toutiao.model.ToutiaoAccountResponse;
import com.dataeye.ad.util.CookieUtil;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;

public class ToutiaoJob extends CrawlerBaseJob {

    private final static CrawlerPlatform platform = CrawlerPlatform.TOUTIAO_AD;

    private final static Logger logger = ServerUtil.getLogger(platform);

    private final static String planUrl = "https://ad.toutiao.com/statistics/data/ad_stat/?page={0}&st={1}&et={2}&landing_type=0&status=no_delete&pricing=0&search_type=2&keyword=&sort_stat=&sort_order=1&_=1488685159931";

    private final static String accountUrl = "https://ad.toutiao.com/overture/data/advertiser_info/?_=1491895735501";

    public ToutiaoJob() {
        this(DateUtil.unixTimestamp000());
    }

    public ToutiaoJob(long time) {
        super(time, platform);
    }

    public ToutiaoJob(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }

    public void planSpider(long time, AdMediumAccount account) throws Exception {
        String statDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        // 这里需要特别处理下，对比部分Cookie，模拟请求会导致400错误，经过测试，只需要Cookie部分属性就可以了，为何浏览器却可以？
        String cookie = CookieUtil.cleanToutiaoCookie(account.getCookie());
        int pageId = 1;
        int pageSize = 20;// 这个参数不可变
        PageModel pageModel = new PageModel(1, pageSize);
        // 请求
        String content = HttpUtil.getContentByHttpsGet(
                HttpUtil.urlParamReplace(planUrl, pageId + "", statDate, statDate), getHeaderMap(cookie));
        logger.info(content);
        // 解析
        List<AdPlanStat> planList = parse(content, account, time, pageModel);
        // 入库
        DataProcessor.processPlanDay(planList);
        // 分页
        int totalNum = pageModel.getTotalNum();
        totalNum -= pageSize;
        while (totalNum > 0) {
            pageId += 1;
            // 请求
            content = HttpUtil.getContentByHttpsGet(HttpUtil.urlParamReplace(planUrl, pageId + "", statDate, statDate),
                    getHeaderMap(cookie));
            logger.info(content);
            // 解析
            planList = parse(content, account, time, pageModel);
            // 入库
            DataProcessor.processPlanDay(planList);
            totalNum -= pageSize;
        }
    }

    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = CookieUtil.cleanToutiaoCookie(account.getCookie());
        // 请求
        String content = HttpUtil.getContentByHttpsGet(accountUrl, getHeaderMap(cookie));
        logger.info(content);
        // 解析
        AdAccountStat accountStat = parseAccount(content, account, time);
        // 入库
        DataProcessor.processAccountDay(accountStat);
    }

    /**
     * <pre>
     * 解析响应内容，封装成标准的PO对象
     * @author sam.xie
     * @date Feb 15, 2017 3:12:29 PM
     * @param time
     * @throws ServerException
     */
    private List<AdPlanStat> parse(String content, AdMediumAccount account, long time, PageModel pageModel)
            throws ServerException {
        List<AdPlanStat> planList = new ArrayList<AdPlanStat>();
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        String status = resultJson.get("status").getAsString();
        if (!"success".equals(status)) {
            // {"status": "fail", "msg": "\u7cfb\u7edf\u7e41\u5fd9"} -> {"status": "fail", "msg": "系统繁忙"}
            // update by sam at 2017.06.16，系统繁忙直接跳过，避免频繁的告警邮件
            // ExceptionHandler.throwException(ExceptionEnum.SERVER_ERROR, StringUtil.convertUnicode2String(content));
            return planList;
        }
        JsonObject dataJson = resultJson.get("data").getAsJsonObject();
        if (null == dataJson) {
            return planList;
        }
        //获取分页信息
        JsonObject tableJson = dataJson.get("table").getAsJsonObject();
        if (null == tableJson) {
            return planList;
        }
        int totalNum = tableJson.get("pagination").getAsJsonObject().get("total_count").getAsInt();
        pageModel.setTotalNum(totalNum);
        //获取数据
        JsonArray listJson = tableJson.get("ad_data").getAsJsonArray();
        if (listJson != null && listJson.size() > 0) {
            for (JsonElement item : listJson) {
                JsonObject order = item.getAsJsonObject();
                String planName = order.get("ad_name").getAsString();
                String mPlanId = order.get("ad_id").getAsString();
                // 媒体计划分组ID
                String groupId = order.get("campaign_id").getAsString();
                // 媒体计划分组名
                String groupName = order.get("campaign_name").getAsString();
                // 媒体计划出价
                int planBid = StringUtil.convertBigDecimal(order.get("bid").getAsString(), 100, 0).intValue();
                // 媒体计划出价策略
                String planBidStrategy = order.get("pricing").getAsString();
                // 媒体计划状态
                String planStatus = order.get("status1").getAsString();
                // OCPC出价时的阶段
                String phase = StringUtil.getJsonStrValue(order, "phase");
                // 媒体计划开关
                int planSwitchRaw = StringUtil.convertInt(order.get("status").getAsString(), 0);
                int planSwitch = planSwitchRaw == 0 ? PlanSwitch.ON : PlanSwitch.OFF; // 0表示开启，1表示关闭
                //消耗点击曝光
                JsonObject statJson = order.get("stat_data").getAsJsonObject();
                int cost = StringUtil.convertBigDecimal(statJson.get("stat_cost").getAsString(), 100, 0).intValue();
                int showNum = StringUtil.convertInt(statJson.get("show").getAsString(), 0);
                int clickNum = StringUtil.convertInt(statJson.get("click").getAsString(), 0);
                AdPlanStat plan = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(), account.getId());
                planList.add(plan);
                plan.setmPlanId(mPlanId);
                plan.setPlanName(planName);
                plan.setCost(cost);
                plan.setShowNum(showNum);
                plan.setClickNum(clickNum);
                plan.setBid(planBid);
                plan.setPlanBidStrategy(planBidStrategy);
                plan.setGroupId(groupId);
                plan.setGroupName(groupName);
                plan.setPlanStatus(planStatus);
                plan.setPlanSwitch(planSwitch);
                plan.setPlanExtend(phase);
            }
        }

        return planList;
    }

    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time) throws Exception {
        ToutiaoAccountResponse response = Constant.GSON.fromJson(content, ToutiaoAccountResponse.class);
        String status = response.getStatus();
        if (!"success".equals(status)) {
            // {"status": "fail", "msg": "\u7cfb\u7edf\u7e41\u5fd9"} -> {"status": "fail", "msg": "系统繁忙"}
            // update by sam at 2017.07.22，系统繁忙直接跳过，避免频繁的告警邮件
            // ExceptionHandler.throwException(ExceptionEnum.SERVER_ERROR, StringUtil.convertUnicode2String(content));
            return null;
        }
        int balance = StringUtil.convertDecimal(response.getData().getBalance(), 0f).multiply(BigDecimal.valueOf(100))
                .intValue();
        int costToday = StringUtil.convertDecimal(response.getData().getToday_cost(), 0f)
                .multiply(BigDecimal.valueOf(100)).intValue();
        AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                account.getId());
        accountStat.setBalance(balance);
        accountStat.setCostToday(costToday);
        return accountStat;
    }

    private Map<String, String> getHeaderMap(String cookie) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Cookie", cookie);
        headerMap.put("Host", "ad.toutiao.com");
        headerMap.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0");
        headerMap.put("Accept", "application/json, text/javascript, */*; q=0.01");
        headerMap.put("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
        headerMap.put("Accept-Encoding", "gzip, deflate");
        headerMap.put("Referer", "https://ad.toutiao.com/overture/data/advertiser/ad/?page=1&keyword=");
        headerMap.put("X-Requested-With", "XMLHttpRequest");
        headerMap.put("Connection", "keep-alive");
        return headerMap;
    }
}
