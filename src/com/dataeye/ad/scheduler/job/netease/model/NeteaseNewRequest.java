package com.dataeye.ad.scheduler.job.netease.model;

/**
 * 网易易效新平台请求参数
 * Created by luzhuyou 2017/04/14
 */
public class NeteaseNewRequest {
    private String name;
    private String ad_plan_id;
    private int dateType;
    private String start_date;
    private String end_date;
    private ExtLimit extLimit;
    private int dataStatus;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAd_plan_id() {
        return ad_plan_id;
    }

    public void setAd_plan_id(String ad_plan_id) {
        this.ad_plan_id = ad_plan_id;
    }

    public int getDateType() {
        return dateType;
    }

    public void setDateType(int dateType) {
        this.dateType = dateType;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public ExtLimit getExtLimit() {
        return extLimit;
    }

    public void setExtLimit(ExtLimit extLimit) {
        this.extLimit = extLimit;
    }

    public int getDataStatus() {
        return dataStatus;
    }

    public void setDataStatus(int dataStatus) {
        this.dataStatus = dataStatus;
    }

    @Override
    public String toString() {
        return "NeteaseNewRequest{" +
                "name='" + name + '\'' +
                ", ad_plan_id='" + ad_plan_id + '\'' +
                ", dateType=" + dateType +
                ", start_date='" + start_date + '\'' +
                ", end_date='" + end_date + '\'' +
                ", extLimit=" + extLimit +
                ", dataStatus=" + dataStatus +
                '}';
    }
}

