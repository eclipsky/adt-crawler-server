package com.dataeye.ad.scheduler.job.autohome.model;

/**
 * Created by VitAs on 2017/6/15.
 */
public class AutoHomeModel {

    //客户id
    private int customerId;

    //代理公司id
    private int accountId;

    //公司状态 1-开启
    private int status;

    //当前余额
    private Integer balance;

    public AutoHomeModel(){}

    public AutoHomeModel(int customerId,int accountId,int status,int balance){
        this.customerId=customerId;
        this.accountId=accountId;
        this.status=status;
        this.balance=balance;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }


    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

}
