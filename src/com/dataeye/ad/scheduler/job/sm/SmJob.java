package com.dataeye.ad.scheduler.job.sm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.scheduler.job.CrawlerFilter;
import com.dataeye.ad.scheduler.job.sm.model.BizParam;
import com.dataeye.ad.scheduler.job.sm.model.FilterMapJson;
import com.dataeye.ad.scheduler.job.sm.model.SmResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;

import com.dataeye.ad.common.CookieException;
import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.common.ServerException;
import com.dataeye.ad.constant.Constant.CrawlerPlatform;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.util.CookieUtil;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * 神马搜索爬取任务 Created by huangzehai on 2017/3/28.
 */
public class SmJob extends CrawlerBaseJob {

    private final static CrawlerPlatform platform = CrawlerPlatform.SM_SEARCH;

    private final static Logger logger = ServerUtil.getLogger(platform);

    // 推广单元
    private static final String planUrl = "https://e.sm.cn/wolongweb/unit/listCustomized?uid={0}&context={1}";

    // 推广计划
    private static final String groupUrl = "https://e.sm.cn/wolongweb/plan/listCustomized?uid={0}&context={1}";


    private final static String accountUrl = "https://e.sm.cn/uc/userDetail?uid={0}";

    //推广计划参数
    private final static int groupLevel = 2;

    //推广单元参数
    private final static int planLevel = 3;


    public SmJob() {
        this(DateUtil.unixTimestamp000());
    }

    public SmJob(long time) {
        super(time, platform);
    }

    public SmJob(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }


    /**
     * 推广单元
     *
     * @param time
     * @param account
     * @throws Exception
     */
    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        String cookie = account.getCookie();
        String uid = CookieUtil.getSmUid(cookie, account);
        if (StringUtils.isBlank(uid)) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_NOT_EXISTS, "uid is blank");
        }
        // 获取计划总数
        int page = 1;
        int pageSize = 100;
        PageModel pageModel = new PageModel(page, pageSize);
        //构建参数
        BizParam bizParam = new BizParam(new FilterMapJson(), uid, planLevel, reportDate, reportDate, page, pageSize);
        SmResponse smResponse = new SmResponse(bizParam);
        String resJson = new Gson().toJson(smResponse);
        String resJsonEncode = HttpUtil.urlEncode(resJson);
        // 请求
        String url = HttpUtil.urlParamReplace(planUrl, uid, resJsonEncode);
        String content = HttpUtil.getContentByHttpsGet(url, getHeaderMap(cookie));
        logger.info(content);
        // 解析
        List<AdPlanStat> poList = parse(content, account, time, pageModel);
        // 入库
        if (!poList.isEmpty()) {
            DataProcessor.processPlanDay(poList);
        }
        int totalNum = pageModel.getTotalNum();
        totalNum -= pageSize;
        while (totalNum > 0) {
            page++;
            bizParam.setReqPageIndex(page);
            smResponse.setBizParam(bizParam);
            resJsonEncode = HttpUtil.urlEncode(new Gson().toJson(smResponse));
            url = HttpUtil.urlParamReplace(planUrl, uid, resJsonEncode);
            // 请求
            content = HttpUtil.getContentByHttpsGet(url, getHeaderMap(cookie));
            logger.info(content);
            // 解析
            poList = parse(content, account, time, pageModel);
            // 入库
            if (!poList.isEmpty()) {
                DataProcessor.processPlanDay(poList);
                totalNum -= pageSize;
            }
        }
    }

    /**
     * 推广计划
     *
     * @param time
     * @param account
     * @throws Exception
     */
    @Override
    public void planGroupSpider(long time, AdMediumAccount account) throws Exception {
        String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        String cookie = account.getCookie();
        String uid = CookieUtil.getSmUid(cookie, account);
        if (StringUtils.isBlank(uid)) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_NOT_EXISTS, "uid is blank");
        }
        // 获取计划总数
        int page = 1;
        int pageSize = 100;
        PageModel pageModel = new PageModel(page, pageSize);
        //构建参数
        BizParam bizParam = new BizParam(new FilterMapJson(), uid, groupLevel, reportDate, reportDate, page, pageSize);
        SmResponse smResponse = new SmResponse(bizParam);
        String resJson = new Gson().toJson(smResponse);
        String resJsonEncode = HttpUtil.urlEncode(resJson);
        // 请求
        String url = HttpUtil.urlParamReplace(planUrl, uid, resJsonEncode);
        String content = HttpUtil.getContentByHttpsGet(url, getHeaderMap(cookie));
        logger.info(content);
        // 解析
        List<AdPlanStat> poList = parseGroup(content, account, time, pageModel);
        // 入库
        if (!poList.isEmpty()) {
            DataProcessor.processPlanDay(poList);
        }
        int totalNum = pageModel.getTotalNum();
        totalNum -= pageSize;
        while (totalNum > 0) {
            page++;
            bizParam.setReqPageIndex(page);
            smResponse.setBizParam(bizParam);
            resJsonEncode = HttpUtil.urlEncode(new Gson().toJson(smResponse));
            url = HttpUtil.urlParamReplace(planUrl, uid, resJsonEncode);
            // 请求
            content = HttpUtil.getContentByHttpsGet(url, getHeaderMap(cookie));
            logger.info(content);
            // 解析
            poList = parseGroup(content, account, time, pageModel);
            // 入库
            if (!poList.isEmpty()) {
                DataProcessor.processPlanDay(poList);
                totalNum -= pageSize;
            }
        }
    }

    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        String uid = CookieUtil.getSmUid(cookie, account);
        if (StringUtils.isBlank(uid)) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_NOT_EXISTS, "uid is blank");
        }
        // 请求
        String content = HttpUtil.getContentByHttpsGet(HttpUtil.urlParamReplace(accountUrl, uid), getHeaderMap(cookie));
        logger.info(content);
        // 解析
        AdAccountStat accountStat = parseAccount(content, account, time);
        // 入库
        DataProcessor.processAccountDay(accountStat);
    }

    /**
     * <pre>
     * 解析推广单元，封装成标准的PO对象
     * @author sam.xie
     * @date Feb 15, 2017 3:12:29 PM
     * @param time
     * @throws ServerException
     */
    private List<AdPlanStat> parse(String response, AdMediumAccount account, long time, PageModel pageModel)
            throws ServerException, CookieException {
        List<AdPlanStat> poList = new ArrayList<>();
        JsonObject resultJson = StringUtil.jsonParser.parse(response).getAsJsonObject();
        int status = resultJson.get("status").getAsInt();
        if (status != 0) {
            if (status == 919) {
                // 919:未登录，重定向到登录页面.
                ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, response);
            } else {
                ExceptionHandler.throwException(ExceptionEnum.REQUEST_ERROR, response);
            }
        } else {
            // 获取分页信息
            JsonObject conf = resultJson.get("data").getAsJsonObject().getAsJsonObject("queryCondition")
                    .getAsJsonObject();
            int totalNum = conf.get("totalSize").getAsInt();
            int pageSize = conf.get("pageSize").getAsInt();
            int totalPage = conf.get("totalPage").getAsInt();
            pageModel.setTotalNum(totalNum);
            pageModel.setPageSize(pageSize);
            pageModel.setTotalPages(totalPage);
            pageModel.setTotalPages(totalPage);
            pageModel.setTotalPages(totalPage);

            // 获取分页内容
            JsonArray list = resultJson.get("data").getAsJsonObject().get("target").getAsJsonArray();
            if (list != null) {
                for (JsonElement item : list) {
                    JsonObject order = item.getAsJsonObject();
                    // 点击数
                    int clickNum = StringUtil.getJsonIntValue(order, "clickNum");
                    // 消耗
                    double cost = StringUtil.getJsonDoubleValue(order, "consume");
                    // 曝光数
                    int showNum = StringUtil.getJsonIntValue(order, "showNum");
                    // 计划（sm的单元对应adt的计划）
                    String planName = order.get("unitName").getAsString();
                    String mPlanId = order.get("unitId").getAsString();

                    // 分组（sm的计划对应adt的分组）
                    String groupName = order.get("planName").getAsString();
                    String groupId = order.get("planId").getAsString();

                    //出价
                    double bid = StringUtil.getJsonDoubleValue(order, "bid");

                    //状态 2- 启用 0- 暂停
                    int planStatus = StringUtil.getJsonIntValue(order, "frontState");

                    //开关
                    Integer planSwitch = null;
                    int switchNum = StringUtil.getJsonIntValue(order, "isPaused");
                    if (switchNum == 0) {
                        planSwitch = Constant.PlanSwitch.ON;
                    } else if (switchNum == 1) {
                        planSwitch = Constant.PlanSwitch.OFF;
                    }


                    AdPlanStat reportPO = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(),
                            account.getId());
                    poList.add(reportPO);
                    reportPO.setClickNum(clickNum);
                    // 单位由元转化为分
                    reportPO.setCost(new BigDecimal(cost).multiply(new BigDecimal(100))
                            .setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
                    reportPO.setShowNum(showNum);
                    reportPO.setPlanName(planName);
                    reportPO.setmPlanId(mPlanId);
                    reportPO.setGroupName(groupName);
                    reportPO.setGroupId(groupId);
                    reportPO.setBid(new BigDecimal(bid).multiply(new BigDecimal(100))
                            .setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
                    reportPO.setPlanStatus(planStatus + "");
                    reportPO.setPlanSwitch(planSwitch);

                }
            }
        }
        return poList;
    }


    /**
     * 推广计划解析
     *
     * @param response
     * @param account
     * @param time
     * @param pageModel
     * @return
     * @throws ServerException
     * @throws CookieException
     */
    private List<AdPlanStat> parseGroup(String response, AdMediumAccount account, long time, PageModel pageModel)
            throws ServerException, CookieException {
        List<AdPlanStat> poList = new ArrayList<>();
        JsonObject resultJson = StringUtil.jsonParser.parse(response).getAsJsonObject();
        int status = resultJson.get("status").getAsInt();
        if (status != 0) {
            if (status == 919) {
                // 919:未登录，重定向到登录页面.
                ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, response);
            } else {
                ExceptionHandler.throwException(ExceptionEnum.REQUEST_ERROR, response);
            }
        } else {
            // 获取分页信息
            JsonObject conf = resultJson.get("data").getAsJsonObject().getAsJsonObject("queryCondition")
                    .getAsJsonObject();
            int totalNum = conf.get("totalSize").getAsInt();
            int pageSize = conf.get("pageSize").getAsInt();
            int totalPage = conf.get("totalPage").getAsInt();
            pageModel.setTotalNum(totalNum);
            pageModel.setPageSize(pageSize);
            pageModel.setTotalPages(totalPage);
            pageModel.setTotalPages(totalPage);
            pageModel.setTotalPages(totalPage);

            // 获取分页内容
            JsonArray list = resultJson.get("data").getAsJsonObject().get("target").getAsJsonArray();
            if (list != null) {
                for (JsonElement item : list) {
                    JsonObject order = item.getAsJsonObject();
                    // 点击数
                    int clickNum = StringUtil.getJsonIntValue(order, "clickNum");
                    // 消耗
                    double cost = StringUtil.getJsonDoubleValue(order, "consume");
                    // 曝光数
                    int showNum = StringUtil.getJsonIntValue(order, "showNum");
                    // 计划（sm的单元对应adt的计划）
                    String planName = order.get("planName").getAsString();
                    String mPlanId = order.get("planId").getAsString();

                    //状态 4- 启用 0- 暂停
                    int planStatus = StringUtil.getJsonIntValue(order, "frontState");

                    //开关
                    Integer planSwitch = null;
                    int switchNum = StringUtil.getJsonIntValue(order, "isPaused");
                    if (switchNum == 0) {
                        planSwitch = Constant.PlanSwitch.ON;
                    } else if (switchNum == 1) {
                        planSwitch = Constant.PlanSwitch.OFF;
                    }

                    AdPlanStat reportPO = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(),
                            account.getId());
                    poList.add(reportPO);
                    reportPO.setClickNum(clickNum);
                    // 单位由元转化为分
                    reportPO.setCost(new BigDecimal(cost).multiply(new BigDecimal(100))
                            .setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
                    reportPO.setShowNum(showNum);
                    reportPO.setPlanName(planName);
                    reportPO.setmPlanId(mPlanId);
                    reportPO.setPlanStatus(planStatus + "");
                    reportPO.setPlanSwitch(planSwitch);

                }
            }
        }
        return poList;
    }


    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time) throws Exception {
        JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
        if (content.contains("ssoRedirect")) { // sso重定向
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE);
        }
        int balance = StringUtil.convertBigDecimal(
                resultJson.get("data").getAsJsonObject().get("balanceStr").getAsString(), 100, 0).intValue();
        AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                account.getId());
        accountStat.setBalance(balance);
        return accountStat;
    }


    private Map<String, String> getHeaderMap(String cookie) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Cookie", cookie);
        headerMap.put("Host", "e.sm.cn");
        headerMap.put("Referer", "https://e.sm.cn/cpc/static/index.html?uid=16659203");
        headerMap.put("X-Requested-With", "XMLHttpRequest");
        headerMap.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0");
        headerMap.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        headerMap.put("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
        headerMap.put("Accept-Encoding", "gzip, deflate");
        headerMap.put("Connection", "keep-alive");
        headerMap.put("Upgrade-Insecure-Requests", "1");
        headerMap.put("Content-Type", "application/x-www-form-urlencoded");
        return headerMap;
    }

}
