package com.dataeye.ad.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.GzipDecompressingEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.slf4j.Logger;

import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.util.log.DELogger;
import com.google.gson.JsonObject;

public class QQUtil {

	private final static Logger logger = DELogger.getLogger("qq_util");

	/**
	 * <pre>
	 * 通过skey计算出g_tk
	 * 用于广点通爬虫
	 * https://seofangfa.com/other-note/qzone-g_tk.html
	 * @author sam.xie
	 * @date Feb 18, 2017 3:19:17 PM
	 * @param skey
	 * @return
	 */
	public static String getGTK(String cookies) {
		String skey = getSkeyFromCookie(cookies);
		int hash = 5381;
		for (int i = 0, len = skey.length(); i < len; ++i) {
			hash += (hash << 5) + (int) (char) skey.charAt(i);
		}
		return (hash & 0x7fffffff) + "";
	}

	public static String getOwner(AdMediumAccount account, String cookie, String g_tk) throws Exception {
		String owner = account.getSkey();
		if (StringUtil.isEmpty(owner)) {
			// 调用接口，获取登录信息
			String url = "http://e.qq.com/ec/loginfo.php?callback=jQuery112205364835465155953_1488781514329&g_tk={0}&_=1488781514330";
			String content = requestLoginfo(HttpUtil.urlParamReplace(url, g_tk), cookie);
			/**
			 * document.domain='qq.com';jQuery112208928726562651388_1493026143958({"ret":0,"msg":"","data":{"uid":2120639,"notice":"","userlist":[2120639],"loginlist":[{"uid":3865235,"name":"深圳市佳游科技有限公司","role":"广告主","url":"http:\/\/e.qq.com\/atlas\/3865235\/","isagency":0}],"isagency":0,"loguid":2442944883,"uname":"深圳市佳游科技有限公司","isgdtuser":1,"status":1,"targettype":25,"homepage":"http:\/\/e.qq.com\/atlas\/3865235\/","isdev":0,"aduid":3865235}});
			 * document.domain='qq.com';jQuery112208329930291483143_1493026436335({"ret":0,"msg":"","data":{"uid":1393341,"notice":"","userlist":[1393341],"loginlist":[{"uid":1393341,"name":"上海草花互动网络科技有限公司","role":"广告主","url":"http:\/\/e.qq.com\/atlas\/1393341\/","isagency":0}],"isagency":0,"loguid":2910679793,"uname":"上海草花互动网络科技有限公司","isgdtuser":1,"status":1,"targettype":24,"homepage":"http:\/\/e.qq.com\/atlas\/","isdev":0,"aduid":1393341}});
			 * 这里存在uid，aduid，一般情况下两者一致，但是以aduid为准！
			 */
			String result = content.substring(content.indexOf("({") + 1, content.indexOf("})") + 1);
			JsonObject resultJson = StringUtil.jsonParser.parse(result).getAsJsonObject();
			int ret = resultJson.get("ret").getAsInt();
			if (ret == 0) {
				owner = String.valueOf(resultJson.get("data").getAsJsonObject().get("aduid").getAsInt());
			} else {
	            // {"ret":4512,"msg":"反CSRF验证失败"}
	            // {"ret":4501,"msg":"请先登录"}
	            // {"ret":4904,"msg":"登录态过期"}
	            if (ret == 4512 || ret == 4501 || ret == 4904) {
	                ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, "获取owner失败：" + result);
	            } else {
	                // {"ret":4003,"msg":"抱歉，不支持查询90天以前的数据","data":""}
	                // {"ret":4903,"msg":"登录态校验失败，请稍后重试"}
	                ExceptionHandler.throwException(ExceptionEnum.REQUEST_ERROR, "获取owner失败：" + result);
	            }
			}
			// 保存skey
			if (!StringUtil.isEmpty(owner)) {
				CookieUtil.updateSkey(owner, account);
			}
		}
		return owner;
	}

	private static String requestLoginfo(String url, String cookie) throws Exception {
		HttpClient httpClient = HttpUtil.getHttpClient();
		// 1. 获取计划数量
		HttpGet httpGet = new HttpGet(url);
		// 设置头字段
		setHeader(httpGet, cookie);
		// 发送请求
		HttpResponse response = httpClient.execute(httpGet);
		// 检查状态
		int statusCode = response.getStatusLine().getStatusCode();
		Header contentEncoding = response.getLastHeader("Content-Encoding");
		HttpEntity entity = response.getEntity();
		String content = null;
		// 之前使用gzip可以成功解压缩，结果后来无法识别，这里做兼容
		if (null != contentEncoding && contentEncoding.getValue().contains("gzip")) {
			content = HttpUtil.getContent(new GzipDecompressingEntity(entity));
		} else {
			content = HttpUtil.getContent(entity);
		}
		logger.info(content);
		if (statusCode != 200) {
			ExceptionHandler.throwException(ExceptionEnum.COOKIE_EXPIRE);
		}
		return content;
	}

	public static Map<String, String> getCookieMap(String cookies) {
		Map<String, String> cookieMap = new HashMap<String, String>();
		String[] cookieArr = cookies.split(";");
		for (String cookie : cookieArr) {
			String[] kv = cookie.split("=");
			String key = kv[0].trim();
			String value = "";
			if (kv.length == 2) {
				value = kv[1].trim();
			}
			cookieMap.put(key, value);
		}
		return cookieMap;
	}

	public static String getSkeyFromCookie(String cookies) {
		String[] cookieArr = cookies.split(";");
		for (String cookie : cookieArr) {
			if (cookie.contains("skey=")) {
				String[] kv = cookie.split("=");
				String value = kv[1].trim();
				return value;
			}
		}
		return null;
	}

	public static String getLoginIdInTuiQQ(String cookie) {
		return getCookieMap(cookie).get("login_id");
	}

	private static void setHeader(HttpRequestBase request, String cookie) {
		request.setHeader("Cookie", cookie);
		request.setHeader("Host", "e.qq.com");
		request.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0");
		request.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		request.setHeader("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
		request.setHeader("Accept-Encoding", "gzip, deflate");
		request.setHeader("Connection", "keep-alive");
		request.setHeader("Upgrade-Insecure-Requests", "1");
		request.setHeader("Content-Type", "application/x-www-form-urlencoded");
	}

	public static void main(String[] args) {
		String content = "document.domain='qq.com';jQuery112202943331780298203_1489483860205({\"ret\":0,\"msg\":\"\",\"data\":{\"uid\":162952,\"notice\":\"\",\"userlist\":[162952],\"loginlist\":[{\"uid\":162952,\"name\":\"深圳市佳游科技有限公司\",\"role\":\"广告主\",\"url\":\"http://e.qq.com/atlas/162952/\",\"isagency\":0}],\"isagency\":0,\"loguid\":2055460477,\"uname\":\"深圳市佳游科技有限公司\",\"isgdtuser\":1,\"status\":1,\"targettype\":25,\"homepage\":\"http://e.qq.com/atlas/\",\"isdev\":0,\"aduid\":162952}});";
		System.out.println(content.substring(content.indexOf("({") + 1, content.indexOf("})")));
		System.out
				.println(getGTK("pgv_pvi=2419362816; pgv_si=s8903574528; pgv_info=ssid=s5556120864; pgv_pvid=6582228115; pt2gguin=o2055460477; ptisp=ctc; RK=2emXV9rbXN; ptcz=cb5b5ee661ef6e3000d2ad3b772651d541adc1e990cae1c2bc480955a690a418; mtaH5CloseMenuId=#; gdt_refer=e.qq.com; gdt_full_refer=http%3A%2F%2Fe.qq.com%2F; gdt_original_refer=e.qq.com; gdt_original_full_refer=http%3A%2F%2Fe.qq.com%2F; dm_cookie=version=new&log_type=internal_click&ssid=s5556120864&pvid=6582228115&qq=2055460477&loadtime=2486&url=http%3A%2F%2Fe.qq.com%2Fads&gdt_refer=e.qq.com&gdt_full_refer=http%3A%2F%2Fe.qq.com%2F&gdt_original_refer=e.qq.com&gdt_original_full_refer=http%3A%2F%2Fe.qq.com%2F&gdt_from=&uid=162952&hottag=atlas&hottagtype=header; site_type=new; hottag=atlas; hottagtype=header; portalversion=new; ptui_loginuin=2055460477; atlas_platform=atlas; webwx_data_ticket=gSenQj5wKPKoFkqYQvgp8rfu; uin=o2055460477; skey=@AXVouZxBn"));
	}
}
