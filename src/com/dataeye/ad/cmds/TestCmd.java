package com.dataeye.ad.cmds;

import org.springframework.stereotype.Service;

import com.dataeye.ad.util.StringUtil;
import com.xunlei.netty.httpserver.cmd.BaseCmd;
import com.xunlei.netty.httpserver.cmd.CmdMapper;
import com.xunlei.netty.httpserver.component.XLHttpRequest;
import com.xunlei.netty.httpserver.component.XLHttpResponse;

@Service
public class TestCmd extends BaseCmd {

	@CmdMapper("/upload")
	public Object restorePinyou(XLHttpRequest request, XLHttpResponse response) {
		String sign = request.getParameter("sign");
		request.getContent();
		String myfile = request.getParameter("myfile");
		return "success";
	}
	
	public static void main(String[] args){
		String num = "66.45";
		System.out.println(StringUtil.convertBigDecimal(num, 0).movePointRight(2).intValue());
		System.out.println(StringUtil.convertFloat(num, 0) * 100);
	}
}
