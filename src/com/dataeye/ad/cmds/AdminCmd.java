package com.dataeye.ad.cmds;

import org.quartz.SchedulerException;
import org.springframework.stereotype.Service;

import com.dataeye.ad.scheduler.QuartzManager;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;
import com.xunlei.netty.httpserver.cmd.BaseCmd;
import com.xunlei.netty.httpserver.cmd.CmdMapper;
import com.xunlei.netty.httpserver.component.XLHttpRequest;
import com.xunlei.netty.httpserver.component.XLHttpResponse;

@Service
public class AdminCmd extends BaseCmd {

    @CmdMapper("/quartz/start")
    public Object start(XLHttpRequest request, XLHttpResponse response) {
        try {
            QuartzManager.start();
        } catch (SchedulerException e) {
            e.printStackTrace();
            return "fail!\n" + ServerUtil.printStackTrace(e);
        }
        return "success";
    }

    @CmdMapper("/quartz/standby")
    public Object standby(XLHttpRequest request, XLHttpResponse response) {
        try {
            QuartzManager.standby();
        } catch (SchedulerException e) {
            e.printStackTrace();
            return "fail!\n" + ServerUtil.printStackTrace(e);
        }
        return "success";
    }

    @CmdMapper("/quartz/pause")
    public Object pause(XLHttpRequest request, XLHttpResponse response) {
        try {
            QuartzManager.pause();
        } catch (SchedulerException e) {
            e.printStackTrace();
            return "fail!\n" + ServerUtil.printStackTrace(e);
        }
        return "success";
    }

    @CmdMapper("/quartz/resume")
    public Object resume(XLHttpRequest request, XLHttpResponse response) {
        try {
            QuartzManager.resume();
        } catch (SchedulerException e) {
            e.printStackTrace();
            return "fail!\n" + ServerUtil.printStackTrace(e);
        }
        return "success";
    }

    public static void main(String[] args) {
        String num = "66.45";
        System.out.println(StringUtil.convertBigDecimal(num, 0).movePointRight(2).intValue());
        System.out.println(StringUtil.convertFloat(num, 0) * 100);
    }

}
