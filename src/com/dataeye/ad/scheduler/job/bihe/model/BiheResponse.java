package com.dataeye.ad.scheduler.job.bihe.model;

import java.util.List;

public class BiheResponse {

	private String page;

	private String totalCount;

	private List<BiheData> result;

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(String totalCount) {
		this.totalCount = totalCount;
	}

	public List<BiheData> getResult() {
		return result;
	}

	public void setResult(List<BiheData> result) {
		this.result = result;
	}

}
