package com.dataeye.ad.scheduler.job.weixin;

import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.scheduler.job.CrawlerBaseExecutor;
import com.dataeye.ad.util.DateUtil;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * @author lingliqi
 * @date 2017-12-26 15:46
 */
public class WeixinJobDay implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        CrawlerBaseExecutor.addJob(new WeixinJob(DateUtil.unixTimestamp000() - Constant.ONEDAY));
    }
}
