package com.dataeye.ad.scheduler.job.sinafensitong.service;

import com.dataeye.ad.common.CookieException;
import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.common.ServerException;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.StringUtil;
import com.dataeye.util.log.DELogger;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by huangzehai on 2017/3/28.
 */
@Deprecated
public class PlanServiceImpl implements PlanService {
    private final static Logger logger = DELogger.getLogger("group_service_log");
    private static final String URL = "http://bp.biz.weibo.com/bid/groupajax/orderlist";

    /**
     * 获取计划ID列表
     *
     * @param groupId
     * @return
     */
    @Override
    public List<Integer> getPlanIds(String cookie, Integer groupId) throws IOException, CookieException, ServerException {
        List<Integer> planIds = new ArrayList<>();
        int page = 1;
        int pageSize = 5;
        PageModel pageModel = new PageModel(page, pageSize);
        String response = request(URL, cookie, formData(groupId, page, pageSize));
        List<Integer> planIdsOfOnePage = parseResponse(response, pageModel);
        planIds.addAll(planIdsOfOnePage);
        page = 2;
        while (page <= pageModel.getTotalPages()) {
            response = request(URL, cookie, formData(groupId, page, pageSize));
            planIdsOfOnePage = parseResponse(response, pageModel);
            planIds.addAll(planIdsOfOnePage);
            page++;
        }
        return planIds;
    }

    /**
     * 发起HTTP请求
     *
     * @param url
     * @param cookie
     * @param params
     * @return
     * @throws IOException
     * @throws CookieException
     */
    private String request(String url, String cookie, List<NameValuePair> params) throws IOException, CookieException {
        HttpClient httpClient = HttpUtil.getHttpClient();
        // 1. 获取计划数量
        HttpPost httpPost = new HttpPost(url);
        // 设置头字段
        setHeader(httpPost, cookie);
        // 设置表单参数
        httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
        // 发送请求
        HttpResponse response = httpClient.execute(httpPost);
        // 检查状态
        int statusCode = response.getStatusLine().getStatusCode();
        String content = HttpUtil.getContent(response.getEntity());
        logger.info(content);
        if (statusCode != 200) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE);
        }
        return content;
    }

    /**
     * 设置请求表单数据
     *
     * @return
     */
    public List<NameValuePair> formData(Integer groupId, int page, int pageSize) {
        List<NameValuePair> pairs = new ArrayList<>();
        pairs.add(new BasicNameValuePair("groupId", String.valueOf(groupId)));
        pairs.add(new BasicNameValuePair("page", String.valueOf(page)));
        pairs.add(new BasicNameValuePair("pageSize", String.valueOf(pageSize)));
        pairs.add(new BasicNameValuePair("orderName", ""));
        pairs.add(new BasicNameValuePair("orderStart", ""));
        pairs.add(new BasicNameValuePair("orderEnd", ""));
        pairs.add(new BasicNameValuePair("orderType", "-1"));
        pairs.add(new BasicNameValuePair("orderStatus", "-1"));
        return pairs;
    }

    /**
     * 设置请求头
     *
     * @param request
     * @param cookie
     */
    private void setHeader(HttpRequestBase request, String cookie) {
        request.setHeader("Cookie", cookie);
        request.setHeader("Host", "bp.biz.weibo.com");
        request.setHeader("Origin", "bp.biz.weibo.com");
        request.setHeader("Referer", "http://bp.biz.weibo.com/bid/group");
        request.setHeader("Authorization", "Basic ZGF0YWV5ZTpkaWdpdGN1YmU=");
        request.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36");
        request.setHeader("X-Requested-With", "XMLHttpRequest");
        request.setHeader("Accept", "application/json, text/javascript, */*; q=0.01");
        request.setHeader("Accept-Language", "zh-CN,zh;q=0.8");
        request.setHeader("Accept-Encoding", "gzip, deflate");
        request.setHeader("Connection", "keep-alive");
//        request.setHeader("Content-Length", "103");
        request.setHeader("Upgrade-Insecure-Requests", "1");
        request.setHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    }

    /**
     * 解析返回值
     *
     * @param response
     * @param pageModel
     * @return
     * @throws ServerException
     */
    private List<Integer> parseResponse(String response, PageModel pageModel)
            throws ServerException {
        List<Integer> groupIds = new ArrayList<>();
        // 返回结果集中类型不统一，同一个字段可能为字符串也可能为对象还可能为数字
        JsonObject jsonObj = StringUtil.jsonParser.parse(response).getAsJsonObject();
        int status = jsonObj.get("status").getAsInt();
        if (status == 0) {
            //提取分页信息
            JsonObject data = jsonObj.get("data").getAsJsonObject();
            pageModel.setTotalNum(data.get("total").getAsInt());
            pageModel.setPageSize(data.get("pageSize").getAsInt());
            pageModel.setTotalPages(data.get("totalPages").getAsInt());

            JsonArray groups = jsonObj.get("data").getAsJsonObject().get("result").getAsJsonArray();
            if (groups != null) {
                for (JsonElement item : groups) {
                    JsonObject group = item.getAsJsonObject();
                    int id = StringUtil.getJsonIntValue(group, "id");
                    if (id > 0) {
                        groupIds.add(id);
                    }
                }
            }
        } else {
            ExceptionHandler.throwException(ExceptionEnum.REQUEST_ERROR, response);
        }
        return groupIds;
    }
}
