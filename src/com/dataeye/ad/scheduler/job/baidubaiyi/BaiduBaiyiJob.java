package com.dataeye.ad.scheduler.job.baidubaiyi;

import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.constant.Constant.*;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.scheduler.job.CrawlerFilter;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.slf4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by VitAs on 2017/8/7.
 */
public class BaiduBaiyiJob extends CrawlerBaseJob {

    private final static CrawlerPlatform platform = CrawlerPlatform.BAIDU_BAIYI;

    private final static Logger logger = ServerUtil.getLogger(platform);

    private final static String accountUrl = "http://baiyi.baidu.com/user/account.json?order=desc&orderBy=date&" +
            "page=0&pageSize=20&from={0}&to={1}&_=1502096672082";

    private final static String planUrl = "http://baiyi.baidu.com/user/listGroupData.json?planId=&order=asc&orderBy=status&" +
            "page={0}&pageSize={1}&from={2}&to={3}&keyType=7&_=1502096457372";


    public BaiduBaiyiJob() {
        this(DateUtil.unixTimestamp000());
    }

    public BaiduBaiyiJob(long time) {
        super(time, platform);
    }

    public BaiduBaiyiJob(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }

    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        String cookie = account.getCookie();
        // 请求
        String url = HttpUtil.urlParamReplace(accountUrl, reportDate, reportDate);
        String content = HttpUtil.getContentByGet(url, getHeaderMap(cookie));
        logger.info(content);
        // 解析
        AdAccountStat accountStat = parseAccount(content, account, time);
        // 入库
        DataProcessor.processAccountDay(accountStat);

    }

    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        String cookie = account.getCookie();
        int page = 0;
        int pageSize = 100;
        PageModel pageModel = new PageModel(page, pageSize);
        // 请求
        String url = HttpUtil.urlParamReplace(planUrl, page + "", pageSize + "", reportDate, reportDate);
        String content = HttpUtil.getContentByGet(url, getHeaderMap(cookie));
        logger.info(content);
        // 解析
        List<AdPlanStat> planList = parsePlan(content, account, time, pageModel);
        // 入库
        DataProcessor.processPlanDay(planList);
        int totalNum = pageModel.getTotalNum();
        totalNum -= pageSize;
        while (totalNum > 0) {
            page++;
            // 请求
            url = HttpUtil.urlParamReplace(planUrl, page + "", pageSize + "", reportDate, reportDate);
            content = HttpUtil.getContentByGet(url, getHeaderMap(cookie));
            logger.info(content);
            // 解析
            planList = parsePlan(content, account, time, pageModel);
            // 入库
            DataProcessor.processPlanDay(planList);
            totalNum -= pageSize;
        }

    }

    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time)
            throws Exception {
        JsonObject jsonResult = StringUtil.jsonParser.parse(content).getAsJsonObject();
        JsonElement status = jsonResult.get("success");
        if (null != status && status.getAsBoolean() != true) {
            ExceptionHandler.throwException(ExceptionEnum.SERVER_ERROR, content);
        }
        AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                account.getId());
        //页面上有百度推广的总余额和DSP移动专属余额
        //在这里 先取DSP移动专属余额为所需的值
        //同时由于精度未知，默认以分为精度
        JsonObject jsonObject = jsonResult.get("model").getAsJsonObject();
        if (null != jsonObject) {
            int balance = StringUtil.convertInt(jsonObject.get("dspBalance").getAsString(), 0);
            accountStat.setBalance(balance);
        }
        return accountStat;
    }

    private List<AdPlanStat> parsePlan(String content, AdMediumAccount account, long time, PageModel pageModel)
            throws Exception {
        List<AdPlanStat> poList = new ArrayList<>();
        JsonObject jsonResult = StringUtil.jsonParser.parse(content).getAsJsonObject();
        JsonElement status = jsonResult.get("success");
        if (null != status && status.getAsBoolean() != true) {
            ExceptionHandler.throwException(ExceptionEnum.SERVER_ERROR, content);
        }
        JsonObject jsonData = jsonResult.get("model").getAsJsonObject().get("list").getAsJsonObject();
        int totalSize = StringUtil.convertInt(jsonData.get("totalSize").getAsString(), 0);
        pageModel.setTotalNum(totalSize);
        JsonArray listData = jsonData.get("data").getAsJsonArray();
        if (listData.size() > 0) {
            for (JsonElement item : listData) {
                AdPlanStat adPlanStat = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(), account.getId());
                poList.add(adPlanStat);
                JsonObject order = item.getAsJsonObject();
                int clickNum = StringUtil.getJsonIntValue(order, "clickNum");
                int showNum = StringUtil.getJsonIntValue(order, "showNum");
                //精度未知，默认以分为精度
                int cost = StringUtil.getJsonIntValue(order, "totalCost");
                //计划ID
                String mPlanId = order.get("id").getAsString();
                //计划名称
                String planName = order.get("name").getAsString();
                //分组ID
                String groupId = order.get("planId").getAsString();
                //分组名称
                String groupName = order.get("planName").getAsString();
                //计划出价
                double bidPrice = StringUtil.getJsonDoubleValue(order, "bid");
                //计划状态
                String planStatus = order.get("status").getAsString();
                adPlanStat.setmPlanId(mPlanId);
                adPlanStat.setPlanName(planName);
                adPlanStat.setGroupName(groupName);
                adPlanStat.setGroupId(groupId);
                adPlanStat.setBid(new BigDecimal(bidPrice).multiply(new BigDecimal(100))
                        .setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
                adPlanStat.setPlanStatus(planStatus);
                adPlanStat.setClickNum(clickNum);
                adPlanStat.setShowNum(showNum);
                adPlanStat.setCost(cost);

            }
        }
        return poList;

    }


    private Map<String, String> getHeaderMap(String cookie) {
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Accept", "application/json, text/javascript, */*; q=0.01");
        headerMap.put("Accept-Encoding", "gzip, deflate");
        headerMap.put("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.6,en;q=0.4");
        headerMap.put("Connection", "keep-alive");
        headerMap.put("Cookie", cookie);
        headerMap.put("Host", "baiyi.baidu.com");
        headerMap.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.86 Safari/537.36");
        headerMap.put("X-Requested-With", "XMLHttpRequest");
        return headerMap;

    }
}
