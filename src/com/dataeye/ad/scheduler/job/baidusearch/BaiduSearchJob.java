package com.dataeye.ad.scheduler.job.baidusearch;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.scheduler.job.CrawlerFilter;
import com.dataeye.ad.scheduler.job.baidusearch.model.*;
import com.google.gson.JsonArray;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;

import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.constant.Constant.CrawlerPlatform;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.util.CookieUtil;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class BaiduSearchJob extends CrawlerBaseJob {

    private final static CrawlerPlatform platform = CrawlerPlatform.BAIDU_SEARCH;

    private final static Logger logger = ServerUtil.getLogger(platform);

    private final static String planUrl = "http://fengchao.baidu.com/nirvana/request.ajax?path=vega/GET/mtllist/unit";

    private final static String groupUrl = "http://fengchao.baidu.com/nirvana/request.ajax?path=vega/GET/mtllist/plan";

    private final static String accountUrl = "https://tuiguang.baidu.com/request.ajax?path=GET/user/balance";

    public BaiduSearchJob() {
        this(DateUtil.unixTimestamp000());
    }

    public BaiduSearchJob(long time) {
        super(time, platform);
    }

    public BaiduSearchJob(long time, CrawlerFilter filter) {
        super(time, platform, filter);
    }

    @Override
    public void planSpider(long time, AdMediumAccount account) throws Exception {
        String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        String cookie = account.getCookie();
        String userId = CookieUtil.getBaiduUserId(cookie);
        String token = CookieUtil.getBaiduToken(cookie);
        int pageNum = 1;
        int pageSize = 100;
        PageModel pageModel = new PageModel(pageNum, pageSize);
        // 请求
        String content = HttpUtil.getContentByPost(planUrl, getHeaderMap(cookie, planUrl),
                getFormData(userId, token, reportDate, reportDate, pageNum, pageSize));
        logger.info(content);
        // 解析
        List<AdPlanStat> planList = parsePlan(content, account, time, pageModel);
        // 入库
        DataProcessor.processPlanDay(planList);
        int totalNum = pageModel.getTotalNum();
        totalNum -= pageSize;
        while (totalNum > 0) {
            pageNum++;
            // 请求
            content = HttpUtil.getContentByPost(planUrl, getHeaderMap(cookie, planUrl),
                    getFormData(userId, token, reportDate, reportDate, pageNum, pageSize));
            logger.info(content);
            // 解析
            planList = parsePlan(content, account, time, pageModel);
            // 入库
            DataProcessor.processPlanDay(planList);
            totalNum -= pageSize;
        }
    }

    @Override
    public void accountSpider(long time, AdMediumAccount account) throws Exception {
        String cookie = account.getCookie();
        String token = CookieUtil.getBaiduToken(cookie);
        String userId = CookieUtil.getBaiduUserId(cookie);
        String content = HttpUtil.getContentByHttpsPost(accountUrl, getHeaderMap(cookie, accountUrl),
                getAccountFormData(userId, token));
        logger.info(content);
        AdAccountStat accountStat = parseAccount(content, account, time);
        DataProcessor.processAccountDay(accountStat);
    }

    @Override
    public void planGroupSpider(long time, AdMediumAccount account) throws Exception {
        String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
        String cookie = account.getCookie();
        String userId = CookieUtil.getBaiduUserId(cookie);
        String token = CookieUtil.getBaiduToken(cookie);
        int pageNum = 1;
        int pageSize = 100;
        PageModel pageModel = new PageModel(pageNum, pageSize);
        // 请求
        String content = HttpUtil.getContentByPost(groupUrl, getHeaderMap(cookie, groupUrl),
                getGroupFormData(userId, token, reportDate, reportDate, pageNum, pageSize));
        logger.info(content);
        // 解析
        List<AdPlanStat> groupList = parseGroup(content, account, time, pageModel);
        // 入库
        DataProcessor.processPlanDay(groupList);
        int totalNum = pageModel.getTotalNum();
        totalNum -= pageSize;
        while (totalNum > 0) {
            pageNum++;
            // 请求
            content = HttpUtil.getContentByPost(groupUrl, getHeaderMap(cookie, groupUrl),
                    getGroupFormData(userId, token, reportDate, reportDate, pageNum, pageSize));
            logger.info(content);
            // 解析
            groupList = parseGroup(content, account, time, pageModel);
            // 入库
            DataProcessor.processPlanDay(groupList);
            totalNum -= pageSize;
        }

    }

    /**
     * <pre>
     * 获取表单列表
     * @author sam.xie
     * @date Mar 1, 2017 6:24:10 PM
     * @param startDate
     * @param endDate
     * @return
     */
    private List<NameValuePair> getPlanFormData(String userId, String token, String startDate, String endDate) {
        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("params", generateParams(startDate, endDate, userId)));
        pairs.add(new BasicNameValuePair("userid", userId));
        pairs.add(new BasicNameValuePair("token", token));
        pairs.add(new BasicNameValuePair("path", "pluto/GET/mars/reportdata"));
        // pairs.add(new BasicNameValuePair("eventId", "488355524907c144"));
        // pairs.add(new BasicNameValuePair("reqid", "4b534c46-f52d-47fd-655e-148835552490"));
        return pairs;
    }

    private List<NameValuePair> getFormData(String userId, String token, String startDate, String endDate, int pageNum, int pageSize) {
        List<NameValuePair> pairs = new ArrayList<>();
        pairs.add(new BasicNameValuePair("userid", userId));
        pairs.add(new BasicNameValuePair("token", token));
        pairs.add(new BasicNameValuePair("path", "vega/GET/mtllist/unit"));
        pairs.add(new BasicNameValuePair("params", getParams(userId, startDate, endDate, pageNum, pageSize)));
        return pairs;
    }

    private String getParams(String userId, String startDate, String endDate, int pageNum, int pageSize) {
        BaiduRequest baiduRequest = new BaiduRequest();
        LevelCond levelCond = new LevelCond(userId);
        baiduRequest.setStartTime(startDate);
        baiduRequest.setEndTime(endDate);
        baiduRequest.setLevelCond(levelCond);
        baiduRequest.setPageNo(pageNum);
        baiduRequest.setPageSize(pageSize);
        String resultJson = Constant.GSON.toJson(baiduRequest);
        return resultJson;
    }

    private List<NameValuePair> getGroupFormData(String userId, String token, String startDate, String endDate, int pageNum, int pageSize) {
        List<NameValuePair> pairs = new ArrayList<>();
        pairs.add(new BasicNameValuePair("userid", userId));
        pairs.add(new BasicNameValuePair("token", token));
        pairs.add(new BasicNameValuePair("path", "vega/GET/mtllist/plan"));
        pairs.add(new BasicNameValuePair("params", getGroupParams(userId, startDate, endDate, pageNum, pageSize)));
        return pairs;
    }

    private String getGroupParams(String userId, String startDate, String endDate, int pageNum, int pageSize) {
        BaiduGroupRequest baiduGroupRequest = new BaiduGroupRequest();
        LevelCond levelCond = new LevelCond(userId);
        baiduGroupRequest.setStartTime(startDate);
        baiduGroupRequest.setEndTime(endDate);
        baiduGroupRequest.setLevelCond(levelCond);
        baiduGroupRequest.setPageNo(pageNum);
        baiduGroupRequest.setPageSize(pageSize);
        String resultJson = Constant.GSON.toJson(baiduGroupRequest);
        return resultJson;
    }

    private List<NameValuePair> getAccountFormData(String userId, String token) {
        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("params", "{}"));
        pairs.add(new BasicNameValuePair("userid", userId));
        pairs.add(new BasicNameValuePair("token", token));
        pairs.add(new BasicNameValuePair("path", "GET/user/balance"));
        return pairs;
    }

    private String generateParams(String startDate, String endDate, String userId) {
        BaiduFormParam param = new BaiduFormParam();
        ReportInfo reportInfo = new ReportInfo();
        param.setReportinfo(reportInfo);
        param.setMaxRecordNum(0);
        param.setPageNo(1);
        param.setPageSize(120);
        reportInfo.setReportid(0);
        reportInfo.setStarttime(startDate);
        reportInfo.setEndtime(endDate);
        reportInfo.setIsrelativetime(0);
        reportInfo.setRelativetime(14);
        reportInfo.setMtldim(2);
        reportInfo.setIdset(Integer.parseInt(userId));
        reportInfo.setMtllevel(200);
        reportInfo.setReporttype(201);
        reportInfo.setPlatform(0);
        reportInfo.setSplitDim("");
        reportInfo
                .setDataitem("date,planinfo,useracct,shows,clks,paysum,clkrate,avgprice,trans,bridgetrans,phonetrans");
        reportInfo.setReporttag(0);
        reportInfo.setReportcycle(1);
        reportInfo.setTimedim(5);
        reportInfo.setFirstday(1);
        reportInfo.setIsmail(0);
        reportInfo.setMailaddr("");
        reportInfo.setSortlist("date DESC,planid ASC");
        reportInfo.setReportname("计划报告");
        reportInfo.setUserid(Integer.parseInt(userId));
        reportInfo.setReportlevel(100);
        reportInfo.setRgtag(null);
        reportInfo.setMtag(null);
        reportInfo.setTargetingType("0|6|7");
        reportInfo.setTotalCount(0);
        String result = Constant.GSON.toJson(param);
        return result;
    }

    /**
     * <pre>
     * 解析响应内容，封装成标准的PO对象
     * @author sam.xie
     * @date Feb 15, 2017 3:12:29 PM
     * @param time
     */
    private List<AdPlanStat> parse(String content, AdMediumAccount account, long time) {
        List<AdPlanStat> planList = new ArrayList<AdPlanStat>();
        if (StringUtil.isEmpty(content)) {
            return planList;
        }
        BaiduResponse model = Constant.GSON.fromJson(content, BaiduResponse.class);
        for (BaiduData item : model.getData().getDATA()) {
            String planName = item.getPlaninfo().getName();
            int adID = item.getPlaninfo().getId();
            float totalPay = item.getPaysum();
            int exposureNum = item.getShows();
            int clickNum = item.getClks();
            if (totalPay > 0 || exposureNum > 0) {
                // 过滤未消耗且未曝光的记录（后续考虑直接抓取计划中的，减少请求数）
                AdPlanStat plan = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(), account.getId());
                planList.add(plan);
                plan.setmPlanId(adID + "");
                plan.setPlanName(planName);
                plan.setCost((int) (totalPay * 100)); // 转换为分
                plan.setShowNum(exposureNum);
                plan.setClickNum(clickNum);
            }
        }
        return planList;
    }

    private List<AdPlanStat> parsePlan(String content, AdMediumAccount account, long time, PageModel pageModel) throws Exception {
        List<AdPlanStat> poList = new ArrayList<>();
        JsonObject jsonResult = StringUtil.jsonParser.parse(content).getAsJsonObject();
        //响应状态判断
        JsonElement status = jsonResult.get("status");
        if (null != status && status.getAsInt() != 200) {
            ExceptionHandler.throwException(ExceptionEnum.SERVER_ERROR, content);
        }
        JsonObject jsonData = jsonResult.get("data").getAsJsonObject();
        int totalNum = jsonData.get("totalCount").getAsInt();
        pageModel.setTotalNum(totalNum);
        JsonArray listData = jsonData.get("listData").getAsJsonArray();
        for (JsonElement item : listData) {
            AdPlanStat adPlanStat = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(), account.getId());
            poList.add(adPlanStat);
            JsonObject order = item.getAsJsonObject();
            int clickNum = StringUtil.getJsonIntValue(order, "clks");
            int showNum = StringUtil.getJsonIntValue(order, "shows");
            double cost = StringUtil.getJsonDoubleValue(order, "paysum");
            //计划ID  GSON解析得到的是Long类型
            String mPlanId = order.get("unitid").getAsString();
            //计划名称
            String planName = order.get("unitname").getAsString();
            //分组ID  GSON解析得到的是Integer类型
            String groupId = order.get("planid").getAsString();
            //分组名称
            String groupName = order.get("planname").getAsString();
            //计划出价
            double bidPrice = StringUtil.getJsonDoubleValue(order, "unitbid");
            //计划状态 0-有效 1-暂停 11-组暂停
            int planStatus = order.get("unitstat").getAsInt();
            //计划开关
            Integer planSwitch = planStatus == 1 ? Constant.PlanSwitch.OFF : Constant.PlanSwitch.ON;
            adPlanStat.setmPlanId(mPlanId);
            adPlanStat.setPlanName(planName);
            adPlanStat.setGroupName(groupName);
            adPlanStat.setGroupId(groupId);
            adPlanStat.setBid(new BigDecimal(bidPrice).multiply(new BigDecimal(100))
                    .setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
            adPlanStat.setPlanStatus(planStatus + "");
            adPlanStat.setClickNum(clickNum);
            adPlanStat.setShowNum(showNum);
            adPlanStat.setCost(new BigDecimal(cost).multiply(new BigDecimal(100))
                    .setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
            adPlanStat.setPlanSwitch(planSwitch);

        }
        return poList;

    }

    private List<AdPlanStat> parseGroup(String content, AdMediumAccount account, long time, PageModel pageModel) throws Exception {
        List<AdPlanStat> poList = new ArrayList<>();
        JsonObject jsonResult = StringUtil.jsonParser.parse(content).getAsJsonObject();
        //响应状态判断
        JsonElement status = jsonResult.get("status");
        if (null != status && status.getAsInt() != 200) {
            ExceptionHandler.throwException(ExceptionEnum.SERVER_ERROR, content);
        }
        JsonObject jsonData = jsonResult.get("data").getAsJsonObject();
        int totalNum = jsonData.get("totalCount").getAsInt();
        pageModel.setTotalNum(totalNum);
        JsonArray listData = jsonData.get("listData").getAsJsonArray();
        for (JsonElement item : listData) {
            AdPlanStat adPlanStat = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(), account.getId());
            poList.add(adPlanStat);
            JsonObject order = item.getAsJsonObject();
            // 分组id
            String mPlanId = order.get("planid").getAsString();
            // 分组名称
            String planName = order.get("planname").getAsString();
            // 点击量
            int clickNum = StringUtil.getJsonIntValue(order, "clks");
            // 展示量
            int showNum = StringUtil.getJsonIntValue(order, "shows");
            // 消耗
            double cost = StringUtil.getJsonDoubleValue(order, "paysum");
            // 分组状态 0-有效 2-暂停推广
            String planStatus = order.get("planstat").getAsString();
            adPlanStat.setmPlanId(mPlanId);
            adPlanStat.setPlanName(planName);
            adPlanStat.setClickNum(clickNum);
            adPlanStat.setShowNum(showNum);
            adPlanStat.setCost(new BigDecimal(cost).multiply(new BigDecimal(100))
                    .setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
            adPlanStat.setPlanStatus(planStatus);
        }
        return poList;

    }

    private AdAccountStat parseAccount(String content, AdMediumAccount account, long time) throws Exception {
        JsonObject jsonResult = StringUtil.jsonParser.parse(content).getAsJsonObject();
        JsonElement status = jsonResult.get("status");
        if (null != status && status.getAsInt() != 200) {
            ExceptionHandler.throwException(ExceptionEnum.SERVER_ERROR, content);
        }
        JsonElement ifRedirect = jsonResult.get("redirect");
        if (null != ifRedirect && "true".equals(ifRedirect.getAsString())) {
            ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, content);
        }
        int balance = StringUtil.convertBigDecimal(
                jsonResult.get("data").getAsJsonObject().get("probalance").getAsString(), 100, 0).intValue();
        AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
                account.getId());
        accountStat.setBalance(balance);
        return accountStat;
    }

    private Map<String, String> getHeaderMap(String cookie, String url) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap
                .put("User-Agent",
                        "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
        if (url.contains("tuiguang.baidu.com")) {
            headerMap.put("Host", "tuiguang.baidu.com");
        } else {
            headerMap.put("Host", "fengchao.baidu.com");
        }
        headerMap.put("Connection", "keep-alive");
        headerMap.put("Accept", "*/*");
        headerMap.put("Accept-Encoding", "gzip, deflate");
        headerMap.put("Cookie", cookie);
        return headerMap;
    }
}
