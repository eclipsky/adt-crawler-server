package com.dataeye.ad.scheduler.job.yidianzixun;

import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.scheduler.job.CrawlerBaseExecutor;
import com.dataeye.ad.util.DateUtil;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Created by VitAs on 2017/6/26.
 */
public class YidianzixunJobDay implements Job{

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        CrawlerBaseExecutor.addJob(new YidianzixunJob(DateUtil.unixTimestamp000() - Constant.ONEDAY));
    }

}
