package com.dataeye.ad.scheduler.job.tui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dataeye.ad.scheduler.job.CrawlerFilter;
import org.slf4j.Logger;

import com.dataeye.ad.common.ExceptionEnum;
import com.dataeye.ad.common.ExceptionHandler;
import com.dataeye.ad.constant.Constant.CrawlerPlatform;
import com.dataeye.ad.constant.Constant.PlanSwitch;
import com.dataeye.ad.constant.Constant.TuiQQQueryOperator;
import com.dataeye.ad.constant.PageModel;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountStat;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.scheduler.job.tui.model.FilterParameter;
import com.dataeye.ad.scheduler.job.tui.model.TuiQQData;
import com.dataeye.ad.scheduler.job.tui.model.TuiQQItem;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.QQUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;
import com.google.gson.JsonObject;

//import com.dataeye.common.string.StringUtil;

public class TuiQQJob extends CrawlerBaseJob {

	private final static CrawlerPlatform platform = CrawlerPlatform.TENCENT_TUI;

	private final static Logger logger = ServerUtil.getLogger(platform);

	private final static String planUrl = "http://tui.qq.com/misapi/adgroup/select?filter={0}&page={1}&page_size={2}&advertiser_id={3}";

	private final static String accountUrl = "http://tui.qq.com/misapi/home/account_balance?advertiser_id={0}";

	public TuiQQJob() {
		this(DateUtil.unixTimestamp000());
	}

	public TuiQQJob(long time) {
		super(time, platform);
	}

	public TuiQQJob(long time, CrawlerFilter filter) {
		super(time, platform, filter);
	}


	@Override
	public void accountSpider(long time, AdMediumAccount account) throws Exception {
		String cookie = account.getCookie();
		String advertiserId = QQUtil.getLoginIdInTuiQQ(cookie);
		if (StringUtil.isEmpty(advertiserId)) {
			advertiserId = account.getSkey();
		}
		// 请求
		String content = HttpUtil.getContentByGet(HttpUtil.urlParamReplace(accountUrl, advertiserId),
				getHeaderMap(cookie));
		logger.info(content);
		// 解析
		AdAccountStat accountStat = parseAccount(content, account, time);
		// 入库
		DataProcessor.processAccountDay(accountStat);
	}

	@Override
	public void planSpider(long time, AdMediumAccount account) throws Exception {
		String statDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
		String cookie = account.getCookie();
		String advertiserId = QQUtil.getLoginIdInTuiQQ(cookie);
		if (StringUtil.isEmpty(advertiserId)) {
			advertiserId = account.getSkey();
		}
		String queryFilter = HttpUtil.urlEncode(generatorFilter(statDate, advertiserId));
		int pageId = 1;
		int pageSize = 50;
		PageModel pageModel = new PageModel(1, 50);
		// 请求
		String content = HttpUtil.getContentByGet(
				HttpUtil.urlParamReplace(planUrl, queryFilter, pageId + "", pageSize + "", advertiserId),
				getHeaderMap(cookie));
		logger.info(content);
		// 解析
		List<AdPlanStat> list = parse(content, account, time, pageModel);
		// 入库
		DataProcessor.processPlanDay(list);
		int totalNum = pageModel.getTotalNum();
		totalNum -= pageSize;
		// 处理分页
		while (totalNum > 0) {
			pageId++;
			// 请求
			content = HttpUtil.getContentByGet(
					HttpUtil.urlParamReplace(planUrl, queryFilter, pageId + "", pageSize + "", advertiserId),
					getHeaderMap(cookie));
			logger.info(content);
			// 解析
			list = parse(content, account, time, pageModel);
			// 入库
			DataProcessor.processPlanDay(list);
			totalNum -= pageSize;
		}
	}

	private String generatorFilter(String reportDate, String advertiserId) {
		List<FilterParameter> list = new ArrayList<FilterParameter>();
		list.add(new FilterParameter("adgroup_name", TuiQQQueryOperator.CONTAINS, ""));
		list.add(new FilterParameter("status", TuiQQQueryOperator.EQUALS, "ADGROUP_STATUS_UN_DELETED")); // 所有未删除广告
		list.add(new FilterParameter("price_mode", TuiQQQueryOperator.EQUALS, ""));
		list.add(new FilterParameter("product_type", TuiQQQueryOperator.EQUALS, ""));
		list.add(new FilterParameter("begin_date", TuiQQQueryOperator.EQUALS, reportDate));
		list.add(new FilterParameter("end_date", TuiQQQueryOperator.EQUALS, "9999-12-31"));
		list.add(new FilterParameter("data_begin_date", TuiQQQueryOperator.EQUALS, reportDate));
		list.add(new FilterParameter("data_end_date", TuiQQQueryOperator.EQUALS, reportDate));
		list.add(new FilterParameter("is_filter_data", TuiQQQueryOperator.EQUALS, "false"));// true：过滤无消耗广告，false：不过滤
		String filter = StringUtil.gson.toJson(list);
		return filter;
	}

	private AdAccountStat parseAccount(String content, AdMediumAccount account, long time) throws Exception {
		JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
		int code = resultJson.get("code").getAsInt();
		// code验证
		codeCheck(code, content);
		JsonObject data = resultJson.get("data").getAsJsonObject();
		int balance = StringUtil.convertInt(data.get("balance").getAsString(), 0);
		int costToday = data.get("cost").getAsJsonObject().get("today").getAsJsonObject().get("cost").getAsInt();
		AdAccountStat accountStat = new AdAccountStat(time, account.getCompanyId(), account.getPlatformId(),
				account.getId());
		accountStat.setBalance(balance);
		accountStat.setCostToday(costToday);
		return accountStat;
	}

	private List<AdPlanStat> parse(String content, AdMediumAccount account, long time, PageModel pageModel)
			throws Exception {
		List<AdPlanStat> resultList = new ArrayList<AdPlanStat>();
		JsonObject resultJson = StringUtil.jsonParser.parse(content).getAsJsonObject();
		int code = resultJson.get("code").getAsInt();
		// code验证
		codeCheck(code, content);
		String dataStr = resultJson.get("data").toString();
		TuiQQData data = StringUtil.gson.fromJson(dataStr, TuiQQData.class);
		pageModel.setTotalNum(data.getPage_info().getTotal_num());
		for (TuiQQItem item : data.getList()) {
			AdPlanStat adPlanDay = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(),
					account.getId());
			resultList.add(adPlanDay);
			// 媒体计划ID
			String mPlanId = item.getAdgroup_id();
			// 媒体计划名
			String planName = item.getAdgroup_name();
			// 媒体计划分组ID
			String groupId = item.getCampaign_id();
			// 媒体计划分组名
			String groupName = item.getCampaign_name();
			// 媒体计划出价
			int planBid = (int) (StringUtil.convertFloat(item.getUnit_price(), 0));
			// 媒体计划出价策略
			String planBidStrategy = item.getPrice_mode();
			// 媒体计划状态
			String planStatus = item.getStatus();
			// 媒体计划开关
			String planSwitchRaw = item.getOperation_status();
			// 落地页
			String landingPage = item.getProduct_content().getProduct_click_url();
			int planSwitch = "ENABLE".equals(planSwitchRaw) ? PlanSwitch.ON : PlanSwitch.OFF;
			// 消耗
			int cost = (int) StringUtil.convertFloat(item.getCost(), 0);
			// 曝光
			int showNum = StringUtil.convertInt(item.getPv(), 0);
			// 点击
			int clickNum = StringUtil.convertInt(item.getClick(), 0);
			adPlanDay.setmPlanId(mPlanId);
			adPlanDay.setPlanName(planName);
			adPlanDay.setCost(cost);
			adPlanDay.setShowNum(showNum);
			adPlanDay.setClickNum(clickNum);
			adPlanDay.setBid(planBid);
			adPlanDay.setPlanBidStrategy(planBidStrategy);
			adPlanDay.setGroupId(groupId);
			adPlanDay.setGroupName(groupName);
			adPlanDay.setPlanStatus(planStatus);
			adPlanDay.setPlanSwitch(planSwitch);
			adPlanDay.setLandingPage(landingPage);
		}
		return resultList;
	}

	private Map<String, String> getHeaderMap(String cookie) {
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("Platform", platform.getLabel());
		headerMap.put("Cookie", cookie);
		headerMap.put("Host", "tui.qq.com");
		headerMap.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0");
		headerMap.put("Accept", "application/json, text/plain, */*");
		headerMap.put("X-Requested-With", "XMLHttpRequest");
		headerMap.put("Accept-Encoding", "gzip, deflate");
		return headerMap;
	}

	public void codeCheck(int code, String content) throws Exception {
		// 如果正常查询 data为对象类型，如果异常，data为数组类型，这里要分开处理
		if (code != 0) {
			// 例如
			// code = 1000，"系统开小差啦，请刷新页面再试吧"
			// code = 1110，"自助平台登录已过期"
			// String msg = resultJson.get("msg").getAsString();
			if (code == 1110) {
				ExceptionHandler.throwCookieException(ExceptionEnum.COOKIE_EXPIRE, content);
			} else {
				ExceptionHandler.throwException(ExceptionEnum.SERVER_ERROR, content);
			}
		}
	}
}
