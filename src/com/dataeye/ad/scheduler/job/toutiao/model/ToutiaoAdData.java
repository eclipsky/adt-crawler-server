package com.dataeye.ad.scheduler.job.toutiao.model;

import java.util.List;

public class ToutiaoAdData {

    private int status;

    private String budget_mode;

    private long ad_id;

    private int content_type;

    private long start_time;

    private ToutiaoStatData stat_data;

    private long campaign_id;

    private int target_purpose;

    private String digg_count;

    private Integer phase;

    private String pricing;

    private List<String> ad_inventory_types;

    private String campaign_name;

    private String ad_name;

    private String external_action;

    private boolean is_previewable;

    private String status1;

    private String bury_count;

    private String repin_count;

    private boolean can_copy;

    private String budget;

    private String comment_count;

    private long end_time;

    private String target_purpose_name;

    private String share_count;

    private String bid;

    private boolean channel;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getBudget_mode() {
        return budget_mode;
    }

    public void setBudget_mode(String budget_mode) {
        this.budget_mode = budget_mode;
    }

    public long getAd_id() {
        return ad_id;
    }

    public void setAd_id(long ad_id) {
        this.ad_id = ad_id;
    }

    public int getContent_type() {
        return content_type;
    }

    public void setContent_type(int content_type) {
        this.content_type = content_type;
    }

    public long getStart_time() {
        return start_time;
    }

    public void setStart_time(long start_time) {
        this.start_time = start_time;
    }

    public ToutiaoStatData getStat_data() {
        return stat_data;
    }

    public void setStat_data(ToutiaoStatData stat_data) {
        this.stat_data = stat_data;
    }

    public long getCampaign_id() {
        return campaign_id;
    }

    public void setCampaign_id(long campaign_id) {
        this.campaign_id = campaign_id;
    }

    public int getTarget_purpose() {
        return target_purpose;
    }

    public void setTarget_purpose(int target_purpose) {
        this.target_purpose = target_purpose;
    }

    public String getDigg_count() {
        return digg_count;
    }

    public void setDigg_count(String digg_count) {
        this.digg_count = digg_count;
    }

    public String getPricing() {
        return pricing;
    }

    public void setPricing(String pricing) {
        this.pricing = pricing;
    }

    public List<String> getAd_inventory_types() {
        return ad_inventory_types;
    }

    public void setAd_inventory_types(List<String> ad_inventory_types) {
        this.ad_inventory_types = ad_inventory_types;
    }

    public String getCampaign_name() {
        return campaign_name;
    }

    public void setCampaign_name(String campaign_name) {
        this.campaign_name = campaign_name;
    }

    public String getAd_name() {
        return ad_name;
    }

    public void setAd_name(String ad_name) {
        this.ad_name = ad_name;
    }

    public String getExternal_action() {
        return external_action;
    }

    public void setExternal_action(String external_action) {
        this.external_action = external_action;
    }

    public boolean isIs_previewable() {
        return is_previewable;
    }

    public void setIs_previewable(boolean is_previewable) {
        this.is_previewable = is_previewable;
    }

    public String getStatus1() {
        return status1;
    }

    public void setStatus1(String status1) {
        this.status1 = status1;
    }

    public String getBury_count() {
        return bury_count;
    }

    public void setBury_count(String bury_count) {
        this.bury_count = bury_count;
    }

    public String getRepin_count() {
        return repin_count;
    }

    public void setRepin_count(String repin_count) {
        this.repin_count = repin_count;
    }

    public boolean isCan_copy() {
        return can_copy;
    }

    public void setCan_copy(boolean can_copy) {
        this.can_copy = can_copy;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getComment_count() {
        return comment_count;
    }

    public void setComment_count(String comment_count) {
        this.comment_count = comment_count;
    }

    public long getEnd_time() {
        return end_time;
    }

    public void setEnd_time(long end_time) {
        this.end_time = end_time;
    }

    public String getTarget_purpose_name() {
        return target_purpose_name;
    }

    public void setTarget_purpose_name(String target_purpose_name) {
        this.target_purpose_name = target_purpose_name;
    }

    public String getShare_count() {
        return share_count;
    }

    public void setShare_count(String share_count) {
        this.share_count = share_count;
    }

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public Integer getPhase() {
        return phase;
    }

    public void setPhase(Integer phase) {
        this.phase = phase;
    }

    /**
     * @return the channel
     */
    public boolean isChannel() {
        return channel;
    }

    /**
     * @param channel the channel to set
     */
    public void setChannel(boolean channel) {
        this.channel = channel;
    }
}
