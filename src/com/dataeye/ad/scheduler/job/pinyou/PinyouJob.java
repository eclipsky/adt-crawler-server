package com.dataeye.ad.scheduler.job.pinyou;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.dataeye.ad.scheduler.job.CrawlerFilter;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;

import com.dataeye.ad.common.ServerException;
import com.dataeye.ad.constant.Constant.CrawlerPlatform;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountBase;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.util.CookieUtil;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;

public class PinyouJob extends CrawlerBaseJob {

	private final static CrawlerPlatform platform = CrawlerPlatform.PINYOU;

	private final static Logger logger = ServerUtil.getLogger(platform);

	private static final String columnUrl = "http://console.ipinyou.com/report/column/";

	private static final String planUrl = "http://console.ipinyou.com/report/effect/findPage/?type=list&dimensionId=31711&platform=&beginDate={0}&endDate={1}&page.size=500";

	public PinyouJob() {
		this(DateUtil.unixTimestamp000());
	}

	public PinyouJob(long time) {
		super(time, platform);
	}

	public PinyouJob(long time, CrawlerFilter filter) {
		super(time, platform, filter);
	}

	@Override
	public void planSpider(long time, AdMediumAccount account) throws Exception {
		String reportDate = DateUtil.getTimeString(time, "yyyy/MM/dd");
		String reportDateURLEncode = HttpUtil.urlEncode(reportDate);
		// 这里使用帐号密码登录
		// String cookie = CookieUtil.getPinyouLoginCookie(account.getAccount(), account.getPassword());
		String cookie = account.getCookie();
		CookieUtil.isCookieValid(cookie);
		// TODO分页
		// 先设置显示列（会影响解析）
		HttpUtil.getContentByPut(columnUrl, getHeaderMap(cookie), getFormList());
		// 请求
		String content = HttpUtil.getContentByGet(
				HttpUtil.urlParamReplace(planUrl, reportDateURLEncode, reportDateURLEncode), getHeaderMap(cookie));
		logger.info(content);
		// 解析
		List<AdPlanStat> poList = parse(content, account, time);
		// 入库
		DataProcessor.processPlanDay(poList);
	}

	@Override
	public void accountSpider(long time, AdMediumAccount account) throws Exception {
		// TODO Auto-generated method stub

	}

	public List<NameValuePair> getFormList() {
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("columns", "advCost,imp,click,"));
		return pairs;
	}

	private List<AdPlanStat> parse(String content, AdAccountBase account, long time) throws ServerException {
		List<AdPlanStat> poList = new ArrayList<AdPlanStat>();
		Document document = Jsoup.parse(content);
		// 计划名称
		Element element = document.getElementById("leftBottom");
		if (null == element) { // 无数据，直接返回空列表
			return poList;
		}
		Elements es = element.getElementsByTag("td");

		Map<Integer, AdPlanStat> map = new HashMap<Integer, AdPlanStat>();
		int leftIndex = 0;
		for (Element e : es) {
			if ("left ellipsis".equals(e.className())) {
				AdPlanStat data = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(), account.getId());
				data.setPlanName(e.attr("title"));
				map.put(leftIndex, data);
				leftIndex++;
			}
		}
		// 计划数据
		Element planTable = document.getElementById("rightBottom");
		Elements planTr = planTable.getElementsByTag("tr");
		int rightIndex = 0;
		for (Element e : planTr) {
			if ("h-44 td-bb-solid".equals(e.className())) {
				AdPlanStat data = map.get(rightIndex);
				int cost = 0;
				int showNum = 0;
				int clickNum = 0;
				Elements tddatas = e.getElementsByTag("td");
				for (int i = 0; i < tddatas.size(); i++) {
					if (i == 0) {
						// 总花费
						cost = (int) (StringUtil.convertFloat(tddatas.get(i).text().replaceAll(",", ""), 0) * 100);
					} else if (i == 1) {
						// 曝光数
						showNum = StringUtil.convertInt(tddatas.get(i).text().replaceAll(",", ""), 0);
					} else if (i == 2) {
						// 点击数
						clickNum = StringUtil.convertInt(tddatas.get(i).text().replaceAll(",", ""), 0);
					}
				}
				data.setCost(cost);
				data.setClickNum(clickNum);
				data.setShowNum(showNum);
				rightIndex++;
			}
		}

		// 去掉总计，index = 1
		for (Entry<Integer, AdPlanStat> entry : map.entrySet()) {
			if (entry.getKey() == 0) {
				continue;
			}
			poList.add(entry.getValue());
		}
		return poList;
	}

	private Map<String, String> getHeaderMap(String cookie) {
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("Cookie", cookie);
		headerMap.put("Host", "console.ipinyou.com");
		// request.setHeader("User-Agent",
		// "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
		return headerMap;
	}

}
