package com.dataeye.ad.scheduler.job.neteaseold;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dataeye.ad.scheduler.job.CrawlerFilter;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;

import com.dataeye.ad.constant.Constant.CrawlerPlatform;
import com.dataeye.ad.pipeline.DataProcessor;
import com.dataeye.ad.pipeline.po.AdAccountBase;
import com.dataeye.ad.pipeline.po.AdMediumAccount;
import com.dataeye.ad.pipeline.po.AdPlanStat;
import com.dataeye.ad.scheduler.job.CrawlerBaseJob;
import com.dataeye.ad.util.DateUtil;
import com.dataeye.ad.util.HttpUtil;
import com.dataeye.ad.util.ServerUtil;
import com.dataeye.ad.util.StringUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class NeteaseOldJob extends CrawlerBaseJob {

	private final static CrawlerPlatform platform = CrawlerPlatform.NETEASEOLD;

	private final static Logger logger = ServerUtil.getLogger(platform);
	
	private final static String planUrl = "http://wb.gongheshengshi.com/dsp/Rpt_plan_hour_stcAction_getRpt_plan_dayListIndex?_=1495178475468";

	public NeteaseOldJob() {
		this(DateUtil.unixTimestamp000());
	}

	public NeteaseOldJob(long time) {
		super(time, platform);
	}

	public NeteaseOldJob(long time, CrawlerFilter filter) {
		super(time, platform, filter);
	}

	@Override
	public void planSpider(long time, AdMediumAccount account) throws Exception {
		String reportDate = DateUtil.getTimeString(time, "yyyy-MM-dd");
		String cookie = account.getCookie();
		int start = 0;
		int size = 50;
		// 请求
		String content = HttpUtil.getContentByPost(planUrl, getHeaderMap(cookie),
				getFormDataDayList(reportDate, reportDate, start, size));
		content = content.replace("\n", "");
		logger.info(content);
		// 解析
		List<AdPlanStat> poList = parse(content, account, time);
		// 入库
		DataProcessor.processPlanDay(poList);
	}

	@Override
	public void accountSpider(long time, AdMediumAccount account) throws Exception {
		// TODO
	}

	/**
	 * <pre>
	 * 解析响应内容，封装成标准的PO对象
	 * @author sam.xie
	 * @date Feb 15, 2017 3:12:29 PM
	 * @param accountID
	 * @param jdbcTemplate
	 * @param list
	 * @param time
	 */
	private List<AdPlanStat> parse(String content, AdAccountBase account, long time) {
		List<AdPlanStat> poList = new ArrayList<AdPlanStat>();
		JsonArray jsonArray = StringUtil.jsonParser.parse(content).getAsJsonArray();
		for (JsonElement ele : jsonArray) {
			JsonObject item = ele.getAsJsonObject();
			AdPlanStat planPO = new AdPlanStat(time, account.getCompanyId(), account.getPlatformId(), account.getId());
			String planName = item.get("plan_name").getAsString();
			String mPlanId = item.get("policy_id").getAsString();
			int showNum = item.get("show_num_sum").getAsInt();
			int clickNum = item.get("click_num_sum").getAsInt();
			int cost = item.get("total_money_sum").getAsInt();
			planPO.setPlanName(planName);
			planPO.setmPlanId(mPlanId);
			planPO.setClickNum(clickNum);
			planPO.setShowNum(showNum);
			planPO.setCost(cost);
			poList.add(planPO);
		}
		return poList;
	}

	public List<NameValuePair> getFormDataDayList(String startDate, String endDate, int start, int size) {
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("groupBy", "policy_id"));
		pairs.add(new BasicNameValuePair("plan_type", "0"));
		pairs.add(new BasicNameValuePair("exchange_id", ""));
		pairs.add(new BasicNameValuePair("export_type", ""));
		pairs.add(new BasicNameValuePair("begin_date_str", startDate));
		pairs.add(new BasicNameValuePair("end_date_str", endDate));
		pairs.add(new BasicNameValuePair("plan_name", ""));
		pairs.add(new BasicNameValuePair("exe_status", ""));
		pairs.add(new BasicNameValuePair("not_exe_status", ""));
		pairs.add(new BasicNameValuePair("statusStr", "1,2,3,4,5,6"));
		pairs.add(new BasicNameValuePair("undefined", "0"));
		pairs.add(new BasicNameValuePair("undefined_text", "所有推广状态"));
		pairs.add(new BasicNameValuePair("start", start + ""));
		pairs.add(new BasicNameValuePair("size", size + ""));
		pairs.add(new BasicNameValuePair("sort", "sum(show_num)"));
		pairs.add(new BasicNameValuePair("dir", "desc"));
		return pairs;
	}

	private Map<String, String> getHeaderMap(String cookie) {
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("Content-Type", "application/x-www-form-urlencoded");
		headerMap.put("Connection", "keep-alive");
		headerMap.put("Host", "wb.gongheshengshi.com");
		headerMap
				.put("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
		headerMap.put("X-Requested-With", "XMLHttpRequest");
		headerMap.put("Origin", "http://wb.gongheshengshi.com");
		headerMap.put("Referer", "http://wb.gongheshengshi.com/dsp/index.jsp");
		headerMap.put("Cookie", cookie);
		return headerMap;
	}

}
