package com.dataeye.ad.scheduler.job.dongqiudi;

import com.dataeye.ad.constant.Constant;
import com.dataeye.ad.scheduler.job.CrawlerBaseExecutor;
import com.dataeye.ad.util.DateUtil;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Created by VitAs on 2017/8/11.
 */
public class DongqiudiJobDay implements Job {

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        CrawlerBaseExecutor.addJob(new DongqiudiJob(DateUtil.unixTimestamp000() - Constant.ONEDAY));

    }
}
