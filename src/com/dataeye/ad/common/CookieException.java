package com.dataeye.ad.common;

/**
 * 抽象异常类
 * 
 * @author sam.xie
 * @date Feb 10, 2017 2:18:10 PM
 * @version 1.0
 */
public class CookieException extends Exception {

	private static final long serialVersionUID = 1L;

	public CookieException(ExceptionEnum exception, String msg) {
		super(exception.getMsg() + " -> " + msg);
	}

	public CookieException(ExceptionEnum exception) {
		super(exception.getMsg());
	}

	public CookieException(String msg) {
		super(msg);
	}

	@Override
	public String getMessage() {
		return super.getMessage();
	}

}
